//
//  DOTViewController1.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 8/2/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BNHtmlPdfKit.h"
#import "menuScreenViewController.h"
#import "DBManager1.h"
#import "TestDataXML.h"
#import "DOTInspection.h"
extern NSString *master_menu;


@interface DOTViewController1 : UIViewController<UIWebViewDelegate,UIPopoverControllerDelegate>{
    
    UIDatePicker *datepicker;
    UIPopoverController *popOverForDatepicker;
    UIView *viewForDatePicker;
    UIView *dimm;
    
}
@property (nonatomic, strong) DBManager1 *dbManager;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigation1;
- (IBAction)saveAsPdf:(id)sender;
@property (weak, nonatomic) IBOutlet UIWebView *web_dot;
@property (weak, nonatomic) IBOutlet UIScrollView *scroller_dot1;
@property (weak, nonatomic) IBOutlet UIButton *savebtn;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@end
