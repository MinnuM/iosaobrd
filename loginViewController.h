//
//  loginViewController.h
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 6/7/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import <Security/Security.h>
#import "DBManager1.h"
#import "LoginClass.h"
#import "LoginResponse.h"
#import "DBManager/DBManager.h"
#import "User.h"


extern NSString *sucess;
extern NSString *path1;
extern NSString *username;
extern NSString *password;
extern NSString *USer;
extern NSString *master;

@interface loginViewController : UIViewController<UITextFieldDelegate>{

NSMutableData *receivedData;

NSString *filePath;
NSString *str;
//NSArray *myArray;

BOOL connectionSuccess;

}
//@property (strong, nonatomic) IBOutlet UIButton *Menu;
@property (weak, nonatomic) IBOutlet UILabel *forgotPassword;
@property (strong, nonatomic) NSArray *myArray1;
@property (weak, nonatomic) IBOutlet UIButton *signInBtn;
@property (strong, nonatomic) NSString *databasePath;
@property (nonatomic) sqlite3 *contactDB;
@property (weak, nonatomic) IBOutlet UITextField *user;
@property (weak, nonatomic) IBOutlet UITextField *pass;
@property (nonatomic, strong) DBManager1 *dbManager;
@property (weak, nonatomic) IBOutlet UILabel *failed_msg;
@property (atomic, assign) BOOL canceled;

@property (nonatomic,strong) LoginResponse *obj1;

@property (nonatomic, strong) IBOutlet UILabel *lblSignUp;

- (IBAction)submit:(id)sender;
-(IBAction)signUpAction:(id)sender;
@end
