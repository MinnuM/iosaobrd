//
//  customCell.h
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/25/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *textlabel;

@end
