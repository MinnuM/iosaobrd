//
//  CarrierSettings.m
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/12/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "CarrierSettings.h"
#import "menuScreenViewController.h"
#import "insertDutyCycle.h"
#import "KeychainItemWrapper.h"


@interface CarrierSettings ()

@end

@implementation CarrierSettings
@synthesize dbManager;
@synthesize scroller05;
@synthesize carrierLabel;
@synthesize mainLabel,homeLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addRequired:carrierLabel];
    [self addRequired:mainLabel];
    [self addRequired:homeLabel];
    
    _main_state1.delegate=self;
    _home_state.delegate=self;
    _carrier_txtfield.delegate=self;
    _officeaddress1.delegate=self;
    _city1.delegate=self;
    _city2.delegate=self;
    _zip1.delegate=self;
    _zip2.delegate=self;
    _home1.delegate=self;
    
    //BFLog(@"%@",carrier1);
    dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
//    settingsMenu *carrierData=[settingsMenu new];
   // NSLog(@"Carrier: %@", _carrierData);
    if(_carrierData !=nil){
    _carrier_txtfield.text=_carrierData.name;
    _home_state.text=_carrierData.homeState;
    _main_state1.text=_carrierData.mainState;
    _home1.text=_carrierData.homeAddress;
    _city2.text=_carrierData.homeCity;
    _city1.text=_carrierData.mainCity;
    _officeaddress1.text=_carrierData.mainAddress;
    _zip1.text=_carrierData.mainZip;
    _zip2.text=_carrierData.homeZip;
    
    if(carrier1[@"homeZip"]!=[NSNull class] && ![carrier1[@"homeZip"] isEqualToString:@"<null>"] && [carrier1[@"homeZip"] length]!=0){
        _zip2.text = _carrierData.homeZip;
    }
    }
    /*if ([carrier1 count]!=0) {
        
    _carrier_txtfield.text=carrier1[@"Carrier"];
    _home_state.text=carrier1[@"homeAddressline3"];
    _main_state1.text=carrier1[@"offAddressline3"];
    _home1.text=carrier1[@"homeAddressline1"];
    _city2.text=carrier1[@"homeAddressline2"];
        _city1.text=carrier1[@"offAddressline2"];
        _officeaddress1.text=carrier1[@"offAddressline1"];
        _zip1.text=carrier1[@"offZip"];
        if(carrier1[@"homeZip"]!=[NSNull class] && ![carrier1[@"homeZip"] isEqualToString:@"<null>"] && [carrier1[@"homeZip"] length]!=0){
        _zip2.text = carrier1[@"homeZip"];
        }
    }else{
         NSString *query0 = [NSString stringWithFormat:@"select * from Carrier_Sett;"];
        NSArray *carrier_Data = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
       // BFLog(@"Result:%@",carrier_Data);
        if ([carrier_Data count]!=0) {
            if ([carrier_Data[0][1] isEqualToString:@"(null)"]) {
                _carrier_txtfield.text=@"";
            }else{
            _carrier_txtfield.text=carrier_Data[0][1];
            }
            if (![carrier_Data[0][2] isEqualToString:@",,,"] && ![carrier_Data[0][3] isEqualToString:@"(null),(null),(null),(null)"]) {
              NSArray *offAddress=[carrier_Data[0][2] componentsSeparatedByString:@","];
             _officeaddress1.text=offAddress[0];
             _city1.text=offAddress[1];
             _main_state1.text=offAddress[2];
                _zip1.text=offAddress[3];
            }else if([carrier_Data[0][2] isEqualToString:@"(null),(null),(null),(null)"]) {
                
                _officeaddress1.text=@"";
                _city1.text=@"";
                _main_state1.text=@"";
                _zip1.text=@"";
            }else{
                
                _officeaddress1.text=@"";
                _city1.text=@"";
                _main_state1.text=@"";
                _zip1.text=@"";
                
            }
            if (![carrier_Data[0][3] isEqualToString:@",,,"] && ![carrier_Data[0][3] isEqualToString:@"(null),(null),(null),(null)"]) {
            
            NSArray *homeAddress=[carrier_Data[0][3] componentsSeparatedByString:@","];
             _home1.text=homeAddress[0];
             _city2.text=homeAddress[1];
             _home_state.text=homeAddress[2];
                _zip2.text=homeAddress[3];
            
            
            }else if([carrier_Data[0][3] isEqualToString:@"(null),(null),(null),(null)"]) {
                
                _home1.text=@"";
                _city2.text=@"";
                _home_state.text=@"";
                _zip2.text=@"";
            }else{
                
                _home1.text=@"";
                _city2.text=@"";
                _home_state.text=@"";
                _zip2.text=@"";
                
            }

            
            
            
            
            
        }

        
    }
 */
   
    
    // Get the results.
    //minnu
    UITapGestureRecognizer *backgroundTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backgroundTap)];
    [self.view addGestureRecognizer:backgroundTapped];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    //minnu
    
    
    self.main_state1.delegate=self;
    mainPicker1=[[UIPickerView alloc] init];
    
    //[event_picker addTarget:self action:@selector(ShowSelectedDate1) forControlEvents:UIControlEventValueChanged];
    _main_state1.inputView = mainPicker1;
    // _Enddate.inputView = datepicker;
    
    UIToolbar *toolBar1=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
    [toolBar1 setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn1=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePressed1)];
    UIBarButtonItem *spac1=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar1 setItems:[NSArray arrayWithObjects:spac1, doneBtn1,nil]];
    [self.main_state1  setInputAccessoryView:toolBar1];
    
    _startes = [[NSArray alloc] initWithObjects:@"Alabama"
                ,@"Alaska"
                ,@"Arizona"
                ,@"Arkansas"
                ,@"California"
                ,@"Colorado"
                ,@"Connecticut"
                ,@"Delaware"
                ,@"District Of Columbia"
                ,@"Florida"
                ,@"Georgia"
                ,@"Hawaii"
                ,@"Idaho"
                ,@"Illinois"
                ,@"Indiana"
                ,@"Iowa"
                ,@"Kansas"
                ,@"Kentucky"
                ,@"Louisiana"
                ,@"Maine"
                ,@"Maryland"
                ,@"Massachusetts"
                ,@"Michigan"
                ,@"Minnesota"
                ,@"Mississippi"
                ,@"Missouri"
                ,@"Montana"
                ,@"Nebraska"
                ,@"Nevada"
                ,@"New Hampshire"
                ,@"New Jersey"
                ,@"New Mexico"
                ,@"New York"
                ,@"North Carolina"
                ,@"North Dakota"
                ,@"Ohio"
                ,@"Oklahoma"
                ,@"Oregon"
                ,@"Pennsylvania"
                ,@"Rhode Island"
                ,@"South Carolina"
                ,@"South Dakota"
                ,@"Tennessee"
                ,@"Texas"
                ,@"Utah"
                ,@"Vermont"
                ,@"Virginia"
                ,@"Washington"
                ,@"West Virginia"
                ,@"Wisconsin"
                ,@"Wyoming",
                nil];
    
    
    
    _statecodes = [[NSArray alloc] initWithObjects:@"AL"
                   ,@"AK"
                   ,@"AZ"
                   ,@"AR"
                   ,@"CA"
                   ,@"CO"
                   ,@"CT"
                   ,@"DE"
                   ,@"DC"
                   ,@"FL"
                   ,@"GA"
                   ,@"HI"
                   ,@"ID"
                   ,@"IL"
                   ,@"IN"
                   ,@"IA"
                   ,@"KS"
                   ,@"KY"
                   ,@"LA"
                   ,@"ME"
                   ,@"MD"
                   ,@"MA"
                   ,@"MI"
                   ,@"MN"
                   ,@"MS"
                   ,@"MO"
                   ,@"MT"
                   ,@"NE"
                   ,@"NV"
                   ,@"NH"
                   ,@"NJ"
                   ,@"NM"
                   ,@"NY"
                   ,@"NC"
                   ,@"ND"
                   ,@"OH"
                   ,@"OK"
                   ,@"OR"
                   ,@"PA"
                   ,@"RI"
                   ,@"SC"
                   ,@"SD"
                   ,@"TN"
                   ,@"TX"
                   ,@"UT"
                   ,@"VT"
                   ,@"VA"
                   ,@"WA"
                   ,@"WV"
                   ,@"WI"
                   ,@"WY", nil];
    // CatPicker.hidden = NO;
    mainPicker1.delegate= self;
    mainPicker1.dataSource=self;
    [mainPicker1 removeFromSuperview];
    //[ setInputView:event_picker];
    [mainPicker1 reloadAllComponents];
    // BFLog(,@"testing--------------------->%d",opt);
    [mainPicker1 selectRow:0 inComponent:0 animated:YES];
    
    
    _home_state.delegate=self;
    homePicker=[[UIPickerView alloc] init];
    
    //[event_picker addTarget:self action:@selector(ShowSelectedDate1) forControlEvents:UIControlEventValueChanged];
    _home_state.inputView = homePicker;
    // _Enddate.inputView = datepicker;
    
    UIToolbar *toolBar2=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
    [toolBar2 setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn2=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePressed2)];
    UIBarButtonItem *spac2=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar2 setItems:[NSArray arrayWithObjects:spac2, doneBtn2,nil]];
    [self.home_state  setInputAccessoryView:toolBar2];
    
    homePicker.delegate= self;
    homePicker.dataSource=self;
    [homePicker removeFromSuperview];
    //[ setInputView:event_picker];
    [homePicker reloadAllComponents];
    // BFLog(,@"testing--------------------->%d",opt);
    [homePicker selectRow:0 inComponent:0 animated:YES];
    
    
    // Do any additional setup after loading the view.
}

-(void)addRequired:(UILabel*)label{
    
    NSMutableAttributedString *text =
    [[NSMutableAttributedString alloc]
     initWithAttributedString: label.attributedText];
    
    [text addAttribute:NSForegroundColorAttributeName
                 value:[UIColor redColor]
                 range:NSMakeRange([label.text length]-1, 1)];
    [label setAttributedText: text];

    
    
}
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scroller05.contentInset = contentInsets;
    scroller05.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect textfieldFrame;
    CGRect aRect = self.view.frame;
    
    if([self.carrier_txtfield isFirstResponder])
    {
        textfieldFrame=[self.carrier_txtfield frame];
    }
    else if([self.main_state1 isFirstResponder])
    {
        textfieldFrame=[_main_state1 frame];
    }
    else if([self.home_state isFirstResponder])
    {
        textfieldFrame=[_home_state frame];
    }
    else if([self.city1 isFirstResponder])
    {
        textfieldFrame=[_city1 frame];
    }
    else if([self.zip1 isFirstResponder])
    {
        textfieldFrame=[_zip1 frame];
    }
    else  if([self.officeaddress1 isFirstResponder])
    {
        textfieldFrame=[_officeaddress1 frame];
    }
    else if([self.city2 isFirstResponder])
    {
        textfieldFrame=[_city2 frame];
    }
    else if([self.home1 isFirstResponder])
    {
        textfieldFrame=[_home1 frame];
    }
    else
    {
        textfieldFrame=[_zip2 frame];
    }
    if (aRect.size.height < (textfieldFrame.origin.y+15) ) {
        CGPoint scrollPoint = CGPointMake(0.0, textfieldFrame.origin.y);
        
        [scroller05 setContentOffset:scrollPoint animated:YES];
    }}


// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scroller05.contentInset = contentInsets;
    scroller05.scrollIndicatorInsets = contentInsets;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
  
    return YES;
    
}

- (void)backgroundTap {
    [self.view endEditing:YES];
   }


- (void)donePressed1{
    
    
    [self.main_state1 resignFirstResponder];
    
    
}
- (void)donePressed2{
    
    
    [self.home_state resignFirstResponder];
    
    
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    
    return YES;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
    
    
}


//  returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    int row3=0;
    if ([pickerView isEqual:mainPicker1]) {
        row3 = (int)[_startes count];
        
        
    }
    else if([pickerView isEqual:homePicker]){
        
        row3 =(int) [_startes count];
        
    }
    
    return row3;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSString *selection=@"";
    if ([pickerView isEqual:homePicker]) {
        selection = [_startes objectAtIndex:row];
    }
    else if ([pickerView isEqual:mainPicker1]){
        selection = [_startes objectAtIndex:row];
    }
    
    return selection;
    
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSInteger row1=0;
    NSString *str1=@"";
    
    if ([pickerView isEqual:mainPicker1]) {
        row1=[mainPicker1 selectedRowInComponent:0];
        str1 = [_statecodes objectAtIndex:row1];
        _main_state1.text =str1;
        
    }
    else if ([pickerView isEqual:homePicker]){
        
        row1=[homePicker selectedRowInComponent:0];
        str1 = [_statecodes objectAtIndex:row1];
        _home_state.text =str1;
        
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)saveToServer:(NSMutableDictionary *)driver_data{
    
//    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
//    //getting authCode
//    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
//    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
//    NSString *authCode = @"";
//    if([authData count]!=0){
//        authCode = authData[0][1];
//        
//    }
//    //---------------------------
//   NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/saveHosCarrierData.php?masterUser=%@&User=%@&authCode=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"masteruser"],[[NSUserDefaults standardUserDefaults] valueForKey:@"user"],authCode];
//    // NSString *urlString = [NSString stringWithFormat:@"http://23.239.28.100/mydealership/saveHosGraphData_app.php?masterUser=%@&user=%@&date=%@&approve=0",master_menu,user_menu,current_date];
//    NSURL *url = [NSURL URLWithString:urlString];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    [request setHTTPMethod:@"HEAD"];
//    NSURLResponse *response;
//    NSError *error;
//    NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
//    if(httpResponse!=nil){
//        if ([httpResponse statusCode] ==200 ) {
//            BFLog(@"Device connected to the internet");
//            NSURL *url = [NSURL URLWithString:urlString];
//            
//            //NSError *error;
//            
//            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
//                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                               timeoutInterval:60.0];
//            
//            [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//            [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
//            
//            [request setHTTPMethod:@"POST"];
//            /* NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
//             @"IOS TYPE", @"typemap",
//             nil];*/
//            
//            //NSMutableDictionary *currentStatus=[[NSMutableDictionary alloc]init];
//            //[currentStatus setValue:status forKey:@"Status"];*/
//            /*  NSMutableArray *inner = [[NSMutableArray alloc] initWithArray:@[@"00:00",@"OFF"]];
//             NSMutableDictionary *data_dict = [[NSMutableDictionary alloc] init];
//             [data_dict setObject:inner forKey:@"payload"];
//             NSError *error;*/
//            NSData *postData = [NSJSONSerialization dataWithJSONObject:driver_data options:0 error:&error];
//            [request setHTTPBody:postData];
//            
//            NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                
//                NSDictionary *responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//                if(error==nil){
//                    
//                    UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
//                                                                     message:@"Data Saved Successfully!!!"
//                                                                    delegate:self
//                                                           cancelButtonTitle:@"OK"
//                                                           otherButtonTitles:nil, nil];
//                    
//                    [alert2 show];
//                    
//                    
//                    
//                }
//                
//            }];
//            
//            [postDataTask resume];
//        }else if([httpResponse statusCode]==403){
//            if(autoLoginCount<=1){
//                insertDutyCycle *obj = [insertDutyCycle new];
//                BOOL success =  [obj autoLogin];
//                
//                if(success){
//                    [self saveToServer:driver_data];
//                    
//                }else{
//                    KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
//                    [credentials resetKeychainItem];
//                    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
//                                                                     message:@"Please Login Again"
//                                                                    delegate:self
//                                                           cancelButtonTitle:@"OK"
//                                                           otherButtonTitles:nil, nil];
//                    alert1.tag = 1;
//                    alert1.delegate = self;
//                    
//                    [alert1 show];
//                    
//                }
//            }else{
//                KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
//                [credentials resetKeychainItem];
//                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
//                                                                 message:@"Please Login Again"
//                                                                delegate:self
//                                                       cancelButtonTitle:@"OK"
//                                                       otherButtonTitles:nil, nil];
//                alert1.tag = 2;
//                alert1.delegate = self;
//                
//                [alert1 show];
//                
//                
//                
//            }
//            
//        }else if([httpResponse statusCode]==503){
//            BFLog(@"Device not connected to the internet");
//            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Service Unavailable"
//                                                             message:@"No connectivity with server at this time,it will sync automatically"
//                                                            delegate:self
//                                                   cancelButtonTitle:@"OK"
//                                                   otherButtonTitles:nil, nil];
//            
//            [alert1 show];
//            
//        }
//    }
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag ==1 || alertView.tag ==2){
        
        if(buttonIndex==[alertView cancelButtonIndex]){
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"loginscreen"];
                [self presentViewController:vc animated:YES completion:nil];
            });
            
        }
        
        
    }
    
    
    
}



- (IBAction)Savebtn:(id)sender {
    autoLoginCount=0;
    if([_carrier_txtfield.text isEqual:@""] || [_officeaddress1.text isEqual:@""] || [_city1.text isEqual:@""]||[_main_state1.text isEqual:@""] ||[_zip1.text isEqual:@""] ||[_home1.text isEqualToString:@""] ||[_city2.text isEqual:@""] ||[_home_state.text isEqual:@""]||[_zip2.text isEqualToString:@""]){
        
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"Required fiels cannot be empty!"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        
        [alert1 show];
        
    }else{
        Carrier *objCarrier=[Carrier new];
        DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
        
        NSDictionary *token;
        NSMutableDictionary *whereToken;
        NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
        if([userInfoArray count]!=0){
            int userId = [userInfoArray[0][0] intValue];
        token=@{@"columnName":@"userid",@"entry":[NSString stringWithFormat:@"%d",userId]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        NSArray * carrierIdInfo= [[NSArray alloc] initWithArray:[objDB getCarrierId:whereToken]];
            if([carrierIdInfo count]!=0){
                token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%@", carrierIdInfo[0][0]]};
                ;
            }
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        
        NSArray *values1 = [[NSArray alloc] initWithArray:[objDB selectFromTable:objCarrier whereToken:whereToken]];
                
        //NSString *query2 = [NSString stringWithFormat:@"select * from Carrier_Sett"];
    
    // Get the results.
    
    //NSArray *values1 = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query2]];
        objCarrier.identifier=@"1";
        objCarrier.name=_carrier_txtfield.text;
        objCarrier.mainAddress=_officeaddress1.text;
        objCarrier.mainZip=_zip1.text;
        objCarrier.mainCity=_city1.text;
        objCarrier.mainState=_main_state1.text;
        objCarrier.homeZip=_zip2.text;
        objCarrier.homeCity=_city2.text;
        objCarrier.homeState=_home_state.text;
        objCarrier.homeAddress=_home1.text;
        
    if ([values1 count]!=0) {
        
        
        [objDB updateIntoTable:objCarrier whereToken:whereToken];
        
        if (objDB.affectedRows != 0) {
            BFLog(@"Table updated successfully. Affected rows = %d", objDB.affectedRows);
            UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                             message:@"Data Saved!!!"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
            
            [alert2 show];
        }
        else{
            BFLog(@"Could not execute the query");
        }
       /*raghee NSString *query_update = [NSString stringWithFormat:@"UPDATE Carrier_Sett SET carrier_name='%@',main_addr='%@',home_addr='%@'",_carrier_txtfield.text,[NSString stringWithFormat:@"%@,%@,%@,%@",_officeaddress1.text,_city1.text,_main_state1.text,_zip1.text],[NSString stringWithFormat:@"%@,%@,%@,%@",_home1.text,_city2.text,_home_state.text,_zip2.text]];
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_update];
        
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
           // BFLog(@"Table updated successfully. Affected rows = %d", dbManager.affectedRows);
            [carrier1 removeAllObjects];

            UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                             message:@"Data Saved Successfully!!!"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
            
            //[alert2 show];
        }
        else{
            //BFLog(@"Could not execute the query");
        }*/
    }
    
    else{
        [objDB  insertIntoTable:objCarrier];
        int carrierId= [objDB getInsertId:@"Carrier"];
        user2carrier *objUser2carrier=[user2carrier new];
        objUser2carrier.userid=[NSString stringWithFormat:@"%d",userId];
        objUser2carrier.carrierid=[NSString stringWithFormat:@"%d",carrierId];
        [objDB  insertIntoTable:objUser2carrier];
        
        
    /*  raghee  NSString *query_insert1 = [NSString stringWithFormat:@"insert into Carrier_Sett values(null, '%@', '%@', '%@')",_carrier_txtfield.text,[NSString stringWithFormat:@"%@,%@,%@,%@",_officeaddress1.text,_city1.text,_main_state1.text,_zip1.text],[NSString stringWithFormat:@"%@,%@,%@,%@",_home1.text,_city2.text,_home_state.text,_zip2.text]];
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_insert1];
        
        // If the query was successfully executed then pop the view controller.*/
        if (objDB.affectedRows != 0) {
            //BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
            [carrier1 removeAllObjects];
            UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                             message:@"Data Saved!!!"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
            
            [alert2 show];
            
            
            
        }
        else{
           BFLog(@"Could not execute the query");
        }
        
      
    }
        
    }
    NSMutableDictionary *driver_data = [[NSMutableDictionary alloc] init];
    
    [driver_data setValue:_carrier_txtfield.text forKey:@"Carrier"];
    [driver_data setValue:_officeaddress1.text forKey:@"OfficeAddress1"];
    [driver_data setValue:_city1.text forKey:@"OfficeAddress2"];
    [driver_data setValue:_main_state1.text forKey:@"OffState"];
    [driver_data setValue:_home1.text forKey:@"HomeAddress1"];
    [driver_data setValue:_city2.text forKey:@"HomeAddress2"];
    [driver_data setValue:_home_state.text forKey:@"HomeState"];
    [driver_data setValue:_zip1.text forKey:@"OffZip"];
    [driver_data setValue:_zip2.text forKey:@"HomeZip"];
   //     [self saveToServer:driver_data];
    //BFLog(@"data for save:%@",driver_data);
    /*NSError *error;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
        //getting authCode
        NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
        NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
        NSString *authCode = @"";
        if([authData count]!=0){
            authCode = authData[0][1];
            
        }
        //---------------------------
    NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp1/saveHosCarrierData.php?masterUser=%@&User=%@&authCode=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"masteruser"],[[NSUserDefaults standardUserDefaults] valueForKey:@"user"],authCode];
    NSURL *url = [NSURL URLWithString:urlString];
    //NSData *data = [NSData dataWithContentsOfURL:url];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"HEAD"];
        NSURLResponse *response;
       // NSError *error;
        NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
if(httpResponse!=nil){
    if ([httpResponse statusCode] ==200 ) {
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    /* NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
     @"IOS TYPE", @"typemap",
     nil];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:driver_data options:0 error:&error];
    [request setHTTPBody:postData];
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error1) {
       
      //  BFLog(@"error:%@,%@",error,response);
        if(error1==nil){
            
            UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                             message:@"Data Saved Successfully!!!"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
            
            [alert2 show];

            
            
        }
        
        
        
        
    }];
        ;
    [postDataTask resume];
    }else  if ([httpResponse statusCode] ==503 ){
        
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Service Unavailable"
                                                         message:@"Please check your internet connectivity!!!"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        
        [alert1 show];
        
        
    }else{
        
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                         message:@"Internal Issue.Please Contact the Admin "
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        
        [alert1 show];
        
        
    }
}else{
    if(error.code==-1001){
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Request Timed Out"]
                                                     message:@"Please check your internet connectivity!!!"
                                                    delegate:self
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil, nil];
    
    [alert1 show];

    
    }
}*/
    }
    
}
@end
