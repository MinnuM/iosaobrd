//
//  SignaturePad.h
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/7/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
extern UIBezierPath *path;

@interface SignaturePad : UIView
-(void)erase;
@end
