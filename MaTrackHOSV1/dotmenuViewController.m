//
//  dotmenuViewController.m
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/25/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "dotmenuViewController.h"
#import "menuScreenViewController.h"


@interface dotmenuViewController ()

@end
int beginHeight;
int sendHeight;
UIView *transparentView_dot;
@implementation dotmenuViewController
@synthesize dotTable;
@synthesize menu_dot;
- (void)viewDidLoad {
    [super viewDidLoad];
    dotTable.delegate=self;
    dotTable.dataSource=self;
    beginHeight=dotTable.frame.origin.y;
   
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 2;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 100.0;
    
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    NSString *cellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    switch (indexPath.row) {
        case 0:
        {
            cell.textLabel.text=@"Begin Inspection";
            cell.textLabel.font=[UIFont fontWithName:@"Gurmukhi MN Bold" size:16.0];
            cell.textLabel.textColor=[UIColor colorWithRed:0.125 green:0.525 blue:0.125 alpha:1.0];//[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
            cell.detailTextLabel.text=@"Inspect logs for previous 7 days + today";
            cell.imageView.image=[UIImage imageNamed:@"icons/begin.png"];;
            
        }
            break;
            
        case 1:{
            
            cell.textLabel.text=@"Send Logs";
            cell.textLabel.font=[UIFont fontWithName:@"Gurmukhi MN Bold" size:16.0];
            cell.textLabel.textColor=[UIColor colorWithRed:0.125 green:0.525 blue:0.125 alpha:1.0];//[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
            
            cell.detailTextLabel.text=@"Send logs for previous 7 days + today";
            cell.imageView.image=[UIImage imageNamed:@"icons/sendlogs.png"];;

        }
            break;
            
        default:
            break;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    switch (indexPath.row) {
        case 0:
        {
            UIStoryboard *storyBoard = self.storyboard;
            UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"DOTInspectionViewController"];//DOTInspectionViewController//dot
            [self presentViewController:vc animated:YES completion:nil];
            break;
    }
case 1:
    {
        
//        UIStoryboard *storyBoard = self.storyboard;
//        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"listfiles"];
//        [self presentViewController:vc animated:YES completion:nil];
        [self performSegueWithIdentifier:@"goToSendEmail" sender:nil];
    }
            break;
        default:
            break;
    }
    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
   // UITouch *touch = [touches anyObject];
    if([[self.view subviews] containsObject:transparentView_dot]){
        [transparentView_dot removeFromSuperview];
        
    }
}


-(void)showHelp{
    
    sourceVC = [self valueForKey:@"storyboardIdentifier"];
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc1 = [storyBoard instantiateViewControllerWithIdentifier:@"helpScreen"];
    [self presentViewController:vc1 animated:YES completion:nil];
    
    
    
}


- (IBAction)helpAlert:(id)sender {
    
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    transparentView_dot  = [[UIView alloc] initWithFrame:screenSize];
    transparentView_dot.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:0.6];
    UIView *view1=[menu_dot valueForKey:@"view"];
    UIButton *tutorialLink = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    tutorialLink.frame = CGRectMake(screenSize.origin.x+(screenSize.size.width/3), screenSize.size.height-40, screenSize.size.width/3, 40);
    [tutorialLink addTarget:self action:@selector(showHelp) forControlEvents:UIControlEventTouchUpInside];
    tutorialLink.tintColor = [UIColor yellowColor];
    [tutorialLink setTitle:@"<<Know More>>" forState:UIControlStateNormal];
    
    [transparentView_dot addSubview:tutorialLink];
    
    
    
    UIImageView *arrow1=[[UIImageView alloc]initWithFrame:CGRectMake(view1.frame.origin.x+view1.frame.size.width, view1.frame.origin.y, 50, 50)];
    [arrow1 setImage:[UIImage imageNamed:@"icons/arrow2.png"]];
    UILabel *dashboard  = [[UILabel alloc] initWithFrame:CGRectMake(arrow1.frame.origin.x+arrow1.frame.size.width+20, arrow1.frame.origin.y+arrow1.frame.size.height/2, screenSize.size.width/2, 20)];
    dashboard.text=@"Dashboard";
    dashboard.numberOfLines=2;
    dashboard.textColor=[UIColor whiteColor];
    dashboard.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    dashboard.minimumScaleFactor=10./dashboard.font.pointSize;
    dashboard.adjustsFontSizeToFitWidth=YES;
    
    UIImageView *arrow2=[[UIImageView alloc]initWithFrame:CGRectMake(screenSize.size.width-80, beginHeight+50, 50, 50)];
    [arrow2 setImage:[UIImage imageNamed:@"icons/arrow3.png"]];
    UILabel *begin  = [[UILabel alloc] initWithFrame:CGRectMake(screenSize.origin.x+10,beginHeight+75, screenSize.size.width-80, 20)];
    begin.text=@"Click here to see the DOT Inspection Report";
    begin.numberOfLines=2;
    begin.textColor=[UIColor whiteColor];
    begin.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
   begin.minimumScaleFactor=10./begin.font.pointSize;
    begin.adjustsFontSizeToFitWidth=YES;
    UIImageView *arrow3=[[UIImageView alloc]initWithFrame:CGRectMake(screenSize.size.width-80, beginHeight+150, 50, 50)];
    [arrow3 setImage:[UIImage imageNamed:@"icons/arrow3.png"]];
    UILabel *send  = [[UILabel alloc] initWithFrame:CGRectMake(screenSize.origin.x+10,beginHeight+175, screenSize.size.width-80, 20)];
    send.text=@"Click here to send the DOT Inspection Report";
    send.numberOfLines=2;
    send.textColor=[UIColor whiteColor];
    send.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    send.minimumScaleFactor=10./send.font.pointSize;
    send.adjustsFontSizeToFitWidth=YES;
    [transparentView_dot addSubview:arrow3];
    [transparentView_dot addSubview:send];
    [transparentView_dot addSubview:arrow2];
    [transparentView_dot addSubview:begin];
    [transparentView_dot addSubview:dashboard];
    [transparentView_dot addSubview:arrow1];
 
    [self.view addSubview:transparentView_dot];
}
@end
