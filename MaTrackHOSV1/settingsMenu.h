//
//  settingsMenu.h
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/12/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ViewController.h"
#import "logScreenViewController.h"
#import "DBManager1.h"
#import "KeychainItemWrapper.h"
#import "Carrier.h"
#import "User.h"
#import "preferences.h"
#import "DBManager.h"
#import "LogSettings.h"
#import "rules.h"
extern NSString *property_carrying;
extern NSString *passenger_carrying;
extern NSString *oilandgas;
extern NSString *carrierName;
extern NSString *usa70hr;
extern NSString *usa60hr;
extern NSString *texas70hr;
extern NSString *californiasouth80hr;
//extern NSString *others;
extern NSDictionary *cargo;
extern NSDictionary *carrier;
extern NSDictionary *cycle;
extern NSDictionary *restart;
extern NSDictionary *breakdetails;
extern NSDictionary *driver;
extern NSDictionary *logs;
extern NSDictionary *odometer;
@interface settingsMenu : UIViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButton;
@property (weak, nonatomic) IBOutlet UITableView *settings_menu;
- (IBAction)helpAlert:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menu_settings;
@property (nonatomic, strong) DBManager1 *dbManager;
@property (nonatomic, strong) Carrier *objCarierFromMenu;
@property (nonatomic,strong) User *objUserFromMenu;
@property (nonatomic,strong) rules *objRulesFromMenu;
@end
