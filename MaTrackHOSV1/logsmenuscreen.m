//
//  logsmenuscreen.m
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/15/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "logsmenuscreen.h"
#import "previousDayViewController.h"
#import "logForm.h"
#import "SignLogViewController.h"
#import "DVIR_Main.h"
#import "menuScreenViewController.h"
#import "MBProgressHUD.h"
#import "StatusViewController.h"
#import "NotificationHandler.h"
UIView *transparentView1;
UIView *viewForDatePicker;
UIDatePicker *datepickerView;
UILabel *dateString;
UIView *dimm;
NSString * dateValue;
BOOL datePickerClicked=false;
@interface logsmenuscreen ()<UIAlertViewDelegate>
{
    UIView *titleView;
        UIAlertView *  alert;
}
@end

@implementation logsmenuscreen
@synthesize menu1;
@synthesize info1,dbManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    sign=0;
    trailer1=0;
    vehicle1=0;
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(handleUpdatedData:)
//                                                 name:@"DataUpdated"
//                                               object:nil];
     dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
    //[self loadFormData];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"inspectionType"];
    NSDate * now = [NSDate date];
    UIView *menu1View = [menu1 valueForKey:@"view"];
    UIView *info1View = [info1 valueForKey:@"view"];
    CGFloat spaceBtwnBtn = info1View.frame.origin.x-(menu1View.frame.size.width);
    titleView  = [[UIView alloc] initWithFrame:CGRectMake(menu1View.frame.size.width,0, spaceBtwnBtn, self.navigation.bounds.size.height-10)];
//    dateString = [[UILabel alloc] initWithFrame:CGRectMake(0, 5,spaceBtwnBtn/4, titleView.bounds.size.height-15)];
//    dateString.minimumScaleFactor=8./dateString.font.pointSize;
    dateString = [[UILabel alloc] initWithFrame:CGRectMake(titleView.bounds.size.width-100, 5, 100, 21)];
//    dateString.layer.backgroundColor = [UIColor redColor].CGColor;
    dateString.adjustsFontSizeToFitWidth=YES;
    
   
    UIButton *datepicker = [UIButton buttonWithType:UIButtonTypeCustom];
    NSString *device = [UIDevice currentDevice].model;
    // CGSize cellSize = CGSizeMake(100, 100);
    if([device isEqualToString:@"iPad"] || [device isEqualToString:@"iPad Simulator"]){
        
        dateString.textAlignment=NSTextAlignmentRight;
        datepicker.frame = CGRectMake(titleView.bounds.size.width-100, 5, 50, titleView.bounds.size.height-15);
        
    }else{
        
        datepicker.frame = CGRectMake(titleView.bounds.size.width-10, 5, 50, titleView.bounds.size.height-15);
    }
    
    [datepicker setImage:[UIImage imageNamed:@"datepicker.png"] forState:UIControlStateNormal];
    [datepicker setUserInteractionEnabled:YES];
    [datepicker addTarget:self action:@selector(showDatePicker) forControlEvents:UIControlEventTouchUpInside];
    [datepicker setClipsToBounds:YES];
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    
    if (section_index==1) {
        
        
        NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
        [outputFormatter setDateFormat:@"MM/dd/YYYY"];
        NSString *newDateString = [outputFormatter stringFromDate:now];
        BFLog(@"newDateString %@", newDateString);
       // self.navigation.topItem.title=selected_date;
        dateString.text = selected_date;
      
        
        
    }
    else {
        if(datePickerClicked){
            
            datePickerClicked = false;
            if([selected_date isEqualToString:@""])
            dateString.text = selected_previous;
            else
                dateString.text = selected_date;
            
            
        }else{
        NSDate *previous = [now dateByAddingTimeInterval:-(row_index+1)*24*60*60];
        NSDateFormatter *outputFormatter1 = [[NSDateFormatter alloc] init];
        [outputFormatter1 setDateFormat:@"MM/dd/YYYY"];
        NSString *previousdate = [outputFormatter1 stringFromDate:previous];
        BFLog(@"previous:%@",previousdate);
//        self.navigation.topItem.title=selected_previous;
        dateString.text = selected_previous;
        }
    }
    /*minnu*/
    dateValue=dateString.text;
    /*minnu*/
    [titleView addSubview:dateString];
    [titleView addSubview:datepicker];
    [titleView bringSubviewToFront:datepicker];
    [self.navigation.topItem.titleView addGestureRecognizer:singleFingerTap];
    
//    self.navigation.topItem.titleView = titleView;
    if (sign_id==1) {
        self.containerLog.alpha=0;
        self.containerForm.alpha=0;
        self.containerSign.alpha=1;
        self.containerDVIR.alpha=0;
        _options.selectedSegmentIndex=2;
          self.navigation.topItem.titleView = [[UIView alloc] initWithFrame:CGRectZero];
        
    }else if(form_id==1){
        self.containerLog.alpha=0;
        self.containerForm.alpha=1;
        self.containerSign.alpha=0;
        self.containerDVIR.alpha=0;
        _options.selectedSegmentIndex=1;
        self.navigation.topItem.titleView = [[UIView alloc] initWithFrame:CGRectZero];
        
    }else if(DVIR_selected==1){
        self.containerLog.alpha=0;
        self.containerForm.alpha=0;
        self.containerSign.alpha=0;
        self.containerDVIR.alpha=1;
        _options.selectedSegmentIndex=3;
        self.navigation.topItem.titleView = [[UIView alloc] initWithFrame:CGRectZero];
        
    }else{
//        _date_today.text=newDateString;
        self.containerLog.alpha=1;
        self.containerForm.alpha=0;
        self.containerSign.alpha=0;
        self.containerDVIR.alpha=0;

        self.navigation.topItem.titleView = titleView;
       
    }
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DataUpdated" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUpdatedData:)
                                                 name:@"DataUpdated"
                                               object:nil];
}
-(void)handleUpdatedData:(NSNotification *)notification {
    NSDictionary* userInfo = notification.userInfo;
    driver_status drvStat;
    int driverStat;
    driverStat = [[userInfo valueForKey:@"driverStatus"] intValue];
    if(driverStat == 1)
        drvStat = driver_status_driving;
    else if (driverStat == 2)
        drvStat = driver_status_onDuty;
    else if (driverStat == 3)
        drvStat = driver_status_offDuty;
    else if (driverStat == 4)
        drvStat = driver_status_none;
    else if (driverStat == 6)
        drvStat = driver_status_onDuty_immediate;
    else
        drvStat = driver_status_connect_back;
//    if(driverStat == 2){
//        dispatch_async(dispatch_get_main_queue(), ^{
//            alert = [[UIAlertView alloc]initWithTitle:@"Status Change" message:@"Do you want to continue driving or change your duty status" delegate:self cancelButtonTitle:@"Continue Driving" otherButtonTitles:@"Change Status", nil];
//            alert.tag=1005;
//            alert.delegate=self;
//            [alert show];
//            [self performSelector:@selector(dismissAlertView) withObject:nil afterDelay:60.0];
//        });
//    }else{
        NotificationHandler *objHelper = [NotificationHandler new];
        [objHelper recievePushNotification:self withDriverStatus:drvStat];
  //  }
}
- (void)dismissAlertView {
    NotificationHandler *objHelper = [NotificationHandler new];
    [objHelper recievePushNotification:self withDriverStatus:driver_status_onDuty_immediate];
    [alert dismissWithClickedButtonIndex:-1 animated:YES];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)showDatePicker{
    
    
    
    dimm = [[UIView alloc] initWithFrame:CGRectMake(0, self.options.bounds.origin.y+self.options.bounds.size.height+200, self.options.bounds.size.width, self.view.bounds.size.height-self.options.bounds.size.height)];
    dimm.backgroundColor=[UIColor colorWithRed:0.92 green:0.91 blue:0.91 alpha:1.0];

    viewForDatePicker=[[UIView alloc] initWithFrame:CGRectMake(25, 150, dimm.bounds.size.width-50, 200)];
    viewForDatePicker.backgroundColor=[UIColor colorWithRed:0.75 green:0.71 blue:0.75 alpha:1.0];
    viewForDatePicker.layer.borderWidth=2.0;
    viewForDatePicker.layer.borderColor=[[UIColor blackColor] CGColor];
    
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,viewForDatePicker.bounds.size.width,44)];
    [toolBar setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(selectDate:)];
    UIBarButtonItem *label = [[UIBarButtonItem alloc] init];
    [label setTitle:@"Select Last 90 days only"];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:label,space,doneBtn,nil]];
    
    
    
//    datepickerView=[[UIDatePicker alloc] initWithFrame:CGRectMake(0, toolBar.bounds.origin.y+toolBar.bounds.size.height, viewForDatePicker.bounds.size.width, viewForDatePicker.bounds.size.height-toolBar.bounds.size.height)];

    datepickerView=[[UIDatePicker alloc] initWithFrame:CGRectMake(0, toolBar.bounds.origin.y+44, viewForDatePicker.bounds.size.width, viewForDatePicker.bounds.size.height-toolBar.bounds.size.height)];
    datepickerView.datePickerMode=UIDatePickerModeDate;
       datepickerView.date=[NSDate date];
    NSDate *minDate = [[NSDate date] dateByAddingTimeInterval:60*60*24*-90];
    [datepickerView setMaximumDate:[NSDate date]];
    [datepickerView setMinimumDate:minDate];
    // [datepicker addTarget:self action:@selector(selectDate:) forControlEvents:UIControlEventValueChanged];
    [viewForDatePicker addSubview:datepickerView];
    [viewForDatePicker addSubview:toolBar];
    
       [self.view addSubview:dimm];
    [dimm addSubview:viewForDatePicker];
    [self.view addSubview:dimm];
    //  datepicker.center=viewForDatePicker.center;
    
    
}

-(void)selectDate:(id)sender{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.label.text = @"Please wait a moment";
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_async(group, queue, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDateFormatter *outputFormatter_dot11 = [[NSDateFormatter alloc] init];
            [outputFormatter_dot11 setDateFormat:@"MM/dd/YYYY"];
            NSString *today1 = [outputFormatter_dot11 stringFromDate:[datepickerView date]];
            dateString.text = today1;
            
            section_index = 2;
            commonDataHandler * common=[commonDataHandler new];
            if([today1 isEqualToString:[common getCurrentTimeInLocalTimeZone]]){
                selected_date=today1;
                selected_previous = @"";
                section_index=1;
            }
            else{
                selected_previous = today1;
                selected_date = @"";
                section_index=2;
            }
            [self.containerLog setNeedsDisplay];
            [dimm removeFromSuperview];
        });
                
    datePickerClicked=true;
    });
    dispatch_group_notify(group, queue, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            // [MBProgressHUD hideHUDForView:self.view animated:YES];
            [hud hideAnimated:YES];
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"logsmenu"];
    [self presentViewController:vc animated:YES completion:nil];
        });
    });

    
}

- (IBAction)showComponent:(id)sender {
    if (_options.selectedSegmentIndex == 0) {
        [UIView animateWithDuration:(0.5) animations:^{
            self.containerLog.alpha=1;
            self.containerForm.alpha=0;
            self.containerSign.alpha=0;
            self.containerDVIR.alpha=0;
 self.navigation.topItem.titleView = titleView;
        }];
        
    }
    else if(_options.selectedSegmentIndex == 1){
        
        [UIView animateWithDuration:(0.5) animations:^{
            self.containerLog.alpha=0;
            self.containerForm.alpha=1;
            self.containerSign.alpha=0;
            self.containerDVIR.alpha=0;
            self.navigation.topItem.titleView = [[UIView alloc] initWithFrame:CGRectZero];
            datePickerClicked=true;
            [dimm removeFromSuperview];

        }];
        
        
    }
    else if(_options.selectedSegmentIndex == 2){
        
        [UIView animateWithDuration:(0.5) animations:^{
            self.containerLog.alpha=0;
            self.containerForm.alpha=0;
            self.containerSign.alpha=1;
            self.containerDVIR.alpha=0;
            self.navigation.topItem.titleView = [[UIView alloc] initWithFrame:CGRectZero];
            datePickerClicked=true;

            [dimm removeFromSuperview];

            NSUserDefaults *sign=[NSUserDefaults standardUserDefaults];
            
            if ([sign objectForKey:@"sign"]==nil) {
                /*UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                                 message:@"Please click on Add/Edit sign to get new sign"
                                                                delegate:self
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil, nil];
                
             //   [alert2 show];*/
            }
            
        }];
        
        
    }else{
        [UIView animateWithDuration:(0.5) animations:^{
            self.containerLog.alpha=0;
            self.containerForm.alpha=0;
            self.containerSign.alpha=0;
            self.containerDVIR.alpha=1;
            self.navigation.topItem.titleView = [[UIView alloc] initWithFrame:CGRectZero];
            datePickerClicked=true;

            [dimm removeFromSuperview];

        }];
        [[[UIAlertView alloc] initWithTitle:@"DVIR"
                                    message:@"Coming soon!!!"
                                   delegate:self
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil, nil] show] ;
        
    }
    
    
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
   
    if([[self.view subviews] containsObject:transparentView1]){
        [transparentView1 removeFromSuperview];
        
    }
    if (_options.selectedSegmentIndex == 0){
        CGPoint location = [[[event allTouches] anyObject] locationInView:self.view];
        CGRect statusBarFrame = self.navigation.frame;
        if (CGRectContainsPoint(statusBarFrame, location)) {
            [self showDatePicker];
        }
    }
}
-(void)showHelp{
    
    sourceVC = [self valueForKey:@"storyboardIdentifier"];
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc1 = [storyBoard instantiateViewControllerWithIdentifier:@"helpScreen"];
    [self presentViewController:vc1 animated:YES completion:nil];
    
    
    
}
- (IBAction)helpAlert:(id)sender {
    
   
    
        CGRect screenSize = [[UIScreen mainScreen] bounds];
        transparentView1  = [[UIView alloc] initWithFrame:screenSize];
        transparentView1.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:0.6];
    UIButton *tutorialLink = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    tutorialLink.frame = CGRectMake(screenSize.origin.x+(screenSize.size.width/3), screenSize.size.height-40, screenSize.size.width/3, 40);
    [tutorialLink addTarget:self action:@selector(showHelp) forControlEvents:UIControlEventTouchUpInside];
    tutorialLink.tintColor = [UIColor yellowColor];
    [tutorialLink setTitle:@"<<Know More>>" forState:UIControlStateNormal];
    
    [transparentView1 addSubview:tutorialLink];
    

        UIView *view1=[menu1 valueForKey:@"view"];
        UIImageView *arrow3=[[UIImageView alloc]initWithFrame:CGRectMake(view1.frame.origin.x+view1.frame.size.width, view1.frame.origin.y, 50, 50)];
        [arrow3 setImage:[UIImage imageNamed:@"icons/arrow2.png"]];
        UILabel *dashboard  = [[UILabel alloc] initWithFrame:CGRectMake(arrow3.frame.origin.x+arrow3.frame.size.width+20, arrow3.frame.origin.y+arrow3.frame.size.height/2, screenSize.size.width/2, 20)];
        dashboard.text=@"Dashboard";
        dashboard.numberOfLines=2;
        dashboard.textColor=[UIColor whiteColor];
        dashboard.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
        dashboard.minimumScaleFactor=10./dashboard.font.pointSize;
        dashboard.adjustsFontSizeToFitWidth=YES;
        
    UIImageView *arrow4=[[UIImageView alloc]initWithFrame:CGRectMake(screenSize.origin.x+40, screenSize.size.height-120, 50, 50)];
        [arrow4 setImage:[UIImage imageNamed:@"icons/arrow7.png"]];
        UILabel *date  = [[UILabel alloc] initWithFrame:CGRectMake(screenSize.origin.x+10,screenSize.size.height-60, screenSize.size.width, 20)];
        date.text=@"Swipe left each row of status info to edit an event";
        date.numberOfLines=2;
        date.textColor=[UIColor whiteColor];
        date.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
        date.minimumScaleFactor=10./date.font.pointSize;
        date.adjustsFontSizeToFitWidth=YES;
    
    UIImageView *arrow5=[[UIImageView alloc]initWithFrame:CGRectMake(screenSize.size.width-80,cellHeight+30, 50, 50)];
    [arrow5 setImage:[UIImage imageNamed:@"icons/arrow8.png"]];
    UILabel *insert  = [[UILabel alloc] initWithFrame:CGRectMake(screenSize.origin.x,cellHeight+50, screenSize.size.width, 20)];
    insert.text=@"Click here to insert past duty status";
    insert.numberOfLines=2;
   insert.textColor=[UIColor whiteColor];
    insert.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    insert.minimumScaleFactor=10./insert.font.pointSize;
    insert.adjustsFontSizeToFitWidth=YES;
    
    UIImageView *arrow6=[[UIImageView alloc]initWithFrame:CGRectMake(screenSize.size.width-100,formTableHeight/2, 50, 50)];
    [arrow6 setImage:[UIImage imageNamed:@"icons/arrow2.png"]];
    UILabel *editForm  = [[UILabel alloc] initWithFrame:CGRectMake(screenSize.origin.x+10,formTableHeight/2, screenSize.size.width, 20)];
    editForm.text=@"Click any row to edit the details";
    editForm.numberOfLines=2;
   editForm.textColor=[UIColor whiteColor];
    editForm.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    editForm.minimumScaleFactor=10./editForm.font.pointSize;
    editForm.adjustsFontSizeToFitWidth=YES;
    
    UIImageView *arrow7=[[UIImageView alloc]initWithFrame:CGRectMake(editBtnY.size.width/2,editBtnY.origin.y-50, 50, 50)];
    [arrow7 setImage:[UIImage imageNamed:@"icons/arrow6.png"]];
   
    UILabel *editSign  = [[UILabel alloc] initWithFrame:CGRectMake(screenSize.origin.x+20,arrow7.frame.origin.y-arrow7.frame.size.height, screenSize.size.width, 20)];
    editSign.text=@"Click here to edit/add sign";
    editSign.numberOfLines=2;
    editSign.textColor=[UIColor whiteColor];
    editSign.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    editSign.minimumScaleFactor=10./editSign.font.pointSize;
    editSign.adjustsFontSizeToFitWidth=YES;
    
    UIImageView *arrow8=[[UIImageView alloc]initWithFrame:CGRectMake(saveBtnY.origin.x-saveBtnY.size.width/2,saveBtnY.origin.y+saveBtnY.size.height, 50, 50)];
    [arrow8 setImage:[UIImage imageNamed:@"icons/arrow7.png"]];

    UILabel *saveData  = [[UILabel alloc] initWithFrame:CGRectMake(screenSize.origin.x+20,arrow8.frame.origin.y+arrow8.frame.size.height, screenSize.size.width, 20)];
    saveData.text=@"Click here to approve the data";
    saveData.numberOfLines=2;
   saveData.textColor=[UIColor whiteColor];
    saveData.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
   saveData.minimumScaleFactor=10./saveData.font.pointSize;
    saveData.adjustsFontSizeToFitWidth=YES;
    
    UIImageView *arrow9=[[UIImageView alloc]initWithFrame:CGRectMake(screenSize.size.width-50,toolFrame.origin.y-toolFrame.size.height-50, 50, 50)];
    [arrow9 setImage:[UIImage imageNamed:@"icons/arrow6.png"]];
    UILabel *dvirData  = [[UILabel alloc] initWithFrame:CGRectMake(toolFrame.origin.x+10,arrow9.frame.origin.y-20, screenSize.size.width-10, 20)];
    dvirData.text=@"Click here to add new DVIR";
    dvirData.numberOfLines=2;
    dvirData.textColor=[UIColor whiteColor];
    dvirData.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    dvirData.minimumScaleFactor=10./dvirData.font.pointSize;
    dvirData.adjustsFontSizeToFitWidth=YES;

    if (_options.selectedSegmentIndex==0) {
        
        [transparentView1 addSubview:arrow4];
        [transparentView1 addSubview:date];
        [transparentView1 addSubview:arrow5];
        [transparentView1 addSubview:insert];
        
    }else if(_options.selectedSegmentIndex==1){
        
        
         [transparentView1 addSubview:arrow6];
        [transparentView1 addSubview:editForm];
    }else if (_options.selectedSegmentIndex==2){
        
        
    [transparentView1 addSubview:arrow7];
        [transparentView1 addSubview:editSign];
    [transparentView1 addSubview:arrow8];
        [transparentView1 addSubview:saveData];
    }else if(_options.selectedSegmentIndex==3){
        
        [transparentView1 addSubview:arrow9];
        [transparentView1 addSubview:dvirData];
        
    }
        
    
        [transparentView1 addSubview:dashboard];
        [transparentView1 addSubview:arrow3];
     
        [self.view addSubview:transparentView1];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 1005){
        if(buttonIndex==1){
            
            UIStoryboard *storyBoard = self.storyboard;
            UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"changedutystatus"];
            [self presentViewController:vc animated:YES completion:nil];
        }
    }
}
@end
