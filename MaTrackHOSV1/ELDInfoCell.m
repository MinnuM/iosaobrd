//
//  ELDInfoCell.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/25/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "ELDInfoCell.h"

@implementation ELDInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
