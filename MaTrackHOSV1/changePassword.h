//
//  changePassword.h
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/15/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "settingsMenu.h"
#import "DBManager1.h"


@interface changePassword : UIViewController


@property (weak, nonatomic) IBOutlet UITextField *currentPassword;

@property (nonatomic, strong) DBManager1 *dbManager;
- (IBAction)changePassword:(id)sender;
@end
