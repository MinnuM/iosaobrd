//
//  changeStatus.m
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/18/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "changeStatus.h"
#import <unistd.h>
#import "UIView+RNActivityView.h"
#import "MBProgressHUD.h"
#import "accountSettings.h"
#import "KeychainItemWrapper.h"
#import "insertDutyCycle.h"
#import "menuScreenViewController.h"
#import "DutyStatusTableViewCell.h"
#import "SectionRows.h"

firstGraphView *f2;
NSString *status=@"";
int flag=0;
NSString *x1;
NSString *address_full=@"";
NSString *today_sta;
NSString *today_date;
NSString *todays_date1;
UIButton *dButton;
UIButton *address;
NSMutableArray *dataLog;
NSMutableArray *locationList;
NSMutableArray *notesList;
UITextField *location;
NSArray *parts;
UITextField *notes;

@interface changeStatus (){
    
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    NSMutableArray *arrayOfTableData;
}
@property (atomic, assign) BOOL canceled;
@end

@implementation changeStatus
@synthesize locationManager;
@synthesize contentTable;
@synthesize dbManager;
- (void)viewDidLoad {
    [super viewDidLoad];
    dataLog=[[NSMutableArray alloc] init];
    locationList=[[NSMutableArray alloc] init];
    notesList=[[NSMutableArray alloc] init];
    [contentTable registerNib:[UINib nibWithNibName:@"DutyStatusTableViewCell" bundle:nil] forCellReuseIdentifier:@"DutyStatusTableViewCell"];
    [self createTableData];
    
//    UITapGestureRecognizer *backgroundTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backgroundTap)];
//
//    [self.view addGestureRecognizer:backgroundTapped];
//    contentTable.delegate=self;
//    contentTable.dataSource=self;
    dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];

    locationManager = [[CLLocationManager alloc] init];
    geocoder = [[CLGeocoder alloc] init];
    //minnu
    locationManager.delegate = self;
    [locationManager requestWhenInUseAuthorization];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    NSDate * now_sta = [NSDate date];
    NSDateFormatter *outputFormatter_sta1 = [[NSDateFormatter alloc] init];
    [outputFormatter_sta1 setDateFormat:@"MM/dd/yyyy"];
    NSString *currentDate = [outputFormatter_sta1 stringFromDate:now_sta];
    dataarray=[[NSMutableArray alloc]initWithArray:[dataDictArray objectAtIndex:0]];
    //minnu
    
    NSString *query1 = [NSString stringWithFormat:@"select data from TempTable where date='%@'",currentDate];
    
    // Get the results.
    NSArray *data = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query1]];
    if(data.count > 0){
        // BFLog(@"Result2:%@",data);
        NSString *data1=data[0][0];
        NSData *data2= [data1 dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
        //BFLog(@"Dictionary of db data:%@",json_log1);
        NSMutableArray *dict = [json_log1 objectForKey:@"payload"];
        locationList = [[json_log1 objectForKey:@"location"] mutableCopy];
        notesList = [[json_log1 objectForKey:@"notes"] mutableCopy];
        //BFLog(@"Location array:%@",location_array);
        int json_size = (int)[dict count];
        //BFLog(@"inner json count:%d",json_size);
        [dataLog removeAllObjects];
        for (int i=0; i<json_size; i++) {
            
            
            float posx=[[dict[i] objectForKey:@"posx"] floatValue];
            float x1 = posx - 40.0;
            int x2 = x1/12.25;
            int x3 = x2/4.0;
            int x4 = x2%4;
            
            NSString *minutes;
            if (15*x4 <15) {
                minutes=@"00";
            }
            else if(15*x4 >=15 && 15*x4 <30){
                minutes=@"15";
            }else if(15*x4 >=30 && 15*x4 <45){
                minutes=@"30";
            }else if(15*x4 >=45 && 15*x4 <60){
                minutes=@"45";
            }
            NSString *posx_1 = [NSString stringWithFormat:@"%d:%@",x3,minutes];
            
            // float time = [posx_1 floatValue];
            int event_no =(int)[[dict[i] objectForKey:@"posy"] integerValue];
            NSString * event;
            if (event_no == 245) {
                event=@"ON";
            }
            else if(event_no == 125){
                event=@"SB";
                
            }else if (event_no == 185){
                event = @"D";
            }else if (event_no == 65){
                event = @"OFF";
            }
            NSArray *final_data = [[NSArray alloc] initWithObjects:posx_1,event, nil];
            [dataLog addObject:final_data];
            
            
        }
        
    }
    
    NSDateFormatter *outputFormatter_sta = [[NSDateFormatter alloc] init];
    [outputFormatter_sta setDateFormat:@"HH:mm"];
    today_sta = [outputFormatter_sta stringFromDate:now_sta];
    parts=[today_sta componentsSeparatedByString:@":"];
    NSString *minutes;
    if([parts[1] integerValue]<15){
        
        minutes=@"00";
        
    }else if([parts[1] integerValue]>=15 && [parts[1] integerValue]<30){
        
        minutes=@"15";
    }else if([parts[1] integerValue]>=30 && [parts[1] integerValue]<45){
        
        minutes=@"30";
        
    }else if([parts[1] integerValue]>=45 && [parts[1] integerValue]<60){
        
        minutes=@"45";
        
    }
    todays_date1 = [NSString stringWithFormat:@"%@:%@",parts[0],minutes];
    NSDateFormatter *outputFormatter_date = [[NSDateFormatter alloc] init];
    [outputFormatter_date setDateFormat:@"MM/dd/YYYY"];
    today_date = [outputFormatter_date stringFromDate:now_sta];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark    - <UITableView Delegates & Datasources>
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return arrayOfTableData.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    Section *objSec = [arrayOfTableData objectAtIndex:section];
    return objSec.arrayRows.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Section *objSec = [arrayOfTableData objectAtIndex:indexPath.section];
    SectionRows *objRow = [objSec.arrayRows objectAtIndex:indexPath.row];
    
    NSString *cellIdentifier = @"cell";
    switch (objRow.rowType) {
        case row_type_event:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
            UILabel *events;
            if (cell==nil) {
                cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            }
            if (([cell.contentView viewWithTag:1]))
            {
                [[cell.contentView viewWithTag:1]removeFromSuperview];
            }
            events = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 500, 40)];
            events.tag = 1;
            NSString *textTitle = @"Events*";
            NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:textTitle];
            NSRange range = {[textTitle length]-1,1};
            UIColor *color = [UIColor redColor];
            UIColor *color1 =[UIColor colorWithRed:0.20 green:0.60 blue:0.07 alpha:1.0];
            [str addAttribute:NSForegroundColorAttributeName value:color range:range];
            [str addAttribute:NSForegroundColorAttributeName value:color1 range:NSMakeRange(0,[textTitle length]-1)];
            [events setAttributedText:str];
            [cell.contentView addSubview:events];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
            
        }
        case row_type_driver_onDuty:
        case row_type_driver_offDuty:
        case row_type_driver_driving:
        case row_type_driver_sleeping:
        case row_type_driver_none:
        {
            DutyStatusTableViewCell *cell = (DutyStatusTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"DutyStatusTableViewCell" forIndexPath:indexPath];
            if (cell == nil) {
                cell = [[DutyStatusTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                      reuseIdentifier:@"DutyStatusTableViewCell"];
            }
            [cell setCellData:objRow];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        case row_type_location_title:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
            if (cell==nil) {
                cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];

            }

            if (([cell.contentView viewWithTag:2]))
            {
                [[cell.contentView viewWithTag:2]removeFromSuperview];
            }
            
            UILabel *events = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 500, 40)];
            events.tag = 2;
            NSString *textTitle = @"Location*";
            NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:textTitle];
            NSRange range = {[textTitle length]-1,1};
            UIColor *color = [UIColor redColor];
            UIColor *color1 =[UIColor colorWithRed:0.20 green:0.60 blue:0.07 alpha:1.0];
            [str addAttribute:NSForegroundColorAttributeName value:color range:range];
            [str addAttribute:NSForegroundColorAttributeName value:color1 range:NSMakeRange(0,[textTitle length]-1)];
            [events setAttributedText:str];
            
            [cell.contentView addSubview:events];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;

            return cell;
        }
        case row_type_location:{
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
            if (cell==nil) {
                cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];

            }
            if ((([cell.contentView viewWithTag:3]) && ([cell.contentView viewWithTag:4])))
            {
                [[cell.contentView viewWithTag:3]removeFromSuperview];
                [[cell.contentView viewWithTag:4]removeFromSuperview];
            }
            
            location = [[UITextField alloc] initWithFrame:CGRectMake(50, 0, 130, 40)];
            [location setKeyboardType:UIKeyboardTypeDefault];
            location.tag = 3;
            location.placeholder=@"Location";
            location.delegate=self;
            location.textAlignment=NSTextAlignmentJustified;
            
            [cell.contentView addSubview:location];
            
            CGRect buttonRect3 = CGRectMake(230, 10, 30, 30);
            address = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            address.frame = buttonRect3;
            address.tag = 4;
            [address setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [address addTarget:self action:@selector(address) forControlEvents:UIControlEventTouchUpInside];
            [address setBackgroundImage:[UIImage imageNamed:@"location.png"] forState:UIControlStateNormal];
            [cell.contentView addSubview:address];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
            
        }
            
        case row_type_notes_title:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
            if (cell==nil) {
                cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
               
            }
            if (([cell.contentView viewWithTag:5]))
            {
                [[cell.contentView viewWithTag:5]removeFromSuperview];
            }
            UILabel *events = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 500, 40)];
            events.text = @"Notes";
            events.tag = 5;
            [events setFont:[UIFont fontWithName:@"Helvetica Neue Bold" size:15.0]];
            events.textColor = [UIColor colorWithRed:0.20 green:0.60 blue:0.07 alpha:1.0];
            [cell.contentView addSubview:events];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
            
        }
        case row_type_notes:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
            if (cell==nil) {
                cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
                

            }
            if (([cell.contentView viewWithTag:6]))
            {
                [[cell.contentView viewWithTag:6]removeFromSuperview];
            }
            notes = [[UITextField alloc] initWithFrame:CGRectMake(50, 0, 550, 40)];
            [notes setKeyboardType:UIKeyboardTypeDefault];
            notes.tag = 6;
            notes.placeholder=@"Notes";
            notes.delegate=self;
            [cell.contentView addSubview: notes];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        default:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
            if (cell==nil) {
                cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            }
            return cell;
        }
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Section *objSec = [arrayOfTableData firstObject];
    SectionRows *objRow = [objSec.arrayRows objectAtIndex:indexPath.row];
    if(objRow.rowType == row_type_driver_onDuty)
        [self statusOnDutyAction:objRow withIndexpath:indexPath];
    else if (objRow.rowType == row_type_driver_offDuty)
        [self statusOffDutyAction:objRow withIndexpath:indexPath];
    else if (objRow.rowType == row_type_driver_driving)
        [self statusDrivingAction:objRow withIndexpath:indexPath];
    else if (objRow.rowType == row_type_driver_sleeping)
        [self statusSleepAction:objRow withIndexpath:indexPath];
    else if (objRow.rowType == row_type_driver_none)
        [self statusNoneAction:objRow withIndexpath:indexPath];
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


-(void)saveCurrent:(NSString *)status{
    NSError *error;
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    //getting authCode
    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    NSString *authCode = @"";
    if([authData count]!=0){
        authCode = authData[0][1];
        
    }
    //---------------------------
    NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/saveHosCurrentStatus.php?masterUser=%@&User=%@&authCode=%@",masterUser,user,authCode];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSURLResponse *response;
    // NSError *error;
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    if(httpResponse!=nil){
        if ([httpResponse statusCode] ==200 ) {
            BFLog(@"Device connected to the internet");
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:60.0];
            
            [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
            
            [request setHTTPMethod:@"POST"];
            /* NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
             @"IOS TYPE", @"typemap",
             nil];*/
            
            NSMutableDictionary *currentStatus=[[NSMutableDictionary alloc]init];
            [currentStatus setValue:status forKey:@"Status"];
            
            NSData *postData = [NSJSONSerialization dataWithJSONObject:currentStatus options:0 error:&error];
            [request setHTTPBody:postData];
            
            
            NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                //BFLog(@"error:%@,%@",error,response);
                
            }];
            
            [postDataTask resume];
        }else if([httpResponse statusCode]==503){
            
            //  BFLog(@"Device not connected to the internet");
            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Service Unavailable"
                                                             message:@"Please check your internet connectivity!!!"
                                                            delegate:self
                                                   cancelButtonTitle:@"Cancel"
                                                   otherButtonTitles:@"OK", nil];
            
            [alert1 show];
            
            
        }
    }
    
}

- (IBAction)SaveBtn:(id)sender {
    autoLoginCount=0;
   /* if([location.text isEqualToString:@""]){
        
        
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"Required fields cannot be empty!"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        
        [alert1 show];
        
    }else{*/
      //  [self savetoTemp:x1];
        //  [self saveToServer:dataarray];
        // currentStatus_1=@"SB";
        // [[NSUserDefaults standardUserDefaults] setObject:x1 forKey:@"currentStatus"];
   //     [self saveCurrent:x1];
    [self saveData];
  //  }
}

-(void)saveData{
    DBManager *objDB = [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    NSDictionary *token;
    ;
    NSMutableDictionary *whereToken;
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
        token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        
        //[objDB getLogEditData:whereToken];
        commonDataHandler *common=[commonDataHandler new];
        NSString * date=[common getCurrentTimeInLocalTimeZone];
        token=@{@"userid":[NSString stringWithFormat:@"%d",userId],@"date":[NSString stringWithFormat:@"%@",date]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        NSArray* dataFromDB=[[NSArray alloc]initWithArray:[objDB getLogEditData:whereToken]];
        NSString *statusString=@"";
        for(int j=0;j<[dataFromDB count];j++){
            if([dataFromDB[j][1] isEqualToString:@""])
                statusString= [statusString stringByAppendingString:[NSString stringWithFormat:@"1"]];
            else
                statusString= [statusString stringByAppendingString:[NSString stringWithFormat:@"%@",[[dataFromDB objectAtIndex:j] objectAtIndex:1]]];
        }
        
        NSString* charStr=[@"" stringByPaddingToLength:96 withString:@"1" startingAtIndex:0];
        if([dataFromDB count]==0)
            statusString=charStr;
        else
        {
            if([statusString isEqualToString:@""])
                statusString=charStr;
            
        }
        statusString=[common modifyData:statusString date:date];
        NSString *statusStringTemp=[statusString stringByReplacingOccurrencesOfString:@"0" withString:@""];
        int currentSlot=(int)statusStringTemp.length;
        statusString=[statusString stringByReplacingCharactersInRange:NSMakeRange((int)(currentSlot-1), 1) withString:[self getStatusCode:x1]];
        
        loggraph *objLog=[loggraph new];
        for(int i=0;i<statusString.length;i++){
            objLog.slot=[NSString stringWithFormat:@"%d",(int)(1+i)];
            objLog.driverStatus=[statusString substringWithRange:NSMakeRange(i, 1)];
            objLog.calculatedStatus=[statusString substringWithRange:NSMakeRange(i, 1)];
            objLog.date=date;
            objLog.userId=[NSString stringWithFormat:@"%d",userId];
            objLog.insert=[NSString stringWithFormat:@"%d",1];
            [objDB insertPastStatus:objLog];
            }
        if(objDB.affectedRows){
            current_status=x1;
        }else{
           /* alert anjali [[[UIAlertView alloc] initWithTitle:@"Success" message:@"Modifications saved successfully"
                                       delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil, nil] show];*/
        }

    }
    
 }
-(NSString *)getStatusCode:(NSString *)status_new{
    NSString *statusNew;
    if([status_new isEqualToString:@"D"])
        statusNew=@"7";
    else if([status_new isEqualToString:@"SB"])
        statusNew=@"6";
    else if([status_new isEqualToString:@"OFF"])
        statusNew=@"5";
    else if([status_new isEqualToString:@"ON"])
        statusNew=@"8";
    else if([status_new isEqualToString:@"NONE"])
        statusNew=@"0";
    return statusNew;
}


-(NSInteger)duration:(NSString *)startTime end:(NSString *)endTime{
    NSInteger duration=0;
    NSArray *startParts = [startTime componentsSeparatedByString:@":"];
    NSArray *endParts = [endTime componentsSeparatedByString:@":"];
    NSInteger startMinutes = [startParts[0] integerValue]*60+[startParts[1] integerValue];
    NSInteger endMinutes = [endParts[0] integerValue]*60+[endParts[1] integerValue];
    duration=endMinutes-startMinutes;
    
    
    
    
    
    return duration;
    
}

//minnu
#pragma mark    - <UITextField Delegates>
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    //BFLog(@"entered return key");
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:self.contentTable];
    CGPoint contentOffset = self.contentTable.contentOffset;
    
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height)-100;
    
    BFLog(@"contentOffset is: %@", NSStringFromCGPoint(contentOffset));
    
    [self.contentTable setContentOffset:contentOffset animated:YES];
    
    return YES;
}


-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        UITableViewCell *cell = (UITableViewCell*)textField.superview.superview;
        NSIndexPath *indexPath = [self.contentTable indexPathForCell:cell];
        
        [self.contentTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    }
    
    return YES;
}
- (void)backgroundTap {
    [self.view endEditing:YES];
    BFLog(@"Touched Inside");}


//minnu



- (void)address {
    locationManager.delegate = self;
    [locationManager requestWhenInUseAuthorization];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    if ([address_full isEqualToString:@""]) {
        MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        // Set the determinate mode to show task progress.
        hud.mode = MBProgressHUDModeDeterminate;
        hud.label.text = NSLocalizedString(@"Fetching address...", @"HUD loading title");
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            // Do something useful in the background and update the HUD periodically.
            [self doSomeWorkWithProgress];
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                
                
                [hud hideAnimated:YES];
            });
            location.text=address_full;
            
            
        });
    }else{
        location.text=address_full;
    }
    
    // [self CurrentLocationIdentifier];
    //_location.text=address_full;
    BFLog(@"address:%@",address_full);
    
}
- (void)doSomeWorkWithProgress {
    self.canceled = NO;
    // This just increases the progress indicator in a loop.
    float progress = 0.0f;
    while ([address_full isEqualToString:@""]&&progress<=1.0) {
        if (self.canceled) break;
        progress += 0.01f;
        dispatch_async(dispatch_get_main_queue(), ^{
            // Instead we could have also passed a reference to the HUD
            // to the HUD to myProgressTask as a method parameter.
            [MBProgressHUD HUDForView:self.navigationController.view].progress = progress;
        });
        
        usleep(50000);
        
    }
    progress=1.0f;
    /* if ([address_full isEqualToString:@""]) {
     UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Some Error in Fetcing the Address" preferredStyle:UIAlertControllerStyleAlert];
     UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertViewStyleDefault handler:^(UIAlertAction * action) {
     [alert1 dismissViewControllerAnimated:YES completion:nil];
     }];
     
     
     [alert1 addAction:ok1];
     [self presentViewController:alert1 animated:YES completion:nil];
     
     
     }else{
     _location.text=address_full;}*/
}

-(void)CurrentLocationIdentifier
{    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
}
#pragma mark    - <Duty Status Selection>
-(void)statusOffDutyAction:(SectionRows *)objRow withIndexpath:(NSIndexPath *)indexP{
    x1=@"OFF";
    current_status = @"OFF";
    NSInteger duration=[self duration:dataarray[[dataarray count]-2][0] end:dataarray[[dataarray count]-1][0]];
    if (duration >=15) {
        NSArray *con2 = @[dataarray[[dataarray count]-1][0],x1];
        [dataarray addObject:con2];
        [dataarray addObject:con2];
        
        
        
    }else{
        if([dataarray count]%2==0){
            [dataarray removeLastObject];
            [dataarray removeLastObject];
            NSArray *con2 = @[todays_date1,x1];
            [dataarray addObject:con2];
            [dataarray addObject:con2];
        }else{
            
            [dataarray removeLastObject];
            NSArray *con2 = @[todays_date1,x1];
            [dataarray addObject:con2];
            [dataarray addObject:con2];
            
        }
    }
    Section *objSec = [arrayOfTableData firstObject];
    for (int i= 0 ; i< objSec.arrayRows.count; i++) {
        SectionRows *row = [objSec.arrayRows objectAtIndex:i];
        row.isSelected = NO;
        [objSec.arrayRows replaceObjectAtIndex:i withObject:row];
    }
    NSInteger index = [objSec.arrayRows indexOfObject:objRow];
    objRow.isSelected = YES;
    [objSec.arrayRows replaceObjectAtIndex:index withObject:objRow];
    
    NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:1 inSection:0];
    NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:2 inSection:0];
    NSIndexPath *indexPath3 = [NSIndexPath indexPathForRow:3 inSection:0];
    NSIndexPath *indexPath4 = [NSIndexPath indexPathForRow:4 inSection:0];
    NSIndexPath *indexPath5 = [NSIndexPath indexPathForRow:5 inSection:0];
    
    [contentTable beginUpdates];
    [contentTable reloadRowsAtIndexPaths:@[indexPath1, indexPath2, indexPath3, indexPath4, indexPath5] withRowAnimation:UITableViewRowAnimationNone];
    [contentTable endUpdates];
 //   [contentTable reloadData];
    
}
- (void)statusSleepAction:(SectionRows *)objRow withIndexpath:(NSIndexPath *)indexP{
    x1=@"SB";
     current_status = @"SB";
    if(dataarray.count > 0){
        
        NSInteger duration=[self duration:dataarray[[dataarray count]-2][0] end:dataarray[[dataarray count]-1][0]];
        if (duration >=15) {
            NSArray *con2 = @[dataarray[[dataarray count]-1][0],x1];
            [dataarray addObject:con2];
            [dataarray addObject:con2];
            
            
            
            
        }else{
            if([dataarray count]%2==0){
                [dataarray removeLastObject];
                [dataarray removeLastObject];
                NSArray *con2 = @[todays_date1,x1];
                [dataarray addObject:con2];
                [dataarray addObject:con2];
            }else{
                
                [dataarray removeLastObject];
                NSArray *con2 = @[todays_date1,x1];
                [dataarray addObject:con2];
                [dataarray addObject:con2];
                
            }
        }
    }
    Section *objSec = [arrayOfTableData firstObject];
    for (int i= 0 ; i< objSec.arrayRows.count; i++) {
        SectionRows *row = [objSec.arrayRows objectAtIndex:i];
        row.isSelected = NO;
        [objSec.arrayRows replaceObjectAtIndex:i withObject:row];
    }
    NSInteger index = [objSec.arrayRows indexOfObject:objRow];
    objRow.isSelected = YES;
    [objSec.arrayRows replaceObjectAtIndex:index withObject:objRow];
    NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:1 inSection:0];
    NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:2 inSection:0];
    NSIndexPath *indexPath3 = [NSIndexPath indexPathForRow:3 inSection:0];
    NSIndexPath *indexPath4 = [NSIndexPath indexPathForRow:4 inSection:0];
    NSIndexPath *indexPath5 = [NSIndexPath indexPathForRow:5 inSection:0];
    
    [contentTable beginUpdates];
    [contentTable reloadRowsAtIndexPaths:@[indexPath1, indexPath2, indexPath3, indexPath4, indexPath5] withRowAnimation:UITableViewRowAnimationNone];
    [contentTable endUpdates];
   // [contentTable reloadData];
}
- (void)statusOnDutyAction:(SectionRows *)objRow withIndexpath:(NSIndexPath *)indexP{
    flag=flag+1;
    x1=@"ON";
     current_status = @"ON";
    NSInteger duration=[self duration:dataarray[[dataarray count]-2][0] end:dataarray[[dataarray count]-1][0]];
    if (duration >=15) {
        NSArray *con2 = @[dataarray[[dataarray count]-1][0],x1];
        [dataarray addObject:con2];
        [dataarray addObject:con2];
        
        
        
    }else{
        if([dataarray count]%2==0){
            [dataarray removeLastObject];
            [dataarray removeLastObject];
            NSArray *con2 = @[todays_date1,x1];
            [dataarray addObject:con2];
            [dataarray addObject:con2];
        }else{
            
            [dataarray removeLastObject];
            NSArray *con2 = @[todays_date1,x1];
            [dataarray addObject:con2];
            [dataarray addObject:con2];
            
        }
    }
    Section *objSec = [arrayOfTableData firstObject];
    for (int i= 0 ; i< objSec.arrayRows.count; i++) {
        SectionRows *row = [objSec.arrayRows objectAtIndex:i];
        row.isSelected = NO;
        [objSec.arrayRows replaceObjectAtIndex:i withObject:row];
    }
    NSInteger index = [objSec.arrayRows indexOfObject:objRow];
    objRow.isSelected = YES;
    [objSec.arrayRows replaceObjectAtIndex:index withObject:objRow];
    NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:1 inSection:0];
    NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:2 inSection:0];
    NSIndexPath *indexPath3 = [NSIndexPath indexPathForRow:3 inSection:0];
    NSIndexPath *indexPath4 = [NSIndexPath indexPathForRow:4 inSection:0];
    NSIndexPath *indexPath5 = [NSIndexPath indexPathForRow:5 inSection:0];
    
    [contentTable beginUpdates];
    [contentTable reloadRowsAtIndexPaths:@[indexPath1, indexPath2, indexPath3, indexPath4, indexPath5] withRowAnimation:UITableViewRowAnimationNone];
    [contentTable endUpdates];
  //  [contentTable reloadData];
}

- (void)statusDrivingAction:(SectionRows *)objRow withIndexpath:(NSIndexPath *)indexP{
    x1=@"D";
     current_status = @"D";
    NSInteger duration=[self duration:dataarray[[dataarray count]-2][0] end:dataarray[[dataarray count]-1][0]];
    if (duration >=15) {
        NSArray *con2 = @[dataarray[[dataarray count]-1][0],x1];
        [dataarray addObject:con2];
        [dataarray addObject:con2];
        
        
        
    }else{
        if([dataarray count]%2==0){
            [dataarray removeLastObject];
            [dataarray removeLastObject];
            NSArray *con2 = @[todays_date1,x1];
            [dataarray addObject:con2];
            [dataarray addObject:con2];
        }else{
            
            [dataarray removeLastObject];
            NSArray *con2 = @[todays_date1,x1];
            [dataarray addObject:con2];
            [dataarray addObject:con2];
        }
    }
    Section *objSec = [arrayOfTableData firstObject];
    for (int i= 0 ; i< objSec.arrayRows.count; i++) {
        SectionRows *row = [objSec.arrayRows objectAtIndex:i];
        row.isSelected = NO;
        [objSec.arrayRows replaceObjectAtIndex:i withObject:row];
    }
    NSInteger index = [objSec.arrayRows indexOfObject:objRow];
    objRow.isSelected = YES;
    [objSec.arrayRows replaceObjectAtIndex:index withObject:objRow];
    NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:1 inSection:0];
    NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:2 inSection:0];
    NSIndexPath *indexPath3 = [NSIndexPath indexPathForRow:3 inSection:0];
    NSIndexPath *indexPath4 = [NSIndexPath indexPathForRow:4 inSection:0];
    NSIndexPath *indexPath5 = [NSIndexPath indexPathForRow:5 inSection:0];
    
    [contentTable beginUpdates];
    [contentTable reloadRowsAtIndexPaths:@[indexPath1, indexPath2, indexPath3, indexPath4, indexPath5] withRowAnimation:UITableViewRowAnimationNone];
    [contentTable endUpdates];
 //   [contentTable reloadData];
}

-(void)statusNoneAction:(SectionRows *)objRow withIndexpath:(NSIndexPath *)indexP{
    x1=@"NONE";
     current_status = @"NONE";
    NSInteger duration=[self duration:dataarray[[dataarray count]-2][0] end:dataarray[[dataarray count]-1][0]];
    if (duration >=15) {
        NSArray *con2 = @[dataarray[[dataarray count]-1][0],@"OFF"];
        [dataarray addObject:con2];
        [dataarray addObject:con2];
        
        
        
    }else{
        if([dataarray count]%2==0){
            [dataarray removeLastObject];
            [dataarray removeLastObject];
            NSArray *con2 = @[todays_date1,@"OFF"];
            [dataarray addObject:con2];
            [dataarray addObject:con2];
        }else{
            
            [dataarray removeLastObject];
            NSArray *con2 = @[todays_date1,@"OFF"];
            [dataarray addObject:con2];
            [dataarray addObject:con2];
        }
    }
    Section *objSec = [arrayOfTableData firstObject];
    for (int i= 0 ; i< objSec.arrayRows.count; i++) {
        SectionRows *row = [objSec.arrayRows objectAtIndex:i];
        row.isSelected = NO;
        [objSec.arrayRows replaceObjectAtIndex:i withObject:row];
    }
    NSInteger index = [objSec.arrayRows indexOfObject:objRow];
    objRow.isSelected = YES;
    [objSec.arrayRows replaceObjectAtIndex:index withObject:objRow];
    NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:1 inSection:0];
    NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:2 inSection:0];
    NSIndexPath *indexPath3 = [NSIndexPath indexPathForRow:3 inSection:0];
    NSIndexPath *indexPath4 = [NSIndexPath indexPathForRow:4 inSection:0];
    NSIndexPath *indexPath5 = [NSIndexPath indexPathForRow:5 inSection:0];
    
    [contentTable beginUpdates];
    [contentTable reloadRowsAtIndexPaths:@[indexPath1, indexPath2, indexPath3, indexPath4, indexPath5] withRowAnimation:UITableViewRowAnimationNone];
    [contentTable endUpdates];
 //   [contentTable reloadData];
}
-(void)saveToServer:(NSMutableDictionary *)driver_data{
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag ==1 || alertView.tag ==2){
        if(buttonIndex==[alertView cancelButtonIndex]){
            dispatch_async(dispatch_get_main_queue(), ^{
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"loginscreen"];
                [self presentViewController:vc animated:YES completion:nil];
            });
            
        }
    }
}

-(void)savetoTemp:(NSString *)status{
    if (notesList==nil) {
        notesList=[[NSMutableArray alloc] init];
        
    }
    NSInteger idValue = [dataarray count]-1;
    
    NSMutableDictionary *dict0 = [[NSMutableDictionary alloc] init];
    [dict0 setValue:[NSString stringWithFormat:@"%ld",(long)idValue] forKey:@"id"];
    [dict0 setValue:notes.text forKey:@"value"];
    [notesList addObject:dict0];
    if (locationList==nil) {
        locationList=[[NSMutableArray alloc] init];
    }
    
    NSMutableDictionary *dictLog = [[NSMutableDictionary alloc] init];
    [dictLog setValue:[NSString stringWithFormat:@"%ld",(long)idValue] forKey:@"id"];
    [dictLog setValue:location.text forKey:@"value"];
    BFLog(@"Dict:%@",dictLog);
    [locationList addObject:dictLog];

    BFLog(@"data:%@",dataarray);
    NSString *location = _location.text;
    int id1 =(int) [dataarray count]-1;
    NSMutableDictionary *dict1 = [[NSMutableDictionary alloc] init];
    [dict1 setValue:@(id1) forKey:@"id"];
    [dict1 setValue:location forKey:@"location"];

    NSMutableArray *outer_array = [[NSMutableArray alloc] init];
    NSMutableDictionary *data_dict = [[NSMutableDictionary alloc] init];
    
    for (int i=0; i<[dataarray count]; i++) {
        
        NSString *x12=dataarray[i][0];
        NSArray *time = [x12 componentsSeparatedByString:@":"];
        int x4 =(int) [time[1] integerValue]/15;
        int x2 =(int) [time[0] integerValue]*4+x4;
        float x1 = 12.25*x2;
        float posx = x1+40.0;
        int posy = 65;
        BFLog(@"posx:%.2f",posx);
        if ([dataarray[i][1] isEqualToString:@"OFF"]) {
            posy = 65;
        }else if([dataarray[i][1] isEqualToString:@"ON"]){
            posy = 245;
        }else if([dataarray[i][1] isEqualToString:@"D"]){
            posy=185;
        }else if ([dataarray[i][1] isEqualToString:@"SB"]){
            posy=125;
        }
        
        NSMutableDictionary *cont1 = [[NSMutableDictionary alloc] init];
        [cont1 setValue:@(i) forKey:@"id"];
        [cont1 setValue:@(posx) forKey:@"posx"];
        [cont1 setValue:@(posy) forKey:@"posy"];
        
        [outer_array addObject:cont1];
    }
    
    [data_dict setObject:outer_array forKey:@"payload"];
    if (locationList !=NULL && locationList!=(id)[NSNull null]  && locationList!=nil && [locationList count]!=0) {
        
        [data_dict setObject:locationList forKey:@"location"];
        
        
    }
    if (notesList !=NULL && notesList!=(id)[NSNull null]  && notesList!=nil && [notesList count]!=0) {
        [data_dict setObject:notesList forKey:@"notes"];
    }
    BFLog(@"dictionary:%@",data_dict);
    NSData *data = [NSJSONSerialization dataWithJSONObject:data_dict options:0 error:nil];
    NSString *jsonStr = [[NSString alloc] initWithData:data
                                              encoding:NSUTF8StringEncoding];
    BFLog(@"JSON:%@",jsonStr);
    NSString *query1 = [NSString stringWithFormat:@"select * from TempTable where date='%@'",selected_date];
    NSArray *values = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query1]];
    if ([values count]!=0) {
        NSString *query_update = [NSString stringWithFormat:@"UPDATE TempTable SET data='%@',last_status='%@' WHERE date='%@'",jsonStr,status,selected_date];
        
        BFLog(@"Query:%@",query_update);
        [dbManager executeQuery:query_update];
        
        if (dbManager.affectedRows != 0) {
            BFLog(@"Table updated successfully. Affected rows = %d", dbManager.affectedRows);
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"Status successfully changed to %@ ",status] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else{
            BFLog(@"Could not execute the query");
        }
        [self saveToServer:data_dict];
    }
    else{
        NSString *query_insert1 = [NSString stringWithFormat:@"insert into TempTable values(null, '%@', '%@', '%@' ,'%d')", selected_date,jsonStr,status,0];
        [dbManager executeQuery:query_insert1];
        
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
            BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention!!!" message:@"Status Changed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
        }
        else{
            BFLog(@"Could not execute the query");
        }

    }
    
}
#pragma mark    - <CLLocationManager Delegate>
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *newLocation = [locations lastObject];
    [geocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error == nil&& [placemarks count] >0) {
            placemark = [placemarks lastObject];
            NSString *latitude, *longitude, *state, *country,*locality;
            latitude = [NSString stringWithFormat:@"%f",newLocation.coordinate.latitude];
            longitude = [NSString stringWithFormat:@"%f",newLocation.coordinate.longitude];
            state = placemark.administrativeArea;
            country = placemark.country;
            locality=placemark.locality;
            BFLog(@"Address:%@,%@,%@",locality,state,country);
            address_full=[NSString stringWithFormat:@"%@, %@",locality,state];
            location.text=address_full;
            //address_full=@"";
        } else {
            // BFLog(@"%@", error.debugDescription);
        }
    }];
    // Turn off the location manager to save power.
    [manager stopUpdatingLocation];
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    BFLog(@"Cannot find the location.");
}
#pragma mark    - <Load Table Data>
-(void)createTableData{
    arrayOfTableData = [NSMutableArray new];
    Section *objSec = [Section new];
    
    SectionRows *objRow;
    objRow = [SectionRows new];
    objRow.rowType = row_type_event;
    objRow.isSelected = NO;
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_driver_offDuty;
    if([current_status isEqualToString:@"OFF"])
        objRow.isSelected = YES;
    else
        objRow.isSelected = NO;
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_driver_driving;
    if([current_status isEqualToString:@"D"])
        objRow.isSelected = YES;
    else
        objRow.isSelected = NO;
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_driver_onDuty;
    if([current_status isEqualToString:@"ON"])
        objRow.isSelected = YES;
    else
        objRow.isSelected = NO;
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_driver_sleeping;
    if([current_status isEqualToString:@"SB"])
        objRow.isSelected = YES;
    else
        objRow.isSelected = NO;
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_driver_none;
    if([current_status isEqualToString:@"NONE"])
        objRow.isSelected = YES;
    else
        objRow.isSelected = NO;
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_location_title;
    objRow.isSelected = NO;
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_location;
    objRow.isSelected = NO;
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_notes_title;
    objRow.isSelected = NO;
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_notes;
    objRow.isSelected = NO;
    [objSec.arrayRows addObject:objRow];
    
    [arrayOfTableData addObject:objSec];
    
    [contentTable reloadData];
}
@end

