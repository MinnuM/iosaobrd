//
//  loginLandingViewController.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 2/2/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "loginLandingViewController.h"
#import "loginViewController.h"
#import "pageContentViewController.h"
#import "welcomeScreenViewController.h"
#import "settingsWelcomeViewController.h"
#import "KeychainItemWrapper.h"

NSString *masterLogin=@"";
NSString *userLogin=@"";
NSString *cycle_flag1;
NSInteger decision1;
NSDate *now2;
NSString *today_log_login1;
NSDictionary *json_login1;
@interface loginLandingViewController ()

@end

@implementation loginLandingViewController
@synthesize loaderImage;
@synthesize messageLabel;
@synthesize dbManager;
@synthesize pageTitles;
@synthesize pageImages;
@synthesize pageViewController;

- (void)viewDidLoad {
    [super viewDidLoad];
    pageTitles = @[@"Welcome to MaTrack HOS.Please follow these steps before begin", @"Step1. Select settings ", @"Step2.Select Driver Details ,Fill the Details and Hit Save", @"Step4. Select the Timezone & Cycle, fill the details and save"];
    pageImages = @[@"menu.png", @"settings.png", @"driver.png", @"timezone.png"];
    //pageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentViewController"];
    now2 = [NSDate date];
    NSDateFormatter *outputFormatter_log = [[NSDateFormatter alloc] init];
    [outputFormatter_log setDateFormat:@"MM/dd/YYYY"];
    today_log_login1 = [outputFormatter_log stringFromDate:now2];
    dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
    messageLabel.minimumScaleFactor=10./messageLabel.font.pointSize;
    messageLabel.adjustsFontSizeToFitWidth=YES;
    cycle_flag1 =[[NSUserDefaults standardUserDefaults] objectForKey:@"decision_to_db_load"];
    if ([cycle_flag1 integerValue]==1) {
        if([[[NSUserDefaults standardUserDefaults] objectForKey:@"previousUser"] isEqualToString:USer]){
            
            cycle_flag1=@"1";
            decision1=1;
        }else{
            cycle_flag1=@"0";
        }
    }
    //[self performSelectorInBackground:@selector(rotateLayerInfinite:loaderImage:.layer) withObject:nil];
    [self rotateLayerInfinite:loaderImage.layer];
    [self LoadDataToDB];

}

-(void)LoadDataToDB{
    
    if ([cycle_flag1 integerValue]==0) {
        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"decision_to_db_load"];
        if([[NSUserDefaults standardUserDefaults] objectForKey:@"USER_SIGNATURE_PATH"]!=nil){
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"USER_SIGNATURE_PATH"];
        }
        if([[NSUserDefaults standardUserDefaults] objectForKey:@"sign"]!=nil){
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"sign"];
        }
        if([[NSUserDefaults standardUserDefaults] objectForKey:@"USER_SIGNATURE_PATH_MECH"]!=nil){
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"USER_SIGNATURE_PATH_MECH"];
        }
        [self clearDB];
    }else{
        
        [self moveToMenu];
    }
    
}

-(void)moveToMenu{
    masterLogin=[[NSUserDefaults standardUserDefaults] valueForKey:@"masteruser"];
    userLogin=[[NSUserDefaults standardUserDefaults] valueForKey:@"user"];
    dispatch_async(dispatch_get_main_queue(), ^{
        if([[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"firstLaunch%@",USer]]==nil){
            
            self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
            self.pageViewController.dataSource = self;
            
            settingsWelcomeViewController *startingViewController = [self viewControllerAtIndex:0];
            NSArray *viewControllers = @[startingViewController];
            [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
            
            // Change the size of page view controller
            self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 30);
            
            [self addChildViewController:pageViewController];
            [self.view addSubview:pageViewController.view];
            [self.pageViewController didMoveToParentViewController:self];
            [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:[NSString stringWithFormat:@"firstLaunch%@",USer]];
            
        }else{
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"menuscreen"];
    [self presentViewController:vc animated:YES completion:nil];
        }
    });
    
}
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((pageContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((pageContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.pageTitles count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (settingsWelcomeViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count])) {
        return nil;
    }
   settingsWelcomeViewController *pageContentViewController;
    if(index==0){
        pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"welcomeScreen"];
        
    }else{
    // Create a new view controller and pass suitable data.
    pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"settingsWelcome"];
    pageContentViewController.imageFile = self.pageImages[index];
    pageContentViewController.titleText = self.pageTitles[index];
    pageContentViewController.pageIndex = index;
    }
    return pageContentViewController;
}
- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}
-(void)clearDB{
    
    NSString *query_del =[NSString stringWithFormat:@"delete from TempTable;"];
    [dbManager executeQuery:query_del];
    NSString *query_del0 =[NSString stringWithFormat:@"delete from SamTable;"];
    [dbManager executeQuery:query_del0];
    NSString *query_del1 =[NSString stringWithFormat:@"delete from FinalTable;"];
    [dbManager executeQuery:query_del1];
    NSString *query_del2 =[NSString stringWithFormat:@"delete from Docs;"];
    [dbManager executeQuery:query_del2];
    NSString *query_del3 =[NSString stringWithFormat:@"delete from OtherSettings;"];
    [dbManager executeQuery:query_del3];
    
    NSString *query_del4 =[NSString stringWithFormat:@"delete from GeneralSettings;"];
    [dbManager executeQuery:query_del4];
    NSString *query_del5 =[NSString stringWithFormat:@"delete from Carrier_Sett;"];
    [dbManager executeQuery:query_del5];
    NSString *query_del6 =[NSString stringWithFormat:@"delete from DVIR_Report;"];
    [dbManager executeQuery:query_del6];
    NSString *query_del7 =[NSString stringWithFormat:@"delete from AccountSettings;"];
    [dbManager executeQuery:query_del7];
    NSString *query_del8 =[NSString stringWithFormat:@"delete from LogSettings;"];
    [dbManager executeQuery:query_del8];
    
    [self initializeDB];
    
}
-(void)initializeDB{
    // dispatch_group_t group = dispatch_group_create();
    dispatch_queue_t queue = dispatch_queue_create("com.SievaNetworks.app.queue", DISPATCH_QUEUE_CONCURRENT);
    // dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    //dispatch_group_t group = dispatch_group_create();
  //  BFLog(@"success-------------%@\n",sucess);
    
      dispatch_sync(queue, ^{
                int countday = 90;
                // [dataarray1 removeAllObjects];
          //getting authCode
          NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
          NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
          NSString *authCode = @"";
          if([authData count]!=0){
              authCode = authData[0][1];
              
          }
          //---------------------------

                NSString *urlString1 = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/getHosLast7DaysData.php?masterUser=%@&user=%@&date=%@&countDays=%d&authCode=%@",master,USer,today_log_login1,countday,authCode];
                BFLog(@"Url:%@",urlString1);
                NSURLSession *session1 = [NSURLSession sharedSession];
                NSURLSessionDataTask *dataTask1 = [session1 dataTaskWithURL:[NSURL URLWithString:urlString1] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    //  BFLog(@"Retrieved Data:%@,%@",data,response);
                    BFLog(@"---------------------------------------");
                  
                    if (error.code==0) {
                        
                    
                    json_login1 = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    //if (error == nil) {
                    //NSString *query_del = @"delete from TempTable";
                    //[dbManager executeQuery:query_del];
                    BFLog(@"Data From File:%@",json_login1);
                    NSArray *keys = [json_login1 allKeys];
                    for (NSString *key in keys){
                        
                        NSString *value=[json_login1 objectForKey:key];
                        BFLog(@"values in dict:key:%@,value:%@",key,value);
                        if([key isEqualToString:@"01/22/2017"]){
                            BFLog(@"");
                        }
                        if ([value isEqualToString:@""]) {
                            if (![key isEqualToString:today_log_login1]) {
                                
                                NSMutableDictionary *main = [[NSMutableDictionary alloc] init];
                                NSMutableArray *data1 = [[NSMutableArray alloc]init];
                                NSMutableDictionary *data0 = [[NSMutableDictionary alloc] init];
                                [data0 setValue:@"0" forKey:@"id"];
                                [data0 setValue:@"40" forKey:@"posx"];
                                [data0 setValue:@"65" forKey:@"posy"];
                                NSMutableDictionary *data2 = [[NSMutableDictionary alloc] init];
                                [data2 setValue:@"1" forKey:@"id"];
                                [data2 setValue:@"1216" forKey:@"posx"];
                                [data2 setValue:@"65" forKey:@"posy"];
                                [data1 addObject:data0];
                                [data1 addObject:data2];
                                [main setValue:data1 forKey:@"payload"];
                                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                formatter.dateFormat=@"MM/dd/yyyy";
                                [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                                NSDate *date1=[formatter dateFromString:key];
                                NSTimeInterval interval=[date1 timeIntervalSince1970]*1000;
                                BFLog(@"Timeinterval:%f",interval);
                                BFLog(@"Main:%@",main);
                                NSData *data = [NSJSONSerialization dataWithJSONObject:main options:NSJSONWritingPrettyPrinted error:nil];
                                NSString *jsonStr1 = [[NSString alloc] initWithData:data
                                                                           encoding:NSUTF8StringEncoding];
                                NSString *query_insert1 = [NSString stringWithFormat:@"insert into TempTable values(null, '%@', '%@', 'OFF' ,'%d','%d','%f')", key,jsonStr1,0,1,interval];
                                
                                [dbManager executeQuery:query_insert1];
                                
                                // If the query was successfully executed then pop the view controller.
                                if (dbManager.affectedRows != 0) {
                                    BFLog(@"Query was executed successfully for empty data. Affected rows = %d", dbManager.affectedRows);
                                    
                                    // Pop the view controller.
                                    //[self.navigationController popViewControllerAnimated:YES];
                                }
                                else{
                                    BFLog(@"Could not execute the query");
                                }
                                
                            }
                        }
                        else{
                            
                            NSArray *parts=[value componentsSeparatedByString:@"$"];
                            BFLog(@"data:%@",parts[2]);
                            if ([parts[2]  isEqual: @"[]"] || [parts[2]  isEqual: @"null"]) {
                                BFLog(@"Empty");
                                
                            }
                            
                            
                            NSData *jdata = [parts[2] dataUsingEncoding:NSUTF8StringEncoding];
                            NSDictionary *data = [NSJSONSerialization JSONObjectWithData:jdata options:0 error:nil];
                            NSArray *payload = data[@"payload"];
                            // BFLog(@"%lu",(unsigned long)[payload count]);
                            //minnu
                            
                            
                            if (payload != (id)[NSNull null] && payload!=nil && [payload count]!=0) {
                                NSString *status = [self getLastStatus:parts[2]];
                                BFLog(@"status:%@",status);
                                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                formatter.dateFormat=@"MM/dd/yyyy";
                                [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                                NSDate *date1=[formatter dateFromString:parts[0]];
                                NSTimeInterval interval=[date1 timeIntervalSince1970]*1000;
                                
                                
                                NSString *query_insert = [NSString stringWithFormat:@"insert into TempTable values(null, '%@', '%@', '%@' ,'%@','%d','%f')", parts[0],parts[2],status,parts[4],0,interval];
                                
                                // Execute the query.
                                
                                //NSString *query_del = @"delete from TempTable";
                                [dbManager executeQuery:query_insert];
                                
                                // If the query was successfully executed then pop the view controller.
                                if (dbManager.affectedRows != 0) {
                                    BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
                                    
                                }
                                else{
                                    BFLog(@"Could not execute the query");
                                }

                               NSString *query_insert1 = [NSString stringWithFormat:@"insert into FinalTable values(null, '%@', '%@', '%@' ,'%@','%d')", parts[0],parts[2],status,parts[4],0];

                                
                                // Execute the query.
                                
                                //NSString *query_del = @"delete from TempTable";
                                [dbManager executeQuery:query_insert1];
                                
                                // If the query was successfully executed then pop the view controller.
                                if (dbManager.affectedRows != 0) {
                                    BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
                                    
                                }
                                else{
                                    BFLog(@"Could not execute the query");
                                }
                                
                                
                            }
                            else{
                                
                                NSMutableDictionary *main = [[NSMutableDictionary alloc] init];
                                NSMutableArray *data1 = [[NSMutableArray alloc]init];
                                NSMutableDictionary *data0 = [[NSMutableDictionary alloc] init];
                                [data0 setValue:@"0" forKey:@"id"];
                                [data0 setValue:@"40" forKey:@"posx"];
                                [data0 setValue:@"65" forKey:@"posy"];
                                NSMutableDictionary *data2 = [[NSMutableDictionary alloc] init];
                                [data2 setValue:@"1" forKey:@"id"];
                                [data2 setValue:@"1216" forKey:@"posx"];
                                [data2 setValue:@"65" forKey:@"posy"];
                                [data1 addObject:data0];
                                [data1 addObject:data2];
                                [main setValue:data1 forKey:@"payload"];
                                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                formatter.dateFormat=@"MM/dd/yyyy";
                                [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                                NSDate *date1=[formatter dateFromString:key];
                                NSTimeInterval interval=[date1 timeIntervalSince1970]*1000;
                                BFLog(@"Timeinterval:%f",interval);
                                
                                BFLog(@"Main:%@",main);
                                NSData *data = [NSJSONSerialization dataWithJSONObject:main options:NSJSONWritingPrettyPrinted error:nil];
                                NSString *jsonStr1 = [[NSString alloc] initWithData:data
                                                                           encoding:NSUTF8StringEncoding];
                                NSString *query_insert1 = [NSString stringWithFormat:@"insert into TempTable values(null, '%@', '%@', 'OFF' ,'%d','%d','%f')", key,jsonStr1,0,1,interval];
                                
                                [dbManager executeQuery:query_insert1];
                                
                                // If the query was successfully executed then pop the view controller.
                                if (dbManager.affectedRows != 0) {
                                    BFLog(@"Query was executed successfully for empty data. Affected rows = %d", dbManager.affectedRows);
                                    
                                    // Pop the view controller.
                                    //[self.navigationController popViewControllerAnimated:YES];
                                }
                                else{
                                    BFLog(@"Could not execute the query");
                                }
                                
                                
                                
                            }
                            
                        }
                    }
                    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"decision_to_db_load"];
                    }else{
                        
                        
                        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                         message:@"Could not connect to server.Please try again later!!!"
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil, nil];
                        
                        [alert1 show];
                 
                    }
                }];
                
                [dataTask1 resume];
                
                sleep(2);
                [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"decision_to_db_load"];
                
          //  });
            /*   dispatch_sync(queue, ^{
             BFLog(@"Loading settings info");
             
             NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/getHosSettings.php?masterUser=%@&user=%@",master,USer];
             NSURLSession *session = [NSURLSession sharedSession];
             NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:urlString] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
             //  BFLog(@"Retrieved Data:%@,%@",data,response);
             BFLog(@"---------------------------------------");
             BFLog(@"error:%@",error);
             NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
             //if (error == nil) {
             cargo1=json[@"Cargo"];
             carrier1=json[@"Carrier"];
             cycle1=json[@"Cycle"];
             restart1=json[@"Restart"];
             breakdetails1=json[@"Break"];
             driver1=json[@"Driver"];
             logs1=json[@"Logs"];
             BFLog(@"cargo:%@",cargo1);
             BFLog(@"carrier1:%@",carrier1);
             
             BFLog(@"cycle1:%@",cycle1);
             BFLog(@"restart1:%@",restart1);
             BFLog(@"breakdetails1:%@",breakdetails1);
             BFLog(@"driver1:%@",driver1);
             BFLog(@"logs1:%@",logs1);
             
             
             NSManagedObjectContext *context=[self managedObjectContext];
             NSManagedObject *settings = [NSEntityDescription insertNewObjectForEntityForName:@"RulesSettings" inManagedObjectContext:context];
             [settings setValue:cycle1[@"CycleType"] forKey:@"cycle"];
             [settings setValue:cargo1[@"CargoType"] forKey:@"cargoType"];
             [context save:&error];
             NSError *error1 = nil;
             // Save the object to persistent store
             if (![context save:&error1]) {
             BFLog(@"Can't Save! %@ %@", error1, [error1 localizedDescription]);
             }
             
             
             NSString *query_insert1 = [NSString stringWithFormat:@"insert into LogSettings values(null, '%@', '%@', '%@','%@','%@')",logs1[@"Timezone"],cycle1[@"CycleType"],cargo1[@"CargoType"],@"",restart1[@"RestartType"]];
             
             // Execute the query.
             
             //NSString *query_del = @"delete from TempTable";
             [dbManager executeQuery:query_insert1];
             
             // If the query was successfully executed then pop the view controller.
             if (dbManager.affectedRows != 0) {
             BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
             //c_flag=1;
             }
             else{
             BFLog(@"Could not execute the query");
             }
             NSString *query_insert2 = [NSString stringWithFormat:@"insert into AccountSettings values(null, '%@', '%@', '%@','%@','%@')",[NSString stringWithFormat:@"%@ %@",driver1[@"FirstName"],driver1[@"LastName"]],driver1[@"LicenceNumber"],driver1[@"Phone"],driver1[@"Email"],@""];
             
             // Execute the query.
             
             //NSString *query_del = @"delete from TempTable";
             [dbManager executeQuery:query_insert2];
             
             // If the query was successfully executed then pop the view controller.
             if (dbManager.affectedRows != 0) {
             BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
             //c_flag=1;
             }
             else{
             BFLog(@"Could not execute the query");
             }
             
             NSString *query_insert3 = [NSString stringWithFormat:@"insert into Carrier_Sett values(null, '%@', '%@', '%@')", carrier1[@"Carrier"],[NSString stringWithFormat:@"%@,%@,%@", carrier1[@"offAddressline1"],carrier1[@"offAddressline2"],carrier1[@"offAddressline3"]],[NSString stringWithFormat:@"%@,%@,%@",carrier1[@"homeAddressline1"],carrier1[@"homeAddressline2"],carrier1[@"homeAddressline3"]] ];
             
             // Execute the query.
             
             //NSString *query_del = @"delete from TempTable";
             [dbManager executeQuery:query_insert3];
             
             // If the query was successfully executed then pop the view controller.
             if (dbManager.affectedRows != 0) {
             BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
             //c_flag=1;
             }
             else{
             BFLog(@"Could not execute the query");
             }
             }];
             [dataTask resume];
             });*/
            
            
            
           /*
            dispatch_barrier_sync(queue, ^{
                //  dispatch_group_notify(group, queue, ^{
                 BFLog(@"settings info loaded");
                 BFLog(@"cargo:%@",cargo1);
                 BFLog(@"carrier1:%@",carrier1);
                 
                 BFLog(@"cycle1:%@",cycle1);
                 BFLog(@"restart1:%@",restart1);
                 BFLog(@"breakdetails1:%@",breakdetails1);
                 BFLog(@"driver1:%@",driver1);
                 BFLog(@"logs1:%@",logs1);
                
                
                
                
                
                BFLog(@"loading to db finished....");
                decision=1;
                BFLog(@"Decision1--------%d\n",decision);
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[MBProgressHUD hideHUDForView:self.view animated:YES];
                    
                    
                    //[hud hideAnimated:YES];
                });
                //[self movetoscreen];
            });*/
            
      });
    
     dispatch_barrier_sync(queue, ^{
         [self moveToMenu];
         
     });
                
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSString *)getLastStatus:(NSString *)dataString{
    
                               NSData *data2= [dataString dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
                NSArray *dict = [json_log1 objectForKey:@"payload"];
                int json_size1 = (int)[dict count];
                int event_no1 =(int)[[dict[json_size1-1] objectForKey:@"posy"] integerValue];
                
                NSString * event1;
                if (event_no1 == 245) {
                    event1=@"ON";
                }
                else if(event_no1 == 125){
                    event1=@"SB";
                    
                }else if (event_no1 == 185){
                    event1 = @"D";
                }else if (event_no1 == 65){
                    event1 = @"OFF";
                }
                
                BFLog(@"Last stored status:%@",event1);
    return event1;
            }


-(void)rotateLayerInfinite:(CALayer *)layer
{
     //dispatch_async(dispatch_get_main_queue(), ^{
    CABasicAnimation *rotation;
    rotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    rotation.fromValue = [NSNumber numberWithFloat:0];
    rotation.toValue = [NSNumber numberWithFloat:(2 * M_PI)];
    rotation.duration = 5.0f; // Speed
    rotation.repeatCount = HUGE_VALF; // Repeat forever. Can be a finite number.
    [layer removeAllAnimations];
    [layer addAnimation:rotation forKey:@"Spin"];
   //  });
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
