//
//  insertDutyCycle.h
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/19/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "AppDelegate.h"
#import "insertView.h"
#import "DBManager1.h"
#import "logScreenViewController.h"
#import "previousDayViewController.h"
#import "menuScreenViewController.h"
#import "graphDataOperations.h"
#import "annotation.h"
#import "loggraph.h"
#import "DBManager.h"
#import "logsmenuscreen.h"
#import "commonDataHandler.h"
extern NSInteger autoLoginCount;

@interface insertDutyCycle : UIViewController<UITextFieldDelegate,CLLocationManagerDelegate,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{
    CLLocationManager *locationManager;


    
    UIDatePicker *startPicker;
    UIDatePicker *endPicker;
    UIButton *dButton1;
    NSString *noteStr;
    NSString *locStr;
}
@property (nonatomic, strong) DBManager1 *dbManager;
@property (weak, nonatomic) IBOutlet UITableView *contentTable;


- (IBAction)Back:(id)sender;
-(BOOL)autoLogin;
-(void)moveToLogin;

- (IBAction)saveBtn:(id)sender;
@end
