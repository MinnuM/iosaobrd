//
//  LogSettings.m
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/12/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "LogSettings.h"
#import "HOSRecap.h"
#import <CoreData/CoreData.h>
#import "KeychainItemWrapper.h"
#import "insertDutyCycle.h"

NSString *previousCycle=@"";
int numCycles2;
int flagSelection=0;
float break_tym2=0.0;
float driving_hr2=0.0;
float duty_hr2=0.0;
float duty_window2=0.0;
float restart_hr2;
float brk_hr2=0.0;
float cycle_tym2=0.0;
float cycle_tot2=0.0;
float offduty2=0.0;
float onduty2=0.0;
float driving2=0.0;
float driving_cons2=0.0;
NSArray *previousdates_2;
int numCycleStop2=0;
BOOL restart_found2=false;
BOOL duty_flag2=false;
BOOL break_flag2=false;
BOOL break_302=false;
BOOL break_long2=false;
BOOL driving_flag2=false;
BOOL finished2=false;
NSMutableArray *violationInfo2;
NSMutableArray *messages2;
NSMutableArray *submessages2;
@interface LogSettings ()

@end

@implementation LogSettings

@synthesize homeLabel,cycleLabel,odometerLabel,restartLabel,cargoLabel,restBreakLabel;
- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

@synthesize dbManager;
@synthesize scroller06;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addRequired:homeLabel];
    [self addRequired:cycleLabel];
    [self addRequired:odometerLabel];
    [self addRequired:restartLabel];
    [self addRequired:cargoLabel];
    [self addRequired:restBreakLabel];
    _cycle.delegate=self;
    _timeZone.delegate=self;
    _cargoType.delegate=self;
    _odometer.delegate=self;
    _restart.delegate=self;
    _restBreak.delegate=self;
      _restart.text=@"34";
    UITapGestureRecognizer *backgroundTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backgroundTap)];
    [self.view addGestureRecognizer:backgroundTapped];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    //minnu
    dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
    NSString *query0 = [NSString stringWithFormat:@"select * from LogSettings;"];
    
    
    NSArray *logs_Data = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    BFLog(@"Result:%@",logs_Data);
    NSInteger size = [logs_Data count]-1;
   // if ([logs_Data count]!=0) {
        NSString * odounit=_logSettingsData.odounit;
        if(![odounit isEqualToString:@"(null)"] && ![odounit isEqualToString:@""] && odounit.length!=0){
        if ([odounit isEqualToString:@"miles"]) {
            _odometer.text=@"miles";
            
        }else if ([odounit isEqualToString:@"kilometer"]){
            _odometer.text=@"kilometer";
        }
            
        }
        NSString *cargotype = _logSettingsData.cargotype;//logs_Data[size][3];
        if(![cargotype isEqualToString:@"(null)"] && ![cargotype isEqualToString:@""] && cargotype.length!=0){
        if ([cargotype isEqualToString:@"PropertyCarrying"]) {
            _cargoType.text=@"Property";
            cargoinfo=@"Property";
            
        }else if ([cargotype isEqualToString:@"PassengerCarrying"]){
            _cargoType.text=@"Passenger";
            cargoinfo=@"Passenger";
            
        }else if ([cargotype isEqualToString:@"OilandGas"]){
            _cargoType.text=@"Oil and Gas";
            cargoinfo=@"Oil and Gas";
        }
        }else{
            _cargoType.text=@"";
            cargoinfo=@"";
            
        }
        NSString *cycletype =_logSettingsData.cycle;//
        cycletype=[cycletype stringByReplacingOccurrencesOfString:@" " withString:@""];
         if(![cycletype isEqualToString:@"(null)"] && ![cycletype isEqualToString:@""] && cycletype.length!=0){
        if([cycletype isEqualToString:@"USA70hours/8days"]){
            
            _cycle.text=@"USA 70 hours/8 days";
            cycleinfo=@"USA 70 hours/8 days";
            previousCycle = @"USA 70 hours/8 days";
            
        }else if([cycletype isEqualToString:@"USA60hours/7days"]){
            _cycle.text=@"USA 60 hours/7 days";
            cycleinfo=@"USA 60 hours/7 days";
            previousCycle = @"USA 60 hours/7 days";

            
            
        }else if([cycletype isEqualToString:@"Texas70hours/7days"]){
            _cycle.text=@"Texas 70 hours/7 days";
            cycleinfo=@"Texas 70 hours/7 days";
            previousCycle = @"Texas 70 hours/7 days";

            
            
        }else if([cycletype isEqualToString:@"California80hours/8days"]){
            _cycle.text=@"California 80 hours/8 days";
            cycleinfo=@"California 80 hours/8 days";
            previousCycle = @"California 80 hours/8 days";
            
            
        }else if([cycletype isEqualToString:@"Others"]){
            _cycle.text=@"Others";
            cycleinfo=@"Others";
            
            
        }
         }else{
             _cycle.text=@"";
             cycleinfo=@"";
             previousCycle = @"";
             
             
         }
          if(![_logSettingsData.timezone isEqualToString:@"(null)"] && ![_logSettingsData.timezone isEqualToString:@""]){
              if([_logSettingsData.timezone isEqualToString:@"EDT"])
                  _timeZone.text=@"Eastern Daylight Time";
              else if([_logSettingsData.timezone isEqualToString:@"EST"])
                  _timeZone.text=@"Eastern Standard Time";
              else if([_logSettingsData.timezone isEqualToString:@"MDT"])
                  _timeZone.text=@"Mountain Daylight Time";
              else if([_logSettingsData.timezone isEqualToString:@"MST"])
                  _timeZone.text=@"Mountain Standard Time";
              else if([_logSettingsData.timezone isEqualToString:@"CDT"])
                  _timeZone.text=@"Central Daylight Time";
              else if([_logSettingsData.timezone isEqualToString:@"CST"])
                  _timeZone.text=@"Central Standard Time";
              else if([_logSettingsData.timezone isEqualToString:@"PDT"])
                  _timeZone.text=@"Pacific Daylight Time";
              else if([_logSettingsData.timezone isEqualToString:@"PST"])
                  _timeZone.text=@"Pacific Standard Time";
                  
          }else{
              
              _timeZone.text=@"";
          }
        //_cycle.text=logs_Data[0][2];
        // _cargoType.text=logs_Data[0][3];
       /* if(![logs_Data[size][4] isEqualToString:@"(null)"] && ![logs_Data[size][4] isEqualToString:@""]){
        _odometer.text=logs_Data[size][4];
        }else{
            
            _odometer.text=@"";
        }*/
        if (![_logSettingsData.restart isEqualToString:@""] && ![_logSettingsData.restart isEqualToString:@"(null)"]) {
            _restart.text=_logSettingsData.restart;//logs_Data[size][5];
            
            
        }else{
            _restart.text=_logSettingsData.restart;
        
        }
        if (![_logSettingsData.breakhour isEqualToString:@""] && ![_logSettingsData.breakhour isEqualToString:@"(null)"]) {
            _restBreak.text=_logSettingsData.breakhour;//logs_Data[size][6];
            
            
        }else{
            _restBreak.text=@"";
            
        }

        
   // }
    
    //  _timeZone.text=logs[@"Timezone"];
    // _restart.text=restart[@"RestartType"];
    _timeZone.delegate=self;
    timezonePicker=[[UIPickerView alloc] init];
    
    //[event_picker addTarget:self action:@selector(ShowSelectedDate1) forControlEvents:UIControlEventValueChanged];
    _timeZone.inputView = timezonePicker;
    // _Enddate.inputView = datepicker;
    
    UIToolbar *toolBar1=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
    [toolBar1 setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn1=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePressed1)];
    UIBarButtonItem *spac1=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar1 setItems:[NSArray arrayWithObjects:spac1, doneBtn1,nil]];
    [self.timeZone setInputAccessoryView:toolBar1];
    
    _timeZones = [[NSArray alloc] initWithObjects:@"Pacific Standard Time",@"Pacific Daylight Time",@"Mountain Standard Time",@"Mountain Daylight Time", @"Central Standard Time",@"Central Daylight Time",@"Eastern Standard Time",@"Eastern Daylight time", nil];
    // CatPicker.hidden = NO;
    timezonePicker.delegate= self;
    timezonePicker.dataSource=self;
    [timezonePicker removeFromSuperview];
    //[ setInputView:event_picker];
    [timezonePicker reloadAllComponents];
    // BFLog(@"testing--------------------->%d",opt);
    [timezonePicker selectRow:0 inComponent:0 animated:YES];
    
    
    _cycle.delegate=self;
    cyclePicker=[[UIPickerView alloc] init];
    
    //[event_picker addTarget:self action:@selector(ShowSelectedDate1) forControlEvents:UIControlEventValueChanged];
    _cycle.inputView = cyclePicker;
    // _Enddate.inputView = datepicker;
    
    UIToolbar *toolBar2=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
    [toolBar2 setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn2=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePressed2)];
    UIBarButtonItem *spac2=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar2 setItems:[NSArray arrayWithObjects:spac2, doneBtn2,nil]];
    [self.cycle setInputAccessoryView:toolBar2];
    
    _cycles = [[NSArray alloc] initWithObjects:@"USA 70 hours/8 days",@"USA 60 hours/7 days",@"California 80 hours/8 days",@"Texas 70 hours/7 days",@"Other",nil];
    
    // CatPicker.hidden = NO;
    cyclePicker.delegate= self;
    cyclePicker.dataSource=self;
    [cyclePicker removeFromSuperview];
    //[ setInputView:event_picker];
    [cyclePicker reloadAllComponents];
    // BFLog(@"testing--------------------->%d",opt);
    [cyclePicker selectRow:0 inComponent:0 animated:YES];
    
    //-------------------------------------------------------------------
    _restart.delegate=self;
    restartPicker=[[UIPickerView alloc] init];
    
    //[event_picker addTarget:self action:@selector(ShowSelectedDate1) forControlEvents:UIControlEventValueChanged];
    _restart.inputView = restartPicker;
    // _Enddate.inputView = datepicker;
    
    UIToolbar *toolBar5=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
    [toolBar5 setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn5=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePressed5)];
    UIBarButtonItem *spac5=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar5 setItems:[NSArray arrayWithObjects:spac5, doneBtn5,nil]];
    [self.restart setInputAccessoryView:toolBar5];
    
    _restartvalues = [[NSArray alloc] initWithObjects:@"34",@"24",nil];
    
    // CatPicker.hidden = NO;
    restartPicker.delegate= self;
    restartPicker.dataSource=self;
    [restartPicker removeFromSuperview];
    //[ setInputView:event_picker];
    [restartPicker reloadAllComponents];
    // BFLog(@"testing--------------------->%d",opt);
    [restartPicker selectRow:0 inComponent:0 animated:YES];
    
    //-------------------------------------------------------------------
    
    _cargoType.delegate=self;
    cargoPicker=[[UIPickerView alloc] init];
    
    //[event_picker addTarget:self action:@selector(ShowSelectedDate1) forControlEvents:UIControlEventValueChanged];
    _cargoType.inputView = cargoPicker;
    // _Enddate.inputView = datepicker;
    
    UIToolbar *toolBar3=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
    [toolBar3 setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn3=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePressed3)];
    UIBarButtonItem *spac3=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar3 setItems:[NSArray arrayWithObjects:spac3, doneBtn3,nil]];
    [self.cargoType setInputAccessoryView:toolBar3];
    
    _cargotypes = [[NSArray alloc] initWithObjects:@"Property",@"Passenger",@"Oil and Gas",nil];
    // CatPicker.hidden = NO;
    cargoPicker.delegate= self;
    cargoPicker.dataSource=self;
    [cargoPicker removeFromSuperview];
    //[ setInputView:event_picker];
    [cargoPicker reloadAllComponents];
    // BFLog(@"testing--------------------->%d",opt);
    [cargoPicker selectRow:0 inComponent:0 animated:YES];
    
    //-------------------------------------------------------------------
    
    
    _odometer.delegate=self;
    odometerPicker=[[UIPickerView alloc] init];
    
    //[event_picker addTarget:self action:@selector(ShowSelectedDate1) forControlEvents:UIControlEventValueChanged];
    _odometer.inputView = odometerPicker;
    // _Enddate.inputView = datepicker;
    
    UIToolbar *toolBar4=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
    [toolBar4 setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn4=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePressed4)];
    UIBarButtonItem *spac4=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar4 setItems:[NSArray arrayWithObjects:spac4, doneBtn4,nil]];
    [self.odometer  setInputAccessoryView:toolBar4];
    
    _odometervalues = [[NSArray alloc] initWithObjects:@"miles",@"kilometer",nil];
    // CatPicker.hidden = NO;
    odometerPicker.delegate= self;
    odometerPicker.dataSource=self;
    [odometerPicker removeFromSuperview];
    //[ setInputView:event_picker];
    [odometerPicker reloadAllComponents];
    // BFLog(@"testing--------------------->%d",opt);
    [odometerPicker selectRow:0 inComponent:0 animated:YES];
    
    
    //--------------------------------------------------------------------------
    
    
    /*  _logIncrement.delegate=self;
     logincremenrPicker=[[UIPickerView alloc] init];
     
     //[event_picker addTarget:self action:@selector(ShowSelectedDate1) forControlEvents:UIControlEventValueChanged];
     _logIncrement.inputView = logincremenrPicker;
     // _Enddate.inputView = datepicker;
     
     UIToolbar *toolBar5=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
     [toolBar5 setTintColor:[UIColor blackColor]];
     UIBarButtonItem *doneBtn5=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePressed5)];
     UIBarButtonItem *spac5=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
     [toolBar5 setItems:[NSArray arrayWithObjects:spac5, doneBtn5,nil]];
     [self.logIncrement  setInputAccessoryView:toolBar5];
     
     _locincvalues = [[NSArray alloc] initWithObjects:@"15 Minutes",@"1 Minutes",nil];
     // CatPicker.hidden = NO;
     logincremenrPicker.delegate= self;
     logincremenrPicker.dataSource=self;
     [logincremenrPicker removeFromSuperview];
     //[ setInputView:event_picker];
     [logincremenrPicker reloadAllComponents];
     // BFLog(@"testing--------------------->%d",opt);
     [logincremenrPicker selectRow:0 inComponent:0 animated:YES];*/
    
    
    
    
    
    
    
    
    
    
    
    // Do any additional setup after loading the view.
}
-(void)addRequired:(UILabel*)label{
    
    NSMutableAttributedString *text =
    [[NSMutableAttributedString alloc]
     initWithAttributedString: label.attributedText];
    
    [text addAttribute:NSForegroundColorAttributeName
                 value:[UIColor redColor]
                 range:NSMakeRange([label.text length]-1, 1)];
    [label setAttributedText: text];
    
    
    
}

//minnu
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scroller06.contentInset = contentInsets;
    scroller06.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect textfieldFrame;
    CGRect aRect = self.view.frame;
    //aRect.size.height -= (30+kbSize.height);
    //BFLog(@"hei"arect.size.height);
    if([self.timeZone isFirstResponder])
    {
        textfieldFrame=[_timeZone frame ];
    }
    else if([self.cycle isFirstResponder])
    {
        textfieldFrame=[_cycle frame ];
    }
    else if([self.cargoType isFirstResponder])
    {
        textfieldFrame=[_cargoType frame];
    }
    else if([self.odometer isFirstResponder])
    {
        textfieldFrame=[_odometer frame];
    }
    else
    {
        textfieldFrame=[_restart frame];
    }
    BFLog(@"General-scrollpoint--(%f,%f)",textfieldFrame.origin.x,textfieldFrame.origin.y);
    if (aRect.size.height < (textfieldFrame.origin.y+15)) {
        CGPoint scrollPoint = CGPointMake(0.0, textfieldFrame.origin.y);
        //BFLog(@"scrollpoint--%f",textfieldFrame.origin.y-kbSize.height);
        [scroller06 setContentOffset:scrollPoint animated:YES];
    }}


// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scroller06.contentInset = contentInsets;
    scroller06.scrollIndicatorInsets = contentInsets;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    BFLog(@"entered return key");
    return YES;
    
}

- (void)backgroundTap {
    [self.view endEditing:YES];
    BFLog(@"Touched Inside");}

//-(void)textFieldEndBeginEditing:(UITextField *)textField{
//}

//minnu

- (void)donePressed1{
    
  //  _timeZone.text=@"Pacific Standard Time";
    [self.timeZone resignFirstResponder];

}
- (void)donePressed2{
    /*_cycle.text=@"USA 70 hours/8 days";
    cycleinfo=@"USA 70 hours/8 days";
    previousCycle = @"USA 70 hours/8 days";*/
    [self.cycle resignFirstResponder];

}

- (void)donePressed3{
   /* _cargoType.text=@"Property";
    cargoinfo=@"Property";*/
    [self.cargoType resignFirstResponder];
    
}
- (void)donePressed4{
   /* _odometer.text=@"miles";*/
    [self.odometer resignFirstResponder];
    
}
- (void)donePressed5{
  /*  _restart.text=@"34";*/
    [self.restart resignFirstResponder];
    
}
/*- (void)donePressed5{
 
 
 [self.logIncrement resignFirstResponder];
 
 }*/
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
    
    
}


//  returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    int row=0;
    if ([pickerView isEqual:timezonePicker]) {
        row = (int)[_timeZones count];
        
        
    }
    else if([pickerView isEqual:cyclePicker]){
        
        row =(int)[_cycles count];
        
    }
    else if([pickerView isEqual:cargoPicker]){
        
        row =(int) [_cargotypes count];
        
    }
    else if([pickerView isEqual:odometerPicker]){
        
        row =(int) [_odometervalues count];
        
    }else if([pickerView isEqual:restartPicker]){
        
        row =(int) [_restartvalues count];
        
    }
    return  row;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSString *selection=@"";
    if ([pickerView isEqual:timezonePicker]) {
        selection = [_timeZones objectAtIndex:row];
    }
    else if ([pickerView isEqual:cyclePicker]){
        selection = [_cycles objectAtIndex:row];
    }
    else if ([pickerView isEqual:cargoPicker]){
        selection = [_cargotypes objectAtIndex:row];
    }
    else if ([pickerView isEqual:odometerPicker]){
        selection = [_odometervalues objectAtIndex:row];
    }
    else if ([pickerView isEqual:restartPicker]){
        selection = [_restartvalues objectAtIndex:row];
    }
    return selection;
    
    
}


-(float)calculatePosx:(NSString *)time{
    NSArray *parts=[time componentsSeparatedByString:@":"];
    float x1=4*[parts[0] integerValue]+([parts[1] integerValue]/15);
    float posx=x1*12.25+40;
    BFLog(@"from function:%f",posx);
    return posx;
    
}


-(NSString*)calculateTime:(float)posx{
    
    int end=(posx-40)/12.25;
    int x1=end/4;
    int x2=end%4;
    NSString *decimalpart,*time;
    if (x2*15==0) {
        decimalpart=@"00";
    }else{
        
        decimalpart=[NSString stringWithFormat:@"%d",x2*15];
    }
    
    if (x1<10) {
        time=[NSString stringWithFormat:@"0%d:%@",x1,decimalpart];
    }else{
        
        time=[NSString stringWithFormat:@"%d:%@",x1,decimalpart];
    }
    
    return time;
    
}

-(NSMutableArray*)findNewViolation:(NSInteger)cycleStart data:(NSArray*)dataArray cycle:(NSString*)cycle{
    messages2 = [[NSMutableArray alloc]init];
    submessages2  = [[NSMutableArray alloc]init];
    violationInfo2 = [[NSMutableArray alloc]init];
    cycle_tot2 =0.0;
    if([cargoinfo isEqualToString:@"Passenger"]){
        driving_hr2=530.00;//posx of 10:00
        duty_window2=775.00;//posx of 15:00
        break_tym2=[self calculatePosx:@"08:00"];
        
        
    }else{
        driving_hr2=579.00;//posx of 11:00
        duty_window2=726.00;//posx of 14:00
        break_tym2=[self calculatePosx:@"10:00"];
        
        
    }

    for(NSInteger i=cycleStart;i<[dataArray count];i++){
        
        NSString *dbresult = dataArray[i][1];
        NSString *date=dataArray[i][0];
        NSData *data2= [dbresult dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
        
        NSMutableArray *dict = [[json_log1 objectForKey:@"payload"] mutableCopy];
        
        NSDate * now_sta = [NSDate date];
        NSDateFormatter *outputFormatter_sta1 = [[NSDateFormatter alloc] init];
        [outputFormatter_sta1 setDateFormat:@"MM/dd/yyyy"];
        NSString *currentDate = [outputFormatter_sta1 stringFromDate:now_sta];
        int dicSize =(int)[dict count];
        if(![currentDate isEqualToString:date]){
            
            
            
            if([dict count]%2!=0){
                
                NSMutableDictionary *dic_new = [[NSMutableDictionary alloc] init];
                [dic_new setValue:[NSString stringWithFormat:@"%d",dicSize] forKey:@"id"];
                [dic_new setValue:@"1216" forKey:@"posx"];
                [dic_new setValue:dict[[dict count]-1][@"posy"] forKey:@"posy"];
                [dict addObject:dic_new];
                
            }else{
                
                if([dict[[dict count]-1][@"posy"] floatValue]!=1216){
                [dict removeLastObject];
                NSMutableDictionary *dic_new = [[NSMutableDictionary alloc] init];
                [dic_new setValue:[NSString stringWithFormat:@"%d",dicSize] forKey:@"id"];
                [dic_new setValue:@"1216" forKey:@"posx"];
                [dic_new setValue:dict[[dict count]-1][@"posy"] forKey:@"posy"];
                [dict addObject:dic_new];

                }
                
                
            }
        }else{
            
            NSDate * now_sta = [NSDate date];
            NSDateFormatter *outputFormatter_sta1 = [[NSDateFormatter alloc] init];
            [outputFormatter_sta1 setDateFormat:@"HH:mm"];
            NSString *currentDate = [outputFormatter_sta1 stringFromDate:now_sta];
            if([dict count]%2!=0){
                NSMutableDictionary *dic_new = [[NSMutableDictionary alloc] init];
                [dic_new setValue:[NSString stringWithFormat:@"%d",dicSize] forKey:@"id"];
                [dic_new setValue:[NSString stringWithFormat:@"%f",[self calculatePosx:currentDate]] forKey:@"posx"];
                [dic_new setValue:dict[[dict count]-1][@"posy"] forKey:@"posy"];
                [dict addObject:dic_new];
            }
            
            
        }//data cleansing
        
        //check for violations
        
        int json_size = (int)[dict count];
        int j=0;
        for (int k=0; k<json_size; k=k+2) {
            
            j=k;
            int event_no =(int)[[dict[j] objectForKey:@"posy"] integerValue];
            float posx1=[[dict[j] objectForKey:@"posx"] floatValue];
            float posx2=[[dict[j+1] objectForKey:@"posx"] floatValue];
            float duration=posx2-posx1;
            
            if(event_no==65 || event_no==125){//break time
                 driving_cons2=0.0;
                
                if (event_no==65) {
                    
                    
                    offduty2=offduty2+duration;
                    brk_hr2=brk_hr2+duration;
                 }
                if(event_no==125){
                       brk_hr2=brk_hr2+duration;
                      }
            }else if (event_no==185 || event_no==245){//duty events
                
                
                if (event_no==245) {
                    onduty2=onduty2+duration;
                    duty_hr2=duty_hr2+duration;
                    driving_cons2=0.0;
                    driving_flag2=false;
                    cycle_tot2=cycle_tot2+duration;
                    
                    
                }
                if (event_no==185) {
                    driving2=driving2+duration;
                    duty_hr2=duty_hr2+duration;
                    driving_cons2=driving_cons2+duration;
                    driving_flag2=true;
                    cycle_tot2=cycle_tot2+duration;
                }
                
                

                
            }

            
        }//end of for
        
        
        if(duty_hr2>duty_window2){
            if (duty_window2==726.00) {
                
                // BFLog(@"14 hour Rule violation");
                NSString *violation = [NSString stringWithFormat:@"14hr duty rule violation on %@",date];
                if (![messages2 containsObject:violation]) {
                    [messages2 addObject:violation];
                    NSString *sub=@"Maximum 14 consecutive duty hours allowed!!!!";
                    [submessages2 addObject:sub];
                    NSArray *sam=@[date,@"14hour"];
                    [violationInfo2 addObject:sam];
                }
                
            }else{
                
                // BFLog(@"15 hour Rule violation");
                NSString *violation = [NSString stringWithFormat:@"15hr duty rule violation on %@",date];
                if (![messages2 containsObject:violation]) {
                    [messages2 addObject:violation];
                    NSString *sub=@"Maximum 15 consecutive duty hours allowed!!!!";
                    [submessages2 addObject:sub];
                    NSArray *sam=@[date,@"15hour"];
                    [violationInfo2 addObject:sam];
                }
                
                
            }
        }
        //------------------------------------------
        BFLog(@"%f",[self calculatePosx:@"08:00"]);
        if (driving_cons2>[self calculatePosx:@"08:00"]) {
            // BFLog(@"Maximum 8 hour driving is allowed");
            NSString *violation = [NSString stringWithFormat:@"8-hour driving rule violation  on %@",date];
            if (![messages2 containsObject:violation]) {
                NSString *sub=@"30minutes break required!!!!";
                [messages2 addObject:violation];
                [submessages2 addObject:sub];
                NSArray *sam=@[date,@"8hour"];
                [violationInfo2 addObject:sam];
            }
            
        }
        
        if (driving2>driving_hr2) {
            
            if (driving_hr2==579.00) {
                // BFLog(@"11 hour driving rule");
                NSString *violation = [NSString stringWithFormat:@"11-hour driving rule violation  on %@",date];
                if (![messages2 containsObject:violation]) {
                    NSString *sub=@"Maximum 11 hours driving allowed in 14 hours duty cycle!!!!";
                    [submessages2 addObject:sub];
                    
                    [messages2 addObject:violation];
                    NSArray *sam=@[date,@"11hour"];
                    [violationInfo2 addObject:sam];
                }
            }else{
                // BFLog(@"10 hour driving rule");
                NSString *violation = [NSString stringWithFormat:@"10-hour driving rule violation  on %@",date];
                if (![messages2 containsObject:violation]) {
                    NSString *sub=@"Maximum 10 hours driving allowed in 14 hours duty cycle!!!!";
                    [submessages2 addObject:sub];
                    
                    [messages2 addObject:violation];
                    NSArray *sam=@[date,@"10hour"];
                    [violationInfo2 addObject:sam];
                    
                }
                
            }
            
        }

        

        
        
        
        
        
        
        
    }//for loop end
    float cycle_tym1 ;
    NSInteger numCycles1=7;
    if([cycle isEqualToString:@"USA 70 hours/8 days"]||[cycleinfo isEqualToString:@"California 80 hours/8 days"]){
        numCycles1=8;
        cycle_tym1=3470.00;//posx value of 70hr
        
    }else{
        numCycles1=7;
        cycle_tym1=2980.00;//posx value for 60hr
    }
    if(cycle_tot2>cycle_tym1){
        
        //  BFLog(@"Total maximum duty hour for the cycle %@ is",cycleinfo);
        NSString *violation = [NSString stringWithFormat:@"34hr mandatory restart required"];
        if (![messages2 containsObject:violation]) {
            [messages2 addObject:violation];
            NSString *sub=@"Need to take 34hr or more off duty to restart the cycle!!!!";
            [submessages2 addObject:sub];
            NSArray *sam=@[@"",@"34hour"];
            [violationInfo2 addObject:sam];
        }
        
    }
    NSInteger numofWorkingDays = [dataArray count]-cycleStart;
    if(numofWorkingDays>numCycles1){
        
        
        NSString *violation = [NSString stringWithFormat:@"34hr mandatory restart required"];
        if (![messages2 containsObject:violation]) {
            [messages2 addObject:violation];
            NSString *sub=@"Need to take 34hr or more off duty to restart the cycle!!!!";
            [submessages2 addObject:sub];
            NSArray *sam=@[@"",@"34hour"];
            [violationInfo2 addObject:sam];
        }
        
        
        
        
    }

    
    
    BFLog(@"%@",violationInfo2);
    return violationInfo2;
    
    
}

-(void)checkForNewViolation :(NSString*)newCycle{
    
    
    if(![newCycle isEqualToString:previousCycle]){
        
       
        NSInteger numCycles1;
        float cycle_tym1;
        if([cycleinfo isEqualToString:@"USA 70 hours/8 days"]||[cycleinfo isEqualToString:@"California 80 hours/8 days"]){
            numCycles1=8;
            cycle_tym1=3470.00;//posx value of 70hr
            
        }else{
            numCycles1=7;
            cycle_tym1=2980.00;//posx value for 60hr
        }
        
        dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
        
        
        //NSDate * now1 = [NSDate date];
        
        NSString *query0 = [NSString stringWithFormat:@"select date,data,flag from TempTable order by dateInSecs desc limit 15;"];
        
        // Get the results.
        
        NSArray *previousdates1 = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
        NSArray *previousdates=[[previousdates1 reverseObjectEnumerator] allObjects];
        // BFLog(@"Result:%@",previousdates);
        HOSRecap *obj  = [HOSRecap new];
        NSInteger cycleRestart = [obj calculateRestart:previousdates];
        if(cycleRestart != [previousdates count]-1){
            NSMutableArray *newViolations = [[NSMutableArray alloc] init];
            newViolations = [self findNewViolation:cycleRestart data:previousdates cycle:newCycle];
        
            NSMutableArray *existingViolations = [self findNewViolation:cycleRestart data:previousdates cycle:previousCycle];
        
        
        
        
        
        
        
        
        
        
        
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"Do you want to change the Cycle?!"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        
       // [alert1 show];
        }
    }
    
    
    
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    //  opt = opt +1;
    NSInteger row1=0;
    NSString *str1=@"";
    
    if ([pickerView isEqual:timezonePicker]) {
        row1=[timezonePicker selectedRowInComponent:0];
        str1 = [_timeZones objectAtIndex:row1];
        _timeZone.text =str1;
        
    }
    else if ([pickerView isEqual:cyclePicker]){
        
        row1=[cyclePicker selectedRowInComponent:0];
        str1 = [_cycles objectAtIndex:row1];
        _cycle.text =str1;
       // [self checkForNewViolation:str1];
        
        
            
            
            
        
        
    }else if ([pickerView isEqual:cargoPicker]){
        
        row1=[cargoPicker selectedRowInComponent:0];
        str1 = [_cargotypes objectAtIndex:row1];
        _cargoType.text =str1;
        
    }
    else if ([pickerView isEqual:odometerPicker]){
        
        row1=[odometerPicker selectedRowInComponent:0];
        str1 = [_odometervalues objectAtIndex:row1];
        _odometer.text =str1;
        
    }
    else if ([pickerView isEqual:restartPicker]){
        
        row1=[restartPicker selectedRowInComponent:0];
        str1 = [_restartvalues objectAtIndex:row1];
        _restart.text =str1;
        
    }
    BFLog(@"selected%@",str1);
    //BFLog(@"testing2-------->%d",opt);
    
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


- (IBAction)save:(id)sender {
    autoLoginCount=0;
    if([_cargoType.text  isEqual: @""] || [_cycle.text  isEqual:@""] || [_odometer.text isEqual:@""] || [_restart.text isEqual:@""] || [_cargoType.text isEqual:@""]||[_restBreak.text isEqualToString:@""]){
        
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"Required fields cannot be empty!"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        
        [alert1 show];
        
        
    }else{
    
    //minnu
        rules *objRule=[rules new];
        User *objUser=[User new];
        DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
        NSDictionary *token;
        NSMutableDictionary *whereToken;
        NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
        if([userInfoArray count]!=0){
            int userId = [userInfoArray[0][0] intValue];
            token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
            ;
            whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
      NSArray *values1 = [[NSArray alloc] initWithArray:[objDB selectFromTable:objUser whereToken:whereToken]];
            objRule.userid=[NSString stringWithFormat:@"%d",userId];
            objRule.restart=_restart.text;
            objRule.breakhour=_restBreak.text;
            if([ _cycle.text isEqualToString:@"USA 70 hours/8 days"]){
                
                objRule.cycle=@"1";
                
            }else if([ _cycle.text isEqualToString:@"USA 60 hours/7 days"]){
                
                objRule.cycle=@"2";
                
                
            }else if([_cycle.text isEqualToString:@"Texas 70 hours/7 days"]){
                objRule.cycle=@"3";
                
                
            }else if([ _cycle.text isEqualToString:@"California 80 hours/8 days"]){
                objRule.cycle=@"4";
                
                
            }else {
                objRule.cycle=@"5";
                
                
            }
            if ([ _cargoType.text isEqualToString:@"Property"]) {
                objRule.cargotype=@"2";
            }else if ([ _cargoType.text isEqualToString:@"Passenger"]){
                objRule.cargotype=@"1";
            }else if ([_cargoType.text isEqualToString:@"Oil and Gas"]){
                objRule.cargotype=@"3";
            }
            if ([ _odometer.text isEqualToString:@"kilometer"]) {
                objRule.odounit=@"2";
            }else if ([ _odometer.text isEqualToString:@"miles"]){
                objRule.odounit=@"1";
            }
            if([_timeZone.text isEqualToString:@"Eastern Standard Time"])
                objRule.timezone=@"7";
            else if([_timeZone.text  isEqualToString:@"Eastern Daylight Time"])
                objRule.timezone=@"8";
            else if([_timeZone.text  isEqualToString:@"Mountain Standard Time"])
                objRule.timezone=@"3";
            else if([_timeZone.text  isEqualToString:@"Mountain Daylight Time"])
                objRule.timezone=@"4";
            else if([_timeZone.text  isEqualToString:@"Central Standard Time"])
                objRule.timezone=@"5";
            else if([_timeZone.text  isEqualToString:@"Central Daylight Time"])
                objRule.timezone=@"6";
            else if([_timeZone.text  isEqualToString:@"Pacific Daylight Time"])
                objRule.timezone=@"2";
            else
                objRule.timezone=@"1";
            
    //minnu
  /*  NSString *query2 = [NSString stringWithFormat:@"select * from LogSettings"];
    cargoinfo=_cargoType.text;
    cycleinfo=_cycle.text;
    
   NSString *cargotype=@"";
    if ([ _cargoType.text isEqualToString:@"Property"]) {
       cargotype=@"PropertyCarrying";
    }else if ([ _cargoType.text isEqualToString:@"Passenger"]){
       cargotype=@"PassengerCarrying";
    }else if ([_cargoType.text isEqualToString:@"Oil and Gas"]){
        cargotype=@"OilandGas";
    }
    
    NSString *cycletype = @"";
    if([ _cycle.text isEqualToString:@"USA 70 hours/8 days"]){
        
       cycletype=@"USA70hours";
        
    }else if([ _cycle.text isEqualToString:@"USA 60 hours/7 days"]){
       
        cycletype=@"USA60hours";
        
        
    }else if([_cycle.text isEqualToString:@"Texas 70 hours/7 days"]){
       cycletype =@"Texas70hours";
        
        
    }else if([ _cycle.text isEqualToString:@"California 80 hours/8 days"]){
       cycletype=@"California80hours";
        
        
    }else if([_cycle.text isEqualToString:@"Others"]){
       cycletype=@"Others";
        
        
    }

    NSArray *values1 = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query2]];
    */
    if ([values1 count]!=0) {
        
        
        [objDB updateLogSettings:objRule];
      /*  NSString *query_update = [NSString stringWithFormat:@"UPDATE LogSettings SET timezone='%@',cycle='%@',cargo_type='%@',odometer='%@',restart='%@',restBreak='%@'",_timeZone.text,cycletype,cargotype,_odometer.text,_restart.text,_restBreak.text];
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_update];
        */
        // If the query was successfully executed then pop the view controller.
        if (objDB.affectedRows != 0) {
            BFLog(@"Table updated successfully. Affected rows = %d", objDB.affectedRows);
            UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                             message:@"Data Saved!!!"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
            
            [alert2 show];
        }
        else{
            BFLog(@"Could not execute the query");
        }}
    
    else{
        [objDB insertLogSettings:objRule];
     /*   NSString *query_insert2 = [NSString stringWithFormat:@"insert into LogSettings values(null, '%@', '%@', '%@','%@','%@','%@')",_timeZone.text,cycletype,cargotype,_odometer.text,_restart.text,_restBreak.text];
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_insert2];
       */
        // If the query was successfully executed then pop the view controller.
        if (objDB.affectedRows != 0) {
            BFLog(@"Query was inserted the data successfully. Affected rows = %d", objDB.affectedRows);
            UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                             message:@"Data Saved!!!"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
            
            [alert2 show];
        }
        else{
            BFLog(@"Could not execute the query");
        }
    }
        
    }
    
   /* NSMutableDictionary *driver_data = [[NSMutableDictionary alloc] init];
   // NSString *yy= [_cycle.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    [driver_data setValue:_timeZone.text forKey:@"timezone"];
    [driver_data setValue:cycletype forKey:@"cycle"];
    [driver_data setValue:cargotype forKey:@"cargo"];
    [driver_data setValue:_odometer.text forKey:@"odometer"];
    [driver_data setValue:_restart.text forKey:@"restart"];
     [driver_data setValue:_restBreak.text forKey:@"break"];
    BFLog(@"data for save:%@",driver_data);
        [self saveToServer:driver_data];*/
   /* NSError *error;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
        //getting authCode
        NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
        NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
        NSString *authCode = @"";
        if([authData count]!=0){
            authCode = authData[0][1];
            
        }
        //---------------------------
    NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp1/saveHosLogData.php?masterUser=%@&User=%@&authCode=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"masteruser"],[[NSUserDefaults standardUserDefaults] valueForKey:@"user"],authCode];
    NSURL *url = [NSURL URLWithString:urlString];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"HEAD"];
        NSURLResponse *response;
        // NSError *error;
        NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if(httpResponse!=nil){
            if ([httpResponse statusCode] ==200 ) {
                
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    /* NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
     @"IOS TYPE", @"typemap",
     nil];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:driver_data options:0 error:&error];
    [request setHTTPBody:postData];
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
      //  BFLog(@"error:%@,%@",error,response);
        
    }];
    
    [postDataTask resume];
        
    }else if([httpResponse statusCode]==503){
        
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Service Unavailable"
                                                         message:@"Please check your internet connectivity!!!"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:@"OK", nil];
        
        [alert1 show];
        
    }
    }*/
    }
}

-(void)saveToServer:(NSMutableDictionary *)data_dict{
    
//    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
//    //getting authCode
//    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
//    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
//    NSString *authCode = @"";
//    if([authData count]!=0){
//        authCode = authData[0][1];
//        
//    }
//    //---------------------------
//    NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/saveHosLogData.php?masterUser=%@&User=%@&authCode=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"masteruser"],[[NSUserDefaults standardUserDefaults] valueForKey:@"user"],authCode];
//    // NSString *urlString = [NSString stringWithFormat:@"http://23.239.28.100/mydealership/saveHosGraphData_app.php?masterUser=%@&user=%@&date=%@&approve=0",master_menu,user_menu,current_date];
//    NSURL *url = [NSURL URLWithString:urlString];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    [request setHTTPMethod:@"HEAD"];
//    NSURLResponse *response;
//    NSError *error;
//    NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
//    if(httpResponse!=nil){
//        if ([httpResponse statusCode] ==200 ) {
//            BFLog(@"Device connected to the internet");
//            NSURL *url = [NSURL URLWithString:urlString];
//            
//            //NSError *error;
//            
//            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
//                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                               timeoutInterval:60.0];
//            
//            [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//            [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
//            
//            [request setHTTPMethod:@"POST"];
//            /* NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
//             @"IOS TYPE", @"typemap",
//             nil];*/
//            
//            //NSMutableDictionary *currentStatus=[[NSMutableDictionary alloc]init];
//            //[currentStatus setValue:status forKey:@"Status"];*/
//            /*  NSMutableArray *inner = [[NSMutableArray alloc] initWithArray:@[@"00:00",@"OFF"]];
//             NSMutableDictionary *data_dict = [[NSMutableDictionary alloc] init];
//             [data_dict setObject:inner forKey:@"payload"];*/
//             NSError *error;
//             NSData *postData = [NSJSONSerialization dataWithJSONObject:data_dict options:0 error:&error];
//            [request setHTTPBody:postData];
//            
//            
//            NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                
//                NSDictionary *responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//                
//            }];
//            
//            [postDataTask resume];
//        }else if([httpResponse statusCode]==403){
//            if(autoLoginCount<=1){
//                insertDutyCycle *obj = [insertDutyCycle new];
//                BOOL success =  [obj autoLogin];
//                
//                if(success){
//                    [self saveToServer:data_dict];
//                    
//                }else{
//                    KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
//                    [credentials resetKeychainItem];
//                    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
//                                                                     message:@"Please Login Again"
//                                                                    delegate:self
//                                                           cancelButtonTitle:@"OK"
//                                                           otherButtonTitles:nil, nil];
//                    alert1.tag = 1;
//                    alert1.delegate = self;
//                    
//                    [alert1 show];
//                    
//                }
//            }else{
//                KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
//                [credentials resetKeychainItem];
//                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
//                                                                 message:@"Please Login Again"
//                                                                delegate:self
//                                                       cancelButtonTitle:@"OK"
//                                                       otherButtonTitles:nil, nil];
//                alert1.tag = 2;
//                alert1.delegate = self;
//                
//                [alert1 show];
//                
//                
//                
//            }
//            
//        }else if([httpResponse statusCode]==503){
//            BFLog(@"Device not connected to the internet");
//            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Service Unavailable"
//                                                             message:@"No connectivity with server at this time,it will sync automatically"
//                                                            delegate:self
//                                                   cancelButtonTitle:@"OK"
//                                                   otherButtonTitles:nil, nil];
//            
//            [alert1 show];
//            
//        }
//    }
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag ==1 || alertView.tag ==2){
        
        if(buttonIndex==[alertView cancelButtonIndex]){
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"loginscreen"];
                [self presentViewController:vc animated:YES completion:nil];
            });
            
        }
        
        
    }
    
    
    
}

@end
