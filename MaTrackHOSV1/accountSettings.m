//
//  accountSettings.m
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/15/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "accountSettings.h"
#import "loginLandingViewController.h"
#import "KeychainItemWrapper.h"
#import "insertDutyCycle.h"
#import "NSString+NullCheck.h"


@interface accountSettings ()

@end

@implementation accountSettings
@synthesize dbManager;
@synthesize scroller04;
@synthesize driverIdLabel,mobileLabel,emailLabel,dotLabel,nameLabel,lastnameLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    /*for(id view in [self.view subviews]){
        if([view isKindOfClass:[UILabel class]]){
           
            [self addRequired:view];
            
        }
        
    }*/
    [self addRequired:driverIdLabel];
    [self addRequired:mobileLabel];
    [self addRequired:emailLabel];
    [self addRequired:_lastName];
    //[self addRequired:dotLabel];
    [self addRequired:nameLabel];
    _name.delegate=self;
    _driverid.delegate=self;
    _email.delegate=self;
    _mobile.delegate=self;
    _DOT.delegate=self;
    UITapGestureRecognizer *backgroundTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backgroundTap)];
    [self.view addGestureRecognizer:backgroundTapped];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    if(_accountData.firstname!=[NSNull class] && ![_accountData.firstname isEqualToString:@"<null>"] && [_accountData.firstname length]!=0){
         _name.text=[_accountData.firstname NullCheck];
    }
    else
        _name.text=@"";
    if(_accountData.lastname!=[NSNull class] && ![_accountData.lastname isEqualToString:@"<null>"] && [_accountData.lastname length]!=0){
    lastnameLabel.text=[_accountData.lastname NullCheck];
    }
    else
        lastnameLabel.text=@"";
    if(_accountData.driverid!=[NSNull class] && ![_accountData.driverid isEqualToString:@"<null>"] && [_accountData.driverid length]!=0){
    _driverid.text=[_accountData.driverid NullCheck];
    }
    else
        _driverid.text=@"";
    if(_accountData.mobile!=[NSNull class] && ![_accountData.mobile isEqualToString:@"<null>"] && [_accountData.mobile length]!=0){
    _mobile.text=[_accountData.mobile NullCheck];
    }
    if(_accountData.email!=[NSNull class] && ![_accountData.email isEqualToString:@"<null>"] && [_accountData.email length]!=0){
    _email.text=[_accountData.email NullCheck];
    }
    else
        _email.text=@"";
    if(_accountData.dotNo!=[NSNull class] && ![_accountData.dotNo isEqualToString:@"<null>"] && [_accountData.dotNo length]!=0){
    _DOT.text=[_accountData.dotNo NullCheck];
    }
    else
        _DOT.text=@"";
    
        
    /* Raghee
     dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
    
     NSString *query0 = [NSString stringWithFormat:@"select * from AccountSettings;"];
    
    // Get the results.
    
    NSArray *account_Data = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
   // BFLog(@"Result:%@",account_Data);
    if ([account_Data count]!=0) {
        if([account_Data[0][1] isEqualToString:@"(null)"]){
            _name.text=@"";
            
        }else{
        _name.text=account_Data[0][1];
        }
        if([account_Data[0][2] isEqualToString:@"(null)"]){
           lastnameLabel.text=@"";
        }else{
            lastnameLabel.text=account_Data[0][2];
        }
         if([account_Data[0][3] isEqualToString:@"(null)"]){
             _driverid.text=@"";
         }else{
        _driverid.text=account_Data[0][3];
         }
        if([account_Data[0][4] isEqualToString:@"(null)"]){
            _mobile.text=@"";

        }else{
        _mobile.text=account_Data[0][4];
        }
        if([account_Data[0][5] isEqualToString:@"(null)"]){
            _email.text=@"";
            
        }else{

        _email.text=account_Data[0][5];
        }
                if([account_Data[0][6] isEqualToString:@"(null)"]){
            _DOT.text=@"";
            
        }else{
            
            _DOT.text=account_Data[0][6];
        }
        
        
        
    }else{
        
        _name.text=@"";
        _driverid.text=@"";
        _mobile.text=@"";
        _email.text=@"";
      
        _DOT.text=@"";
        
    }*/
    
    // Do any additional setup after loading the view.
}


-(void)addRequired:(UILabel*)label{
    
    NSMutableAttributedString *text =
    [[NSMutableAttributedString alloc]
     initWithAttributedString: label.attributedText];
    
    [text addAttribute:NSForegroundColorAttributeName
                 value:[UIColor redColor]
                 range:NSMakeRange([label.text length]-1, 1)];
    [label setAttributedText: text];
    
    
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scroller04.contentInset = contentInsets;
    scroller04.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect textfieldFrame;
    CGRect aRect = self.view.frame;
    //aRect.size.height -= (30+kbSize.height);
    //BFLog(@"hei"arect.size.height);
    if([self.name isFirstResponder])
    {
        textfieldFrame=[self.name frame];
    }
    else if([self.driverid isFirstResponder])
    {
        textfieldFrame=[self.driverid frame];
    }
    else if([self.email isFirstResponder])
    {
        textfieldFrame=[self.email frame];
    }
    else if([self.mobile isFirstResponder])
    {
        textfieldFrame=[self.mobile frame ];
    }
    else
    {
        textfieldFrame=[_DOT frame];
    }
   // BFLog(@"General-scrollpoint--(%f,%f)",textfieldFrame.origin.x,textfieldFrame.origin.y);
    if (aRect.size.height < (textfieldFrame.origin.y+15)) {
        CGPoint scrollPoint = CGPointMake(0.0, textfieldFrame.origin.y);
        //BFLog(@"scrollpoint--%f",textfieldFrame.origin.y-kbSize.height);
        [scroller04 setContentOffset:scrollPoint animated:YES];
    }}


// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scroller04.contentInset = contentInsets;
    scroller04.scrollIndicatorInsets = contentInsets;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
  //  BFLog(@"entered return key");
    return YES;
    
}

- (void)backgroundTap {
    [self.view endEditing:YES];
   // BFLog(@"Touched Inside");
}

//-(void)textFieldEndBeginEditing:(UITextField *)textField{
//}

//minnu

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}
-(void)saveToServer:(NSMutableDictionary *)driver_data{
    
//    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
//    //getting authCode
//    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
//    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
//    NSString *authCode = @"";
//    if([authData count]!=0){
//        authCode = authData[0][1];
//        
//    }
//    //---------------------------
//     NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/saveHosDriverData.php?masterUser=%@&User=%@&authCode=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"masteruser"],[[NSUserDefaults standardUserDefaults] valueForKey:@"user"],authCode];
//    // NSString *urlString = [NSString stringWithFormat:@"http://23.239.28.100/mydealership/saveHosGraphData_app.php?masterUser=%@&user=%@&date=%@&approve=0",master_menu,user_menu,current_date];
//    NSURL *url = [NSURL URLWithString:urlString];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    [request setHTTPMethod:@"HEAD"];
//    NSURLResponse *response;
//    NSError *error;
//    NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
//    if(httpResponse!=nil){
//        if ([httpResponse statusCode] ==200 ) {
//            BFLog(@"Device connected to the internet");
//            NSURL *url = [NSURL URLWithString:urlString];
//            
//            //NSError *error;
//            
//            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
//                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                               timeoutInterval:60.0];
//            
//            [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//            [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
//            
//            [request setHTTPMethod:@"POST"];
//            /* NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
//             @"IOS TYPE", @"typemap",
//             nil];*/
//            
//            //NSMutableDictionary *currentStatus=[[NSMutableDictionary alloc]init];
//            //[currentStatus setValue:status forKey:@"Status"];*/
//            /*  NSMutableArray *inner = [[NSMutableArray alloc] initWithArray:@[@"00:00",@"OFF"]];
//             NSMutableDictionary *data_dict = [[NSMutableDictionary alloc] init];
//             [data_dict setObject:inner forKey:@"payload"];
//             NSError *error;*/
//             NSData *postData = [NSJSONSerialization dataWithJSONObject:driver_data options:0 error:&error];
//             [request setHTTPBody:postData];
//            
//            NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                
//                NSDictionary *responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//                
//            }];
//            
//            [postDataTask resume];
//        }else if([httpResponse statusCode]==403){
//            if(autoLoginCount<=1){
//                insertDutyCycle *obj = [insertDutyCycle new];
//                BOOL success =  [obj autoLogin];
//                
//                if(success){
//                    [self saveToServer:driver_data];
//                    
//                }else{
//                    KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
//                    [credentials resetKeychainItem];
//                    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
//                                                                     message:@"Please Login Again"
//                                                                    delegate:self
//                                                           cancelButtonTitle:@"OK"
//                                                           otherButtonTitles:nil, nil];
//                    alert1.tag = 1;
//                    alert1.delegate = self;
//                    
//                    [alert1 show];
//                    
//                }
//            }else{
//                KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
//                [credentials resetKeychainItem];
//                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
//                                                                 message:@"Please Login Again"
//                                                                delegate:self
//                                                       cancelButtonTitle:@"OK"
//                                                       otherButtonTitles:nil, nil];
//                alert1.tag = 2;
//                alert1.delegate = self;
//                
//                [alert1 show];
//                
//                
//                
//            }
//            
//        }else if([httpResponse statusCode]==503){
//            BFLog(@"Device not connected to the internet");
//            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Service Unavailable"
//                                                             message:@"No connectivity with server at this time,it will sync automatically"
//                                                            delegate:self
//                                                   cancelButtonTitle:@"OK"
//                                                   otherButtonTitles:nil, nil];
//            
//            [alert1 show];
//            
//        }
//    }
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag ==1 || alertView.tag ==2){
        
        if(buttonIndex==[alertView cancelButtonIndex]){
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"loginscreen"];
                [self presentViewController:vc animated:YES completion:nil];
            });
            
        }
        
        
    }
    
    
    
}

- (IBAction)saveSettings:(id)sender {
    autoLoginCount = 0;
    if([_name.text  isEqual:@""]||[_driverid.text isEqual:@""]||[_mobile.text isEqual:@""]||[_email.text isEqual:@""]||[_lastName.text isEqualToString:@""]){
        
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"Required fiels cannot be empty!"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        
        [alert1 show];
        
    }else{
        
        NSString *query2 = [NSString stringWithFormat:@"select * from AccountSettings"];
        NSString *phoneRegEx = @"[0-9]{10}";
        NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegEx];
        NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
        
        DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
        NSDictionary *token;
        ;
        NSMutableDictionary *whereToken;
        NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
        if([userInfoArray count]!=0){
            int userId = [userInfoArray[0][0] intValue];
            token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
            ;
            whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
            User *objUser=[User new];
            //    NSData *password1 = [credentials objectForKey:(id)kSecValueData];
            //    NSString *password=[[NSString alloc] initWithData:password1 encoding:NSUTF8StringEncoding];
            //NSData *password1 = [credentials objectForKey:(id)kSecValueData];
            //NSString *password=[[NSString alloc] initWithData:password1 encoding:NSUTF8StringEncoding];
            //objUser.username=username;
            //objUser.password=password;
            objUser.firstname=_name.text;
            objUser.lastname=lastnameLabel.text;
            objUser.mobile=_mobile.text;
            objUser.email=_email.text;
            objUser.driverid=_driverid.text;
            objUser.dotNo=_DOT.text;
            
            NSArray *values1 = [[NSArray alloc] initWithArray:[objDB selectFromTable:objUser whereToken:whereToken]];
            
            // Get the results.
            
            //Raghee values1 = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query2]];
            
            if ([values1 count]!=0) {
                
                
                if ([phoneTest evaluateWithObject:_mobile.text] == YES && [emailTest evaluateWithObject:_email.text] == YES) {
                    [objDB updateIntoTable:objUser whereToken:whereToken];
                    /* Raghee NSString *query_update = [NSString stringWithFormat:@"UPDATE AccountSettings SET name='%@',driverid='%@',mobile_num='%@',email='%@',username='%@'",_name.text,_driverid.text,_mobile.text,_email.text,_DOT.text];
                     
                     // Execute the query.
                     
                     //NSString *query_del = @"delete from TempTable";
                     [dbManager executeQuery:query_update];
                     */
                    // If the query was successfully executed then pop the view controller.
                    if (objDB.affectedRows != 0) {
                        // BFLog(@"Table updated successfully. Affected rows = %d", dbManager.affectedRows);
                        UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                                         message:@"Data Saved!!!"
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil, nil];
                        
                        [alert2 show];
                    }
                    else{
                        // BFLog(@"Could not execute the query");
                    }
                }else{
                    
                    if ([emailTest evaluateWithObject:_email.text] == NO && [phoneTest evaluateWithObject:_mobile.text] == NO) {
                        
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Valid Email Address and Mobile Number." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alert show];
                    }
                    else if([phoneTest evaluateWithObject:_mobile.text] == NO){
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Valid Phone Number." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alert show];
                        
                    }else{
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Valid Email Address." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alert show];
                        
                        
                    }
                    
                    
                }
                
            } else{
                
                if ([phoneTest evaluateWithObject:_email.text] == YES && [emailTest evaluateWithObject:_email.text] == YES) {
                    
                    NSString *query_insert2 = [NSString stringWithFormat:@"insert into AccountSettings values(null, '%@','%@', '%@', '%@','%@','%@')",_name.text,lastnameLabel.text,_driverid.text,_mobile.text,_email.text,_DOT.text];
                    
                    // Execute the query.
                    //minnu
                    
                    [objDB  insertIntoTable:objUser];
                    //minnu
                    
                    //NSString *query_del = @"delete from TempTable";
                    [dbManager executeQuery:query_insert2];
                    
                    // If the query was successfully executed then pop the view controller.
                    if (objDB.affectedRows != 0) {
                        // BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
                        UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                                         message:@"Data Saved!!!"
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil, nil];
                        
                        [alert2 show];        }
                    else{
                        //BFLog(@"Could not execute the query");
                    }}else{
                        
                        if ([emailTest evaluateWithObject:_email.text] == NO && [phoneTest evaluateWithObject:_mobile.text] == NO) {
                            
                            
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Valid Email Address and Mobile Number." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            [alert show];
                        }
                        else if([phoneTest evaluateWithObject:_mobile.text] == NO){
                            
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Valid Phone Number." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            [alert show];
                            
                        }else if([emailTest evaluateWithObject:_email.text] == NO){
                            
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Valid Email Address." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            [alert show];
                            
                            
                        }
                        
                        
                        
                    }
                
                
            }
            if ([phoneTest evaluateWithObject:_mobile.text] == YES && [emailTest evaluateWithObject:_email.text] == YES) {
                NSMutableDictionary *driver_data = [[NSMutableDictionary alloc] init];
                
                [driver_data setValue:_name.text forKey:@"FirstName"];
                
                
                
                [driver_data setValue:_lastName.text forKey:@"LastName"];
                
                [driver_data setValue:_driverid.text forKey:@"LicenceNumber"];
                [driver_data setValue:_mobile.text forKey:@"Phone"];
                
                [driver_data setValue:_email.text forKey:@"Email"];
                [driver_data setValue:_DOT.text forKey:@"dotNumber"];
                //Raghee      [self saveToServer:driver_data];
                
            }
        }// BFLog(@"data for save:%@",driver_data);
    }

}


@end
