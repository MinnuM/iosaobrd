//
//  diagnosticsSettings.h
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 05/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface diagnosticsSettings : UIViewController<UITableViewDelegate,UITableViewDataSource>
- (IBAction)helpAlert:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scroller_diag;
@property (weak, nonatomic) IBOutlet UITableView *list_diagnostics;
@end

