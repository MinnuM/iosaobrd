
//
//  main.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/25/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "TIMERUIApplication.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv,NSStringFromClass([UIApplication class]),NSStringFromClass([AppDelegate class]));
    }
}
