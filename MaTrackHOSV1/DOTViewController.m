//
//  DOTViewController.m
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/6/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "DOTViewController.h"
#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "KeychainItemWrapper.h"
#import "insertDutyCycle.h"


UIBarButtonItem *previous;
UIBarButtonItem *next;
NSString *date=@"";
int date_marker=0;
NSDictionary *json1;
int count=0;
NSMutableArray *dates;
NSArray *section1;
NSArray *section2;
NSArray *cells;
@interface DOTViewController ()

@end

@implementation DOTViewController{
    NSArray *cellTexts;
    
}

@synthesize dbManager;
- (void)viewDidLoad {
    [super viewDidLoad];
     dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
    [_scroller_dot setScrollEnabled:YES];
    [_scroller_dot setContentSize:(CGSizeMake(600, 1000))];
    CGPoint bottomOffset = CGPointMake(0, 1000-_scroller_dot.bounds.size.height);
    [_scroller_dot setContentOffset:bottomOffset animated:YES];
    _settingsdata.delegate=self;
    _settingsdata.dataSource=self;
    _settingsdata.layer.borderWidth=0.6f;
    _settingsdata.layer.borderColor=[UIColor blackColor].CGColor;
    next = [[UIBarButtonItem alloc]init];
    next.title=@"Next";
    next.action=@selector(nextBtn);
    next.tintColor=[UIColor blackColor];
    next.image=[UIImage imageNamed:@"next2.png"];
    previous = [[UIBarButtonItem alloc] init];
    previous.title = @"Previous";
    previous.image = [UIImage imageNamed:@"back2.png"];
    previous.tintColor=[UIColor blackColor];
    previous.action=@selector(previousBtn);
    next.tintColor=[UIColor blackColor];
    UIBarButtonItem *back = [[UIBarButtonItem alloc] init];
    back.title=@"Back";
    back.tintColor=[UIColor blackColor];
    UIBarButtonItem *save = [[UIBarButtonItem alloc] init];
    save.title=@"Save";
    save.tintColor=[UIColor blackColor];
    next.tintColor=[UIColor blackColor];
    _navigation.leftBarButtonItems=@[back,previous];
    _navigation.rightBarButtonItems=@[save,next];
    cellTexts = [[NSArray alloc] initWithObjects:@"driver1"@"driver2",@"codriver1",@"codriver2",@"distance1",@"distance2",@"vehicles1",@"vehicles2",@"carrier1",@"carrier2",@"mainoffice1",@"mainoffice2", nil];
    
    section1 = [[NSArray alloc] initWithObjects:@"driver1"@"driver2",@"codriver1",@"codriver2", nil];
    section2 = [[NSArray alloc] initWithObjects:@"distance1",@"distance2",@"vehicles1",@"vehicles2", nil];
    cells = [[NSArray alloc] initWithObjects:section1,section2, nil];
    dates=[[NSMutableArray alloc]init];
    if ([dates count]==0) {
        next.enabled=NO;
        
        
    }
    NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/client/demo/maps/hos/graph/DOTInspection.php?zone=-420"];
    //NSString *urlString = @"%@/trackreport_b_new1.php?driver=%@&startdate=%@&enddate=%@&select=%@",path,driver,start,end,option;
    // BFLog(@"%@",urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [_dotweb loadRequest:urlRequest];
    
   /* UILabel *ontime = [[UILabel alloc] initWithFrame:(CGRectMake(500, 200, 10, 25))];
    ontime.text = @"sample data";*/
   /* UIStackView *stackView = [[UIStackView alloc] initWithFrame:(CGRectMake(400, 320, 200,400))];
    stackView.axis = UILayoutConstraintAxisVertical;
    stackView.distribution = UIStackViewDistributionEqualSpacing;
    stackView.alignment = UIStackViewAlignmentCenter;
    stackView.backgroundColor = [UIColor greenColor];
    UILabel *label = [[UILabel alloc]init];
    label.text=@"Recap";
    
    UILabel *day1 = [[UILabel alloc] init];
    day1.text=@"ysretday details";
    
    [stackView addArrangedSubview:label] ;
    [stackView addArrangedSubview:day1];
    [self.view addSubview:stackView];*/
    //----------------------------------------------------------
    
   /*
    //View 1
    UILabel *view1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 40)];
    view1.backgroundColor = [UIColor blueColor];
    [view1.heightAnchor constraintEqualToConstant:100].active = true;
    [view1.widthAnchor constraintEqualToConstant:120].active = true;
    
    
    //View 2
    UIView *view2 = [[UIView alloc] init];
    view2.backgroundColor = [UIColor greenColor];
    [view2.heightAnchor constraintEqualToConstant:100].active = true;
    [view2.widthAnchor constraintEqualToConstant:70].active = true;
    
    //View 3
    UIView *view3 = [[UIView alloc] init];
    view3.backgroundColor = [UIColor magentaColor];
    [view3.heightAnchor constraintEqualToConstant:100].active = true;
    [view3.widthAnchor constraintEqualToConstant:180].active = true;
    
    //Stack View
    UIStackView *stackView = [[UIStackView alloc] init];
    
    stackView.axis = UILayoutConstraintAxisVertical;
    stackView.distribution = UIStackViewDistributionEqualSpacing;
    stackView.alignment = UIStackViewAlignmentCenter;
    stackView.spacing = 30;
    
    
    [stackView addArrangedSubview:view1];
    [stackView addArrangedSubview:view2];
    [stackView addArrangedSubview:view3];
    
    stackView.translatesAutoresizingMaskIntoConstraints = false;
    [self.view addSubview:stackView];
    
    
    //Layout for Stack View
    //[stackView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = true;
    //[stackView.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor].active = true;
    
    */
    
    //-------------------------------------------------------------
    
    NSDate * now = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"MMM dd"];
    NSString *newDateString = [outputFormatter stringFromDate:now];
    _navigation.title=newDateString;
    NSDateFormatter *outputFormatter_tody = [[NSDateFormatter alloc] init];
    [outputFormatter_tody setDateFormat:@"MM/dd/YYYY"];
    NSString *newDateString2 = [outputFormatter_tody stringFromDate:now];
    _navigation.title=newDateString2;
    [self getData:newDateString2];
        
   
                                       
}
-(void)viewWillAppear:(BOOL)animated{
    
    
   // count=0;
}

-(void)getData:(NSString*)date {
    [dataarray1 removeAllObjects];
    //getting authCode
    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    NSString *authCode = @"";
    if([authData count]!=0){
        authCode = authData[0][1];
        
    }
    //---------------------------
    NSString *urlString1 = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/getHosDataPerDate.php?masterUser=%@&user=%@&date=%@&authCode=%@",masterUser,user,date,authCode];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString1]];
    [request setHTTPMethod:@"HEAD"];
    NSURLResponse *response;
    NSError *error;
    NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    if(httpResponse!=nil){
        if ([httpResponse statusCode] ==200 ) {
            
            
            NSURLSession *session1 = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask1 = [session1 dataTaskWithURL:[NSURL URLWithString:urlString1] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                //  BFLog(@"Retrieved Data:%@,%@",data,response);
                BFLog(@"---------------------------------------");
                BFLog(@"error:%@",error);
                json1 = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                //if (error == nil) {
                BFLog(@"Data From File:%@",json1);
                //NSString *text = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                if ([json1 count] == 0) {
                    
                    UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"No data available for the current date..!!" preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                        [alert1 dismissViewControllerAnimated:YES completion:nil];
                    }];
                    
                    
                    [alert1 addAction:ok1];
                    [self presentViewController:alert1 animated:YES completion:nil];
                    
                    
                    
                }
                else{
                    
                    BFLog(@"data portion:%@",[json1 objectForKey:@"payload"]);
                    NSArray *dict = [json1 objectForKey:@"payload"];
                    int json_size = (int)[dict count];
                    BFLog(@"inner json count:%d",json_size);
                    
                    for (int i=0; i<json_size; i++) {
                        
                        BFLog(@"posx:%@",[dict[0] objectForKey:@"posx"]);
                        float posx=[[dict[i] objectForKey:@"posx"] floatValue];
                        float x1 = posx - 40.0;
                        int x2 = x1/12.25;
                        int x3 = x2/4.0;
                        int x4 = x2%4;
                        NSString *posx_1 = [NSString stringWithFormat:@"%d.%d",x3,15*x4];
                        // float time = [posx_1 floatValue];
                        int event_no =(int)[[dict[i] objectForKey:@"posy"] integerValue];
                        NSString * event;
                        if (event_no == 245) {
                            event=@"ON";
                        }
                        else if(event_no == 125){
                            event=@"SB";
                            
                        }else if (event_no == 185){
                            event = @"D";
                        }else if (event_no == 65){
                            event = @"OFF";
                        }
                        NSArray *final_data = [[NSArray alloc] initWithObjects:posx_1,event, nil];
                        [dataarray1 addObject:final_data];
                        
                    }
                    
                    BFLog(@"Final array for loading:%@",dataarray1);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSArray *subViews = [self.view subviews];
                        for(UIView *view in subViews){
                            if ([view isKindOfClass:[DOTView class]]) {
                                [view removeFromSuperview];
                                flag1 = flag1 +1;
                                
                                DOTView *f = [[DOTView alloc] initWithFrame:(CGRectMake(0, 170, 600, 100))];
                                y_origin1=0;
                                [f setOpaque:NO];
                                
                                [self.view addSubview:f];
                                [f setNeedsDisplay];
                            }
                        }
                        
                    });
                    
                    
                    
                    
                }
                
            }];
            
            [dataTask1 resume];

            
            
        }else if ([httpResponse statusCode]==403){
            
            if(autoLoginCount<=1){
                insertDutyCycle *obj = [insertDutyCycle new];
                BOOL success =  [obj autoLogin];
                
                if(success){
                    [self getData:date];
                    
                }else{
                    KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
                    [credentials resetKeychainItem];
                    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
                                                                     message:@"Please Login Again"
                                                                    delegate:self
                                                           cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil, nil];
                    alert1.tag = 1;
                    alert1.delegate = self;
                    
                    [alert1 show];
                    
                }
            }else{
                KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
                [credentials resetKeychainItem];
                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
                                                                 message:@"Please Login Again"
                                                                delegate:self
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil, nil];
                alert1.tag = 2;
                alert1.delegate = self;
                
                [alert1 show];
                
                
                
            }

            
            
        }else if ([httpResponse statusCode]==404){
            
            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Server Down!!"
                                                             message:@"Please Try Again"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
            alert1.tag = 3;
            alert1.delegate = self;
            
            [alert1 show];
            
            
        }
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag ==1 ||alertView.tag==2)
    {
        if(buttonIndex == [alertView cancelButtonIndex]){
            
            
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"loginscreen"];
                [self presentViewController:vc animated:YES completion:nil];
            });
            
            
        }
        
        
        
        
        
    }
    
    
    
}
-(void)nextBtn{

   if ([dates count]>0) {
       next.enabled=YES;
        count=count-1;
    BFLog(@"next dates:%@",dates);
        BFLog(@"count:%d",count);
     int last=(int)[dates count];
  // NSString *last1=dates[last-1];
    NSString *current=dates[last-1];
    BFLog(@"nextDate: %@ ...", current);
       [dates removeLastObject];
      /* NSDateFormatter *outputFormatter_next = [[NSDateFormatter alloc] init];
      [outputFormatter_next setDateFormat:@"MM/dd/YYYY"];
       [outputFormatter_next setTimeZone:[NSTimeZone localTimeZone]];
       NSDate *sam = [outputFormatter_next dateFromString:last1];
       NSDateFormatter *outputFormatter_next1 = [[NSDateFormatter alloc] init];
       [outputFormatter_next1 setDateFormat:@"MMM dd"];
       
       NSString *newDateString3 = [outputFormatter_next1 stringFromDate:sam];*/
       _navigation.title=current;
   /*NSDate *tomorrow = [NSDate dateWithTimeInterval:24*60*60 sinceDate:[NSDate date]];*/
   // BFLog(@"next:%@",sam);
   [self getData:current];
  }else{
        UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Next date data not available now!!" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [alert1 dismissViewControllerAnimated:YES completion:nil];
        }];
        
        
        [alert1 addAction:ok1];
        [self presentViewController:alert1 animated:YES completion:nil];
        
      next.enabled=NO;
    }
    }

- (void)previousBtn {
    NSDate *today = [NSDate date];
    if (count==0) {
        NSDateFormatter *outputFormatter_prev1 = [[NSDateFormatter alloc] init];
        [outputFormatter_prev1 setDateFormat:@"MM/dd/YYYY"];
        NSString *newDateString_prev1 = [outputFormatter_prev1 stringFromDate:today];
        [dates addObject:newDateString_prev1];
    }
    count=count+1;
    BFLog(@"count:%d",count);
    //NSString *datefromlabel=_navigation.title;
   // _nextbtn.enabled=YES;
    next.enabled=YES;
    
    NSDate *previous = [today dateByAddingTimeInterval:-count*24*60*60];
    NSDateFormatter *outputFormatter1 = [[NSDateFormatter alloc] init];
    [outputFormatter1 setDateFormat:@"MMM dd"];
    NSString *previousdate = [outputFormatter1 stringFromDate:previous];
    NSDateFormatter *outputFormatter_prev = [[NSDateFormatter alloc] init];
    [outputFormatter_prev setDateFormat:@"MM/dd/YYYY"];
    NSString *newDateString_prev = [outputFormatter_prev stringFromDate:previous];
    [dates addObject:newDateString_prev];
    
    BFLog(@"previous:%@",previousdate);
    _navigation.title=newDateString_prev;
    [self getData:newDateString_prev];

}

/*-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 4;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    return 3;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.textLabel.text=@"hai";
    cell.backgroundColor=[UIColor blueColor];
    return cell;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *section_name=@"";
    if (section==0) {
        section_name=@"Status";
    }
    else if (section==1){
        
        section_name=@"Start Time";
    }
    else if (section==2){
        section_name=@"Duration";
    }
    else if (section==3){
        
        section_name=@"Location";
    }
    return section_name;
    
}

*/

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    
    return 4;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 3;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"cell";
   // [collectionView registerClass:[customCell class] forCellWithReuseIdentifier:cellIdentifier];
    customCell  *cell = (customCell *) [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
      //cell.contentView.layer.borderWidth=0.5f;
    //cell.contentView.layer.borderColor=[UIColor blackColor].CGColor;
  if(indexPath.section==0){
        switch (indexPath.row) {
            case 0:
                
                cell.textlabel.text=@"Driver:";

                break;
            case 1:
                
                cell.textlabel.text=driver_names;
                
                break;
                
            case 2:
                
                cell.textlabel.text=@"Co-Driver:";
                
                break;
            case 3:
                
                cell.textlabel.text=@"Co-Driver";
                
                break;
            default:
                break;
        }
        
    }else if(indexPath.section==1){
        switch (indexPath.row) {
            case 0:
                
                cell.textlabel.text=@"Distance:";
                
                break;
            case 1:
                
                cell.textlabel.text=@"abc";
                
                break;
                
            case 2:
                
                cell.textlabel.text=@"Vehicles:";
                
                break;
            case 3:
                
                cell.textlabel.text=@"abc ";
                
                break;
            default:
                break;
        }
        
    }else if(indexPath.section==2){
        switch (indexPath.row) {
            case 0:
                
                cell.textlabel.text=@"Carrier:";
                
                break;
            case 1:
                
                cell.textlabel.text=carrier_name;
                
                break;
                
            case 2:
                
                cell.textlabel.text=@"Main Office:";
                
                break;
            case 3:
                
                cell.textlabel.text=main_office;
                
                break;
            default:
                break;
        }
        
    }
    //cell.textlabel.text=@"cell";
    BFLog(@"cell:%@",cell.textlabel.text);
    
        BFLog(@"cell:%ld,%ld",(long)indexPath.item,(long)indexPath.row);
    return cell;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    
    return UIEdgeInsetsMake(2, 10, 0, 10);
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    
    
    return 0.0;
}
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    
    
    return 0.0;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
   
    return CGSizeMake(80,30);
    
}
@end
