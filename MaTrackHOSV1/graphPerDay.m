//
//  graphPerDay.m
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/16/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "graphPerDay.h"
#import "previousDayViewController.h"

CGContextRef context_front1;
CGRect current_rectfront11;
NSString *newDateString1;
float kkStepX1;
int flag4=0;
@implementation graphPerDay

- (void)drawLineGraphWithContext:(CGContextRef)ctx
{
    CGRect screenRect=[[UIScreen mainScreen] bounds];
    CGFloat screenWidth =screenRect.size.width;
    float kkOffsetX1=(31.75/414)*screenWidth;
    int multiplier=0;
    //BFLog(@"%@,%@",start_time,end_time);
    BFLog(@"graph loading......%lu",(unsigned long)[dataarray2 count]);
    CGContextSetLineWidth(ctx, 2.0);
    CGContextSetStrokeColorWithColor(ctx, [[UIColor yellowColor] CGColor]);
    CGContextSetFillColorWithColor(ctx, [[UIColor redColor] CGColor]);
    //  if (flag==0) {
    
    // int maxGrap00000hHeight = kGraphHeight - kOffsetY;
    //int y_origin=0;
    int y_offset=0;
    CGContextBeginPath(ctx);
    NSString *x1=dataarray2[0][1];
    if([x1  isEqual: @"ON"]){
        
        y_origin1 = 5;
        CGContextMoveToPoint(ctx, kkOffsetX1, ((y_origin1) * kkStepY1));
        float x = kkOffsetX1;
        y_offset = 23/1.6;
        float y = (( y_origin1) * kkStepY1-y_offset);
        CGRect rect = CGRectMake(x - kkCircleRadius1, y - kkCircleRadius1, 2 * kkCircleRadius1, 2 * kkCircleRadius1);
        CGContextAddEllipseInRect(ctx, rect);
        
        
        
        
        
    }
    else  if([x1  isEqual: @"D"]){
        y_origin1 = 4;
        //y_offset = 23;
        y_offset = 23/2.5;
        float x = kkOffsetX1;
        float y = ((y_origin1) * (kkStepY1))+y_offset;
        CGContextMoveToPoint(ctx, kkOffsetX1, ((y_origin1) * (kkStepY1)));
        CGRect rect = CGRectMake(x - kkCircleRadius1, y - kkCircleRadius1, 2 * kkCircleRadius1, 2 * kkCircleRadius1);
        CGContextAddEllipseInRect(ctx, rect);
        
        
        
        
        
        
        
    }else  if([x1  isEqual: @"SB"]){
        
        y_origin1 = 3;
        CGContextMoveToPoint(ctx, kkOffsetX1, ((y_origin1) * kkStepY1));
        float x = kkOffsetX1;
        y_offset = 23/4;
        float y = ((y_origin1) * kkStepY1)+y_offset;
        CGRect rect = CGRectMake(x - kkCircleRadius1, y - kkCircleRadius1, 2 * kkCircleRadius1, 2 * kkCircleRadius1);
        CGContextAddEllipseInRect(ctx, rect);
        
        
        
    }else  if([x1  isEqual: @"OFF"]){
        
        y_origin1 = 2;
        float x = kkOffsetX1;
        float y = ((y_origin1) * kkStepY1);
        CGRect rect = CGRectMake(x - kkCircleRadius1, y - kkCircleRadius1, 2 * kkCircleRadius1, 2 * kkCircleRadius1);
        CGContextAddEllipseInRect(ctx, rect);
        CGContextMoveToPoint(ctx, kkOffsetX1, ((y_origin1) * kkStepY1));
    }
    BFLog(@"y origin:%d",y_origin1);
    int data_size =(int) [dataarray2 count];
    BFLog(@"array size:%d",data_size);
    for (int i = 0; i < data_size; i++)
    {
        int y_pos=0;
        NSArray *time = [dataarray2[i][0] componentsSeparatedByString:@":"];
        NSString *second_part=@"";
        int float_part =(int) [time[1] integerValue];
        if (float_part>=0 && float_part<15) {
            second_part=@"00";
            multiplier=0;
        }else if (float_part>=15 && float_part <30)    {
            second_part = @"15";
            multiplier=1;
            
        }else if (float_part>=30 && float_part<45){
            second_part=@"30";
            multiplier=2;
            
        }else if (float_part>=45){
            second_part=@"45";
            multiplier=3;
            
            
        }
        
        NSString *starttime = [NSString stringWithFormat:@"%@.%@",time[0],second_part];
        float x_one = [starttime floatValue];
        NSString *time_float=[NSString stringWithFormat:@"%@",time[0]];
        float x_one1 = [time_float floatValue];
        
        int y=0;
        NSString *x2 = dataarray2[i][1];
        if([x2  isEqual: @"D"]){
            
            
            y_pos = 4;
            y_offset = 23/2.5;
            y = ((y_pos) * kkStepY1)+y_offset;            //NSString *posy = [NSString stringWithFormat:@"%d",y];
            //dataarray[i][1]=posy;
            
        }else  if([x2  isEqual: @"ON"]){
            
            y_pos = 5;
            y_offset = 23/1.6;
            y = ((y_pos) * kkStepY1)+y_offset;
            //     y = ((y_pos) * kkStepY1);
            // NSString *posy = [NSString stringWithFormat:@"%d",y];
            //dataarray[i][1]=posy;             ;
        }else  if([x2  isEqual: @"SB"]){
            
            
            y_pos = 3;
            y_offset = 23/4;
            y = ((y_pos) * kkStepY1)+y_offset;
            //NSString *posy = [NSString stringWithFormat:@"%d",y];
            //dataarray[i][1]=posy;
            
        }else  if([x2  isEqual: @"OFF"]){
            
            y_pos = 2;
            y = ((y_pos) * kkStepY1);             //NSString *posy = [NSString stringWithFormat:@"%d",y];
            //dataarray[i][1]=posy;
        }
        BFLog(@"x,y:%f,%d",x_one,y);
        //CGContextAddLineToPoint(ctx, kkOffsetX1+(kkStepX1*(x_one)),y);
        //float x = kkOffsetX1+(kkStepX1*(x_one));
        
        float x = kkOffsetX1+(kkStepX1/4*(x_one1*4+multiplier));
        
        CGContextAddLineToPoint(ctx, (x+kkCircleRadius1),y);
        
        float y1 = y;
        CGRect rect = CGRectMake(x - kkCircleRadius1, y1 - kkCircleRadius1, 2 * kkCircleRadius1, 2 * kkCircleRadius1);
        CGContextAddEllipseInRect(ctx, rect);
        CGContextSetFillColorWithColor(ctx, [[UIColor yellowColor] CGColor]);        //CGContextAddLineToPoint(ctx, kStepX*(0.45)+9,((1 + 3) * 22));
        
        // CGContextAddLineToPoint(ctx, kStepX*(15.45)+9,((1 + 3) * 22));
        
        //CGContextAddLineToPoint(ctx, kStepX*(15.45)+9,((1 + 2) * 23));
        
        // CGContextAddLineToPoint(ctx, kStepX*(15.45)+9,((1 + 2) * 23));
        //  double x1 = [dataarray[0][0] doubleValue];
        //CGContextAddLineToPoint(ctx, x1, ((1 + 2) * 23));
        
        //}
    }
    BFLog(@"new array with new y pos%@",dataarray2);
    CGContextDrawPath(ctx, kCGPathStroke);
    
    
    
    //BFLog(@"dta size:%lu",sizeof(data)/sizeof(float));
    //sample_data = self.data;
    BFLog(@"data:%@",dataarray);
    
    CGContextDrawPath(ctx, kCGPathFillStroke);
    
    
    
    
    //CGContextRetain(ctx);
    UIGraphicsPushContext(ctx);
    CGContextStrokePath(ctx);

    
    
}
-(void)drawAll:(CGContextRef)ctx{
    if([deviceData1 count]!=0){
        UIColor *lineColor;
        int multiplier=0;
        int y_offset=23/1.6;
        float y = ((5.5) * kkStepY)+y_offset;

        
        for(int i=0;i<[deviceData1 count];i++){
            
            if([deviceData1[i][@"event"] isEqual:@"4002"]){
                
                
                lineColor = [UIColor whiteColor];
            }else if([deviceData1[i][@"event"] isEqual:@"6045"]){
                
                
                lineColor = [UIColor greenColor];
            }else if([deviceData1[i][@"event"] isEqual:@"4001"]){
                if([deviceData1[i][@"speed"] floatValue]>0){
                    
                    lineColor = [UIColor colorWithRed:0.00 green:0.93 blue:0.00 alpha:1.0];
                    
                }else{
                    
                    
                    lineColor = [UIColor colorWithRed:0.93 green:0.00 blue:0.00 alpha:1.0];
                }
            }else if([deviceData1[i][@"event"] isEqual:@"6011"] || [deviceData1[i][@"event"] isEqual:@"6012"]){
                
                
                
                lineColor = [UIColor colorWithRed:0.99 green:0.99 blue:0.16 alpha:1.0];
            }else{
                
                
                lineColor = [UIColor whiteColor];
            }
            
            CGRect screenRect=[[UIScreen mainScreen] bounds];
            CGFloat screenWidth =screenRect.size.width;
            float kkOffsetX=(31.75/414)*screenWidth;
            CGContextSetLineWidth(ctx, 3.0);
            CGContextSetStrokeColorWithColor(ctx, [lineColor CGColor]);
            CGContextBeginPath(ctx);
            NSArray *time = [deviceData1[i][@"starttime"] componentsSeparatedByString:@":"];
            NSString *second_part=@"";
            
            int float_part =(int) [time[1] integerValue];
            if (float_part>=0 && float_part<15) {
                second_part=@"00";
                multiplier=0;
            }else if (float_part>=15 && float_part <30)    {
                second_part = @"15";
                multiplier=1;
                
            }else if (float_part>=30 && float_part<45){
                second_part=@"30";
                multiplier=2;
                
                
            }else if (float_part>=45){
                second_part=@"45";
                multiplier=3;
                
                
            }
            //NSString *starttime = [NSString stringWithFormat:@"%@.%@",time[0],second_part];
           // float x_one = [starttime floatValue];
            NSString *time_float = [NSString stringWithFormat:@"%@",time[0]];
            float x_one1 = [time_float floatValue];
            
            float x1 = kkOffsetX+(kkStepX1/4*(x_one1*4+multiplier));
            CGContextMoveToPoint(ctx, x1, y);
            
            /*-------------------------------------------*/
            
            NSArray *time1 = [deviceData1[i][@"endtime"] componentsSeparatedByString:@":"];
            NSString *second_part1=@"";
            
            int float_part1=(int) [time1[1] integerValue];
            if (float_part1>=0 && float_part1<15) {
                second_part1=@"00";
                multiplier=0;
            }else if (float_part1>=15 && float_part1 <30)    {
                second_part1 = @"15";
                multiplier=1;
                
            }else if (float_part1>=30 && float_part1<45){
                second_part1=@"30";
                multiplier=2;
                
                
            }else if (float_part1>=45){
                second_part1=@"45";
                multiplier=3;
                
                
            }
            //NSString *starttime1 = [NSString stringWithFormat:@"%@.%@",time1[0],second_part1];
            //float x_one2 = [starttime1 floatValue];
            NSString *time_float1 = [NSString stringWithFormat:@"%@",time1[0]];
            float x_one3 = [time_float1 floatValue];
            
            float x2 = kkOffsetX+(kkStepX1/4*(x_one3*4+multiplier));
            
            CGContextAddLineToPoint(ctx, (x2+kkCircleRadius),y);
            
            CGContextDrawPath(ctx, kCGPathStroke);
            
            CGContextDrawPath(ctx, kCGPathFillStroke);
            
            UIGraphicsPushContext(ctx);
            CGContextStrokePath(ctx);
            
            
            
            
        }
    }
    
}


- (void)drawRect:(CGRect)rect {
    CGRect screenRect=[[UIScreen mainScreen] bounds];
    CGFloat screenWidth =screenRect.size.width;
    float kkOffsetX1=screenWidth*(31.25/414);
    float kkDefaultGraphWidth1=screenWidth-screenWidth*(58.5/414);
    BFLog(@"view refreshingg....:%@",NSStringFromCGRect(rect));
    current_rectfront11 = CGRectFromString(NSStringFromCGRect(rect));
    //flag=flag1;
    context_front1 = UIGraphicsGetCurrentContext();
    //current_context = UIGraphicsGetCurrentContext();
    BFLog(@"current context:%@",context_front1);
    CGContextSetLineWidth(context_front1, 0.6);
    CGContextSetStrokeColorWithColor(context_front1, [[UIColor lightGrayColor] CGColor]);
    //CGFloat dash[] = {2.0, 2.0};
    //CGContextSetLineDash(context, 0.0, dash, 2);    // How many lines?
    // int howManyk = (kkDefaultGraphWidth1 + kkOffsetX1) / kkStepX1;
    int howManyk=24;
    kkStepX1 = (kkDefaultGraphWidth1)/(howManyk);
    BFLog(@"vertical lines:%d",howManyk);
    // Here the lines go
    for (int i = 0; i <= howManyk; i++)
    {
        CGContextMoveToPoint(context_front1, kkOffsetX1 + i * kkStepX1, kkGraphTop1);
        CGContextAddLineToPoint(context_front1, kkOffsetX1 + i * kkStepX1, kkGraphBottom1);
    }
    //int howManyHorizontal = (kGraphBottom - kGraphTop - kOffsetY) / kStepY;
    int howManyHorizontalk = 4;
    BFLog(@"horizontal lines:%d",howManyHorizontalk);
    for (int i = 0; i <howManyHorizontalk; i++)
    {
        BFLog(@"%f,%d",kkOffsetX1, kkGraphBottom1 - kkOffsetY1 - i * kkStepY1);
        CGContextMoveToPoint(context_front1, kkOffsetX1, kkGraphBottom1 - kkOffsetY1 - i * kkStepY1);
        //BFLog(@"%d,%f",kDefaultGraphWidth, kGraphBottom - kOffsetY - i * kStepY);
        //BFLog(@"------------------");
        CGContextAddLineToPoint(context_front1, kkDefaultGraphWidth1, kkGraphBottom1 - kkOffsetY1 - i * kkStepY1);
    }
    CGContextStrokePath(context_front1);
    CGContextSetLineDash(context_front1, 0, NULL, 0); // Remove the dash
    
    
    UIImage *image = [UIImage imageNamed:@"graph_green.png"];
    CGRect imageRect = CGRectMake(0, 0, screenWidth,117);
    CGContextDrawImage(context_front1, imageRect, image.CGImage);
    
   

    
  
    
    
    //if (flag4 != 0) {
    if ([dataarray2 count] > 0) {
        
        
        BFLog(@"updating the view....");
        [self drawLineGraphWithContext:context_front1];
        [self drawAll:context_front1];
        //flag4 = 0;
    }
    // }
}


@end
