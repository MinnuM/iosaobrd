//
//  insertView.m
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/19/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "insertView.h"
CGContextRef context_front3;
CGRect current_rectfront3;
NSString *newDateString3;
float kkStepX22;
int flag_in=0;
//minnu
int is_editing1=0;
//minnu
@implementation insertView
- (void)drawLineGraphWithContext:(CGContextRef)ctx
{
    CGRect screenRect=[[UIScreen mainScreen] bounds];
    CGFloat screenWidth =screenRect.size.width;
   // float kkDefaultGraphWidth2=screenWidth-screenWidth*(58.5/414);
    float kkOffsetX2=screenWidth*(31.25/414);
   
    CGContextSetLineWidth(ctx, 2.0);
    CGContextSetStrokeColorWithColor(ctx, [[UIColor yellowColor] CGColor]);
    
    //  if (flag==0) {
    
    // int maxGraphHeight = kGraphHeight - kOffsetY;
    //int y_origin=0;
    int y_offset=0;
    CGContextBeginPath(ctx);
    NSString *x1=dataarray4[0][1];
    if([x1  isEqual: @"ON"]){
        y_origin1 = 5;
        CGContextMoveToPoint(ctx, kkOffsetX2, ((y_origin1) * kkStepY2));
        float x = kkOffsetX2;
        //y_offset=23/1.6;
        float y = ((y_origin1) * kkStepY2)+y_offset;
        CGRect rect = CGRectMake(x - kkCircleRadius2, y - kkCircleRadius2, 2 * kkCircleRadius2, 2 * kkCircleRadius2);
        CGContextAddEllipseInRect(ctx, rect);
    }else  if([x1  isEqual: @"D"]){
        y_origin1 = 4;
        // y_offset=23/2.5;
        CGContextMoveToPoint(ctx, kkOffsetX2, ((y_origin1) * kkStepY2));
        float x = kkOffsetX2;
        float y = ((y_origin1) * kkStepY2);
        CGRect rect = CGRectMake(x - kkCircleRadius2, y - kkCircleRadius2, 2 * kkCircleRadius2, 2 * kkCircleRadius2);
        CGContextAddEllipseInRect(ctx, rect);
    }else  if([x1  isEqual: @"SB"]){
        y_origin1 = 3;
        y_offset = 23/8;
        float x = kkOffsetX2;
        float y = ((y_origin1) * kkStepY2)-23/8;
        CGRect rect = CGRectMake(x - kkCircleRadius2, y - kkCircleRadius2, 2 * kkCircleRadius2, 2 * kkCircleRadius2);
        CGContextAddEllipseInRect(ctx, rect);
        CGContextMoveToPoint(ctx, kkOffsetX2, ((y_origin1) * kkStepY2)-23/8);
        
    }else  if([x1  isEqual: @"OFF"]){
        y_origin1 = 2;
        float x = kkOffsetX2;
        float y = ((y_origin1) * kkStepY2)-23/4;
        CGRect rect = CGRectMake(x - kkCircleRadius2, y - kkCircleRadius2, 2 * kkCircleRadius2, 2 * kkCircleRadius2);
        CGContextAddEllipseInRect(ctx, rect);
        CGContextMoveToPoint(ctx, kkOffsetX2, ((y_origin1) * kkStepY2)-23/4);
    }
    int multiplier3=0;
  
    int data_size =(int) [dataarray4 count];
      for (int i = 0; i < data_size; i++)
    {
        int y_pos=0;
        NSArray *time = [dataarray4[i][0] componentsSeparatedByString:@":"];
        NSString *second_part=@"";
        int float_part =(int) [time[1] integerValue];
        if (float_part>=0 && float_part<15) {
            second_part=@"00";
            multiplier3=0;
        }else if (float_part>=15 && float_part <30)    {
            second_part = @"15";
            multiplier3=1;
        }else if (float_part>=30 && float_part<45){
            second_part=@"30";
            multiplier3=2;
            
        }else if (float_part>=45){
            second_part=@"45";
            multiplier3=3;
            
            
        }
        
       
     
        NSString *time_float=[NSString stringWithFormat:@"%@",time[0]];
        float x_one1 = [time_float floatValue];
        
        int y=0;
        NSString *x2 = dataarray4[i][1];
        if([x2  isEqual: @"ON"]){
            y_pos =5;
            // y_offset=23/1.6;
            y = ((y_pos) * kkStepY2);
            //NSString *posy = [NSString stringWithFormat:@"%d",y];
            //dataarray[i][1]=posy;
            
        }else  if([x2  isEqual: @"D"]){
            y_pos = 4;
            //  y_offset=23/2;
            y = ((y_pos) * kkStepY2);
            // NSString *posy = [NSString stringWithFormat:@"%d",y];
            //dataarray[i][1]=posy;             ;
        }else  if([x2  isEqual: @"SB"]){
            y_pos = 3;
            y_offset = 23/8;
            y = ((y_pos) *kkStepY2)-y_offset;
            //NSString *posy = [NSString stringWithFormat:@"%d",y];
            //dataarray[i][1]=posy;
            
        }else  if([x2  isEqual: @"OFF"]){
            y_pos = 2;
            y = ((y_pos) * kkStepY2)-23/4;
            //NSString *posy = [NSString stringWithFormat:@"%d",y];
            //dataarray[i][1]=posy;
        }
      
        float x = kkOffsetX2+(kkStepX22/4*(x_one1*4+multiplier3));
        
        CGContextAddLineToPoint(ctx, (x+kkCircleRadius2),y);
        
        float y1 = y;
        CGRect rect = CGRectMake(x - kkCircleRadius2, y1 - kkCircleRadius2, 2 * kkCircleRadius2, 2 * kkCircleRadius2);
        CGContextAddEllipseInRect(ctx, rect);
        CGContextSetFillColorWithColor(ctx, [[UIColor yellowColor] CGColor]);              }
    
    CGContextDrawPath(ctx, kCGPathStroke);
    CGContextSetFillColorWithColor(ctx, [[UIColor colorWithRed:1.0 green:0.5 blue:0 alpha:2.0] CGColor]);
    
    
    CGContextDrawPath(ctx, kCGPathFillStroke);
    
    
    
    
    //CGContextRetain(ctx);
    UIGraphicsPushContext(ctx);
    CGContextStrokePath(ctx);
    
    
    
}

- (void)drawRect:(CGRect)rect {
    CGRect screenRect=[[UIScreen mainScreen] bounds];
    CGFloat screenWidth =screenRect.size.width;
    float kkDefaultGraphWidth2=screenWidth-screenWidth*(58.5/414);
    float kkOffsetX2=screenWidth*(31.25/414);
    //BFLog(@"view refreshingg....:%@",NSStringFromCGRect(rect));
    current_rectfront3 = CGRectFromString(NSStringFromCGRect(rect));
    //flag=flag1;
    context_front3 = UIGraphicsGetCurrentContext();
    //current_context = UIGraphicsGetCurrentContext();
    //BFLog(@"current context:%@",context_front3);
    CGContextSetLineWidth(context_front3, 0.6);
    CGContextSetStrokeColorWithColor(context_front3, [[UIColor lightGrayColor] CGColor]);
    //CGFloat dash[] = {2.0, 2.0};
    //CGContextSetLineDash(context, 0.0, dash, 2);    // How many lines?
    //int howManyk = (kkDefaultGraphWidth2 - kkOffsetX2) / kkStepX22;
    int howManyk=24;
    kkStepX22=kkDefaultGraphWidth2/howManyk;
      for (int i = 0; i <= howManyk; i++)
    {
        CGContextMoveToPoint(context_front3, kkOffsetX2 + i * kkStepX22, kkGraphTop2);
        CGContextAddLineToPoint(context_front3, kkOffsetX2 + i * kkStepX22, kkGraphBottom2);
    }
    //int howManyHorizontal = (kGraphBottom - kGraphTop - kOffsetY) / kStepY;
    int howManyHorizontalk = 4;
      for (int i = 0; i <howManyHorizontalk; i++)
    {
       
        CGContextMoveToPoint(context_front3, kkOffsetX2, kkGraphBottom2 - kkOffsetY2 - i * kkStepY2);
       
        CGContextAddLineToPoint(context_front3, kkDefaultGraphWidth2, kkGraphBottom2 - kkOffsetY2 - i * kkStepY2);
    }
    CGContextStrokePath(context_front3);
    CGContextSetLineDash(context_front3, 0, NULL, 0); // Remove the dash
    
    
    UIImage *image = [UIImage imageNamed:@"graph_green.png"];
    //minnu
    
    CGRect imageRect = CGRectMake(0, 0,screenWidth,100);
    CGContextDrawImage(context_front3, imageRect, image.CGImage);
  
        [self drawLineGraphWithContext:context_front3];
    if (is_editing1==1) {
        [self drawNewLine:context_front3];
        
        is_editing1=0;
        
    }
    
        flag_in = 0;
    
}
//minnu

-(void)drawNewLine:(CGContextRef)ctx{
    CGRect screenRect=[[UIScreen mainScreen] bounds];
    CGFloat screenWidth =screenRect.size.width;
    float kkOffsetX2=screenWidth*(31.25/414);
     CGContextSetLineWidth(ctx, 2.0);
    CGContextSetStrokeColorWithColor(ctx, [[UIColor cyanColor] CGColor]);
    
   
    int y_offset=0;
    CGContextBeginPath(ctx);
    NSString *x1=newline[0][1];
    if([x1  isEqual: @"ON"]){
        y_origin1 = 5;
        CGContextMoveToPoint(ctx, kkOffsetX2, ((y_origin1) * kkStepY2));
        float x = kkOffsetX2;
        float y = ((y_origin1) * kkStepY2);
        CGRect rect = CGRectMake(x - kkCircleRadius2, y - kkCircleRadius2, 2 * kkCircleRadius2, 2 * kkCircleRadius2);
        CGContextAddEllipseInRect(ctx, rect);
    }else  if([x1  isEqual: @"D"]){
        y_origin1 = 4;
        CGContextMoveToPoint(ctx, kkOffsetX2, ((y_origin1) * kkStepY2));
        float x = kkOffsetX2;
        float y = ((y_origin1) * kkStepY2);
        CGRect rect = CGRectMake(x - kkCircleRadius2, y - kkCircleRadius2, 2 * kkCircleRadius2, 2 * kkCircleRadius2);
        CGContextAddEllipseInRect(ctx, rect);
    }else  if([x1  isEqual: @"SB"]){
        y_origin1 = 3;
        y_offset = 23;
        float x = kkOffsetX2;
        float y = ((y_origin1) * kkStepY2)-23/8;
        CGRect rect = CGRectMake(x - kkCircleRadius2, y - kkCircleRadius2, 2 * kkCircleRadius2, 2 * kkCircleRadius2);
        CGContextAddEllipseInRect(ctx, rect);
        CGContextMoveToPoint(ctx, kkOffsetX2, ((y_origin1) * kkStepY2)-23/8);
        
    }else  if([x1  isEqual: @"OFF"]){
        y_origin1 = 2;
        float x = kkOffsetX2;
        float y = ((y_origin1) * kkStepY2)-23/4;
        CGRect rect = CGRectMake(x - kkCircleRadius2, y - kkCircleRadius2, 2 * kkCircleRadius2, 2 * kkCircleRadius2);
        CGContextAddEllipseInRect(ctx, rect);
        CGContextMoveToPoint(ctx, kkOffsetX2, ((y_origin1) * kkStepY2)-23/4);
    }
    int multiplier1 = 0;
  //  BFLog(@"y origin:%d",y_origin1);
    int data_size =(int) [newline count];
    //BFLog(@"array size:%d",data_size);
    for (int i = 0; i < data_size; i++)
    {
        int y_pos=0;
        NSArray *time = [newline[i][0] componentsSeparatedByString:@":"];
        NSString *second_part=@"";
        int float_part =(int) [time[1] integerValue];
        if (float_part>=0 && float_part<15) {
            second_part=@"00";
            multiplier1=0;
        }else if (float_part>=15 && float_part <30)    {
            second_part = @"15";
            multiplier1=1;
        }else if (float_part>=30 && float_part<45){
            second_part=@"30";
            multiplier1=2;
            
        }else if (float_part>=45){
            second_part=@"45";
            multiplier1=3;
            
            
        }
        
       
            NSString *time_float=[NSString stringWithFormat:@"%@",time[0]];
        float x_one1 = [time_float floatValue];
        int y=0;
        NSString *x2 = newline[i][1];
        if([x2  isEqual: @"ON"]){
            y_pos =5;
            y = ((y_pos) * kkStepY2);
            
        }else  if([x2  isEqual: @"D"]){
            y_pos = 4;
            y = ((y_pos) * kkStepY2);
                       ;
        }else  if([x2  isEqual: @"SB"]){
            y_pos = 3;
            y_offset = 23;
            y = ((y_pos) *kkStepY2)-23/8;
            //NSString *posy = [NSString stringWithFormat:@"%d",y];
            //dataarray[i][1]=posy;
            
        }else  if([x2  isEqual: @"OFF"]){
            y_pos = 2;
            y = ((y_pos) * kkStepY2)-23/4;
            //NSString *posy = [NSString stringWithFormat:@"%d",y];
            //dataarray[i][1]=posy;
        }
        //BFLog(@"x,y:%f,%d",x_one,y);
        float x = kkOffsetX2+(kkStepX22/4*(x_one1*4+multiplier1));
        
        CGContextAddLineToPoint(ctx, (x+kkCircleRadius2),y);
           }
    //BFLog(@"new array with new y pos%@",dataarray5);
    CGContextDrawPath(ctx, kCGPathStroke);
    CGContextSetFillColorWithColor(ctx, [[UIColor cyanColor] CGColor]);
    
    
       CGContextDrawPath(ctx, kCGPathFillStroke);
    
    
    
    
    //CGContextRetain(ctx);
    UIGraphicsPushContext(ctx);
    CGContextStrokePath(ctx);
    
}

@end
