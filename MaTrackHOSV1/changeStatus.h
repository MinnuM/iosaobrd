//
//  changeStatus.h
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/18/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "firstGraphView.h"
#import <CoreLocation/CoreLocation.h>
#import "logScreenViewController.h"
#import "DBManager1.h"
#import "RNActivityView.h"
#import "menuScreenViewController.h"
#import "loggraph.h"


extern int flag;

@interface changeStatus : UIViewController<CLLocationManagerDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
   CLLocationManager *locationManager;    
}
@property (nonatomic, strong) DBManager1 *dbManager;
@property (weak, nonatomic) IBOutlet UITableView *contentTable;
- (IBAction)SaveBtn:(id)sender;


@property (weak, nonatomic) IBOutlet UITextField *location;
@property (nonatomic , strong) CLLocationManager *locationManager;
@end
