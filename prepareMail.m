//
//  prepareMail.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/21/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "prepareMail.h"
#import "Header.h"
#import "commonDataHandler.h"
#import "MobileCheckIn.h"

@interface prepareMail ()

@end

@implementation prepareMail
@synthesize toReceipient;
@synthesize ccReceipient;
@synthesize subject;

- (void)viewDidLoad {
    [super viewDidLoad];
    //minnu
    UITapGestureRecognizer *backgroundTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backgroundTap)];
    [self.view addGestureRecognizer:backgroundTapped];
    //minnu
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
//minnu

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    //(@"entered return key");
    return YES;
    
}

- (void)backgroundTap {
    [self.view endEditing:YES];
    //(@"Touched Inside");
}
//minnu

- (IBAction)send:(id)sender {
    NSInteger count=0;
    NSInteger statusTo=0;
    NSInteger statusFrom=0;
    NSInteger statusSubject=0;
    NSString *emailTitle =subject.text;
    NSString *messageBody = @"Hi, PFA!";
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    if ([emailTest evaluateWithObject:toReceipient.text] == NO) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Valid Email Address." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
       
        
        
    }else{
        
        statusTo=1;
        
    }
    if([emailTest evaluateWithObject:ccReceipient.text] == NO){
        
        
        if (ccReceipient.text && ccReceipient.text.length>0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention!" message:@"Please Enter Valid Email Address." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else{
            count=1;
             statusFrom=1;
            
        }
    }else{
        statusFrom=1;
        
        
    }
    
    if (emailTitle.length>0) {
        statusSubject=1;
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention!" message:@"Subject field is mandatory." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];

    }
    
        
        NSArray *toRecipents;
        if (count==1) {
            toRecipents = [NSArray arrayWithObjects:toReceipient.text,nil];

        }else{
            toRecipents = [NSArray arrayWithObjects:toReceipient.text,ccReceipient.text, nil];

        }
    if (statusTo==1 && statusFrom==1 && statusSubject==1) {
        
    
    if ([MFMailComposeViewController canSendMail]) {
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Determine the file name and extension
    NSArray *filepart = [filePathsArray[selected_fileIndex] componentsSeparatedByString:@"."];
    NSString *filename = [filepart objectAtIndex:0];
    NSString *extension = [filepart objectAtIndex:1];
         NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
         NSString *documentsDirectory = [paths objectAtIndex:0];

    // Get the resource path and read the file using NSData
    NSString *filePath = [documentsDirectory stringByAppendingString:[NSString stringWithFormat:@"/%@",filePathsArray[selected_fileIndex]]];
    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    
    // Determine the MIME type
    NSString *mimeType;
    if ([extension isEqualToString:@"jpg"]) {
        mimeType = @"image/jpeg";
    } else if ([extension isEqualToString:@"png"]) {
        mimeType = @"image/png";
    } else if ([extension isEqualToString:@"doc"]) {
        mimeType = @"application/msword";
    } else if ([extension isEqualToString:@"ppt"]) {
        mimeType = @"application/vnd.ms-powerpoint";
    } else if ([extension isEqualToString:@"html"]) {
        mimeType = @"text/html";
    } else if ([extension isEqualToString:@"pdf"]) {
        mimeType = @"application/pdf";
    }
    
    // Add attachment
    [mc addAttachmentData:fileData mimeType:mimeType fileName:filename];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
     }else{
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device cann't send emails!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }
    }else{
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention!" message:@"Please check all the fields." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];

    }
    
    
}
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
        {
            //BFLog(@"Mail cancelled");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Mail cancelled!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
          
            [alert show];
            break;
        }
        case MFMailComposeResultSaved:{
            //(@"Mail saved");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Mail saved!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case MFMailComposeResultSent:{
            //(@"Mail sent");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Mail sent!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case MFMailComposeResultFailed:{
            //(@"Mail sent failure: %@", [error localizedDescription]);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Mail sent failure,Please try again!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
                        break;
        }
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
#pragma mark    - <send email API call>
-(void)sendEmailAPICall{
    
}


@end
