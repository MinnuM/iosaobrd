//
//  DVIR_Main.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/12/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCFloatingActionButton.h"
#import "previousDayViewController.h"
#import "DBManager1.h"
extern NSInteger editedVehicleDefects;
extern NSInteger editedTrailerDefects;
extern NSMutableArray *expandedCells;
extern NSMutableArray *defectsDummy;
extern NSMutableArray *expandedCellsTrailers;
extern NSMutableArray *defectsDummyTrailers;
extern NSMutableDictionary *selectedDVIR;
extern int selectedDVIRID;
extern NSInteger newDVIRID;
extern NSInteger DVIRID;
extern int editSection;
extern int action;
extern int reportcount;
extern CGRect toolFrame;
@interface DVIR_Main : UIViewController<floatMenuDelegate,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scroller_report;
@property (weak, nonatomic) IBOutlet UITableView *reports_list;
//@property (strong, nonatomic) VCFloatingActionButton *addButton;
@property (nonatomic, strong) DBManager1 *dbManager;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addBtn;

@property (weak, nonatomic) IBOutlet UIToolbar *toolDvir;
@end
