//
//  NSString+NullCheck.m
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/27/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "NSString+NullCheck.h"

@implementation NSString (NullCheck)
-(id)NullCheck{
    if(
    self == nil || self == NULL || [@"" isEqualToString:self] || [[self stringByReplacingOccurrencesOfString:@" " withString:@""] length] == 0||
    [self isEqualToString:@"(null)"] || ([self respondsToSelector:@selector(length)] && [(NSData *) self length] == 0)|| ([self respondsToSelector:@selector(count)] && [(NSArray *) self count] == 0) || [[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0)
        return @"";
    else
        return self;
}
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
@end
