//
//  commonDataHandler.m
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/27/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "commonDataHandler.h"
#import "AppDelegate.h"
#import "Header.h"
#import "LoginResponse.h"
@implementation commonDataHandler
+ (commonDataHandler *)sharedInstance
{
    static commonDataHandler *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[commonDataHandler alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}
-(NSString*)constructInput:(NSMutableDictionary*)listValues{
    NSString *str = @"";
    if(listValues !=nil && [listValues count]!=0){
        for (int i=0; i<[listValues count]; i++) {
            if(i!=0){
                
                str = [str stringByAppendingString:@"###"];
                
            }
            NSString *currentKey = [listValues allKeys][i];
            NSString *currentValue = [listValues allValues][i];
            str = [str stringByAppendingString:currentKey];
            str = [str stringByAppendingString:@"::"];
            str = [str stringByAppendingString:currentValue];
        }
    }
    return str;
    
}
-(NSMutableArray*)parseResponse:(NSString *)responseString{
    
    NSMutableArray *responsArray = [[NSMutableArray alloc] init];
    if (responseString!=nil && [responseString length]!=0) {
        if([responseString containsString:@"==="]){
            [responsArray removeAllObjects];
            NSArray *arrayDict = [responseString componentsSeparatedByString:@"==="];
            for (int i = 0; i < arrayDict.count; i++) {
                if ([arrayDict[i] containsString:@"###"]) {
                    NSArray *stringValues = [arrayDict[i] componentsSeparatedByString:@"###"];
                     NSMutableDictionary *keyList = [[NSMutableDictionary alloc] init];
                    for (NSString *values in stringValues) {
                      
                        if([values containsString:@"::"]){
                            NSArray *data = [values componentsSeparatedByString:@"::"];
                            if(data!=nil && [data count]==2){
                                [keyList setValue:data[1] forKey:data[0]];
                            }
                        }
                        
                    }
                    [responsArray addObject:keyList];
                }
                
            }
        }else{
            if ([responseString containsString:@"###"]) {
                NSArray *stringValues = [responseString componentsSeparatedByString:@"###"];
                NSMutableDictionary *keyList = [[NSMutableDictionary alloc] init];
                for (NSString *values in stringValues) {
                    if([values containsString:@"::"]){
                        NSArray *data = [values componentsSeparatedByString:@"::"];
                        if(data!=nil && [data count]==2){
                            [keyList setValue:data[1] forKey:data[0]];
                        }
                    }
                }
                [responsArray addObject:keyList];
            }
        }
    }
    return responsArray;
}
-(void)reAuthenticateUser:(void (^)(BOOL finished))completion{
    NSString *strUserName = [credentials objectForKey:(id)kSecAttrAccount];
    NSString *strPassword = [credentials objectForKey:(id)kSecValueData];
    if (([strPassword length] != 0) && [strUserName length] != 0)
    {
        //reauth
        NSString *pushId = [[NSUserDefaults standardUserDefaults] valueForKey:@"pushIdentifier"];
        NSMutableDictionary *keyList = [[NSMutableDictionary alloc] init];
        [keyList setValue:@"login" forKey:OPERATION];
        [keyList setValue:strUserName forKey:USER_NAME];
        [keyList setValue:strPassword forKey:@"password"];
        [keyList setValue:[Bugfender deviceIdentifier] forKey:@"mobileidentifier"];
        [keyList setValue:@"ios" forKey:@"useragent"];
        [keyList setValue:pushId forKey:@"pushidentifier"];
     //   LoginClass *obj = [LoginClass new];
        NSString *stringParams = [self constructInput:keyList];
        NSString *urlString = [NSString stringWithFormat:@"%@%@", WEB_ROOT, LOGIN_URL];

        __block NSString *responseString = @"";
        NSString *urlStringWithParam = [NSString stringWithFormat:@"payload=%@",stringParams];
        // NSLog(NSString * _Nonnull format, ...)
        NSString *enodedURl = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:enodedURl] ;
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];

        [request setHTTPBody:[urlStringWithParam dataUsingEncoding:NSUTF8StringEncoding]];
        NSOperationQueue *queue1 = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:request queue:queue1 completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
            responseString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            if(![responseString isEqualToString:@""]){
                NSMutableDictionary *keyListValues = [[NSMutableDictionary alloc] init];
                LoginResponse *obj1 = [LoginResponse new];
                keyListValues =[obj1 parseResponse:responseString];
                if(keyListValues!=nil && [keyListValues count]!=0){
                    for (int j=0; j<[keyListValues count]; j++) {
                        NSString *currentkey = [keyListValues allKeys][j];
                        NSString *currentValue = [keyListValues allValues][j];
                        if([currentkey isEqualToString:@"result"]){
                            [obj1 setResult:currentValue];
                        }
                        if([currentkey isEqualToString:@"authcode"]){
                            [obj1 setAuthCode:currentValue];
                            [commonDataHandler sharedInstance].strAuthCode = currentValue;
                        }
                        if([currentkey isEqualToString:@"reason"]){
                            [obj1 setReason:currentValue];
                        }
                    }//for loop
                }
            }
        }];
        if (completion) {
            completion(YES);
        }
    }
}
-(NSString *)getCurrentTimeInLocalTimeZone{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/YYY"];
   // [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSDictionary*token;NSMutableDictionary *whereToken;
    NSString* timezone;
    DBManager *objDB = [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
        token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        NSArray * value=[[NSArray alloc]initWithArray:[objDB getlogSettings:whereToken]];
        if([value count]!=0){
            timezone=value[0][2];
            
            if([timezone isEqualToString:@""])
                [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            else
                [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:timezone]];
        }
        else
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    }
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

-(NSDate *)getCurrentTimeForPickerInLocalTimeZone{
    NSDate* sourceDate = [NSDate date];
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    NSDictionary*token;NSMutableDictionary *whereToken;
    NSString* timezone;
    DBManager *objDB = [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    NSTimeZone* destinationTimeZone;
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
        token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        NSArray * value=[[NSArray alloc]initWithArray:[objDB getlogSettings:whereToken]];
        if([value count]!=0){
            timezone=value[0][2];
            if(![timezone isEqualToString:@""]){
                destinationTimeZone = [NSTimeZone timeZoneWithAbbreviation:timezone];
            }
            else
                destinationTimeZone = [NSTimeZone localTimeZone];
        }
        else
               destinationTimeZone = [NSTimeZone localTimeZone];
    }
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    NSDate* destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
    return destinationDate;
}
-(NSString *)getTimeDifferenceForGMT{
    
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm"];
//    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation: @"PST"]];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *gmtDateString = [dateFormatter stringFromDate:[NSDate date]];
    
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"dd/MM/yyyy HH:mm"];
    df.timeZone = [NSTimeZone localTimeZone];
//    df.timeZone = [NSTimeZone timeZoneWithAbbreviation: @"PST"];
    NSDate *date = [df dateFromString:gmtDateString];
    
    NSDateFormatter *dfGMT = [NSDateFormatter new];
    [dfGMT setDateFormat:@"dd/MM/yyyy HH:mm"];
    dfGMT.timeZone = [NSTimeZone timeZoneWithAbbreviation: @"GMT"];
    NSDate *dateGMT = [dfGMT dateFromString:gmtDateString];
    
    
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSUInteger unitFlags = NSCalendarUnitSecond | NSCalendarUnitMinute | NSCalendarUnitHour;
    NSDateComponents *components = [gregorian components:unitFlags
                                                fromDate:date
                                                  toDate:dateGMT options:0];
    NSInteger seconds = [components second];
    NSInteger minutes = [components minute];
    NSInteger hours = [components hour];
    NSString *strTimediff;
    if(seconds> 0 || minutes>0)
    strTimediff = [NSString stringWithFormat:@"%ld.%ld", hours, (minutes+seconds)];
    else
        strTimediff = [NSString stringWithFormat:@"%ld", hours];
    
    return strTimediff;
    
}


//calculated status
-(NSString *)calculateStatus:(NSString *)today vehicleStatus:(NSString*)vehicleStatus driverStaus:(NSString *)driverStatus associatedVehicle:(NSString *)associatedVehicle{
    NSString *calculatedStatus=@"1";
    if([today isEqualToString:@"1"])//for current day
    {
        if([driverStatus isEqualToString:@"1"]){
            if([vehicleStatus isEqualToString:@"1"])
                calculatedStatus=@"1";
            else if([vehicleStatus isEqualToString:@"2"])
                calculatedStatus=@"4";
            else if([vehicleStatus isEqualToString:@"3"]){
                if([associatedVehicle isEqualToString:@""])
                    calculatedStatus=@"1";
                else
                    calculatedStatus=@"3";
            }
            
        }
        
        else if([driverStatus isEqualToString:@"2"]){
            calculatedStatus=@"2";
        }
        else if([driverStatus isEqualToString:@"3"]){
            if([vehicleStatus isEqualToString:@"3"])
                calculatedStatus=@"3";
            else if([vehicleStatus isEqualToString:@"2"])
                calculatedStatus=@"2";
            else if([vehicleStatus isEqualToString:@"1"])
                calculatedStatus=@"1";
        }
        else if([driverStatus isEqualToString:@"4"]){
            if([vehicleStatus isEqualToString:@"2"])
                calculatedStatus=@"4";
            else if([vehicleStatus isEqualToString:@"1"])
                calculatedStatus=@"1";
            else if([vehicleStatus isEqualToString:@"3"])
                calculatedStatus=@"3";
        }
        else if([driverStatus isEqualToString:@"9"]){
            calculatedStatus=@"9";
        }
        else if([driverStatus isEqualToString:@"A"]){
            if([vehicleStatus isEqualToString:@"1"])
                calculatedStatus=@"A";
        }
        else if([driverStatus isEqualToString:@"B"]){
            calculatedStatus=@"B";
        }
        else if([driverStatus isEqualToString:@"C"]){
            calculatedStatus=@"C";
        }
    }
    else{
        if([driverStatus isEqualToString:@"5"]){
            calculatedStatus=@"5";
        }
        else if([driverStatus isEqualToString:@"6"]){
            calculatedStatus=@"6";
        }
        if([driverStatus isEqualToString:@"7"]){
            calculatedStatus=@"7";
        }
        if([driverStatus isEqualToString:@"8"]){
            calculatedStatus=@"8";
        }
    }
    return calculatedStatus;
}

-(int)getSlotCount{
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSDictionary*token;NSMutableDictionary *whereToken;
    NSString* timezone;
    DBManager *objDB = [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
        token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        NSArray * value=[[NSArray alloc]initWithArray:[objDB getlogSettings:whereToken]];
        if([value count]!=0){
            timezone=value[0][2];
            if([timezone isEqualToString:@""])
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            else
                [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:timezone]];
        }
        else
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    }
    NSString *dateString = [dateFormatter stringFromDate:date];
    NSArray *dateArr=[dateString componentsSeparatedByString:@":"];
    NSString *dateStr=@"";
    if([dateArr[1] intValue]>=0 && [dateArr[1] intValue]<15){
        dateStr=[NSString stringWithFormat:@"%@:00",dateArr[0]];
    }
    else if([dateArr[1] intValue]>=15 && [dateArr[1] intValue]<30){
        dateStr=[NSString stringWithFormat:@"%@:15",dateArr[0]];
    }
    else if([dateArr[1] intValue]>=30 && [dateArr[1] intValue]<45){
        dateStr=[NSString stringWithFormat:@"%@:30",dateArr[0]];
    }
    else if([dateArr[1] intValue]>=45){
        dateStr=[NSString stringWithFormat:@"%@:45",dateArr[0]];
    }

    NSString *init=@"0:0";
    NSArray *initTime=[init componentsSeparatedByString:@":"];
    NSArray *endTime=[dateString componentsSeparatedByString:@":"];
    int duration=([endTime[0]intValue]*60+[endTime[1] intValue])-([initTime[0]intValue]*60+[initTime[1] intValue]);
    int slot=(int)duration/15;
    //slot=slot+1;
    return slot;
    
}

-(NSString*)modifyData:(NSString *)statusString date:(NSString*)date{

if([date  isEqualToString:[self getCurrentTimeInLocalTimeZone]]){
    if(statusString.length<96)
    statusString=[statusString stringByPaddingToLength:[self getSlotCount] withString:@"0" startingAtIndex:0];
    else
        if((int)([self getSlotCount]!=96)){
        statusString=[statusString substringToIndex:(int)([self getSlotCount])];
     NSString *temp=[@"" stringByPaddingToLength:(int)(96-[self getSlotCount]) withString:@"0" startingAtIndex:0];
    //NSRange pos=[statusString rangeOfString:@"0"];
    NSString *statusStringTemp=[statusString stringByReplacingOccurrencesOfString:@"0" withString:@""];
    NSString *prevStatus=[statusStringTemp substringFromIndex:(int)(statusStringTemp.length-1)];
    statusString=[statusString stringByReplacingOccurrencesOfString:@"0" withString:prevStatus];
    statusString=[NSString stringWithFormat:@"%@%@",statusString,temp];
        }
   }
                 return statusString;
}

@end
