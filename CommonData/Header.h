//
//  Header.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/26/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//
#ifndef Header_h
#define Header_h
#define WEB_ROOT                 @"http://54.164.84.102/client"
#define LOGIN_URL                @"/login.php"
#define MOBILE_CHECK_IN_URL      @"/checkin.php"
#define MOBILE_CHECK_IN_ACK_URL  @"/ack.php"
#define VEHICLE_SELECTION_URL    @"/selectVehicle.php"
#define SAVE_SIGNATURE_URL       @"/saveSignature.php"
#define SEND_SIGNED_DOT_URL      @"/sendDotInspectionReport.php"
#define SEND_EMAIL_URL           @"/sendEmail.php"
#define POSSIBLE_VEHICLE_LIST_URL @"/getPossibleVehicle.php"
#define RESET_PASSWORD_URL       @"/resetPassword.php"
#define LOGOUT_URL               @"/logout.php"

          //***  API KEYS    ***//
#define OPERATION           @"operation"
#define RESULT              @"result"
#define REASON              @"reason"
#define USER_NAME           @"username"
#define DEVICE_ID           @"mobileidentifier"//@"deviceidentifier"
#define AUTH_CODE           @"authcode"
#define AUTH_CODE_INVALID   @"authcodeinvalid"

//VEHICLE_SELECT
#define OPERATION_VEHICLE   @"vehicleselected"
#define SELECTED_VEHICLE    @"selectedvehicle"

//POSSIBLE VEHICLE LIST
#define OPERATION_POSSIBLE_VEHICLE @"possiblevehicle"
#define POSSIBLE_VEHICLE    @"vehiclelist"

//RESET PASSWORD
#define OPERATION_RESET_PASSWORD    @"resetpassword"
#define OLD_PASSWORD                @"oldpassword"
#define NEW_PASSWORD                @"newpassword"
//MOBILE_CHECKIN
#define OPERATION_CHECKIN         @"checkin"
#define RESPONSE_DATE             @"date"
#define VEHICLE_STATUS            @"vehiclestatus"
#define ASSO_VEHICLE              @"associatedvehicle"
#define ODO_NUMBER                @"odo"
#define SLOT                      @"slot"
#define ACK                       @"ack"
#define OPERATION_LOG             @"updateloggraph"
#define OPERATION_SUGGESTION      @"suggestion"
#define SUGGESTION_REPEAT         @"repeat"
#define SUGGESTION_STATUS         @"status"
#define SUGGESTION_BY             @"suggestedby"

//ACKNOWLEDGE
#define OPERATION_ACK             @"checkinacknowledgement"


//SAVE SIGNATURE
#define OPERATION_SAVE_SIGN       @"savesignature"
#define SIGNATURE                 @"signature"

//SEND SIGNED DOT INSPECTION
#define OPERATION_DOT             @"dotinspectionreport"
#define IS_APPROVED               @"isapproved"
#define XML_TEMPLATE              @"xmltemplate"
#define DATE_IN_CURRENT_TZ        @"dateincurrenttimezone"

//SEND EMAIL
#define OPERATION_SEND_EMAIL      @"sendemail"
#define TO_ADDRESS                @"toaddress"
#define CC_ADDRESS                @"ccaddress"
#define TIME_DIFFERENCE           @"timediff"
#define OPTIONAL_TEXT             @"optionaltext"
//LOGOUT
#define OPERATION_LOGOUT @"logout"

#endif /* Header_h */
