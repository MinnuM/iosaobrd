//
//  Trailer_Defects.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/12/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

extern int trailer1;
@interface Trailer_Defects : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>{
    
    UITextField *notes;
    
}
- (IBAction)doneBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scroller_trailer;

@property (weak, nonatomic) IBOutlet UITableView *trilerDefects;
@end
