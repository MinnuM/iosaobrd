//
//  drawProgressBar.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/25/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "drawProgressBar.h"

@implementation drawProgressBar


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
        
    /* Draw a circle */
    // Get the contextRef
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    
    // Set the border width
    CGContextSetLineWidth(contextRef, 1.0);
    
    // Set the circle fill color to GREEN
    CGContextSetRGBFillColor(contextRef, 0.0, 255.0, 0.0, 1.0);
    
    // Set the cicle border color to BLUE
    CGContextSetRGBStrokeColor(contextRef, 0.0, 0.0, 255.0, 1.0);
    
    // Fill the circle with the fill color
    CGContextFillEllipseInRect(contextRef, rect);
    
    // Draw the circle border
    CGContextStrokeEllipseInRect(contextRef, rect);}


@end
