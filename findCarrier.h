//
//  findCarrier.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/21/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "companyConnection.h"


@interface findCarrier : UIViewController<UITableViewDataSource,UITableViewDelegate>


@property (weak, nonatomic) IBOutlet UITableView *carrier_List;
@end
