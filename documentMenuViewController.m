//
//  documentMenuViewController.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/24/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "documentMenuViewController.h"
NSString *selectedFile;
NSString *filenameSelected;
@interface documentMenuViewController ()

@end

@implementation documentMenuViewController
@synthesize selectView;
@synthesize myfiles;
@synthesize savedFiles;
- (void)viewDidLoad {
    [super viewDidLoad];
    myfiles.alpha=1.0;
    savedFiles.alpha=0.0;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)changeView:(id)sender {
    if(selectView.selectedSegmentIndex==0){
        
        myfiles.alpha=1.0;
        savedFiles.alpha=0.0;
    }else{
        
        myfiles.alpha=0.0;
        savedFiles.alpha=1.0;
    }
}
@end
