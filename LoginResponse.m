//
//  LoginResponse.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 10/23/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "LoginResponse.h"

@implementation LoginResponse
@synthesize result,authCode,reason;
-(NSMutableDictionary*)parseResponse:(NSString *)responseString{

    NSMutableDictionary *keyList = [[NSMutableDictionary alloc] init];
    if (responseString!=nil && [responseString length]!=0) {
        if ([responseString containsString:@"###"]) {
            
            NSArray *stringValues = [responseString componentsSeparatedByString:@"###"];
            for (NSString *values in stringValues) {
                if([values containsString:@"::"]){
                    
                    NSArray *data = [values componentsSeparatedByString:@"::"];
                    
                    if(data!=nil && [data count]==2){
                        
                        [keyList setValue:data[1] forKey:data[0]];
                        
                    }
                    
                }
                
            }
            
        }
        
        
        
    }
    
    
    
    return keyList;
    
}

@end
