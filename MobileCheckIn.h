//
//  MobileCheckIn.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/27/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MobileCheckIn : NSObject
@property (nonatomic, strong) NSString *operation;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *vehiclestatus;
@property (nonatomic, strong) NSString *associatedvehicle;
@property (nonatomic, strong) NSString *odo;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *deviceidentifier;
@property (nonatomic, strong) NSString *slot;
@property (nonatomic, strong) NSString *ack;
@property (nonatomic, strong) NSString *result;
@property (nonatomic, strong) NSString *reason;
@property (nonatomic, strong) NSString *authCode;
@property (nonatomic, strong) NSString *calculatedstatus;
@property (nonatomic, strong) NSString *driverstatus;
-(void)getDataFromAPI:(NSMutableDictionary *)responseDict;
- (void)getDataFromAPILogin:(NSMutableDictionary *)responseDict;
@end
