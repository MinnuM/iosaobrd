//
//  formGeneral.h
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 24/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface formGeneral : NSObject
@property (nonatomic, strong) NSString *userid;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *vehicle;
@property (nonatomic, strong) NSString *trailer;
@property (nonatomic, strong) NSString *distance;
@property (nonatomic, strong) NSString *shippingdoc;
@property (nonatomic, strong) NSString *firstname;
@property (nonatomic, strong) NSString *lastname;
@property (nonatomic, strong) NSString *driverid;

@end
