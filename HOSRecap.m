//
//  HOSRecap.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 8/2/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "HOSRecap.h"
#import "LogSettings.h"
#import "menuScreenViewController.h"
#import "MBProgressHUD.h"
NSMutableArray *dates_dict;
NSString *day1;
NSString *totalworking=@"";
NSString *remaing=@"";
float drive_total=0.0;
float total_work_7=0.0;
float break_time=0.0;
float brk_hr1=0.0;
int numCycleStop1=0;
NSInteger cycleRestart =0;
float restart_hr1=0.0;
BOOL restart_found1=false;
NSArray *previousdates;
NSMutableArray *total_work;
int numCycles1=0;
UIView *transparentView_hosrecap;
NSArray *total_work1;
float cycle_tym1=0.0;
int brkReq=0;
@interface HOSRecap ()


@end

@implementation HOSRecap
@synthesize dbManager;
@synthesize menu_hosrecap;

- (void)viewDidLoad {
    [super viewDidLoad];
    NSDate *now=[NSDate date];
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    DBManager *objDB = [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    NSString* timezone;
    NSDictionary*token;NSMutableDictionary *whereToken;
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
        token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        NSArray *settArr=[[NSArray alloc] initWithArray:[objDB getlogSettings:whereToken]];
        if([settArr count]!=0){
            timezone=settArr[0][2];
            if(![timezone isEqualToString:@""])
              [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:timezone]];
            else
                [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
        }
        else
            [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
    }
    NSString *today=[dateFormat stringFromDate:now];
    [self getRecap:today];
    _recap_table.delegate=self;
    _recap_table.dataSource=self;
    [_scroller_recap setScrollEnabled:YES];
    [_scroller_recap setContentSize:(CGSizeMake(600, 1000))];
    CGPoint bottomOffset = CGPointMake(0, 0);
    [_scroller_recap setContentOffset:bottomOffset animated:YES];
    
    
    
}

-(void)getRecap:(NSString *)dateNew{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        });
        drive_total=0.0;
        break_time =0.0;
        total_work_7 = 0.0;
        //minnu
        NSMutableArray *previousdatesTemp;
        DBManager *objDB = [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
        NSDictionary *token;
        NSMutableDictionary *whereToken;
        NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
        if([userInfoArray count]!=0){
            int userId = [userInfoArray[0][0] intValue];
            token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
            ;
            whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
            NSArray *timezoneCycle=[[NSArray alloc]initWithArray:[objDB getlogSettings:whereToken]];
            //        NSLog(@"timezon settings:%@",timezoneCycle);
            
            restart_hr1=[timezoneCycle[0][3] floatValue];//minnu 34/24
            cycleinfo=timezoneCycle[0][1];
            if([cycleinfo isEqualToString:@"USA 70 hours/8 days"]||[cycleinfo isEqualToString:@"California 80 hours/8 days"]){
                numCycles1=8;
                cycle_tym1=3470.00;//posx value of 70hr
                
            }else{
                numCycles1=7;
                cycle_tym1=2980.00;//posx value for 60hr
            }
            //cycle form db
            dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
            
            dates_dict = [[NSMutableArray alloc] init];
            total_work = [[NSMutableArray alloc] init];
            total_work1 = [[NSArray alloc] init];
            //NSDate * now1 = [NSDate date];
            
            NSString *query0 = [NSString stringWithFormat:@"select date,data,flag from TempTable order by dateInSecs desc limit 15;"];
            
            // Get the results.
            
            //[objDB getLogEditData:whereToken];
            NSMutableArray *dateList =[NSMutableArray array];
            for(int i=0;i<15;i++){
                
                NSArray *dateTemp=[[NSArray alloc]initWithArray: [objDB getLast14DaysFromDate:i date:dateNew]];
                token=@{@"userid":[NSString stringWithFormat:@"%d",userId],@"date":[NSString stringWithFormat:@"%@",dateTemp[0][0]]};
                ;
                whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
                NSArray* dataFromDB=[[NSArray alloc]initWithArray:[objDB getLogEditData:whereToken]];
                NSString *statusString=@"";
                if([dateTemp count]!=0){
                    for(int j=0;j<[dataFromDB count];j++){
                        if([dataFromDB[j][1] isEqualToString:@""])
                            statusString= [statusString stringByAppendingString:[NSString stringWithFormat:@"1"]];
                        else
                            statusString= [statusString stringByAppendingString:[NSString stringWithFormat:@"%@",[[dataFromDB objectAtIndex:j] objectAtIndex:1]]];
                    }
                    
                    NSString* charStr=[@"" stringByPaddingToLength:96 withString:@"1" startingAtIndex:0];
                    if([dataFromDB count]==0)
                        [dateList addObject:@{@"date":dateTemp[0][0],@"approved":@"0",@"data":charStr}];
                    else
                    {
                        if([statusString isEqualToString:@""])
                            statusString=charStr;
                        [dateList addObject:@{@"date":dateTemp[0][0],@"approved":dataFromDB[0][0],@"data":statusString}];
                    }
                }
            }
            previousdatesTemp = [[NSMutableArray alloc] init];
            graphDataOperations *obj=[graphDataOperations new];
            for(int i=0;i<[dateList count];i++){
                NSMutableArray *dict=[[NSMutableArray alloc] initWithArray:[obj generateGraphPoints:[[dateList objectAtIndex:i] objectForKey:@"data"]]];
                
                [previousdatesTemp addObject:@{@"date":[[dateList objectAtIndex:i] objectForKey:@"date"], @"payload":dict}];
            }
            //        NSLog(@"GeneratePoints :%@",previousdatesTemp);
        }
        
        //minnu
        cycleRestart=-1;
        NSArray *previousdates1 = [[NSArray alloc] initWithArray:previousdatesTemp];//[self.dbManager loadDataFromDB:query0]];
        previousdates=[[previousdates1 reverseObjectEnumerator] allObjects];
        //    NSLog(@"Result:%@",previousdates);
        if([previousdates count]!=0){
            NSString *restartDate=[objDB returnRestart:whereToken];
            /*if(![restartDate isEqualToString:@""]){
             for(int i=0;i<[previousdates count];i++)
             if([[[previousdates objectAtIndex:i] objectForKey:@"date"] isEqualToString:restartDate]){
             cycleRestart=i;
             }
             
             
             }*/
            cycleRestart =[self calculateRestart:previousdates];
            
        }
        [self calculateHOSData:previousdates restart:cycleRestart];
    });
    

}
-(void)calculateHOSData:(NSArray *)datesArray restart:(NSInteger)restartIndex{
    
    for(NSInteger i=restartIndex;i<[datesArray count];i++){
        
        
        if ([datesArray count]!=0) {
            
            /*    NSString *dbresult = previousdates[i][1];
             NSData *data2= [dbresult dataUsingEncoding:NSUTF8StringEncoding];
             NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
             */
            NSMutableArray *dict = [[[previousdates objectAtIndex:i] objectForKey:@"payload"] mutableCopy];
            [self  getdataforrecap:dict indexvalue:[[previousdates objectAtIndex:i] objectForKey:@"date"] ];
            
        }else{
            
            
            NSArray *inner = @[previousdates[i][0],@"00:00"];
            
            [total_work addObject:inner];
            
        }
        
        
    }
    
    
    NSArray *reversed = [[total_work reverseObjectEnumerator]allObjects];
    total_work1=reversed;
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.recap_table reloadData];
    });
    
}

-(NSInteger)calculateRestart:(NSArray*)datesArray{
    brk_hr1 = 0.0;
    restart_found1=false;
    restart_hr1=[restartinfo floatValue];
    NSInteger restartIndex=0;
    for(int j=0;j<[datesArray count];j++){
        
       /* NSString *dbresult = datesArray[j][1];
        NSData *data2= [dbresult dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
        if([json_log1 count]!=0){*/
        
            
            NSMutableArray *dict = [[datesArray[j] objectForKey:@"payload"] mutableCopy];
            int json_size = (int)[dict count];
            
            for (int k=0; k<json_size; k=k+2) {
                int m=k;
                int event_no =(int)[[dict[m] objectForKey:@"posy"] integerValue];
                if (event_no==65) {
                    
                    float posx1=[[dict[m] objectForKey:@"posx"] floatValue];
                    float posx2=[[dict[m+1] objectForKey:@"posx"] floatValue];
                    brk_hr1 = brk_hr1+posx2-posx1;
                    if (brk_hr1>=restart_hr1) {
                        restartIndex = j;
                        //numCycleStop1=j;
                        restart_found1=true;
                        //break;
                        
                    }
                    
                    
                }else{
                    brk_hr1=0.0;
                }
                
            }//end of for
            
            
        }
        
        
    //}
    
    
    
    return restartIndex;
    
    
    
    
}

-(NSString*)calculateTime:(float)posx{
    
    int end=(posx-40)/12.25;
    int x1=end/4;
    int x2=end%4;
    NSString *decimalpart,*time;
    if (x2*15==0) {
        decimalpart=@"00";
    }else{
        
        decimalpart=[NSString stringWithFormat:@"%d",x2*15];
    }
    
    if (x1<10) {
        time=[NSString stringWithFormat:@"0%d:%@",x1,decimalpart];
    }else{
        
        time=[NSString stringWithFormat:@"%d:%@",x1,decimalpart];
    }
    
    return time;
    
}
-(NSString*)calculateTime1:(float)posx{
    
    int end=(posx)/12.25;
    int x1=end/4;
    int x2=end%4;
    NSString *decimalpart,*time;
    if (x2*15==0) {
        decimalpart=@"00";
    }else{
        
        decimalpart=[NSString stringWithFormat:@"%d",x2*15];
    }
    
    if (x1<10) {
        time=[NSString stringWithFormat:@"0%d:%@",x1,decimalpart];
    }else{
        
        time=[NSString stringWithFormat:@"%d:%@",x1,decimalpart];
    }
    
    return time;
    
}

-(float)calculatePosx:(NSString *)time{
    NSArray *parts=[time componentsSeparatedByString:@":"];
    float x1=4*[parts[0] integerValue]+([parts[1] integerValue]/15);
    float posx=x1*12.25+40;
    //BFLog(@"from function:%f",posx);
    return posx;
    
}

-(void)getdataforrecap:(NSMutableArray *)dict indexvalue:(NSString*)date{
    NSDate * now_sta = [NSDate date];
    NSDateFormatter *outputFormatter_sta1 = [[NSDateFormatter alloc] init];
    [outputFormatter_sta1 setDateFormat:@"MM/dd/yyyy"];
    NSString *currentDate = [outputFormatter_sta1 stringFromDate:now_sta];
    int dicSize =(int)[dict count];
    if(![currentDate isEqualToString:date]){
        
        
        if([dict count]%2!=0){
            
            NSMutableDictionary *dic_new = [[NSMutableDictionary alloc] init];
            [dic_new setValue:[NSString stringWithFormat:@"%d",dicSize] forKey:@"id"];
            [dic_new setValue:@"1216" forKey:@"posx"];
            [dic_new setValue:dict[[dict count]-1][@"posy"] forKey:@"posy"];
            [dict addObject:dic_new];
            
        }else{
            
            if([dict[[dict count]-1][@"posy"] floatValue]!=1216){
                [dict removeLastObject];
                NSMutableDictionary *dic_new = [[NSMutableDictionary alloc] init];
                [dic_new setValue:[NSString stringWithFormat:@"%d",dicSize] forKey:@"id"];
                [dic_new setValue:@"1216" forKey:@"posx"];
                [dic_new setValue:dict[[dict count]-1][@"posy"] forKey:@"posy"];
                [dict addObject:dic_new];
                
            }
            
        }
        
    }else{
        if([dict count]%2!=0){
            NSDate * now_sta = [NSDate date];
            NSDateFormatter *outputFormatter_sta1 = [[NSDateFormatter alloc] init];
            [outputFormatter_sta1 setDateFormat:@"HH:mm"];
            NSString *currentDate = [outputFormatter_sta1 stringFromDate:now_sta];
            NSMutableDictionary *dic_new = [[NSMutableDictionary alloc] init];
            [dic_new setValue:[NSString stringWithFormat:@"%d",dicSize] forKey:@"id"];
            [dic_new setValue:[NSString stringWithFormat:@"%f",[self calculatePosx:currentDate]] forKey:@"posx"];
            [dic_new setValue:dict[[dict count]-1][@"posy"] forKey:@"posy"];
            [dict addObject:dic_new];
            
        }
        
    }
    
    int json_size = (int)[dict count];
    
    // int event_last =(int)[[dict[json_size-1] objectForKey:@"posy"] integerValue];
    // //BFLog(@"inner json count:%d,date:%@",json_size,date);
    float x1=0.0;
    for (int i=0; i<json_size; i=i+2) {
        int j=i;
        
        ////BFLog(@"posx:%@",[dict[0] objectForKey:@"posx"]);
        int event_no =(int)[[dict[j] objectForKey:@"posy"] integerValue];
        float posx1=[[dict[j] objectForKey:@"posx"] floatValue];
        float posx2=[[dict[j+1] objectForKey:@"posx"] floatValue];
        float duration=posx2-posx1;
        if (event_no==245 || event_no==185) {
            
            float posx1=[[dict[j] objectForKey:@"posx"] floatValue];
            float posx2=[[dict[j+1] objectForKey:@"posx"] floatValue];
            x1 = x1+duration;
            total_work_7=total_work_7+posx2-posx1;
            
        }else{
            break_time=break_time+duration;
            
        }
        if (event_no==185) {
            drive_total=drive_total+duration;
        }
        
        
    }
    
    
    int x2 = x1/12.25;
    int x3 = x2/4.0;
    int x4 = x2%4;
    NSString *x3_new;
    NSString *x4_new;
    if (x3<10) {
        x3_new = [NSString stringWithFormat:@"0%d",x3];
    }else{
        
        x3_new = [NSString stringWithFormat:@"%d",x3];        }
    if (15*x4<10) {
        x4_new = [NSString stringWithFormat:@"0%d",15*x4];
    }
    else{
        x4_new = [NSString stringWithFormat:@"%d",15*x4];
        
        
    }
    
    NSString *posx_1 = [NSString stringWithFormat:@"%@:%@",x3_new,x4_new];
    
    ////BFLog(@"time interval:%@",posx_1);
    NSArray *inner = @[date,posx_1];
    
    [total_work addObject:inner];
    int x22 = total_work_7/12.25;
    int x33 = x22/4.0;
    int x44 = x22%4;
    NSString *x34_new;
    NSString *x43_new;
    if (x33<10) {
        x34_new = [NSString stringWithFormat:@"0%d",x33];
    }else{
        
        x34_new = [NSString stringWithFormat:@"%d",x33];        }
    if (15*x4<10) {
        x43_new = [NSString stringWithFormat:@"0%d",15*x44];
    }
    else{
        x43_new = [NSString stringWithFormat:@"%d",15*x44];
        
        
    }
    totalworking = [NSString stringWithFormat:@"%@:%@",x34_new,x43_new];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    int rows;
    if (section==0) {
        rows=4;
    }/*else if(section==1){
      
      rows=(int)[total_work count];
      }*/else {
          rows=3;
      }
    
    return rows;
    
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    NSString *title;
    if (section==0) {
        title=@"Current Cycle Details";
    }/*else if(section==1){
      
      title=[NSString stringWithFormat:@"Last %lu Days Recap",(unsigned long)[total_work count]];
      }*/else if(section==1){
          title=@"Hours Of Service";
      }
    
    return title;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellIdentifier = @"cell";
    // NSString *data=@"";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    //cell.detailTextLabel.backgroundColor=[UIColor grayColor];
    cell.detailTextLabel.textColor=[UIColor blackColor];
    if (indexPath.section==0) {
        
        switch (indexPath.row) {
                
            case 0:
                cell.textLabel.text=@"Cycle";
                cell.detailTextLabel.text=cycleinfo;
                cell.imageView.image=[UIImage imageNamed:@"icons/cycle.png"];
                break;
                
            case 1:
                cell.textLabel.text=@"Cycle Started On";
                if([previousdates count]!=0){
                    if(cycleRestart==-1)
                        cell.detailTextLabel.text=@"On Restart";
                    else
                        cell.detailTextLabel.text=[[previousdates objectAtIndex:cycleRestart] objectForKey:@"date"];
                }else{
                    cell.detailTextLabel.text=@"NA";
                    
                }
                cell.detailTextLabel.textColor=[UIColor colorWithRed:0.00 green:0.58 blue:1.00 alpha:1.0];
                cell.imageView.image=[UIImage imageNamed:@"icons/start.png"];
                break;
                
            case 2:
                cell.textLabel.text=@"Cargo";
                cell.detailTextLabel.text=cargoinfo;
                cell.imageView.image=[UIImage imageNamed:@"icons/cargo.png"];
                break;
            case 3:
                
                cell.textLabel.text=@"Drive";
                cell.detailTextLabel.text=[self calculateTime1:drive_total];
                cell.imageView.image=[UIImage imageNamed:@"icons/drive.png"];
                break;
            default:
                break;
        }
        
        
        
    }else if (indexPath.section==1){
        
        switch (indexPath.row) {
            case 0:{
                
                cell.textLabel.text=@"Total Hours";
                cell.detailTextLabel.text=[self calculateTime:cycle_tym1];
                cell.imageView.image=[UIImage imageNamed:@"icons/time1.png"];
                break;
                
            }
            case 1:{
                cell.textLabel.text=@"Remaining Hours";
                float diff;
                if(cycle_tym1>total_work_7){
                    diff = cycle_tym1-total_work_7;
                    cell.detailTextLabel.text=[self calculateTime:diff];
                }else{
                    diff = 0;
                    cell.detailTextLabel.text=@"00:00";
                    
                }
                // NSString *available = [NSString stringWithFormat:@"%.2f",diff];
                
                cell.imageView.image=[UIImage imageNamed:@"icons/timeavail.png"];
                break;}
            case 2:{
                cell.textLabel.text=@"Hours Worked";
                
                cell.detailTextLabel.text=[self calculateTime1:total_work_7];
                cell.imageView.image=[UIImage imageNamed:@"icons/timeworked.png"];
                break;}
            default:
                break;
        }
        
    }
    return cell;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    // UITouch *touch = [touches anyObject];
    if([[self.view subviews] containsObject:transparentView_hosrecap]){
        [transparentView_hosrecap removeFromSuperview];
        
    }
}









-(void)showHelp{
    
    sourceVC = [self valueForKey:@"storyboardIdentifier"];
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc1 = [storyBoard instantiateViewControllerWithIdentifier:@"helpScreen"];
    [self presentViewController:vc1 animated:YES completion:nil];
    
    
    
}





- (IBAction)helpAlert1:(id)sender {
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    transparentView_hosrecap  = [[UIView alloc] initWithFrame:screenSize];
    transparentView_hosrecap.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:0.6];
    UIView *view1=[menu_hosrecap valueForKey:@"view"];
    
    UIButton *tutorialLink = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    tutorialLink.frame = CGRectMake(screenSize.origin.x+(screenSize.size.width/3), screenSize.size.height-40, screenSize.size.width/3, 40);
    [tutorialLink addTarget:self action:@selector(showHelp) forControlEvents:UIControlEventTouchUpInside];
    tutorialLink.tintColor = [UIColor yellowColor];
    [tutorialLink setTitle:@"<<Know More>>" forState:UIControlStateNormal];
    
    [transparentView_hosrecap addSubview:tutorialLink];
    
    
    
    UIImageView *arrow1=[[UIImageView alloc]initWithFrame:CGRectMake(view1.frame.origin.x+view1.frame.size.width/2-10, view1.frame.size.height+view1.frame.origin.y*3 , 50, 50)];
    [arrow1 setImage:[UIImage imageNamed:@"icons/arrow7.png"]];
    UILabel *dashboard  = [[UILabel alloc] initWithFrame:CGRectMake(view1.frame.origin.x+view1.frame.size.width/2, arrow1.frame.origin.y+arrow1.frame.size.height, screenSize.size.width/2, 20)];
    dashboard.text=@"Dashboard";
    dashboard.numberOfLines=2;
    dashboard.textColor=[UIColor whiteColor];
    dashboard.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    dashboard.minimumScaleFactor=10./dashboard.font.pointSize;
    dashboard.adjustsFontSizeToFitWidth=YES;
    [transparentView_hosrecap addSubview:arrow1];
    [transparentView_hosrecap addSubview:dashboard];
    [self.view addSubview:transparentView_hosrecap];
}
-(NSMutableDictionary *)returnRecapData:(NSString *)dateNew{
    NSMutableDictionary * data=[[NSMutableDictionary alloc]init];
    [data setValue:[NSString stringWithFormat:@"%@",[self calculateTime1:total_work_7]] forKey:@"workedHours"];
    [data setValue:[NSString stringWithFormat:@"%@",[self calculateTime:(float)(cycle_tym1-total_work_7)]] forKey:@"remaining"];
    [data setValue:[NSString stringWithFormat:@"%@",[self calculateTime:cycle_tym1]] forKey:@"cycleHours"];
    if(cycleRestart!=-1)
        [data setValue:[previousdates objectAtIndex:cycleRestart] forKey:@"restartDate"];
    else
        [data setValue:@{@"date":@"On Restart"} forKey:@"restartDate"];
    DBManager *objDB = [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    NSMutableArray *recapDefault=[[NSMutableArray alloc] init];
    
    if([total_work1 count]==0){
        for(int i=1;i<numCycles1;i++){
            
            NSArray *dateTemp=[[NSArray alloc]initWithArray: [objDB getLast14DaysFromDate:i date:dateNew]];
            NSArray *temp=[[NSArray alloc] initWithObjects:dateTemp[0][0],@"00:00", nil];
            [recapDefault addObject:temp];
        }
        [data setValue:recapDefault forKey:@"cycleRecap"];
    }
    else
    {
        recapDefault=[total_work1 mutableCopy];
        [recapDefault removeLastObject];
        if([recapDefault count]==0){
            for(int i=1;i<numCycles1;i++){
                
                NSArray *dateTemp=[[NSArray alloc]initWithArray: [objDB getLast14DaysFromDate:i date:dateNew]];
                NSArray *temp=[[NSArray alloc] initWithObjects:dateTemp[0][0],@"00:00", nil];
                [recapDefault addObject:temp];
            }
        }
        [data setValue:recapDefault forKey:@"cycleRecap"];
    }
    return data;
}

@end
