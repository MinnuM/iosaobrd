//
//  helpTutorialViewController.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/3/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "helpTutorialViewController.h"
#import "menuScreenViewController.h"
#import "loginViewController.h"
#import "menuScreenViewController.h"


@interface helpTutorialViewController ()

@end

@implementation helpTutorialViewController
@synthesize scroller_help;
@synthesize scroller_web;


- (void)viewDidLoad {
    [super viewDidLoad];
    [scroller_help setScrollEnabled:YES];
    [scroller_help setContentSize:(CGSizeMake(600, 1000))];
    CGPoint bottomOffset = CGPointMake(0, 1000-scroller_help.bounds.size.height);
    [scroller_help setContentOffset:bottomOffset animated:YES];
    
    NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/helpPage_IOSApp/indexNew.html"];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSData *data = [NSData dataWithContentsOfURL:url];
    if (data) {
        
       
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
        [scroller_web loadRequest:urlRequest];
        scroller_web.delegate = self;
    }else{
        
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                         message:@"Please check your internet connectivity!!!"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:@"OK", nil];
        
        [alert1 show];
        
    }
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated{
    
    //BFLog(@"Back");
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    if(navigationType == UIWebViewNavigationTypeLinkClicked){
        
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    return YES;
}

- (IBAction)backBuuton:(id)sender {
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc1 = [storyBoard instantiateViewControllerWithIdentifier:sourceVC];
    [self presentViewController:vc1 animated:YES completion:nil];

}
@end
