//
//  StatusViewController.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/25/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CircularProgressTimer.h"
typedef enum{
   driver_status_driving,
    driver_status_onDuty,
    driver_status_offDuty,
    driver_status_none,
    driver_status_onDuty_immediate,
    driver_status_connect_back,
    driver_status_yard_move,
    driver_status_personal_convey
}driver_status;
@interface StatusViewController : UIViewController<UIAlertViewDelegate, UIGestureRecognizerDelegate>{
    
    
    NSTimer *timer;
    NSInteger globalTimer;
    NSInteger counter;
    NSInteger minutesLeft;
    NSInteger secondsLeft;
    UIRefreshControl *refreshControl;
//    CircularProgressTimer *progressTimerView;
}
@property (nonatomic, strong) IBOutlet CircularProgressTimer *progressTimerView;
@property (weak, nonatomic) IBOutlet UIView *buttonClick;
@property (weak, nonatomic) IBOutlet UIView *detailView;
@property (weak, nonatomic) IBOutlet UILabel *deviceStatusLabel;
@property (weak, nonatomic) IBOutlet UIImageView *buttonImage;
@property (nonatomic) driver_status driverStatus;
@property (nonatomic) BOOL isFromPush;
@property (nonatomic, strong) IBOutlet UISwitch *switchPC;
@property (nonatomic, strong) IBOutlet UISwitch *switchYM;
@property (nonatomic, strong) IBOutlet UILabel *lblDriverStatus;
-(IBAction)toggleButtonAction:(id)sender;
-(IBAction)backAction:(id)sender;//goToLogScreen
@end
