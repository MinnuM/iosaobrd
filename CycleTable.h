//
//  CycleTable.h
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 19/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CycleTable : NSObject
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *name;

@end
