//
//  PreferenceViewController.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/29/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "PreferenceViewController.h"
#import "KeychainItemWrapper.h"
#import <Crashlytics/Crashlytics.h>
#import "AppDelegate.h"
@interface PreferenceViewController ()

@end

@implementation PreferenceViewController

@synthesize preferenceList;
- (void)viewDidLoad {
    [super viewDidLoad];
  //   dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
    preferenceList.delegate = self;
    preferenceList.dataSource = self;
    // Do any additional setup after loading the view.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 1;
    
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *title=@"";
    switch (section) {
        case 0:
            title = @"File Location";
            break;
        case 1:
            title =@"Orientation";
            break;
        case 2:
            title = @" ";
            break;
            
        default:
            break;
    }
    
    return title;
    
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    switch (indexPath.section) {
        case 0:
        {
           location = [[UITextField alloc] initWithFrame:CGRectMake(30, 0, tableView.bounds.size.width-30, 40)];
            location.placeholder = @"File Location";
            location.tag=1;
            location.delegate=self;
            [cell.contentView addSubview:location];
             break;
        }
        case 1:{
            
            orientation = [[UITextField alloc] initWithFrame:CGRectMake(30, 0, tableView.bounds.size.width-30, 40)];
            orientation.tag=2;
            orientation.placeholder = @"Orientation";
            orientation.delegate = self;
            [cell.contentView addSubview:orientation];
            break;
            
            
            
        }
        case 2:{
            NSString *usr =@"";
            if([[NSUserDefaults standardUserDefaults] valueForKey:@"username"]!=nil){
                
                usr = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
            }
            if([[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"savePassword_%@",usr]]!=nil){
                
                if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"savePassword_%@",usr]]==YES){
                    
                    cell.imageView.image = [UIImage imageNamed:@"icons/checked.png"];
                    
                }else{
                    
                    cell.imageView.image = [UIImage imageNamed:@"icons/uncheck.png"];
                }
                
                
            }else{
            cell.imageView.image = [UIImage imageNamed:@"icons/checked.png"];
            }
            cell.textLabel.text = @"Save Password";
            break;
            
        }
        
          
            
        default:
            break;
    }
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSData *uncheck = [NSData dataWithData:UIImagePNGRepresentation([UIImage imageNamed:@"icons/uncheck.png"])];
   // NSData *check = [NSData dataWithData:UIImagePNGRepresentation([UIImage imageNamed:@"icons/checked.png"])];
    
    if (indexPath.section==2) {
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];

       
        
        NSData *cellImage = [NSData dataWithData:UIImagePNGRepresentation(cell.imageView.image)];
        if([cellImage isEqualToData:uncheck]){
            
             cell.imageView.image = [UIImage imageNamed:@"icons/checked.png"];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[NSString stringWithFormat:@"savePassword"]];
            [self savePassword];
            
            
        }else{
             cell.imageView.image = [UIImage imageNamed:@"icons/uncheck.png"];
             [[NSUserDefaults standardUserDefaults] setBool:NO forKey:[NSString stringWithFormat:@"savePassword"]];
             [self removeSavedPassword];
        }
    
    }
    
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if(textField.tag==1){
        
        
        
        
        
    }
    
    
    
    
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    if(textField.tag ==2){
        if([textField.text isEqualToString:@"crashnow"] || [textField.text isEqualToString:@"Crashnow"]){
            
            [CrashlyticsKit crash];
        }
        
        
        
    }
    
    
    return YES;
}

-(void)savePassword{
    
    
       
}


-(void)removeSavedPassword{
    
  //  KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
    [credentials resetKeychainItem];
    
    
}




- (IBAction)savePreference:(id)sender {
    if(![location.text isEqualToString:@""] && ![orientation.text isEqualToString:@""]){
    
    /*raghee
        NSString *query1 = [NSString stringWithFormat:@"select * from preferenceDetails"];
    
    // Get the results.
    
    NSArray *values = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query1]];
*/
        
        DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
               NSDictionary *token;
        ;
        NSMutableDictionary *whereToken;
        NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
        if([userInfoArray count]!=0){
            int userId = [userInfoArray[0][0] intValue];
        token=@{@"columnName":@"userid",@"entry":[NSString stringWithFormat:@"%d",userId]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        preferences *objPref=[preferences new];
        NSArray *values = [[NSArray alloc] initWithArray:[objDB selectFromTable:objPref whereToken:whereToken]];
            if([values count]!=0){
                if(![location.text isEqualToString:@""])
                {
                    objPref.preference=@"location";
                    objPref.val=location.text;
                    [objDB updateIntoTable:objPref whereToken:whereToken];
                    
                    
                }
                if(![orientation.text isEqualToString:@""])
                {
                    objPref.preference=@"orientation";
                    objPref.val=orientation.text;
                    [objDB updateIntoTable:objPref whereToken:whereToken];
                }
                
            
       /*raghee NSString *query_update = [NSString stringWithFormat:@"UPDATE preferenceDetails SET fileLocation = '%@',orientation = '%@'",location.text,orientation.text];
        [dbManager executeQuery:query_update];
        */
         
        // If the query was successfully executed then pop the view controller.
        if (objDB.affectedRows != 0) {
           
            UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                             message:@"Data Saved Successfully!!!"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
            
            [alert2 show];

        }
        else{
            UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                             message:@"Ooops Something went wrong!!!"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
            
            [alert2 show];

        }
        

        
        
    }else{
        if(![location.text isEqualToString:@""])
        {
            objPref.preference=@"location";
            objPref.val=location.text;
            objPref.userid=[NSString stringWithFormat:@"%@",userId];
            [objDB insertIntoTable:objPref];

            
        }
        if(![orientation.text isEqualToString:@""])
        {
            objPref.preference=@"orientation";
            objPref.val=orientation.text;
            objPref.userid=[NSString stringWithFormat:@"%@",userId];
        }
    }
      //raghee  NSString *query_insert1 = [NSString stringWithFormat:@"insert into preferenceDetails values(null, '%@', '%@')", location.text,orientation.text];
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
       // Raghee [dbManager executeQuery:query_insert1];

        
        // If the query was successfully executed then pop the view controller.
        if (objDB.affectedRows != 0) {
            
            UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                             message:@"Data Saved Successfully!!!"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
            
            [alert2 show];
            
        }
        else{
            UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                             message:@"Ooops Something went wrong!!!"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
            
            [alert2 show];
            
        }
        

        
    }
    }
    
}
@end
