//
//  unapprovedViewController.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/25/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "unapprovedViewController.h"
#import "menuScreenViewController.h"
#import "logScreenViewController.h"
#import "AppDelegate.h"

@interface unapprovedViewController ()

@end

@implementation unapprovedViewController
@synthesize unapprovedList;
NSArray *unapproved;
- (void)viewDidLoad {
    [super viewDidLoad];
    unapprovedList.delegate = self;
    unapprovedList.dataSource = self;
    
    DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    NSDictionary *token;
    ;
    NSMutableDictionary *whereToken;
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];;
    token=@{@"columnName":@"userid",@"entry":[NSString stringWithFormat:@"%d",userId]};
    ;
    whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
    commonDataHandler *common=[commonDataHandler new];
    NSString *today = [common getCurrentTimeInLocalTimeZone];
    NSArray*dateList=[[NSArray alloc] initWithArray:[today componentsSeparatedByString:@"/"]];
    //[objDB getUnapprovedList:[NSString stringWithFormat:@"%@-%@-%@ 00:00:00",dateList[2],dateList[0],dateList[1]]];
    NSString * date=[NSString stringWithFormat:@"%@-%@-%@ 00:00:00",dateList[2],dateList[0],dateList[1]];
       
    for(int i=0;i<90;i++){
    NSArray * dateArr=[[NSArray alloc] initWithArray:[objDB getLast14DaysFromDate:i date:date]];
        if([dateArr count]!=0)
        {
            token=@{@"columnName":@"userid",@"entry":[NSString stringWithFormat:@"%d",userId],@"date":dateArr[0][0]};
            ;
            whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
            NSArray * unapprovedTemp = [[NSArray alloc] initWithArray:[objDB getUnapprovedList:whereToken]];
            NSLog(@"%@",unapprovedTemp);
        }
    }
    unapproved = [[NSArray alloc] initWithArray:[objDB getUnapprovedList:whereToken]];
    }
    // Do any additional setup after loading the view.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
   //raghee return [unapprovedDates count];
    return [unapproved count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
      NSString *date_string=@"";
   /* raghee if ([unapprovedDates count]!=0) {
        
        
        NSArray *date_elements = [unapprovedDates[indexPath.row][0] componentsSeparatedByString:@"/"];
        NSString *month=@"";
        switch ([date_elements[0] integerValue]) {
            case 1:
                month=@"Jan";
                break;
            case 2:
                month=@"Feb";
                break;
            case 3:
                month=@"Mar";
                break;
            case 4:
                month=@"Apr";
                break;
            case 5:
                month=@"May";
                break;
            case 6:
                month=@"Jun";
                break;
            case 7:
                month=@"Jul";
                break;
            case 8:
                month=@"Aug";
                break;
            case 9:
                month=@"Sep";
                break;
            case 10:
                month=@"Oct";
                break;
            case 11:
                month=@"Nov";
                break;
            case 12:
                month=@"Dec";
                break;
                
                
                
            default:
                break;
        }
         date_string=[NSString stringWithFormat:@"%@ %@ %@",date_elements[1],month,date_elements[2]];
    
    
        //cell.textLabel.text=dateslist[indexPath.row];
        cell.textLabel.text=date_string;
    
   // UIView *rightView = [[UIView alloc]init];
    UIButton *approve = [UIButton buttonWithType:UIButtonTypeCustom];
    approve.frame = CGRectMake((tableView.bounds.size.width-50), 0, 50, 40);
    [approve setImage:[UIImage imageNamed:@"warning.png"] forState:UIControlStateNormal];
        approve.tag = indexPath.row;
        [approve addTarget:self action:@selector(gotoSign:) forControlEvents:UIControlEventTouchUpInside];
    //[approve setTitle:@"Approve" forState:UIControlStateNormal];
       // approve.titleLabel.textColor =[UIColor redColor];
   // approve.backgroundColor = [UIColor lightGrayColor];
   // [rightView addSubview:approve];
    cell.accessoryView = approve;
    }else{
        
        cell.textLabel.text=@"No Unapproved Dates!!!";

    }*/
    if ([unapproved count]!=0) {
        
        
        NSArray *date_elements = [unapproved[indexPath.row][0] componentsSeparatedByString:@"-"];
        NSString *month=@"";
        switch ([date_elements[0] integerValue]) {
            case 1:
                month=@"Jan";
                break;
            case 2:
                month=@"Feb";
                break;
            case 3:
                month=@"Mar";
                break;
            case 4:
                month=@"Apr";
                break;
            case 5:
                month=@"May";
                break;
            case 6:
                month=@"Jun";
                break;
            case 7:
                month=@"Jul";
                break;
            case 8:
                month=@"Aug";
                break;
            case 9:
                month=@"Sep";
                break;
            case 10:
                month=@"Oct";
                break;
            case 11:
                month=@"Nov";
                break;
            case 12:
                month=@"Dec";
                break;
                
                
                
            default:
                break;
        }
        date_string=[NSString stringWithFormat:@"%@ %@ %@",date_elements[1],month,date_elements[2]];
        
        
        //cell.textLabel.text=dateslist[indexPath.row];
        cell.textLabel.text=date_string;
        
        // UIView *rightView = [[UIView alloc]init];
        UIButton *approve = [UIButton buttonWithType:UIButtonTypeCustom];
        approve.frame = CGRectMake((tableView.bounds.size.width-50), 0, 50, 40);
        [approve setImage:[UIImage imageNamed:@"warning.png"] forState:UIControlStateNormal];
        approve.tag = indexPath.row;
        [approve addTarget:self action:@selector(gotoSign:) forControlEvents:UIControlEventTouchUpInside];
        //[approve setTitle:@"Approve" forState:UIControlStateNormal];
        // approve.titleLabel.textColor =[UIColor redColor];
        // approve.backgroundColor = [UIColor lightGrayColor];
        // [rightView addSubview:approve];
        cell.accessoryView = approve;
    }else{
        
        cell.textLabel.text=@"No Unapproved Dates!!!";
        
    }
    return cell;
    
    
    
}

-(void)gotoSign:(UIButton*)button{
    
    
    NSInteger row = button.tag;
    if ([unapprovedDates count]!=0) {
        sign_id=1;
        selected_previous =  unapprovedDates[row][0];
        section_index = 2;
        UIStoryboard *storyBoard = self.storyboard;
        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"logsmenu"];
        [self presentViewController:vc animated:YES completion:nil];
    }
    

    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
