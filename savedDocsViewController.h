//
//  savedDocsViewController.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/28/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface savedDocsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *pdfList;

@end
