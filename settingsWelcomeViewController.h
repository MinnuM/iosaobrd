//
//  settingsWelcomeViewController.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/17/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface settingsWelcomeViewController : UIViewController
@property NSUInteger pageIndex;
@property NSString *titleText;
@property NSString *imageFile;
@property (weak, nonatomic) IBOutlet UIImageView *imageScreen;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
- (IBAction)skipToMenu:(id)sender;
- (IBAction)okToSettings:(id)sender;

@end
