//
//  loginViewController.m
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 6/7/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "loginViewController.h"
#import <CoreData/CoreData.h>
#import "MBProgressHUD.h"
#import "POP.h"
#import "KeychainItemWrapper.h"
#import "LoginClass.h"
#import "AppDelegate.h"
#import "commonDataHandler.h"
#import "Header.h"
#import "MobileCheckIn.h"

@interface loginViewController ()
{
    NSString *strUserName;
    NSString *strPassword;
}
@end
int decision=0;
NSString *cycle_flag;

NSString *myDB=@"mydatabase.db";
NSString *path=@"";
NSString *item=@"";
NSString *path1=@"";
NSString *usr=@"";
int x=0;
NSString *sucess=@"";
NSMutableArray *driver_name;
NSMutableArray *latitude;
NSMutableArray *longitude;
NSString *master;
NSString *USer;
NSString *currentStatus;
NSString *today_log_login;
NSDate *now1;
NSDictionary *json_login;
NSDictionary *json ;
BOOL loginSuccess = false;
MBProgressHUD *hud;
dispatch_queue_t queue ;
dispatch_group_t group ;
extern NSString *username;
extern NSString *password;
float distance;
@implementation loginViewController
@synthesize user;
@synthesize pass;
@synthesize dbManager,obj1;
@synthesize failed_msg;
@synthesize signInBtn;
@synthesize forgotPassword;


- (void)viewDidLoad {
    
    [super viewDidLoad];
    

    
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:@"Sign Up"];
    [attributeString addAttribute:NSUnderlineStyleAttributeName
                            value:[NSNumber numberWithInt:1]
                            range:(NSRange){0,[attributeString length]}];
    _lblSignUp.attributedText = attributeString;
    
    
    signInBtn.layer.cornerRadius=10.0;
    user.layer.cornerRadius=10.0;
    pass.layer.cornerRadius=10.0;
    user.layer.borderWidth=2.0;
    user.layer.borderColor=[[UIColor grayColor] CGColor];
    pass.layer.borderWidth=2.0;
    pass.layer.borderColor=[[UIColor grayColor] CGColor];
    forgotPassword.minimumScaleFactor=7./forgotPassword.font.pointSize;
    forgotPassword.adjustsFontSizeToFitWidth=YES;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    forgotPassword.userInteractionEnabled=YES;
    UITapGestureRecognizer *frgtPassword=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showScreen)];
    [forgotPassword addGestureRecognizer:frgtPassword];
    
    UITapGestureRecognizer *backgroundTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backgroundTap)];
    [self.view addGestureRecognizer:backgroundTapped];
    //minnu
    failed_msg.hidden=YES;
    user.delegate=self;
    pass.delegate=self;
    dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
    json_login = [[NSDictionary alloc]init];
    now1 = [NSDate date];
    NSDateFormatter *outputFormatter_log = [[NSDateFormatter alloc] init];
    [outputFormatter_log setDateFormat:@"MM/dd/YYYY"];
    today_log_login = [outputFormatter_log stringFromDate:now1];
    
}
-(void)showScreen{
    
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"forgotPassword"];
    [self presentViewController:vc animated:YES completion:nil];
    
    
}
//minnu
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
    
}

- (void)backgroundTap {
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
//minnu


- (void)keyboardWillShow:(NSNotification*)aNotification {
    [UIView animateWithDuration:0.25 animations:^
     {
         //CGRect userFrame=[self.user frame];
         CGRect passFrame=[self.pass frame];
         CGRect screenRect=[[UIScreen mainScreen] bounds];
         CGFloat screenHeight =screenRect.size.height;
         NSDictionary* keyboardInfo = [aNotification userInfo];
         NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
         CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
         CGFloat keyboardHeight=keyboardFrameBeginRect.size.height;
         
         
         CGRect newFrame = [self.view frame];
         
         if((int)[self.view frame].origin.y == 0)
             if ((screenHeight-(passFrame.origin.y+passFrame.size.height/2))<keyboardHeight){
                 newFrame.origin.y -= keyboardHeight-(screenHeight-(passFrame.origin.y+passFrame.size.height)); // tweak here to adjust the moving position
                 distance=keyboardHeight-(screenHeight-(passFrame.origin.y+passFrame.size.height));}
         //newFrame.origin.y -= 150; // tweak here to adjust the moving position
         [self.view setFrame:newFrame];
         
     }completion:^(BOOL finished)
     {
         
     }];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    [UIView animateWithDuration:0.25 animations:^
     {
         CGRect newFrame = [self.view frame];
         if((int)[self.view frame].origin.y != 0)
             newFrame.origin.y +=distance; // tweak here to adjust the moving position
         [self.view setFrame:newFrame];
         
     }completion:^(BOOL finished)
     {
         
     }];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}




-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    
    if (failed_msg.hidden==NO) {
        failed_msg.hidden=YES;
    }
    return YES;
    
}
- (void)doSomeWorkWithProgress {
    self.canceled = NO;
    // This just increases the progress indicator in a loop.
    float progress = 0.0f;
    while (progress < 1.0f) {
        if (self.canceled) break;
        progress += 0.01f;
        dispatch_async(dispatch_get_main_queue(), ^{
            // Instead we could have also passed a reference to the HUD
            // to the HUD to myProgressTask as a method parameter.
            [MBProgressHUD HUDForView:self.navigationController.view].progress = progress;
        });
        usleep(50000);
    }
}
-(void)LoginUser:(NSString*)username withPswrd:(NSString*)Password withpushIdentifier:(NSString*)pushIdentifier{
    NSMutableDictionary *keyList = [[NSMutableDictionary alloc] init];
    [keyList setValue:@"login" forKey:@"operation"];
    [keyList setValue:username forKey:@"username"];
    [keyList setValue:Password forKey:@"password"];
    [keyList setValue:[Bugfender deviceIdentifier] forKey:@"mobileidentifier"];
    [keyList setValue:@"ios" forKey:@"useragent"];
    [keyList setValue:pushIdentifier forKey:@"pushidentifier"];
    LoginClass *obj = [LoginClass new];
    NSString *stringParams = [obj constructInput:keyList];
    NSString *urlString = @"http://54.164.84.102/client/login.php";
    [self getConnection:urlString withParams:stringParams];
    
}
-(void)getConnection:(NSString *)urlString withParams:(NSString*)paramString{
    __block NSString *responseString = @"";
    NSString *urlStringWithParam = [NSString stringWithFormat:@"payload=%@",paramString];
    NSString *enodedURl = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:enodedURl] ;
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    
    [request setHTTPBody:[urlStringWithParam dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue1 = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue1 completionHandler:^(NSURLResponse * _Nullable response, NSData *_Nullable data, NSError * _Nullable connectionError) {
        
        responseString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        if(![responseString isEqualToString:@""]){
            NSMutableArray *arrayAckResponse = [[NSMutableArray alloc] init];
            NSMutableArray *arrayAckResult = [NSMutableArray new];
            arrayAckResponse =[[commonDataHandler sharedInstance] parseResponse:responseString];
            if(arrayAckResponse!=nil && [arrayAckResponse count]!=0){
                
                for (NSMutableDictionary *objDict in arrayAckResponse) {
                    MobileCheckIn *objMobileCheckIn = [MobileCheckIn new];
                    [objMobileCheckIn getDataFromAPILogin:objDict];
                    [arrayAckResult addObject:objMobileCheckIn];
                }
                if(arrayAckResult.count > 0){
                    
                    MobileCheckIn *obj = [arrayAckResult firstObject];
                    obj1 = [LoginResponse new];
                    obj1.result = obj.result;
                    obj1.authCode = obj.authCode;
                    obj1.reason = obj.reason;
                    
                    
                    if([obj1.result isEqualToString:@"failed"]){
                        loginSuccess = false;
                        dispatch_async(dispatch_get_main_queue(), ^{
                            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                             message:obj1.reason
                                                                            delegate:self
                                                                   cancelButtonTitle:@"OK"
                                                                   otherButtonTitles:nil, nil];
                            
                            [alert1 show];
                        });
                        
                        
                    }else{
                        if(![obj1.authCode isEqualToString:@""]){
                            loginSuccess=true;
                            DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
                            User *objUser=[User new];
                            objUser.username=strUserName;
                            objUser.password=strPassword;
                            objUser.authCode=obj1.authCode;
                            objUser.loggedIn=@"1";
                            NSDictionary* token=@{@"columnName":@"username",@"entry":[NSString stringWithFormat:@"%@",strUserName]};
                            ;
                            NSMutableDictionary* whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
                            
                            NSArray *values=[[NSArray alloc]initWithArray:[objDB selectFromTable:objUser whereToken:whereToken]];
                            if([values count]==0)
                                [objDB insertIntoTable:objUser];
                            else
                                [objDB updateCred:objUser];
                            
                            NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
                            NSArray *detailsArray = [userInfoArray firstObject];
                            [commonDataHandler sharedInstance].strUserId = detailsArray[0];
                            
                            [commonDataHandler sharedInstance].strUserName = objUser.username;
                            [commonDataHandler sharedInstance].strAuthCode = objUser.authCode;
                            
                            NSString *query_del10 =[NSString stringWithFormat:@"delete from authDetails;"];
                            [dbManager executeQuery:query_del10];
                            
                            NSString *query_insert2 = [NSString stringWithFormat:@"insert into authDetails values(null, '%@')",obj1.authCode];
                            
                            // Execute the query.
                            
                            //NSString *query_del = @"delete from TempTable";
                            [dbManager executeQuery:query_insert2];
                            
                            // If the query was successfully executed then pop the view controller.
//                            if (dbManager.affectedRows != 0) {
//                                BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
//                            }
//                            else{
//                                BFLog(@"Could not execute the query");
//                            }
                            
                            
                        }
   
                    }
                }
            }else{
                loginSuccess = false;
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                     message:@"Something wrong happened!!!"
                                                                    delegate:self
                                                           cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil, nil];
                    
                    [alert1 show];
                });
            }
        }
        if(loginSuccess){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                sucess=[NSString stringWithFormat:@"%d",1];
                [self moveToscreen];
            });
            
        }else{
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [[[UIAlertView alloc] initWithTitle:@"Error" message:@"No Internet Connectivity" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
//            });
        }
        
    }];
    
    
}


- (IBAction)submit:(id)sender {
    DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    [objDB removeLoggedIn];
    loginSuccess = false;
    strUserName = [user text];
    strPassword = [pass text];
    NSString *pushId = [[NSUserDefaults standardUserDefaults] valueForKey:@"pushIdentifier"];
    
    [self LoginUser:strUserName withPswrd:strPassword withpushIdentifier:pushId];

}

- (void)moveToscreen{
    
    
//    if ([sucess isEqualToString:@"1"]){
        // BFLog(@"Entered");
  if (loginSuccess){
        NSUserDefaults *userdata = [NSUserDefaults standardUserDefaults];
        NSString *usr=@"";
        if([[NSUserDefaults standardUserDefaults] valueForKey:@"username"]!=nil){
            usr = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
        }
        if([[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"savePassword_%@",usr]]!=nil){
            if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"savePassword_%@",usr]]==YES){

                [credentials setObject:[user text] forKey:(id)kSecAttrAccount];
                [credentials setObject:[pass text] forKey:(id)kSecValueData];
            }
            
        }else{
            [credentials setObject:[user text] forKey:(id)kSecAttrAccount];
            [credentials setObject:pass.text forKey:(id)kSecValueData];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[NSString stringWithFormat:@"savePassword_%@",usr]];
        }
        
        
        [userdata setObject:master forKey:@"masteruser"];
        [userdata setObject:USer forKey:@"user"];
        [userdata setValue:[user text] forKey:@"username"];
        [userdata synchronize];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIStoryboard *storyBoard = self.storyboard;
            UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"loginLanding"];
            [self presentViewController:vc animated:YES completion:nil];
        });
        
    }else{
        POPSpringAnimation *shake = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionX];
        
        shake.springBounciness = 10;
        shake.velocity = @(5000);
        
        [self.pass.layer pop_addAnimation:shake forKey:@"shakePassword"];
        POPSpringAnimation *shake1 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionX];
        
        shake1.springBounciness = 10;
        shake1.velocity = @(5000);
        
        [self.user.layer pop_addAnimation:shake1 forKey:@"shakeUser"];
        //[self.user.layer pop_addAnimation:shake forKey:@"shakeUser"];
        /* UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Login Failed"
         message:@"Please check your username or password!!!"
         delegate:self
         cancelButtonTitle:@"OK"
         otherButtonTitles:nil, nil];
         
         [alert1 show];*/
        dispatch_async(dispatch_get_main_queue(), ^{
            //failed_msg.hidden=NO;
        });
        
        
    }
    
}
- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}
#pragma mark    - <SignUp Action>
-(IBAction)signUpAction:(id)sender{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Sign Up"
                                                                   message:@"Please contact your Fleet manager for account details. Fleet manager can create new user accounts from Matrack HOS management console and assign a vehicle for the user"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}
@end
