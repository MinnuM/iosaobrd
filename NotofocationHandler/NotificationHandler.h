//
//  NotificationHandler.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/20/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "StatusViewController.h"
#import "logScreenViewController.h"
@interface NotificationHandler : NSObject
-(void)recievePushNotification:(UIViewController *)currentVC withDriverStatus:(driver_status)driverStat;
@end
