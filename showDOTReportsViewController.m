//
//  showDOTReportsViewController.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 1/11/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "showDOTReportsViewController.h"
#import "listFiles.h"


@interface showDOTReportsViewController ()

@end
UIView *transparentView_senddot;
@implementation showDOTReportsViewController
@synthesize fileView;
@synthesize sendBtn;
@synthesize menu_senddot;
- (void)viewDidLoad {
    [super viewDidLoad];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    // Get the resource path and read the file using NSData
    NSString *filePath = [documentsDirectory stringByAppendingString:[NSString stringWithFormat:@"/%@",filePathsArray[selected_fileIndex]]];
    //BFLog(@"Path:%@",filePath);
    NSURL *targetUrl = [NSURL fileURLWithPath:filePath];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetUrl];
    [fileView loadRequest:request];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    // UITouch *touch = [touches anyObject];
    if([[self.view subviews] containsObject:transparentView_senddot]){
        [transparentView_senddot removeFromSuperview];
        
    }
}





- (IBAction)helpMessage:(id)sender {
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    transparentView_senddot  = [[UIView alloc] initWithFrame:screenSize];
    transparentView_senddot.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:0.6];
    UIView *view1=[menu_senddot valueForKey:@"view"];
    
    
    
    
    UIImageView *arrow1=[[UIImageView alloc]initWithFrame:CGRectMake(view1.frame.origin.x+view1.frame.size.width, view1.frame.origin.y, 50, 50)];
    [arrow1 setImage:[UIImage imageNamed:@"icons/arrow2.png"]];
    UILabel *dashboard  = [[UILabel alloc] initWithFrame:CGRectMake(arrow1.frame.origin.x+arrow1.frame.size.width+20, arrow1.frame.origin.y+arrow1.frame.size.height/2, screenSize.size.width/2, 20)];
    dashboard.text=@"Back to DOT Menu screen";
    dashboard.numberOfLines=2;
    dashboard.textColor=[UIColor whiteColor];
    dashboard.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    dashboard.minimumScaleFactor=10./dashboard.font.pointSize;
    dashboard.adjustsFontSizeToFitWidth=YES;
    
    UIImageView *arrow2=[[UIImageView alloc]initWithFrame:CGRectMake(screenSize.size.width/2,sendBtn.frame.origin.y-50, 50, 50)];
    [arrow2 setImage:[UIImage imageNamed:@"icons/arrow6.png"]];
    UILabel *begin  = [[UILabel alloc] initWithFrame:CGRectMake(screenSize.origin.x+10,sendBtn.frame.origin.y-80, screenSize.size.width-80, 20)];
    begin.text=@"Click here to send the DOT";
    begin.numberOfLines=2;
    begin.textColor=[UIColor whiteColor];
    begin.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    begin.minimumScaleFactor=10./begin.font.pointSize;
    begin.adjustsFontSizeToFitWidth=YES;
   
   
    [transparentView_senddot addSubview:arrow2];
    [transparentView_senddot addSubview:begin];
    [transparentView_senddot addSubview:dashboard];
    [transparentView_senddot addSubview:arrow1];
    
    [self.view addSubview:transparentView_senddot];
}
@end
