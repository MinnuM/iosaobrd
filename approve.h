//
//  approve.h
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 13/11/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface approve : NSObject
@property (nonatomic, strong) NSString *approve;
@property (nonatomic, strong) NSString *userid;
@property (nonatomic, strong) NSString *date;

@end
