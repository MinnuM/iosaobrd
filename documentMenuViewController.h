//
//  documentMenuViewController.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/24/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
extern NSString *selectedFile;
extern NSString *filenameSelected;
@interface documentMenuViewController : UIViewController
- (IBAction)changeView:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *selectView;
@property (weak, nonatomic) IBOutlet UIView *myfiles;

@property (weak, nonatomic) IBOutlet UIView *savedFiles;
@end
