//
//  helpPage.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/19/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "helpPage.h"

@interface helpPage ()

@end

@implementation helpPage
@synthesize scrollView;
@synthesize messageBody;
@synthesize phNumber;
//@synthesize email;

- (void)viewDidLoad {
    [super viewDidLoad];
    _email.delegate=self;
    phNumber.delegate=self;
    messageBody.delegate=self;
    _email.text=@"support.gps@matrackinc.com";
    
    [_email setTag:101];
    
    messageBody.layer.borderWidth=1.0;
    messageBody.layer.borderColor=[[UIColor blackColor] CGColor];
    
    
    UITapGestureRecognizer *backgroundTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backgroundTap)];
    [self.view addGestureRecognizer:backgroundTapped];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    // Do any additional setup after loading the view.
}
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    //CGRect driverFrame=[self.driver frame];
    //CGRect vehiclesFrame=[self.vehicles frame];
    //CGRect trailersFrame=[self.trailers frame];
    //CGRect distanceFrame=[self.distance frame];
    CGRect messageFrame=[messageBody frame];
    CGRect textfieldFrame;
    CGRect aRect = self.view.frame;
    aRect.size.height -= (kbSize.height);
    
    if([phNumber isFirstResponder]){
        
        textfieldFrame = [phNumber frame];
        if (aRect.size.height < (textfieldFrame.origin.y+15)) {
            CGPoint scrollPoint = CGPointMake(0.0, textfieldFrame.origin.y-kbSize.height);
            //BFLog(@"scrollpoint--%f",textfieldFrame.origin.y-kbSize.height);
            [scrollView setContentOffset:scrollPoint animated:YES];
        }
    }else if([messageBody isFirstResponder]) {
        textfieldFrame=messageFrame;
        if (aRect.size.height < (textfieldFrame.origin.y+65)) {
            CGPoint scrollPoint = CGPointMake(0.0, textfieldFrame.origin.y-(kbSize.height/2));
            //BFLog(@"scrollpoint--%f",textfieldFrame.origin.y-kbSize.height);
            [scrollView setContentOffset:scrollPoint animated:YES];
        }
    }
    
    //BFLog(@"General-scrollpoint--(%f,%f)",textfieldFrame.origin.x,textfieldFrame.origin.y);
}


// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    //BFLog(@"entered return key");
    return YES;
    
}

- (void)backgroundTap {
    [self.view endEditing:YES];
    //BFLog(@"Touched Inside");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*-(void)textFieldDidEndEditing:(UITextField *)textField{
 [scrollView setContentOffset:CGPointZero animated:YES];
 
 
 }
 
 -(void)textFieldDidBeginEditing:(UITextField *)textField{
 
 CGPoint point = CGPointMake(0, textField.frame.origin.y+scrollView.contentInset.top);
 [scrollView setContentOffset:point animated:YES];
 }*/

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    
    switch (result) {
        case MFMailComposeResultSent:
            //BFLog(@"Mail sent");
            break;
            
        case MFMailComposeResultSaved:
            //BFLog(@"Message saved");
            break;
            
        case MFMailComposeResultCancelled:
            //BFLog(@"Mail sending cancelled");
            break;
            
        case MFMailComposeResultFailed:
            //BFLog(@"Mail senting failed");
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    if ((textField.tag == 101) && [_email.text isEqualToString:@""]) {
        NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
        
        if ([emailTest evaluateWithObject:_email.text] == NO) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Valid Email Address." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
            
            
        }
        
    }
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if ((textField.tag == 101) && ![_email.text isEqualToString:@""]) {
        NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
        
        if ([emailTest evaluateWithObject:_email.text] == NO) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Valid Email Address." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
            _email.text=@"";
            
            
            
        }
        
    }
    
    
}

- (IBAction)sendMail:(id)sender {
    if ([_email.text isEqualToString:@""]) {
        
        UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"Please fill the Email field!!"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        
        [alert2 show];
    }else if([messageBody.text isEqualToString:@""]){
        
        UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"Please fill the Message field!!"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        
        [alert2 show];
        
        
        
    }else{
        
        if ([MFMailComposeViewController canSendMail]) {
            
            
            MFMailComposeViewController *email = [[MFMailComposeViewController alloc] init];
            email.mailComposeDelegate=self;
            [email setSubject:@"Help from HOS"];
            [email setMessageBody:messageBody.text isHTML:NO];
            [email setToRecipients:@[_email.text]];
            [self presentViewController:email animated:YES completion:NULL];
            
        }else{
            
         //   BFLog(@"This device cannot send the mail");
        }
    }
    
}
@end
