//
//  carrierform2.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/1/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "carrierform2.h"
#import "accountSettings.h"
#import "insertDutyCycle.h"
#import "KeychainItemWrapper.h"
#import "SectionRows.h"
#import "customPickerView.h"

int count_edit2=0;
NSString *carrier_name1;
 NSString *main_street1;
 NSString *main_city1;
 NSString *main_state1;
 NSString *main_zip1;
 NSString *home_street1;
 NSString *home_city1;
 NSString *home_state1;
 NSString *home_zip1;
  CGRect textfieldFrame;

@interface carrierform2 ()<CarrierTwoTextTableViewCellDelegate, FormCommonTableViewCellDelegate>
{
    NSMutableArray *arrayOfTableData;
    UITextField *txtFieldActive;
}
@end

@implementation carrierform2
@synthesize carrier;
@synthesize main_city;
@synthesize main_state;
@synthesize main_street;
@synthesize main_zip,home_city,home_state,home_street,home_zip;
@synthesize done_carrier;
@synthesize scroller02;
@synthesize dbManager;
@synthesize carrierLabel,mainLabel,homeLabel;
- (void)viewDidLoad {
    [super viewDidLoad];

    // Register notification when the keyboard will be show
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillShow:)
//                                                 name:UIKeyboardWillShowNotification
//                                               object:nil];
    
    // Register notification when the keyboard will be hide
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillHide:)
//                                                 name:UIKeyboardWillHideNotification
//                                               object:nil];
//    [_tblCarrierForm registerNib:[UINib nibWithNibName:@"FormCommonTableViewCell" bundle:nil] forCellReuseIdentifier:@"FormCommonTableViewCell"];
//    [_tblCarrierForm registerNib:[UINib nibWithNibName:@"CarrierTwoTextTableViewCell" bundle:nil] forCellReuseIdentifier:@"CarrierTwoTextTableViewCell"];
//    [self createTableData];
    dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
 //   done_carrier.hidden=YES;
    carrier.delegate=self;
    main_state.delegate=self;
    main_street.delegate=self;
    main_city.delegate=self;
    main_zip.delegate=self;
    home_street.delegate=self;
    home_city.delegate=self;
    home_state.delegate=self;
    home_zip.delegate=self;
    accountSettings *obj = [accountSettings new];
    [obj addRequired:carrierLabel];
    [obj addRequired:mainLabel];
    [obj addRequired:homeLabel];
    UITapGestureRecognizer *backgroundTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backgroundTap)];
    [self.view addGestureRecognizer:backgroundTapped];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
   if ([carrier_details count]!=0) {
        
        if ([carrier_details[0][1] isEqualToString:@"(null)"]
            ||[carrier_details[0][1] isEqualToString:@"<null>"]) {
            carrier.text=@"";
        }else{
            carrier.text=carrier_details[0][1];
        }

        if (![carrier_details[0][2] isEqualToString:@""] && ![carrier_details[0][2] isEqualToString:@"(null)"] && ![carrier_details[0][2] isEqualToString:@"<null>"])
            main_street.text=carrier_details[0][2];
        else
            main_street.text=@"";
        if (![carrier_details[0][3] isEqualToString:@""] && ![carrier_details[0][3] isEqualToString:@"(null)"] && ![carrier_details[0][3] isEqualToString:@"<null>"])
            main_city.text=carrier_details[0][3];
        else
            main_city.text=@"";
        if (![carrier_details[0][4] isEqualToString:@""] && ![carrier_details[0][4] isEqualToString:@"(null)"] && ![carrier_details[0][4] isEqualToString:@"<null>"])
            main_state.text=carrier_details[0][4];
        else
            main_state.text=@"";
        if (![carrier_details[0][5] isEqualToString:@""] && ![carrier_details[0][5] isEqualToString:@"(null)"] && ![carrier_details[0][5] isEqualToString:@"<null>"])
            main_zip.text=carrier_details[0][5];
        else
            main_zip.text=@"";
        
        if (![carrier_details[0][6] isEqualToString:@""] && ![carrier_details[0][6] isEqualToString:@"(null)"] && ![carrier_details[0][6] isEqualToString:@"<null>"])
            home_street.text=carrier_details[0][6];
        else
            home_street.text=@"";
        if (![carrier_details[0][7] isEqualToString:@""] && ![carrier_details[0][7] isEqualToString:@"(null)"] && ![carrier_details[0][7] isEqualToString:@"<null>"])
            home_city.text=carrier_details[0][7];
        else
            home_city.text=@"";
        if (![carrier_details[0][8] isEqualToString:@""] && ![carrier_details[0][8] isEqualToString:@"(null)"] && ![carrier_details[0][8] isEqualToString:@"<null>"])
            home_state.text=carrier_details[0][8];
        else
            home_state.text=@"";
        if (![carrier_details[0][9] isEqualToString:@""] && ![carrier_details[0][9] isEqualToString:@"(null)"] && ![carrier_details[0][9] isEqualToString:@"<null>"])
            home_zip.text=carrier_details[0][9];
        else
            home_zip.text=@"";
            
            
        }

        
        //   else{
           /*raghee
            NSString *query0 = [NSString stringWithFormat:@"select * from Carrier_Sett;"];
            NSArray *carrier_Data = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
            // BFLog(@"Result:%@",carrier_Data);
            if ([carrier_Data count]!=0) {
                if ([carrier_Data[0][1] isEqualToString:@"(null)"]) {
                    carrier.text=@"";
                }else{
                    carrier.text=carrier_Data[0][1];
                }
                if (![carrier_Data[0][2] isEqualToString:@",,,"] && ![carrier_Data[0][2] isEqualToString:@"(null),(null),(null),(null)"] && ![carrier_details[0][2] isEqualToString:@"<null>,<null>,<null>,<null>"]) {
                    NSArray *offAddress=[carrier_Data[0][2] componentsSeparatedByString:@","];
                    main_street.text=offAddress[0];
                    main_city.text=offAddress[1];
                   main_state.text=offAddress[2];
                   main_zip.text=offAddress[3];
                }else if([carrier_Data[0][2] isEqualToString:@"(null),(null),(null),(null)"] || [carrier_details[0][2] isEqualToString:@"<null>,<null>,<null>,<null>"]) {
                    
                   main_street.text=@"";
                    main_city.text=@"";
                    main_state.text=@"";
                    main_zip.text=@"";
                }else{
                    
                    main_street.text=@"";
                    main_city.text=@"";
                    main_state.text=@"";
                    main_zip.text=@"";
                    
                }
                if (![carrier_Data[0][3] isEqualToString:@",,,"] && ![carrier_Data[0][3] isEqualToString:@"(null),(null),(null),(null)"] & ![carrier_details[0][3] isEqualToString:@"<null>,<null>,<null>,<null>"]) {
                    
                    NSArray *homeAddress=[carrier_Data[0][3] componentsSeparatedByString:@","];
                    home_street.text=homeAddress[0];
                    home_city.text=homeAddress[1];
                    home_state.text=homeAddress[2];
                    home_zip.text=homeAddress[3];
                    
                    
                }else if([carrier_Data[0][3] isEqualToString:@"(null),(null),(null),(null)"] || [carrier_details[0][3] isEqualToString:@"<null>,<null>,<null>,<null>"]) {
                    
                   home_street.text=@"";
                    home_city.text=@"";
                    home_state.text=@"";
                    home_zip.text=@"";
                }else{
                    
                    home_street.text=@"";
                    home_city.text=@"";
                    home_state.text=@"";
                    home_zip.text=@"";

                    
                }
                
                
                
                
                
                
            }

        */
  //  }
   
    mainPicker=[[UIPickerView alloc] init];
    
    //[event_picker addTarget:self action:@selector(ShowSelectedDate1) forControlEvents:UIControlEventValueChanged];
    main_state.inputView = mainPicker;
    // _Enddate.inputView = datepicker;
    
    UIToolbar *toolBar1=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
    [toolBar1 setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn1=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePressed1)];
    UIBarButtonItem *spac1=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar1 setItems:[NSArray arrayWithObjects:spac1, doneBtn1,nil]];
    [self.main_state  setInputAccessoryView:toolBar1];
    
 /*   _startes = [[NSArray alloc] initWithObjects:@"Alabama"
            ,@"Alaska"
                ,@"Arizona"
                ,@"Arkansas"
                ,@"California"
                ,@"Colorado"
                ,@"Connecticut"
                ,@"Delaware"
                ,@"District Of Columbia"
                ,@"Florida"
                ,@"Georgia"
                ,@"Hawaii"
                ,@"Idaho"
                ,@"Illinois"
                ,@"Indiana"
                ,@"Iowa"
                ,@"Kansas"
                ,@"Kentucky"
                ,@"Louisiana"
                ,@"Maine"
                ,@"Maryland"
                ,@"Massachusetts"
                ,@"Michigan"
                ,@"Minnesota"
                ,@"Mississippi"
                ,@"Missouri"
                ,@"Montana"
                ,@"Nebraska"
                ,@"Nevada"
                ,@"New Hampshire"
                ,@"New Jersey"
                ,@"New Mexico"
                ,@"New York"
                ,@"North Carolina"
                ,@"North Dakota"
                ,@"Ohio"
                ,@"Oklahoma"
                ,@"Oregon"
                ,@"Pennsylvania"
                ,@"Rhode Island"
                ,@"South Carolina"
                ,@"South Dakota"
                ,@"Tennessee"
                ,@"Texas"
                ,@"Utah"
                ,@"Vermont"
                ,@"Virginia"
                ,@"Washington"
                ,@"West Virginia"
                ,@"Wisconsin"
                ,@"Wyoming",
                nil];
    
    
    
    _statecodes = [[NSArray alloc] initWithObjects:@"AL"
                   ,@"AK"
                   ,@"AZ"
                   ,@"AR"
                   ,@"CA"
                   ,@"CO"
                   ,@"CT"
                   ,@"DE"
                   ,@"DC"
                   ,@"FL"
                   ,@"GA"
                   ,@"HI"
                   ,@"ID"
                   ,@"IL"
                   ,@"IN"
                   ,@"IA"
                   ,@"KS"
                   ,@"KY"
                   ,@"LA"
                   ,@"ME"
                   ,@"MD"
                   ,@"MA"
                   ,@"MI"
                   ,@"MN"
                   ,@"MS"
                   ,@"MO"
                   ,@"MT"
                   ,@"NE"
                   ,@"NV"
                   ,@"NH"
                   ,@"NJ"
                   ,@"NM"
                   ,@"NY"
                   ,@"NC"
                   ,@"ND"
                   ,@"OH"
                   ,@"OK"
                   ,@"OR"
                   ,@"PA"
                   ,@"RI"
                   ,@"SC"
                   ,@"SD"
                   ,@"TN"
                   ,@"TX"
                   ,@"UT"
                   ,@"VT"
                   ,@"VA"
                   ,@"WA"
                   ,@"WV"
                   ,@"WI"
                   ,@"WY", nil];*/
    _startes = [[NSArray alloc] initWithObjects:@"Alabama",@"Alaska",@"Arizona",@"Arkansas",@"California",@"Colorado",@"Connecticut",@"Delaware",@"District Of Columbia",@"Florida",@"Georgia",@"Hawaii",@"Idaho",@"Illinois",@"Indiana",@"Iowa",@"Kansas",@"Kentucky",@"Louisiana",@"Maine",@"Maryland",@"Massachusetts",@"Michigan",@"Minnesota",@"Mississippi",@"Missouri",@"Montana",@"Nebraska",@"Nevada",@"New Hampshire",@"New Jersey",@"New Mexico",@"New York",@"North Carolina",@"North Dakota",@"Ohio",@"Oklahoma",@"Oregon",@"Pennsylvania",@"Rhode Island",@"South Carolina",@"South Dakota",@"Tennessee",@"Texas",@"Utah",@"Vermont",@"Virginia",@"Washington",@"West Virginia",@"Wisconsin",@"Wyoming",nil];
    
    _statecodes = [[NSArray alloc] initWithObjects:@"AL",@"AK",@"AZ",@"AR",@"CA",@"CO",@"CT",@"DE",@"DC",@"FL",@"GA",@"HI",@"ID",@"IL",@"IN",@"IA",@"KS",@"KY",@"LA",@"ME",@"MD",@"MA",@"MI",@"MN",@"MS",@"MO",@"MT",@"NE",@"NV",@"NH",@"NJ",@"NM",@"NY",@"NC",@"ND",@"OH",@"OK",@"OR",@"PA",@"RI",@"SC",@"SD",@"TN",@"TX",@"UT",@"VT",@"VA",@"WA",@"WV",@"WI",@"WY", nil];
    // CatPicker.hidden = NO;
    mainPicker.delegate= self;
    mainPicker.dataSource=self;
    [mainPicker removeFromSuperview];
    //[ setInputView:event_picker];
[mainPicker reloadAllComponents];
    // BFLog(,@"testing--------------------->%d",opt);
    [mainPicker selectRow:0 inComponent:0 animated:YES];
    
    
    home_state.delegate=self;
    homePicker=[[UIPickerView alloc] init];
    
//    [event_picker addTarget:self action:@selector(ShowSelectedDate1) forControlEvents:UIControlEventValueChanged];
    home_state.inputView = homePicker;
    // _Enddate.inputView = datepicker;
    
    UIToolbar *toolBar2=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
    [toolBar2 setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn2=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePressed2)];
    UIBarButtonItem *spac2=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar2 setItems:[NSArray arrayWithObjects:spac2, doneBtn2,nil]];
    [self.home_state  setInputAccessoryView:toolBar2];
    
    homePicker.delegate= self;
    homePicker.dataSource=self;
    [homePicker removeFromSuperview];
    //[ setInputView:event_picker];
    [homePicker reloadAllComponents];
    // BFLog(,@"testing--------------------->%d",opt);
    [homePicker selectRow:0 inComponent:0 animated:YES];
    
    
    // Do any additional setup after loading the view.
}
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scroller02.contentInset = contentInsets;
    scroller02.scrollIndicatorInsets = contentInsets;

    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect carrierFrame=[self.carrier frame];
    CGRect main_streetFrame=[self.main_street frame];
    CGRect main_stateFrame=[self.main_state frame];
    CGRect main_zipFrame=[self.main_zip frame];
    CGRect main_cityFrame=[self.main_city frame];
    CGRect home_streetFrame=[self.home_street frame];
    CGRect home_stateFrame=[self.home_state frame];
   // CGRect home_zipFrame=[self.home_zip frame];
    CGRect home_cityFrame=[self.home_city frame];

    CGRect aRect = self.view.frame;
    aRect.size.height -= (kbSize.height+30);
    //BFLog(@"MainScreen ---(%f,%f)",aRect.size.height,aRect.origin.y);
    if([self.carrier isFirstResponder])
    {
        textfieldFrame=carrierFrame;
    }
    else if([self.main_street isEditing])
    {
        textfieldFrame=main_streetFrame;
    }
    else if([self.main_city isEditing])
    {
        textfieldFrame=main_cityFrame;
    }
    else if([self.main_state isEditing])
    {
        textfieldFrame=main_stateFrame;
    }
    else if([self.main_zip isEditing])
    {
        textfieldFrame=main_zipFrame;
    }
    else  if([self.home_street isEditing])
    {
        textfieldFrame=home_streetFrame;
    }
    else if([self.home_city isEditing])
    {
        textfieldFrame=home_cityFrame;
    }
    else if([self.home_state isEditing])
    {
        textfieldFrame=home_stateFrame;
    }
    else
    {
        //textfieldFrame=home_zipFrame;
    }
    //BFLog(@"Carrier-scrollpoint--(%f,%f)",textfieldFrame.origin.x,textfieldFrame.origin.y);
    if (aRect.size.height < (textfieldFrame.origin.y+30) ) {
        CGPoint scrollPoint = CGPointMake(0.0, textfieldFrame.origin.y-kbSize.height);

        [scroller02 setContentOffset:scrollPoint animated:YES];
    }
    }


// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scroller02.contentInset = contentInsets;
    scroller02.scrollIndicatorInsets = contentInsets;
}
/*
#pragma mark    - <UIKeyboard Notifications>
-(void) keyboardWillShow:(NSNotification *)note
{
    // Get the keyboard size
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue: &keyboardBounds];
    
    
    CGSize keyboardSize = [[[note userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    // Detect orientation
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGRect frame = _tblCarrierForm.frame;
    
    // Start animation
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3f];
    
    // Reduce size of the Table view
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
        frame.size.height -=keyboardSize.height;
    else
        frame.size.height -= keyboardSize.width;
    
    // Apply new size of table view
    _tblCarrierForm.frame = frame;
    
    // Scroll the table view to see the TextField just above the keyboard
    if (txtFieldActive)
    {
        CGRect textFieldRect = [_tblCarrierForm convertRect:txtFieldActive.bounds fromView:txtFieldActive];
        [_tblCarrierForm scrollRectToVisible:textFieldRect animated:NO];
    }
    
    [UIView commitAnimations];
}

-(void) keyboardWillHide:(NSNotification *)note
{
    // Get the keyboard size
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue: &keyboardBounds];
    
    // Detect orientation
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGRect frame = _tblCarrierForm.frame;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3f];
    
    // Increase size of the Table view
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
        frame.size.height += keyboardBounds.size.height;
    else
        frame.size.height += keyboardBounds.size.width;
    
    // Apply new size of table view
    _tblCarrierForm.frame = frame;
    
    [UIView commitAnimations];
}

*/
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];

    //BFLog(@"entered return key");
    return YES;
    
}

- (void)backgroundTap {
    [self.view endEditing:YES];
    //BFLog(@"Touched Inside");
}

- (void)donePressed1{


    [self.main_state resignFirstResponder];


}
- (void)donePressed2{


    [self.home_state resignFirstResponder];


}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{

    return 1;


}


//  returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    int row3=0;
    if ([pickerView isEqual:mainPicker]) {
        row3 = (int)[_startes count];


    }
    else if([pickerView isEqual:homePicker]){

        row3 =(int) [_startes count];

    }

    return row3;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSString *selection=@"";
    if ([pickerView isEqual:homePicker]) {
        selection = [_startes objectAtIndex:row];
    }
    else if ([pickerView isEqual:mainPicker]){
        selection = [_startes objectAtIndex:row];
    }

    return selection;

}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSInteger row1=0;
    NSString *str1=@"";

    if ([pickerView isEqual:mainPicker]) {
        row1=[mainPicker selectedRowInComponent:0];
        str1 = [_statecodes objectAtIndex:row1];
       main_state.text =str1;

    }
    else if ([pickerView isEqual:homePicker]){

        row1=[homePicker selectedRowInComponent:0];
        str1 = [_statecodes objectAtIndex:row1];
       home_state.text =str1;

    }
    // Do any additional setup after loading the view.
}
    // Do any additional setup after loading the view.

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    textfieldFrame=textField.frame;
    
    if (count_edit2==0) {
//        done_carrier.hidden=NO;
        count_edit2=1;
         carrier_id=1;
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag ==1 || alertView.tag ==2){
        
        if(buttonIndex==[alertView cancelButtonIndex]){
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"loginscreen"];
                [self presentViewController:vc animated:YES completion:nil];
            });
            
        }
        
        
    }
    
    
    
}



- (IBAction)DoneCarrier:(id)sender {
    if([carrier.text isEqual:@""] || [main_street.text isEqual:@""] || [main_city.text isEqual:@""]||[main_state.text isEqual:@""] ||[main_zip.text isEqual:@""] ||[home_street.text isEqualToString:@""] ||[home_state.text isEqual:@""] ||[home_city.text isEqual:@""]||[home_zip.text isEqualToString:@""]){
        
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"Required fields cannot be empty!"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        
        [alert1 show];
        
    }else{

    carrier_name1=carrier.text;
    main_street1=main_street.text;
    main_city1=main_city.text;
    main_state1=main_state.text;
    main_zip1=main_zip.text;
    home_street1=home_street.text;
    home_state1=home_state.text;
    home_zip1=home_zip.text;
    home_city1=home_city.text;
        DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
        
        NSDictionary *token;
        NSMutableDictionary *whereToken;
        NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
        if([userInfoArray count]!=0){
            int userId = [userInfoArray[0][0] intValue];
            token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
            
        Log *objLog=[Log new];
        objLog.carriername=carrier.text;
        objLog.carriermain=[NSString stringWithFormat:@"%@,%@,%@,%@",main_street.text,main_city.text,main_state.text,main_zip.text];
        objLog.carrierhome=[NSString stringWithFormat:@"%@,%@,%@,%@",home_street.text,home_city.text,home_state.text,home_zip.text];
        objLog.date=dateValue;
        objLog.userid=[NSString stringWithFormat:@"%d",userId];
        [objDB saveCarrierForm:objLog];
        }
    NSString *query2 = [NSString stringWithFormat:@"select * from Carrier_Sett"];
    
    // Get the results.
    
    NSArray *values1 = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query2]];
    
    if ([values1 count]!=0) {
        
        
        
        NSString *query_update = [NSString stringWithFormat:@"UPDATE Carrier_Sett SET carrier_name='%@',main_addr='%@',home_addr='%@'",carrier_name1,[NSString stringWithFormat:@"%@,%@,%@,%@",main_street1,main_city1,main_state1,main_zip1],[NSString stringWithFormat:@"%@,%@,%@,%@",home_street1,home_city1,home_state1,home_zip1]];
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_update];
        
        // If the query was successfully executed then pop the view controller.
        if (objDB.affectedRows != 0) {
           // BFLog(@"Table updated successfully. Affected rows = %d", dbManager.affectedRows);
           // c_flag=1;
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Updated Successfully!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else{
            //BFLog(@"Could not execute the query");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ooops" message:@"Something went wrong!! Please try again later!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }}
    
    else{
        
        NSString *query_insert1 = [NSString stringWithFormat:@"insert into Carrier_Sett values(null, '%@', '%@', '%@')",carrier_name1,[NSString stringWithFormat:@"%@,%@,%@,%@",main_street1,main_city1,main_state1,main_zip1],[NSString stringWithFormat:@"%@,%@,%@,%@",home_street1,home_city1,home_state1,home_zip1] ];
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_insert1];
        
        // If the query was successfully executed then pop the view controller.
        if (objDB.affectedRows != 0) {
         //   BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Updated Successfully!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
           // c_flag=1;
        }
        else{
          //  BFLog(@"Could not execute the query");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ooops" message:@"Something went wrong!! Please try again later!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        

    }
    NSMutableDictionary *driver_data = [[NSMutableDictionary alloc] init];
    
    [driver_data setValue:carrier_name1 forKey:@"Carrier"];
    [driver_data setValue:main_street1 forKey:@"OfficeAddress1"];
    [driver_data setValue:main_city1 forKey:@"OfficeAddress2"];
    [driver_data setValue:main_state1 forKey:@"OffState"];
    [driver_data setValue:home_street1 forKey:@"HomeAddress1"];
    [driver_data setValue:home_city1 forKey:@"HomeAddress2"];
    [driver_data setValue:home_state1 forKey:@"HomeState"];
    [driver_data setValue:main_zip.text forKey:@"OffZip"];
    [driver_data setValue:home_zip.text forKey:@"HomeZip"];
   // BFLog(@"data for save:%@",driver_data);
  /*  NSError *error;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
        //getting authCode
        NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
        NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
        NSString *authCode = @"";
        if([authData count]!=0){
            authCode = authData[0][1];
            
        }
        //---------------------------
    NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp1/saveHosCarrierData.php?masterUser=%@&User=%@&authCode=%@",master_menu,user_menu,authCode];
    NSURL *url = [NSURL URLWithString:urlString];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"HEAD"];
        NSURLResponse *response;
        // NSError *error;
        NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if(httpResponse!=nil){
            if ([httpResponse statusCode] ==200 ) {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
   
    NSData *postData = [NSJSONSerialization dataWithJSONObject:driver_data options:0 error:&error];
    [request setHTTPBody:postData];
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
       // BFLog(@"error:%@,%@",error,response);
        
    }];
    
    [postDataTask resume];
            }else{
                
                
                
            }
        }*/
    
}
}
/*
#pragma mark    - <Save Carrier Data>
- (IBAction)saveCarrierDetails:(id)sender{
    Section *objSec = [arrayOfTableData firstObject];
    Log *objLog=[Log new];
    NSString *strMCity, *strMStreet, *strMState, *strMZip, *strHCity, *strHStreet, *strHState, *strHZip, *strCarrierName;
    for (SectionRows *objRow in objSec.arrayRows) {
        if(objRow.rowType == row_type_form_carrier_name)
            strCarrierName = objRow.strRowContent;
        else if (objRow.rowType == row_type_form_carrier_mainAddress)
            strMStreet = objRow.strRowContent;
        else if (objRow.rowType == row_type_form_carrier_main_state){
            strMCity = objRow.strRowTitle;
            strMState = objRow.strPlaceHolder;
            strMZip = objRow.strRowContent;
        }else if (objRow.rowType == row_type_form_carrier_homeAddress)
            strHStreet = objRow.strRowContent;
        else if (objRow.rowType == row_type_form_carrier_home_state){
            strHCity = objRow.strRowTitle;
            strHState = objRow.strPlaceHolder;
            strHZip = objRow.strRowContent;
        }
    }
    
    if([strMCity isEqual:@""] || [strMStreet isEqual:@""] || [strMState isEqual:@""]||[strMZip isEqual:@""] ||[strHCity isEqual:@""] ||[strHStreet isEqualToString:@""] ||[strHState isEqual:@""] ||[strHZip isEqual:@""]||[strCarrierName isEqualToString:@""]){
        
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"Required fiels cannot be empty!"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        
        [alert1 show];
        
    }else{
        
//        carrier_name1=carrier.text;
//        main_street1=main_street.text;
//        main_city1=main_city.text;
//        main_state1=main_state.text;
//        main_zip1=main_zip.text;
//        home_street1=home_street.text;
//        home_state1=home_state.text;
//        home_zip1=home_zip.text;
//        home_city1=home_city.text;
//
        
        DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
        NSDictionary *token;
        NSMutableDictionary *whereToken;
        NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
        if([userInfoArray count]!=0){
            int userId = [userInfoArray[0][0] intValue];
            token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
            
            objLog.carriername  = strCarrierName;
            objLog.carriermain = [NSString stringWithFormat:@"%@,%@,%@,%@", strMStreet, strMCity, strMState, strMZip];
            objLog.carrierhome = [NSString stringWithFormat:@"%@,%@,%@,%@", strHStreet, strHCity, strHState, strHZip];
            objLog.date=dateValue;
            objLog.userid=[NSString stringWithFormat:@"%d",userId];
            [objDB saveCarrierForm:objLog];
        }
        NSString *query2 = [NSString stringWithFormat:@"select * from Carrier_Sett"];
        NSArray *values1 = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query2]];
        if ([values1 count]!=0) {
            NSString *query_update = [NSString stringWithFormat:@"UPDATE Carrier_Sett SET carrier_name='%@',main_addr='%@',home_addr='%@'",strCarrierName,[NSString stringWithFormat:@"%@,%@,%@,%@",strMStreet,strMCity,strMState,strMZip],[NSString stringWithFormat:@"%@,%@,%@,%@",strHStreet,strHCity,strHState,strHZip]];

            [dbManager executeQuery:query_update];
                        if (dbManager.affectedRows != 0) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Updated Successfully!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ooops" message:@"Something went wrong!! Please try again later!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }}
        
        else{
            
            NSString *query_insert1 = [NSString stringWithFormat:@"insert into Carrier_Sett values(null, '%@', '%@', '%@')",strCarrierName,[NSString stringWithFormat:@"%@,%@,%@,%@",strMStreet,strMCity,strMState,strMZip],[NSString stringWithFormat:@"%@,%@,%@,%@",strHStreet,strHCity,strHState,strHZip] ];
            [dbManager executeQuery:query_insert1];

            if (dbManager.affectedRows != 0) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Updated Successfully!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ooops" message:@"Something went wrong!! Please try again later!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }

        }
//        NSMutableDictionary *driver_data = [[NSMutableDictionary alloc] init];
//
//        [driver_data setValue:strCarrierName forKey:@"Carrier"];
//        [driver_data setValue:strMStreet forKey:@"OfficeAddress1"];
//        [driver_data setValue:strMCity forKey:@"OfficeAddress2"];
//        [driver_data setValue:strMState forKey:@"OffState"];
//        [driver_data setValue:strHStreet forKey:@"HomeAddress1"];
//        [driver_data setValue:strHCity forKey:@"HomeAddress2"];
//        [driver_data setValue:strHState forKey:@"HomeState"];
//        [driver_data setValue:strMZip forKey:@"OffZip"];
//        [driver_data setValue:strHZip forKey:@"HomeZip"];
//        [self saveToServer:driver_data];
    }
        
}

#pragma mark    - <UITableView Delegates & Datasources>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return arrayOfTableData.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    Section *objSec = [arrayOfTableData objectAtIndex:section];
    return objSec.arrayRows.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Section *objSec = [arrayOfTableData objectAtIndex:indexPath.section];
    SectionRows *objRow = [objSec.arrayRows objectAtIndex:indexPath.row];
    if(objRow.rowType == row_type_form_carrier_home_state ||  objRow.rowType == row_type_form_carrier_main_state){
        CarrierTwoTextTableViewCell *cell = (CarrierTwoTextTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"CarrierTwoTextTableViewCell" forIndexPath:indexPath];
        if (cell == nil) {
            cell = [[CarrierTwoTextTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                  reuseIdentifier:@"CarrierTwoTextTableViewCell"];
        }
        [cell setDelegate:self];
        [cell setCellWithData:objRow];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else{
        FormCommonTableViewCell *cell = (FormCommonTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"FormCommonTableViewCell" forIndexPath:indexPath];
        if (cell == nil) {
            cell = [[FormCommonTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                  reuseIdentifier:@"FormCommonTableViewCell"];
        }
        [cell setDelegate:self];
        [cell setCellWithData:objRow];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
//    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    Section *objSec = [arrayOfTableData objectAtIndex:indexPath.section];
    SectionRows *objRow = [objSec.arrayRows objectAtIndex:indexPath.row];
    if(objRow.rowType == row_type_form_carrier_mainAddress || objRow.rowType == row_type_form_carrier_homeAddress || objRow.rowType == row_type_form_carrier_name){
        return 70;
    }else{
        return 100;
    }
}
#pragma mark    - <Create Table Data>
-(void)createTableData{
    arrayOfTableData = [NSMutableArray new];
    Section *objSec = [Section new];
    
    NSString *strCarrierName;
    NSString *strMainStreet;
    NSString *strMainCity;
    NSString *strMainState;
    NSString *strMainZip;
    NSString *strHomeStreet;
    NSString *strHomeCity;
    NSString *strHomeState;
    NSString *strHomeZip;
    if ([carrier_details count]!=0) {
        
        if ([carrier_details[0][1] isEqualToString:@"(null)"]
            ||[carrier_details[0][1] isEqualToString:@"<null>"]) {
            strCarrierName  =   @"";
        }else{
            strCarrierName  =   carrier_details[0][1];
        }
        
        if (![carrier_details[0][2] isEqualToString:@""] && ![carrier_details[0][2] isEqualToString:@"(null)"] && ![carrier_details[0][2] isEqualToString:@"<null>"])
            strMainStreet   =   carrier_details[0][2];
        else
            strMainStreet   =   @"";
        if (![carrier_details[0][4] isEqualToString:@""] && ![carrier_details[0][4] isEqualToString:@"(null)"] && ![carrier_details[0][4] isEqualToString:@"<null>"])
           strMainCity =    carrier_details[0][4];
        else
            main_city.text=@"";
        if (![carrier_details[0][3] isEqualToString:@""] && ![carrier_details[0][3] isEqualToString:@"(null)"] && ![carrier_details[0][3] isEqualToString:@"<null>"])
            strMainState    =   carrier_details[0][3];
        else
            strMainState =  @"";
        if (![carrier_details[0][5] isEqualToString:@""] && ![carrier_details[0][5] isEqualToString:@"(null)"] && ![carrier_details[0][5] isEqualToString:@"<null>"])
           strMainZip   =   carrier_details[0][5];
        else
            strMainZip  =   @"";
        
        if (![carrier_details[0][6] isEqualToString:@""] && ![carrier_details[0][6] isEqualToString:@"(null)"] && ![carrier_details[0][6] isEqualToString:@"<null>"])
            strHomeStreet   =   carrier_details[0][6];
        else
            strHomeStreet   =   @"";
        if (![carrier_details[0][8] isEqualToString:@""] && ![carrier_details[0][8] isEqualToString:@"(null)"] && ![carrier_details[0][6] isEqualToString:@"<null>"])
            home_city.text=carrier_details[0][8];
        else
            home_city.text=@"";
        if (![carrier_details[0][7] isEqualToString:@""] && ![carrier_details[0][7] isEqualToString:@"(null)"] && ![carrier_details[0][7] isEqualToString:@"<null>"])
            strHomeState =   carrier_details[0][7];
        else
            strHomeState    =   @"";
        if (![carrier_details[0][9] isEqualToString:@""] && ![carrier_details[0][9] isEqualToString:@"(null)"] && ![carrier_details[0][9] isEqualToString:@"<null>"])
            strHomeZip  =   carrier_details[0][9];
        else
            strHomeZip  =   @"";
        
        
    }

    SectionRows *objRow;
    objRow = [SectionRows new];
    objRow.rowType = row_type_form_carrier_name;
    objRow.isMandatory = YES;
    objRow.strPlaceHolder = @"Carrier Name";
    objRow.strRowTitle = @"Carrier Name*";
    objRow.strRowContent = strCarrierName;
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_form_carrier_mainAddress;
    objRow.isMandatory = YES;
    objRow.strPlaceHolder = @"Street";
    objRow.strRowTitle = @"Main Office Address*";
    objRow.strRowContent = strMainStreet;
    [objSec.arrayRows addObject:objRow];
    
//    objRow = [SectionRows new];
//    objRow.rowType = row_type_form_carrier_main_city;
//    objRow.isMandatory = NO;
//    objRow.strPlaceHolder = @"City";
//    objRow.strRowTitle = @"";
//    objRow.strRowContent = strMainCity;
//    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_form_carrier_main_state;
    objRow.isMandatory = NO;
    objRow.strPlaceHolder = strMainState;
    objRow.strRowTitle = strMainCity;
    objRow.strRowContent = strMainZip;
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_form_carrier_homeAddress;
    objRow.isMandatory = YES;
    objRow.strPlaceHolder = @"Street";
    objRow.strRowTitle = @"Home Terminal Address*";
    objRow.strRowContent = strHomeStreet;
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_form_carrier_home_state;
    objRow.isMandatory = NO;
    objRow.strPlaceHolder = strHomeState;
    objRow.strRowTitle = strHomeCity;
    objRow.strRowContent = strHomeZip;
    [objSec.arrayRows addObject:objRow];
    
    [arrayOfTableData addObject:objSec];
    
    [_tblCarrierForm reloadData];
}
#pragma mark    -   <Custom Delegates>
//- (void)onTapStateForCell:(CarrierTwoTextTableViewCell *)objCell withRow:(SectionRows *)objRow andTextField:(UITextField *)txtActiveField{
//    customPickerView *customPicker = [[customPickerView alloc] init];
//    customPicker.arrayPickerData = _startes;
//    customPicker.arrayPickerDataAbbr = _statecodes;
//}
#pragma mark    - <Custom Delefates>
- (void)sendChangedRowData:(SectionRows *)objRow fortheCell:(FormCommonTableViewCell *)objCell withTextField:(UITextField *)txtActiveField{
    Section *sec = [arrayOfTableData firstObject];
    NSIndexPath *index = [_tblCarrierForm indexPathForCell:objCell];
    [sec.arrayRows replaceObjectAtIndex:index.row withObject:objRow];
    [_tblCarrierForm beginUpdates];
    txtFieldActive = txtActiveField;
    [_tblCarrierForm reloadRowsAtIndexPaths:[NSArray arrayWithObjects:index, nil] withRowAnimation:UITableViewRowAnimationNone];
    [_tblCarrierForm endUpdates];
    
    
}
- (void)scrollTableViewForTextField:(UITextField *)txtActiveField withCell:(FormCommonTableViewCell *)objCell{
    txtFieldActive = txtActiveField;
}

- (void)onSelectStateForCell:(CarrierTwoTextTableViewCell *)objCell withRow:(SectionRows *)objRow andTextField:(UITextField *)txtActiveField{
    NSLog(@"%@", objRow.strPlaceHolder);
    Section *sec = [arrayOfTableData firstObject];
    NSIndexPath *index = [_tblCarrierForm indexPathForCell:objCell];
//    if([objRow.rowType isEqualToString:@"row_type_form_carrier_main_state"])
    if(objRow.rowType == row_type_form_carrier_main_state)
        [sec.arrayRows replaceObjectAtIndex:2 withObject:objRow];
    else
        [sec.arrayRows replaceObjectAtIndex:4 withObject:objRow];
//    [sec.arrayRows replaceObjectAtIndex:index.row withObject:objRow];
    [_tblCarrierForm beginUpdates];
    txtFieldActive = txtActiveField;
    [_tblCarrierForm reloadRowsAtIndexPaths:[NSArray arrayWithObjects:index, nil] withRowAnimation:UITableViewRowAnimationNone];
    [_tblCarrierForm endUpdates];
}
- (void)sendChangedRowDataCarrier:(SectionRows *)objRow fortheCell:(CarrierTwoTextTableViewCell *)objCell withTextField:(UITextField *)txtActiveField{
    Section *sec = [arrayOfTableData firstObject];
    NSIndexPath *index = [_tblCarrierForm indexPathForCell:objCell];
    [sec.arrayRows replaceObjectAtIndex:index.row withObject:objRow];
    [_tblCarrierForm beginUpdates];
    txtFieldActive = txtActiveField;
    [_tblCarrierForm reloadRowsAtIndexPaths:[NSArray arrayWithObjects:index, nil] withRowAnimation:UITableViewRowAnimationNone];
    [_tblCarrierForm endUpdates];
}
- (void)scrollTableViewForTextFieldCarrier:(UITextField *)txtActiveField withCell:(CarrierTwoTextTableViewCell *)objCell{
    txtFieldActive = txtActiveField;
}*/
@end
