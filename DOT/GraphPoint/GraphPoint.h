//
//  GraphPoint.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/23/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GraphPoint : NSObject
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *slotno;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *annotation;
@property (nonatomic, strong) NSString *origin;
@property (nonatomic, strong) NSString *vehicle;
@property (nonatomic, strong) NSString *trailer;
@end
