//
//  DOTInspection.m
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/23/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "DOTInspection.h"
#import "commonDataHandler.h"

@implementation DOTInspection
@synthesize driverName;
@synthesize carrierName;
@synthesize distance;
@synthesize cycle;
@synthesize totalHours;
@synthesize hoursAvailableToday;
@synthesize mainOfficeAddress;
@synthesize homeTerminalAddress;
@synthesize restart;
@synthesize totalWorkHours;
@synthesize commonDate;
@synthesize timeDiff;
@synthesize timezone;
@synthesize recapList;
@synthesize graphPointList;
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initializeData];
    }
    return self;
}
-(void)initializeData{
    driverName             = @"";
    carrierName            = @"";
    distance               = @"";
    cycle                  = @"";
    totalHours             = @"";
    hoursAvailableToday    = @"";
    mainOfficeAddress      = @"";
    homeTerminalAddress    = @"";
    restart                = @"";
    totalWorkHours         = @"";
    commonDate             = @"";
    timeDiff               = @"";
    timezone               = @"";
    recapList              = [NSMutableArray new];
    graphPointList         = [NSMutableArray new];
}
-(void)getTestData{
    driverName              = @"My driver";
    carrierName             = @"My carrier";
    distance                = @"200";
    cycle                   = @"My cycle";
    totalWorkHours          = @"My total hours";
    hoursAvailableToday     = @"My hours available today";
    mainOfficeAddress       = @"My main office adddress";
    homeTerminalAddress     = @"My home terminal address";
    restart                 = @"My restart";
    totalWorkHours          = @"My total work hours";
    commonDate              = @"My common date";
    timeDiff                = @"my time diff";
    timezone                = @"My time zone";
    
    for (int i = 1; i <= 7; i++) {
        Recap *objRecap     = [Recap new];
        objRecap.date       = [NSString stringWithFormat:@"date %d", i];
        objRecap.hours      = [NSString stringWithFormat:@"hour %d", i];
        objRecap.sno        = [NSString stringWithFormat:@"%d", i];
        [recapList addObject:objRecap];
    }
    
    for (int i = 1; i<=96; i++) {
        GraphPoint *objGraphPoint   = [GraphPoint new];
        objGraphPoint.slotno        = [NSString stringWithFormat:@"%d", i];
        objGraphPoint.date          = [NSString stringWithFormat:@"date %d", i];
        objGraphPoint.status        = @"3";
        objGraphPoint.annotation    = [NSString stringWithFormat:@"annotation %d", i];
        objGraphPoint.location      = [NSString stringWithFormat:@"location %d", i];
        objGraphPoint.origin        = [NSString stringWithFormat:@"Origin %d", i];
        objGraphPoint.vehicle       = [NSString stringWithFormat:@"vehicle %d", i];
        objGraphPoint.trailer       = [NSString stringWithFormat:@"trailer %d", i];
        [graphPointList addObject:objGraphPoint];
    }
}

-(void)getTestDataWithCommonData:(NSMutableDictionary *)dotCommonDict recapData:(NSMutableDictionary *)recapDict andGraphPoint:(NSString *)statusString HavingDBValue:(NSArray*)dataFromDB forDate:(NSString *)strDateValue{
    NSLog(@"%ld",statusString.length);
    NSArray *arrayRecap = [recapDict valueForKey:@"cycleRecap"];
  //  for (int i = 0; i <= arrayRecap.count; i++) {
    int i=0;
        for (NSArray *objReacpArray in arrayRecap) {
            Recap *objRecap     = [Recap new];
            if(arrayRecap.count > 1){
                objRecap.date       = objReacpArray[0];
                objRecap.hours      = objReacpArray[1];
                objRecap.sno        = [NSString stringWithFormat:@"%d", i];
            }
            [recapList addObject:objRecap];
    //    }
            i++;
    }
    
    driverName              =[NSString stringWithFormat:@"%@ %@", [dotCommonDict valueForKey:@"firstname"], [dotCommonDict valueForKey:@"lastname"]];
    carrierName             = [dotCommonDict valueForKey:@"carriername"];
    distance                =  [dotCommonDict valueForKey:@"distance"];
    cycle                   = [dotCommonDict valueForKey:@"cycle"];
    totalWorkHours          = [recapDict valueForKey:@"cycleHours"];
    hoursAvailableToday     = [recapDict valueForKey:@"remaining"];
    mainOfficeAddress       = [dotCommonDict valueForKey:@"carriermain"];
    homeTerminalAddress     = [dotCommonDict valueForKey:@"carrierhome"];
    restart                 = [[recapDict valueForKey:@"restartDate"] valueForKey:@"date"];
    totalWorkHours          = [recapDict valueForKey:@"workedHours"];
    commonDate              = [[commonDataHandler sharedInstance] getCurrentTimeInLocalTimeZone];
    timeDiff                = @"-8";//[dotCommonDict valueForKey:@"timeDiff"];
    timezone                = [dotCommonDict valueForKey:@"timezone"];
    
    for(int i=0;i<statusString.length;i++){
        NSString *status=[statusString substringWithRange:NSMakeRange(i, 1)];
        GraphPoint *objGraphPoint   = [GraphPoint new];
        objGraphPoint.slotno        = dataFromDB[i][4];
        objGraphPoint.date          = /*strDateValue;*/dataFromDB[i][8];
        objGraphPoint.status        = status;
        objGraphPoint.annotation    = dataFromDB[i][3];
        objGraphPoint.location      = dataFromDB[i][2];
        objGraphPoint.origin        = dataFromDB[0][7];
        objGraphPoint.vehicle       = dataFromDB[i][5];
        objGraphPoint.trailer       = dataFromDB[i][6];
        [graphPointList addObject:objGraphPoint];
    }

    
   
}
@end

