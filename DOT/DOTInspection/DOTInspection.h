//
//  DOTInspection.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/23/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GraphPoint.h"
#import "Recap.h"

@interface DOTInspection : NSObject
@property (nonatomic, strong) NSString *driverName;
@property (nonatomic, strong) NSString *carrierName;
@property (nonatomic, strong) NSString *distance;
@property (nonatomic, strong) NSString *cycle;
@property (nonatomic, strong) NSString *totalHours;
@property (nonatomic, strong) NSString *hoursAvailableToday;
@property (nonatomic, strong) NSString *mainOfficeAddress;
@property (nonatomic, strong) NSString *homeTerminalAddress;
@property (nonatomic, strong) NSString *restart;
@property (nonatomic, strong) NSString *totalWorkHours;
@property (nonatomic, strong) NSString *commonDate;
@property (nonatomic, strong) NSString *timeDiff;
@property (nonatomic, strong) NSString *timezone;
@property (nonatomic, strong) NSMutableArray *recapList;
@property (nonatomic, strong) NSMutableArray *graphPointList;
-(void)getTestData;
-(void)getTestDataWithCommonData:(NSMutableDictionary *)dotCommonDict recapData:(NSMutableDictionary *)recapDict andGraphPoint:(NSString *)statusString HavingDBValue:(NSArray*)dataFromDB forDate:(NSString *)strDateValue;
@end
