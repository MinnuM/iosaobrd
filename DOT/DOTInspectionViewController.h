//
//  DOTInspectionViewController.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/24/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SectionRows.h"
#import "GraphRecapTableViewCell.h"
#import "DriverStatusTableViewCell.h"
#import "DOTDriverDetailsTableViewCell.h"
#import "editView.h"
#import "commonDataHandler.h"
@interface DOTInspectionViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *arrayOfTableData;
    UIBarButtonItem *next1;
    UIBarButtonItem *back1;
    int count1;
    NSMutableArray *dates1;
    NSMutableArray *dates_titles;
    NSString *currentDate;
    UIButton *titleLabel;
    UIView *transparentView_dotins;
    IBOutlet UIDatePicker * DatePicker;
    IBOutlet UIView *vwForDatePicker;
    IBOutlet NSLayoutConstraint *_addBannerDistanceFromBottomConstraint;
    IBOutlet UILabel *lblLogDate;
    IBOutlet UIView *vwHeading;

}
@property (weak, nonatomic) IBOutlet UINavigationItem *navigation1;
@property (nonatomic, strong) IBOutlet UITableView *tblDOTInspection;
-(IBAction)saveAsPDFAction:(id)sender;
-(IBAction)backAction:(id)sender;
-(IBAction)datePickerChanged:(id)sender;
-(IBAction)datePickerDoneAction:(id)sender;

@end
