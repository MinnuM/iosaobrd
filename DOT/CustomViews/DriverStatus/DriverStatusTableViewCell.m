//
//  DriverStatusTableViewCell.m
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/23/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "DriverStatusTableViewCell.h"
#import "NSString+NullCheck.h"
@implementation DriverStatusTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    statusDict = [NSMutableDictionary new];
    [_collecionViewStatus registerNib:[UINib nibWithNibName:@"StatusCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"StatusCollectionViewCell"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
#pragma mark    - <UICollectionView Delegates & Datasource>
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    Section *objSec = [arrayCollectionView firstObject];
    return objSec.arrayRows.count;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 7;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    Section *objSec = [arrayCollectionView firstObject];
    SectionRows *objRow = [objSec.arrayRows objectAtIndex:indexPath.section];
    StatusCollectionViewCell *statusCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"StatusCollectionViewCell" forIndexPath:indexPath];
        switch (indexPath.row) {
            case 0:
                statusCell.lblStatus.text = [[objRow.rowDictionary valueForKey:@"slno"] NullCheck];//[NSString stringWithFormat:@"%ld", indexPath.section];
                break;
            case 1:
                statusCell.lblStatus.text = [[objRow.rowDictionary valueForKey:@"status"] NullCheck];//@"Off duty";
                break;
            case 2:
                statusCell.lblStatus.text = [[objRow.rowDictionary valueForKey:@"start"] NullCheck];//@"00:00";
                break;
            case 3:
                statusCell.lblStatus.text = [[objRow.rowDictionary valueForKey:@"duration"] NullCheck];//@"09:00";
                break;
            case 4:
                statusCell.lblStatus.text = [[objRow.rowDictionary valueForKey:@"location"] NullCheck];//@"Mountain View,94043 USA";
                break;
            case 5:
                statusCell.lblStatus.text = [[objRow.rowDictionary valueForKey:@"notes"] NullCheck];//@"break";
                break;
            case 6:
                statusCell.lblStatus.text = [[objRow.rowDictionary valueForKey:@"origin"] NullCheck];//@"break";
                break;

            default:
                break;
        }
    return statusCell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CGRectGetWidth(collectionView.frame)/7, 50);
}
-(void)setCellWithData:(SectionRows *)objRow{
    arrayStatus = objRow.rowMutbleArray;
    dbArray = objRow.rowArray;
    [self createCollectionViewData];
}
-(void)createCollectionViewData{
    arrayCollectionView = [NSMutableArray new];
    NSArray *dbFirstRow  = dbArray[0];
    Section *objSec = [Section new];
    SectionRows *objRow;
    NSMutableDictionary *dictRow = [NSMutableDictionary new];
    objRow = [SectionRows new];
    [dictRow setValue:@"No." forKey:@"slno"];
    [dictRow setValue:@"Status" forKey:@"status"];
    [dictRow setValue:@"Start (PST)" forKey:@"start"];
    [dictRow setValue:@"Duration" forKey:@"duration"];
    [dictRow setValue:@"Location" forKey:@"location"];
    [dictRow setValue:@"Notes" forKey:@"notes"];
    [dictRow setValue:@"Origin" forKey:@"origin"];
    objRow.rowDictionary = dictRow;
    [objSec.arrayRows addObject:objRow];
    
    for(int i=0; i<arrayStatus.count;i++){
        NSArray *arrayTemp = [arrayStatus objectAtIndex:i];
        objRow = [SectionRows new];
        NSMutableDictionary *dictRowStatus = [NSMutableDictionary new];
        [dictRowStatus setValue:[NSString stringWithFormat:@"%d", i+1] forKey:@"slno"];
        [dictRowStatus setValue:arrayTemp[1] forKey:@"status"];
        [dictRowStatus setValue:arrayTemp[0] forKey:@"start"];
        [dictRowStatus setValue:arrayTemp[2] forKey:@"duration"];
        [dictRowStatus setValue:dbFirstRow[2] forKey:@"location"];
        [dictRowStatus setValue:dbFirstRow[3] forKey:@"notes"];
        [dictRowStatus setValue:dbFirstRow[7] forKey:@"origin"];
        objRow.rowDictionary = dictRowStatus;
        [objSec.arrayRows addObject:objRow];
    }
    [arrayCollectionView addObject:objSec];
    [_collecionViewStatus reloadData];
}
@end

