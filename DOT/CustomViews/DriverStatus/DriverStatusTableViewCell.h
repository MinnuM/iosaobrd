//
//  DriverStatusTableViewCell.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/23/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StatusCollectionViewCell.h"
#import "SectionRows.h"
@interface DriverStatusTableViewCell : UITableViewCell<UICollectionViewDelegate, UICollectionViewDataSource>
{
    NSMutableArray *arrayStatus;
    NSArray *dbArray;
    NSMutableDictionary *statusDict;
    NSMutableArray *arrayCollectionView;
}
@property (nonatomic, strong) IBOutlet UICollectionView *collecionViewStatus;
-(void)setCellWithData:(SectionRows *)objRow;
@end
