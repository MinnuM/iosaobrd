//
//  StatusCollectionViewCell.m
//  MaTrackHOSV1
//
//  Created by sieva networks on 11/8/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "StatusCollectionViewCell.h"

@implementation StatusCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code]
    [_vwStatusCell.layer setBorderWidth:1.0f];
    [_vwStatusCell.layer setBorderColor:[UIColor blackColor].CGColor];
}

@end
