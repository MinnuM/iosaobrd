//
//  StatusCollectionViewCell.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 11/8/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatusCollectionViewCell : UICollectionViewCell
{
}
@property (nonatomic, strong) IBOutlet UIView *vwStatusCell;
@property (nonatomic, strong) IBOutlet UILabel *lblStatus;
@end
