//
//  DriverDetailsCollectionViewCell.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 11/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SectionRows.h"
@interface DriverDetailsCollectionViewCell : UICollectionViewCell
{
    IBOutlet UILabel *lblKey;
    IBOutlet UILabel *lblValue;
}
-(void)setCellWithData:(SectionRows *)objRow;
@end
