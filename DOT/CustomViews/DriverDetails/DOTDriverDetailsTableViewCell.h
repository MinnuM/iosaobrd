//
//  DOTDriverDetailsTableViewCell.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/23/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DOTInspection.h"
#import "SectionRows.h"
#import "DriverDetailsCollectionViewCell.h"
@interface DOTDriverDetailsTableViewCell : UITableViewCell<UICollectionViewDelegate, UICollectionViewDataSource>
{
    NSMutableArray *arrayCollectionCells;
}
//@property (nonatomic, strong) IBOutlet UILabel *lblDriverName;
//@property (nonatomic, strong) IBOutlet UILabel *lblCarrierName;
//@property (nonatomic, strong) IBOutlet UILabel *lblDistance;
//@property (nonatomic, strong) IBOutlet UILabel *lblVehicle;
//@property (nonatomic, strong) IBOutlet UILabel *lblCycle;
//@property (nonatomic, strong) IBOutlet UILabel *lblMainOffice;
//-(void)setCellWithData:(DOTInspection *)objDOTInspection;
@property (nonatomic, strong) IBOutlet UICollectionView *collecionViewDetails;
@property (nonatomic, strong) NSMutableDictionary *dictDriverDetails;
-(void)setCellWithData:(SectionRows *)objRow;
@end
