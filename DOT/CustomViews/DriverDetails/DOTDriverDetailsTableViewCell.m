//
//  DOTDriverDetailsTableViewCell.m
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/23/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "DOTDriverDetailsTableViewCell.h"

@implementation DOTDriverDetailsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
        [_collecionViewDetails registerNib:[UINib nibWithNibName:@"DriverDetailsCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"DriverDetailsCollectionViewCell"];
    [self createCollectionViewData];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
//-(void)setCellWithData:(DOTInspection *)objDOTInspection{
//    _lblDriverName.text = objDOTInspection.driverName;
//    _lblCarrierName.text = objDOTInspection.carrierName;
//    _lblDistance.text = objDOTInspection.distance;
////    _lblVehicle.text = objDOTInspection.
//    _lblCycle.text = objDOTInspection.cycle;
//    _lblMainOffice.text = objDOTInspection.mainOfficeAddress;
//}
-(void)setCellWithData:(SectionRows *)objRow{
    _dictDriverDetails = objRow.rowDictionary;
    [self createCollectionViewData];
}
#pragma mark    - <UICollectionView Delegates & Datasource>
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return arrayCollectionCells.count;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    Section *objSec = [arrayCollectionCells objectAtIndex:section];
    return objSec.arrayRows.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    Section *objSec = [arrayCollectionCells objectAtIndex:indexPath.section];
    SectionRows *objRow = [objSec.arrayRows objectAtIndex:indexPath.item];
    DriverDetailsCollectionViewCell *statusCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DriverDetailsCollectionViewCell" forIndexPath:indexPath];
    [statusCell setCellWithData:objRow];
    return statusCell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    Section *objSec = [arrayCollectionCells objectAtIndex:indexPath.section];
    return CGSizeMake(CGRectGetWidth(collectionView.frame)/objSec.arrayRows.count, 30);
}
-(void)createCollectionViewData{
    arrayCollectionCells = [NSMutableArray new];
    Section *objSec;
    SectionRows *objRow;
    
    objSec = [Section new];
    objRow = [SectionRows new];
    objRow.rowType = row_type_dot_report_driver;
    objRow.strRowContent = [_dictDriverDetails valueForKey:@"driver"];
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_dot_report_carrier;
    objRow.strRowContent = [_dictDriverDetails valueForKey:@"carrier"];
    [objSec.arrayRows addObject:objRow];
    [arrayCollectionCells addObject:objSec];
    
    objSec = [Section new];
    objRow = [SectionRows new];
    objRow.rowType = row_type_dot_report_distance;
    objRow.strRowContent = [_dictDriverDetails valueForKey:@"distance"];
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_dot_report_vehicle;
    objRow.strRowContent = [_dictDriverDetails valueForKey:@"vehicle"];
    [objSec.arrayRows addObject:objRow];
    [arrayCollectionCells addObject:objSec];
    
    objSec = [Section new];
    objRow = [SectionRows new];
    objRow.rowType = row_type_dot_report_cycle;
    objRow.strRowContent = [_dictDriverDetails valueForKey:@"cycle"];
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_dot_report_main_office;
    objRow.strRowContent = [_dictDriverDetails valueForKey:@"mainOffice"];
    [objSec.arrayRows addObject:objRow];
    [arrayCollectionCells addObject:objSec];
    
    objSec = [Section new];
    objRow = [SectionRows new];
    objRow.rowType = row_type_dot_report_codriver;
    objRow.strRowContent = [_dictDriverDetails valueForKey:@"codriver"];
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_dot_report_trailer;
    objRow.strRowContent = [_dictDriverDetails valueForKey:@"trailer"];
    [objSec.arrayRows addObject:objRow];
    [arrayCollectionCells addObject:objSec];
    
    objSec = [Section new];
    objRow = [SectionRows new];
    objRow.rowType = row_type_dot_report_shipping;
    objRow.strRowContent = [_dictDriverDetails valueForKey:@"shippingdocno"];//shipping docs
    [objSec.arrayRows addObject:objRow];
    [arrayCollectionCells addObject:objSec];
    
    objSec = [Section new];
    objRow = [SectionRows new];
    objRow.rowType = row_type_dot_report_total_hours;
    objRow.strRowContent = [_dictDriverDetails valueForKey:@"totalHours"];
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_dot_report_restart;
    objRow.strRowContent = [_dictDriverDetails valueForKey:@"restart"];
    [objSec.arrayRows addObject:objRow];
    [arrayCollectionCells addObject:objSec];
    
    objSec = [Section new];
    objRow = [SectionRows new];
    objRow.rowType = row_type_dot_report_available_hours;
    objRow.strRowContent = [_dictDriverDetails valueForKey:@"hoursAvailable"];
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_dot_report_worked_today;
    objRow.strRowContent = [_dictDriverDetails valueForKey:@"workedToday"];
    [objSec.arrayRows addObject:objRow];
    [arrayCollectionCells addObject:objSec];
    [_collecionViewDetails reloadData];
}
@end
