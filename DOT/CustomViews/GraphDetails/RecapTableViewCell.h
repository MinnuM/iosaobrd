//
//  RecapTableViewCell.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 11/8/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecapTableViewCell : UITableViewCell
{
    IBOutlet UIView *vwRecapCell;
}
@property (nonatomic, strong) IBOutlet UILabel *lblRecapText;
@end
