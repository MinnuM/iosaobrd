//
//  GraphRecapTableViewCell.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/23/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecapTableViewCell.h"
#import "editView.h"
#import "SectionRows.h"
#define kkGraphHeight3 75

#define kkGraphBottom3 0
#define kkGraphTop3 117
#define kkStepY3 15
#define kkOffsetY3 0
#define kkCircleRadius3 1

@interface GraphRecapTableViewCell : UITableViewCell<UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UIView *vwRecaptable;
    CGRect current_rectfront4;
    CGContextRef context_front4;
    NSString *newDateString4;
    int flag_in;
    int is_editing;
    
    float kkStepX3;
    int flag_in1;
    int multiplier1;
    
    NSMutableArray *arrayTableData;
    NSMutableDictionary *dictRecap;
}
@property (nonatomic, strong) IBOutlet UITableView *tblRecap;
@property (weak, nonatomic) IBOutlet editView *graph;
-(void)setCellWithData:(SectionRows *)objRow;
@end
