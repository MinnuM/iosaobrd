//
//  LogEventEditCollectionViewCell.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 11/1/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SectionRows.h"
@class LogEventEditCollectionViewCell;
@protocol LogEventEditCollectionViewCellDelegate<NSObject>
-(void)editLogActionWithRow:(SectionRows *)objRow ForCell:(LogEventEditCollectionViewCell *)objCell;
-(void)deleteLogActionWithRow:(SectionRows *)objRow ForCell:(LogEventEditCollectionViewCell *)objCell;
-(void)AcceptLogActionWithRow:(SectionRows *)objRow ForCell:(LogEventEditCollectionViewCell *)objCell;
-(void)rejectLogActionWithRow:(SectionRows *)objRow ForCell:(LogEventEditCollectionViewCell *)objCell;
@end

@interface LogEventEditCollectionViewCell : UICollectionViewCell
{
    IBOutlet UIButton *btnEdit;
    IBOutlet UIButton *btnDelete;
    IBOutlet UIImageView *imgvwEdit;
    IBOutlet UIView *vwDelete;
    IBOutlet UIView *bgView;
    SectionRows *objCurrentRow;
}
@property (nonatomic, weak) id<LogEventEditCollectionViewCellDelegate> delegate;
-(void)setCellWithData:(SectionRows *)objRow;
-(IBAction)editOrAcceptLog:(id)sender;
-(IBAction)deleteLog:(id)sender;
@end
