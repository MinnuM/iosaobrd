//
//  LogEventEditCollectionViewCell.m
//  MaTrackHOSV1
//
//  Created by sieva networks on 11/1/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "LogEventEditCollectionViewCell.h"

@implementation LogEventEditCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setCellWithData:(SectionRows *)objRow{
    objCurrentRow = objRow;
    if([objRow.strRowContent isEqualToString:@"suggestion"]){
        bgView.layer.backgroundColor = [UIColor yellowColor].CGColor;
        bgView.layer.borderWidth = 1.0;
    }else{
        bgView.layer.backgroundColor = [UIColor whiteColor].CGColor;
        bgView.layer.borderWidth = 1.0;
    }
    if(objRow.rowType == row_type_log_edit_suggestion){
        btnEdit.enabled = YES;
        btnDelete.enabled = YES;
        vwDelete.hidden = NO;
        [imgvwEdit setImage:[UIImage imageNamed:@"tick.png"]];
    }else{
        if(objRow.rowType == row_type_log_edit_OFF){
            [imgvwEdit setImage:[UIImage imageNamed:@"editicon1.png"]];
            vwDelete.hidden = YES;
            btnDelete.enabled = NO;
        }else{
            [imgvwEdit setImage:[UIImage imageNamed:@"editicon1.png"]];
            btnEdit.enabled = YES;
            btnDelete.enabled = YES;
            vwDelete.hidden = NO;
        }
    }
}
-(IBAction)editOrAcceptLog:(id)sender
{
    if(objCurrentRow.rowType == row_type_log_edit_suggestion){
        [self.delegate AcceptLogActionWithRow:objCurrentRow ForCell:self];
    }else{
        [self.delegate editLogActionWithRow:objCurrentRow ForCell:self];
    }
    
}
-(IBAction)deleteLog:(id)sender{
    if(objCurrentRow.rowType == row_type_log_edit_suggestion){
        [self.delegate rejectLogActionWithRow:objCurrentRow ForCell:self];
    }else{
        [self.delegate deleteLogActionWithRow:objCurrentRow ForCell:self];
    }
}
@end
