//
//  DutyStatusTableViewCell.m
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/30/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "DutyStatusTableViewCell.h"

@implementation DutyStatusTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    bgView.layer.borderColor = [UIColor colorWithRed:0.125 green:0.525 blue:0.125 alpha:1.0].CGColor;
    bgView.layer.borderWidth = 1.0;
    bgView.layer.cornerRadius = 15;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setCellData:(SectionRows *)objRow{
    switch (objRow.rowType) {
        case row_type_driver_offDuty://Off duty
        {
            lblDutyStatus.text = @"OFF DUTY";
        }
            break;
        case row_type_driver_driving://Driving
        {
            lblDutyStatus.text = @"DRIVING";
        }
            break;
        case row_type_driver_onDuty://On Duty
        {
            lblDutyStatus.text = @"ON DUTY";
        }
            break;
        case row_type_driver_sleeping://Sleeper Berth
        {
            lblDutyStatus.text = @"SLEEPING";
        }
            break;
        case row_type_driver_none://None
        {
            lblDutyStatus.text = @"NONE";
        }
            break;
        default:
            break;
    }
    if(objRow.isSelected){
        bgView.layer.backgroundColor = [UIColor colorWithRed:0.125 green:0.525 blue:0.125 alpha:1.0].CGColor;
        lblDutyStatus.textColor = [UIColor whiteColor];
    }else{
        bgView.layer.backgroundColor = [UIColor whiteColor].CGColor;
        lblDutyStatus.textColor = [UIColor colorWithRed:0.125 green:0.525 blue:0.125 alpha:1.0];
    }
}
@end
