//
//  FormCommonTableViewCell.m
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/31/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "FormCommonTableViewCell.h"

@implementation FormCommonTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self action:@selector(TextFieldDoneButtonPressed)];
    keyboardToolbar.items = @[flexBarButton, doneBarButton];
    txtRowContent.inputAccessoryView = keyboardToolbar;
}
-(void)TextFieldDoneButtonPressed{
    [txtRowContent resignFirstResponder];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    row.strRowContent = textField.text;
    [self.delegate sendChangedRowData:row fortheCell:self withTextField:textField];
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self.delegate scrollTableViewForTextField:textField withCell:self];
    return YES;
}
-(void)setCellWithData:(SectionRows *)objRow{
    row = objRow;
    txtRowContent.placeholder = objRow.strPlaceHolder;
    txtRowContent.text = objRow.strRowContent;
    if(objRow.isMandatory){
        NSMutableAttributedString *text =[[NSMutableAttributedString alloc] initWithString:objRow.strRowTitle];
        
        [text addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange([objRow.strRowTitle length]-1, 1)];
        [lblRowTitle setAttributedText: text];
    }else{
        lblRowTitle.text = objRow.strRowTitle;
    }
    
    if(objRow.rowType == row_type_form_general_trailer || objRow.rowType == row_type_form_general_vehicle ||  objRow.rowType == row_type_form_general_distance){
        txtRowContent.userInteractionEnabled = NO;
    }else{
        txtRowContent.userInteractionEnabled = YES;
    }
}
@end
