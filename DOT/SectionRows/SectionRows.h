//
//  SectionRows.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/24/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum{
    row_type_driver_details,
    row_type_graph_recap,
    row_type_driver_status,
    row_type_driver_onDuty,
    row_type_driver_offDuty,
    row_type_driver_sleeping,
    row_type_driver_driving,
    row_type_driver_driving_immediate,
    row_type_driver_none,
    row_type_event,
    row_type_location,
    row_type_location_title,
    row_type_notes_title,
    row_type_notes,
    row_type_none,
    //LogForm
    row_type_form_general_driver,
    row_type_form_general_driverId,
    row_type_form_general_vehicle,
    row_type_form_general_trailer,
    row_type_form_general_distance,
    row_type_form_general_shippingDoc,
    row_type_form_carrier_name,
    row_type_form_carrier_mainAddress,
    row_type_form_carrier_homeAddress,
    row_type_form_carrier_main_state,
    row_type_form_carrier_home_state,
    row_type_form_carrier_main_city,
    row_type_form_carrier_home_city,
    
    
    row_type_log_sr_title,
    row_type_log_sr,
    row_type_log_status_title,
    row_type_log_status,
    row_type_log_start_title,
    row_type_log_start,
    row_type_log_end_title,
    row_type_log_end,
    row_type_log_notes_title,
    row_type_log_notes,
    row_type_log_origin_title,
    row_type_log_origin,
    row_type_log_edit_title,
    row_type_log_edit_OFF,
    row_type_log_edit_suggestion,
    row_type_log_edit_Others,
    
    row_type_dot_report_driver,
    row_type_dot_report_distance,
    row_type_dot_report_cycle,
    row_type_dot_report_codriver,
    row_type_dot_report_shipping,
    row_type_dot_report_total_hours,
    row_type_dot_report_available_hours,
    row_type_dot_report_carrier,
    row_type_dot_report_vehicle,
    row_type_dot_report_main_office,
    row_type_dot_report_trailer,
    row_type_dot_report_restart,
    row_type_dot_report_worked_today
} row_type;
typedef enum{
    section_type_form_general,
    section_type_form_carrier,
    section_type_form_others,
    section_type_suggestion,
    section_type_off,
    section_type_other,
} section_type;
@interface SectionRows : NSObject
@property (nonatomic) row_type rowType;
@property (nonatomic) BOOL isSelected;
@property (nonatomic, strong) NSString *strPlaceHolder;
@property (nonatomic, strong) NSString *strRowTitle;
@property (nonatomic, strong) NSString *strRowContent;
@property (nonatomic, strong) NSMutableDictionary *rowDictionary;
@property (nonatomic, strong) NSArray *rowArray;
@property (nonatomic, strong) NSMutableArray *rowMutbleArray;
@property (nonatomic) BOOL isMandatory;
@end

@interface Section : NSObject
@property (nonatomic, strong) NSMutableArray *arrayRows;
@property (nonatomic) section_type sectionType;

@end
