//
//  SectionRows.m
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/24/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "SectionRows.h"

@implementation SectionRows
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initializeData];
    }
    return self;
}
-(void)initializeData{
    _rowType = row_type_none;
    _strRowTitle = @"";
    _strRowContent = @"";
    _isSelected = NO;
    _isMandatory = NO;
    _rowDictionary = [NSMutableDictionary new];
    _rowMutbleArray = [NSMutableArray new];
}
@end


@implementation Section
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initializeData];
    }
    return self;
}
-(void)initializeData{
    _arrayRows = [NSMutableArray new];
    _sectionType = section_type_other;
}
@end
