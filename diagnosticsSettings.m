//
//  diagnosticsSettings.m
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 05/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "diagnosticsSettings.h"

@interface diagnosticsSettings ()

@end

@implementation diagnosticsSettings
@synthesize list_diagnostics;
- (void)viewDidLoad {
    [super viewDidLoad];
    list_diagnostics.delegate=self;
    list_diagnostics.dataSource=self;
    [list_diagnostics reloadData];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)loadTable{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [list_diagnostics reloadData];
        
    });
    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    int rows=1;
    return rows;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellIdentifier = @"diagnosticEvent";
    // NSString *cellIdentifier2 =@"more cell";
    // NSString *data=@"";
    // UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    UILabel *timestamp=[[UILabel alloc]initWithFrame:CGRectMake(60, 0, [UIScreen mainScreen].bounds.size.width,20)];
     timestamp.text=@"date";
    timestamp.backgroundColor=[UIColor clearColor];
    timestamp.textColor=[UIColor blackColor ];
    timestamp.minimumScaleFactor=3./cell.textLabel.font.pointSize;
    timestamp.adjustsFontSizeToFitWidth=YES;
    [timestamp setFont:[UIFont systemFontOfSize:12]];
    [cell addSubview:timestamp];
    UILabel *code=[[UILabel alloc]initWithFrame:CGRectMake(60, 20, [UIScreen mainScreen].bounds.size.width,20)];
    code.text=@"diagnostics code";
    code.backgroundColor=[UIColor clearColor];
    code.textColor=[UIColor redColor ];
    code.minimumScaleFactor=8./cell.textLabel.font.pointSize;
    code.adjustsFontSizeToFitWidth=YES;
    [code setFont:[UIFont systemFontOfSize:12]];
    [cell addSubview:code];
    UILabel *name=[[UILabel alloc]initWithFrame:CGRectMake(60, 40, [UIScreen mainScreen].bounds.size.width,20)];
    name.text=@"diagnostics name";
    name.backgroundColor=[UIColor clearColor];
    name.textColor=[UIColor blackColor ];
    name.minimumScaleFactor=8./cell.textLabel.font.pointSize;
    name.adjustsFontSizeToFitWidth=YES;
    [name setFont:[UIFont systemFontOfSize:12]];
    [cell addSubview:name];
    
    cell.imageView.image=[UIImage imageNamed:@"icons/alert.png"];
    return cell;
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

/*-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    // UITouch *touch = [touches anyObject];
    if([[self.view subviews] containsObject:transparentView_messages]){
        [transparentView_messages removeFromSuperview];
        
    }
}




/*-(void)showHelp{
    
    sourceVC = [self valueForKey:@"storyboardIdentifier"];
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc1 = [storyBoard instantiateViewControllerWithIdentifier:@"helpScreen"];
    [self presentViewController:vc1 animated:YES completion:nil];
    
    
    
}






- (IBAction)helpAlert:(id)sender {
    
   /* CGRect screenSize = [[UIScreen mainScreen] bounds];
    transparentView_messages  = [[UIView alloc] initWithFrame:screenSize];
    transparentView_messages.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:0.6];
    UIView *view1=[menu_messages valueForKey:@"view"];
    UIButton *tutorialLink = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    tutorialLink.frame = CGRectMake(screenSize.origin.x+(screenSize.size.width/3), screenSize.size.height-40, screenSize.size.width/3, 40);
    [tutorialLink addTarget:self action:@selector(showHelp) forControlEvents:UIControlEventTouchUpInside];
    tutorialLink.tintColor = [UIColor yellowColor];
    [tutorialLink setTitle:@"<<Know More>>" forState:UIControlStateNormal];
    
    [transparentView_messages addSubview:tutorialLink];
    
    
    
    
    
    UIImageView *arrow1=[[UIImageView alloc]initWithFrame:CGRectMake(view1.frame.origin.x+view1.frame.size.width/2-10, view1.frame.size.height+view1.frame.origin.y*3 , 50, 50)];
    [arrow1 setImage:[UIImage imageNamed:@"icons/arrow7.png"]];
    UILabel *dashboard  = [[UILabel alloc] initWithFrame:CGRectMake(view1.frame.origin.x+view1.frame.size.width/2, arrow1.frame.origin.y+arrow1.frame.size.height, screenSize.size.width/2, 20)];
    dashboard.text=@"Dashboard";
    dashboard.numberOfLines=2;
    dashboard.textColor=[UIColor whiteColor];
    dashboard.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    dashboard.minimumScaleFactor=10./dashboard.font.pointSize;
    dashboard.adjustsFontSizeToFitWidth=YES;
    
    
    UILabel *messages  = [[UILabel alloc] initWithFrame:CGRectMake(screenSize.origin.x+10,screenSize.size.height/2, screenSize.size.width-10, 20)];
    messages.text=@"You can see the violations of last cycle days here";
    messages.numberOfLines=2;
    messages.textColor=[UIColor whiteColor];
    messages.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    messages.minimumScaleFactor=10./messages.font.pointSize;
    messages.adjustsFontSizeToFitWidth=YES;
    [transparentView_messages addSubview:arrow1];
    [transparentView_messages addSubview:dashboard];
    [transparentView_messages addSubview:messages];
    
    
    [self.view addSubview:transparentView_messages];
 
    
}*/
@end
