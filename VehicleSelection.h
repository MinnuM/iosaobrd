//
//  VehicleSelection.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/27/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "commonDataHandler.h"

@interface VehicleSelection : NSObject
@property (nonatomic, strong) NSString *strOperation;
@property (nonatomic, strong) NSString *strResult;
@property (nonatomic, strong) NSString *strReason;
@property (nonatomic, strong) NSArray *arrayPossibleVehicles;
-(void)getDataFromAPI:(NSMutableDictionary *)objDict;
@end
