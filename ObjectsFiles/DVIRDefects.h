//
//  Carrier.h
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 12/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DVIRDefects : NSObject
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString * defectdescription;
-(NSString*)getId;
-(NSString*)getDefectDescription;
-(void)setId:(NSString *)Id;
-(void)setDefectdescription:(NSString *)Defectdescription;
@end
