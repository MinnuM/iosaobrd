//
//  Carrier.h
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 12/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DVIR2Defects : NSObject
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *driverid;
@property (nonatomic, strong) NSString *reason;
@property (nonatomic, strong) NSString *dvirProperty;
@end
