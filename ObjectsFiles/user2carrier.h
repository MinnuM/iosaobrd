//
//  Carrier.h
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 12/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface user2carrier : NSObject
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *userid;
@property (nonatomic, strong) NSString *carrierid;
@end
