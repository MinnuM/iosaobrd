//
//  Carrier.m
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 12/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "User.h"

@implementation User

@synthesize identifier;
@synthesize username;
@synthesize password;
@synthesize firstname;
@synthesize lastname;
@synthesize email;
@synthesize mobile;
@synthesize driverid;
@synthesize homeTimezone;
@synthesize cycle;
@synthesize licenseNo;
@synthesize dotNo;
@synthesize authCode;
@synthesize loggedIn;
@synthesize restartHour;
@synthesize breakHour;
@end
