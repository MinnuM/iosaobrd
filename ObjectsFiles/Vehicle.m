//
//  Carrier.m
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 12/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "Vehicle.h"

@implementation Vehicle

@synthesize identifier;
@synthesize imei;
@synthesize name;
@synthesize odometerUnit;

@end
