//
//  Carrier.m
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 12/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "Carrier.h"

@interface Carrier()
@end

@implementation Carrier
@synthesize identifier , name , mainAddress, mainCity, mainState, mainZip, homeAddress, homeCity, homeState, homeZip;


-(NSString*)getId{
    return identifier;
}

-(NSString*)getName{
    return name;
}

-(NSString*)getMainAddress{
    return mainAddress;
}

-(NSString*)getMainState{
    return mainState;
}

-(NSString*)getMainCity{
    return mainCity;
}

-(NSString*)getMainZip{
    return mainZip;
}

-(NSString*)getHomeAddress{
    return homeAddress;
}

-(NSString*)getHomeState{
    return homeState;
}

-(NSString*)getHomeCity{
    return homeCity;
}

-(NSString*)getHomeZip{
    return homeZip;
}

-(void)setId:(NSString *)Id{
    identifier=Id;
}

-(void)setName:(NSString *)Name{
    name=Name;
}

-(void)setMainAddress:(NSString *)MainAddress{
    mainAddress=MainAddress;
}

-(void)setMainState:(NSString *)MainState{
    mainState=MainState;
}

-(void)setMainZip:(NSString *)MainZip{
    mainZip=MainZip;
}

-(void)setMainCity:(NSString *)MainCity{
    mainCity=MainCity;
}

-(void)setHomeAddress:(NSString *)HomeAddress{
    homeAddress=HomeAddress;
}

-(void)setHomeState:(NSString *)HomeState{
    homeState=HomeState;
}

-(void)setHomeZip:(NSString *)HomeZip{
    homeZip=HomeZip;
}

-(void)setHomeCity:(NSString *)HomeCity{
    homeCity=HomeCity;
}


@end
