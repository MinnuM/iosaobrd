//
//  DVIR_Capture.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/12/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "DVIR_Capture.h"
#import "DVIR_Sign.h"
#import "DVIR_Main.h"
#import "menuScreenViewController.h"



#import <QuartzCore/QuartzCore.h>
int sign=0;
NSData *signData;
NSData *signMech;
NSData *syncResData2;
UIImageView *dum;
NSMutableURLRequest *request2;
#define USER_SIGNATURE_PATH  @"user_signature_path"
#define USER_SIGNATURE_PATH_MECH  @"user_signature_path_mech"

@interface DVIR_Capture ()

@end

@implementation DVIR_Capture
@synthesize signature,headerTitle;

- (void)viewDidLoad {
    [super viewDidLoad];
    sign=1;
    
    NSData *data;
    if ([selected_sign isEqualToString:@"driver"]) {
        data =[[NSUserDefaults standardUserDefaults] objectForKey:USER_SIGNATURE_PATH];
        headerTitle.title = @"Driver Signature";

    }else{
        data =[[NSUserDefaults standardUserDefaults] objectForKey:USER_SIGNATURE_PATH_MECH];
        headerTitle.title = @"Mechanic Signature";

    }
    NSMutableArray *signPathArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    [self.signature setPathArray:signPathArray];
    [self.signature setNeedsDisplay];
    CALayer *borderLayer = [CALayer layer];
    CGRect borderFrame = CGRectMake(0, 0, signature.frame.size.width, signature.frame.size.height);
    [borderLayer setBackgroundColor:[UIColor clearColor].CGColor];
    [borderLayer setFrame:borderFrame];
    [borderLayer setCornerRadius:8.0];
    [borderLayer setBorderWidth:3.0];
    [borderLayer setBorderColor:[[UIColor blackColor]CGColor]];
   // [signature.layer addSublayer:borderLayer];
    signature.layer.borderWidth=2.0;
    signature.layer.borderColor=[[UIColor blackColor] CGColor];
    // Do any additional setup after loading the view.
}
-(void)clearImage{
    [dum removeFromSuperview];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)canBecomeFirstResponder {
    return YES;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //[self becomeFirstResponder];
    
   /* NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:USER_SIGNATURE_PATH];
    NSMutableArray *signPathArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    [self.signature setPathArray:signPathArray];
    [self.signature setNeedsDisplay];*/
}

- (void)viewWillDisappear:(BOOL)animated {
    [self resignFirstResponder];
    [super viewWillDisappear:animated];
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        // your code
    }
}


-(IBAction)captureSign:(id)sender {
    //display an alert to capture the person's name
    
    
}

- (IBAction)clear:(id)sender {
    //[self.signatureView erase];
    //  [self.signatureView erase];
}

-(void)startSampleProcess:(NSString*)text {
    UIImage *captureImage = [self.signature signatureImage:CGPointMake(self.signature.frame.origin.x+10 , self.signature.frame.size.height-25) text:text];
    NSData *imagedata = UIImagePNGRepresentation(captureImage);
    NSString *image_String = [imagedata base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    CGSize destinationSize = CGSizeMake(200, 200);
    UIGraphicsBeginImageContext(destinationSize);
    [captureImage drawInRect:CGRectMake(0, 0, destinationSize.width, destinationSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    signData=UIImageJPEGRepresentation(newImage,0.75);
    if ([selected_sign isEqualToString:@"driver"]) {
        NSUserDefaults *saving_image= [NSUserDefaults standardUserDefaults];
        [saving_image setValue:image_String forKey:@"Driversign"];
        [saving_image setValue:image_String forKey:@"DriversignData"];
        
    }else{
        isMechanicSigned=1;
        NSUserDefaults *saving_image= [NSUserDefaults standardUserDefaults];
        [saving_image setValue:image_String forKey:@"Mechanicsign"];
        [saving_image setValue:image_String forKey:@"MechsignData"];
        [saving_image setValue:signData forKey:@"mechSign"];

    }
   
    [self.delegate processCompleted:captureImage];


[self createConnectionRequestToURL:@"anjana" withImage:captureImage];
//NSData *sign  = UIImagePNGRepresentation(captureImage);


/*NSString *sign_image = [sign base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
NSUserDefaults *saveSign = [NSUserDefaults standardUserDefaults];
[saveSign setValue:sign_image forKey:@"sign"];*/

}

-(BOOL) setParams{
  NSString *urlString =[NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/HOSIOSDotInspection/saveSign.php?user=%@",user_menu];
    //NSString *urlString =[NSString stringWithFormat:@"http://23.239.28.100/mydealership/HOSIOSDotInspection/saveSign.php?user=Anjana"];
    
    if(signData != nil){
        
        
        
        request2 = [NSMutableURLRequest new];
        request2.timeoutInterval = 20.0;
        [request2 setURL:[NSURL URLWithString:urlString]];
        [request2 setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request2 addValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request2 setValue:@"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" forHTTPHeaderField:@"Accept"];
        [request2 setValue:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/536.26.14 (KHTML, like Gecko) Version/6.0.1 Safari/536.26.14" forHTTPHeaderField:@"User-Agent"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        NSString *filename;
        if (![selected_sign isEqualToString:@"driver"]) {
            NSArray *dateParts=[current_date componentsSeparatedByString:@"/"];
            filename=[NSString stringWithFormat:@"mechanic_%@%@%@_%ld",dateParts[0],dateParts[1],dateParts[2],(long)DVIRID];
        }else{
            
            //filename=[NSString stringWithFormat:@"%@",user_menu];
            filename=[NSString stringWithFormat:@"demo"];
        }
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"uploadedfile\"; filename=\"%@.png\"\r\n", filename] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:signData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        [request2 setHTTPBody:body];
        [request2 addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
        
        return TRUE;
        
    }else{
        
        //BFLog(@"Failed");
        
        return FALSE;
    }
}

-(void)createConnectionRequestToURL:(NSString *)filename withImage:(UIImage *)image{
    
    if( [self setParams]){
        
        NSError *error = nil;
        NSURLResponse *responseStr = nil;
        syncResData2 = [NSURLConnection sendSynchronousRequest:request2 returningResponse:&responseStr error:&error];
       
        
        if(error == nil){
            //BFLog(@"%@",returnString);
        }
        
        
        
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData2 = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData2 appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
    
    
    //BFLog(@"_responseData %@", [[NSString alloc] initWithData:_responseData2 encoding:NSUTF8StringEncoding]);
    
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = FALSE;
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    
    //BFLog(@"didFailWithError %@", error);
    
}


- (IBAction)CaptureSign:(id)sender {
    UIAlertController * alertView=   [UIAlertController
                                      alertControllerWithTitle:@"Saving signature with name"
                                      message:@"Please enter your name"
                                      preferredStyle:UIAlertControllerStyleAlert];
    
    [alertView addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Name";
        
    }];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes, please"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    //Handel your yes please button action here
                                    UITextField *textField = alertView.textFields[0];
                                    userName = textField.text;
                                    
                                    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                                    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
                                    signedDate  = [dateFormatter stringFromDate:[NSDate date]];
                                    if(userName != nil && ![userName isEqualToString:@""] && signedDate != nil  && ![signedDate isEqualToString:@""])
                                    {
                                        [alertView dismissViewControllerAnimated:YES completion:nil];
                                        [self.signature captureSignature];
                                        [self startSampleProcess:[NSString stringWithFormat:@"By: %@, %@",userName,signedDate]];
                                        [self.navigationController popToRootViewControllerAnimated:YES];
                                    }
                                    
                                }];
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"No, thanks"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   //Handel no, thanks button
                                   [alertView dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [alertView addAction:yesButton];
    [alertView addAction:noButton];
    [self presentViewController:alertView animated:YES completion:nil];
    

}
@end
