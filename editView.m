//
//  editView.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/9/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "editView.h"
CGContextRef context_front4;
CGRect current_rectfront4;
NSString *newDateString4;
float kkStepX3;
int flag_in1=0;
int is_editing=0;
int multiplier1=0;
@implementation editView
- (void)drawLineGraphWithContext:(CGContextRef)ctx
{
    CGRect screenRect=[[UIScreen mainScreen] bounds];
    CGFloat screenWidth =screenRect.size.width;
    float kkOffsetX3=screenWidth*(31.25/414);
    CGContextSetLineWidth(ctx, 2.0);
    CGContextSetStrokeColorWithColor(ctx, [[UIColor yellowColor] CGColor]);
    int y_offset=0;
    CGContextBeginPath(ctx);
    NSString *x1=dataarray5[0][1];
    if([x1  isEqual: @"ON"]){
        y_origin1 = 5;
        CGContextMoveToPoint(ctx, kkOffsetX3, ((y_origin1) * kkStepY3));
        float x = kkOffsetX3;
        float y = ((y_origin1) * kkStepY3)+y_offset;
        CGRect rect = CGRectMake(x - kkCircleRadius3, y - kkCircleRadius3, 2 * kkCircleRadius3, 2 * kkCircleRadius3);
        CGContextAddEllipseInRect(ctx, rect);
    }else  if([x1  isEqual: @"D"]){
        y_origin1 = 4;
        CGContextMoveToPoint(ctx, kkOffsetX3, ((y_origin1) * kkStepY3));
        float x = kkOffsetX3;
        float y = ((y_origin1) * kkStepY3)+y_offset;
        CGRect rect = CGRectMake(x - kkCircleRadius3, y - kkCircleRadius3, 2 * kkCircleRadius3, 2 * kkCircleRadius3);
        CGContextAddEllipseInRect(ctx, rect);
    }else  if([x1  isEqual: @"SB"]){
        y_origin1 = 3;
        y_offset = 23/8;
        float x = kkOffsetX3;
        float y = ((y_origin1) * kkStepY3)-y_offset;
        CGRect rect = CGRectMake(x - kkCircleRadius3, y - kkCircleRadius3, 2 * kkCircleRadius3, 2 * kkCircleRadius3);
        CGContextAddEllipseInRect(ctx, rect);
        CGContextMoveToPoint(ctx, kkOffsetX3, ((y_origin1) * kkStepY3)-y_offset);
        
    }else  if([x1  isEqual: @"OFF"]){
        y_origin1 = 2;
        float x = kkOffsetX3;
        float y = ((y_origin1) * kkStepY3)-23/4;
        CGRect rect = CGRectMake(x - kkCircleRadius3, y - kkCircleRadius3, 2 * kkCircleRadius3, 2 * kkCircleRadius3);
        CGContextAddEllipseInRect(ctx, rect);
        CGContextMoveToPoint(ctx, kkOffsetX3, ((y_origin1) * kkStepY3)-23/4);
    }
    int data_size =(int) [dataarray5 count];
    for (int i = 0; i < data_size; i++)
    {
        int y_pos=0;
        NSArray *time = [dataarray5[i][0] componentsSeparatedByString:@":"];
        NSString *second_part=@"";
        int float_part =(int) [time[1] integerValue];
        if (float_part>=0 && float_part<15) {
            second_part=@"00";
            multiplier1=0;
        }else if (float_part>=15 && float_part <30)    {
            second_part = @"15";
            multiplier1=1;
        }else if (float_part>=30 && float_part<45){
            second_part=@"30";
            multiplier1=2;
            
        }else if (float_part>=45){
            second_part=@"45";
            multiplier1=3;
            
            
        }
        
//        NSString *starttime = [NSString stringWithFormat:@"%@.%@",time[0],second_part];
        NSString *time_float=[NSString stringWithFormat:@"%@",time[0]];
        float x_one1 = [time_float floatValue];
        int y=0;
        NSString *x2 = dataarray5[i][1];

        if([x2  isEqual: @"ON"]){
            y_pos =5;
            y = ((y_pos) * kkStepY3);

            
        }else  if([x2  isEqual: @"D"]){
            y_pos = 4;
            y = ((y_pos) * kkStepY3);
        }else  if([x2  isEqual: @"SB"]){
            y_pos = 3;
            y_offset = 23/8;
            y = ((y_pos) *kkStepY3)-y_offset;

            
        }else  if([x2  isEqual: @"OFF"]){
            y_pos = 2;
            y = ((y_pos) * kkStepY3)-23/4;
        }
        float x = kkOffsetX3+(kkStepX3/4*(x_one1*4+multiplier1));
        
        CGContextAddLineToPoint(ctx, (x+kkCircleRadius3),y);
        float y1 = y;
        CGRect rect = CGRectMake(x - kkCircleRadius3, y1 - kkCircleRadius3, 2 * kkCircleRadius3, 2 * kkCircleRadius3);
        CGContextAddEllipseInRect(ctx, rect);
        CGContextSetFillColorWithColor(ctx, [[UIColor redColor] CGColor]);
    }
    CGContextDrawPath(ctx, kCGPathStroke);
    CGContextSetFillColorWithColor(ctx, [[UIColor colorWithRed:1.0 green:0.5 blue:0 alpha:2.0] CGColor]);

    
    CGContextDrawPath(ctx, kCGPathFillStroke);

    UIGraphicsPushContext(ctx);
    CGContextStrokePath(ctx);
}
-(void)drawNewLine:(CGContextRef)ctx{
    CGRect screenRect=[[UIScreen mainScreen] bounds];
    CGFloat screenWidth =screenRect.size.width;
    float kkOffsetX3=screenWidth*(31.25/414);
    CGContextSetLineWidth(ctx, 2.0);
    CGContextSetStrokeColorWithColor(ctx, [[UIColor cyanColor] CGColor]);
    int y_offset=0;
    CGContextBeginPath(ctx);
    NSString *x1=newline[0][1];
    if([x1  isEqual: @"ON"]){
        y_origin1 = 5;
        CGContextMoveToPoint(ctx, kkOffsetX3, ((y_origin1) * kkStepY3));
        float x = kkOffsetX3;
        float y = ((y_origin1) * kkStepY3);
        CGRect rect = CGRectMake(x - kkCircleRadius3, y - kkCircleRadius3, 2 * kkCircleRadius3, 2 * kkCircleRadius3);
        CGContextAddEllipseInRect(ctx, rect);
    }else  if([x1  isEqual: @"D"]){
        y_origin1 = 4;
        CGContextMoveToPoint(ctx, kkOffsetX3, ((y_origin1) * kkStepY3));
        float x = kkOffsetX3;
        float y = ((y_origin1) * kkStepY3);
        CGRect rect = CGRectMake(x - kkCircleRadius3, y - kkCircleRadius3, 2 * kkCircleRadius3, 2 * kkCircleRadius3);
        CGContextAddEllipseInRect(ctx, rect);
    }else  if([x1  isEqual: @"SB"]){
        y_origin1 = 3;
        y_offset = 23;
        float x = kkOffsetX3;
        float y = ((y_origin1) * kkStepY3)-23/8;
        CGRect rect = CGRectMake(x - kkCircleRadius3, y - kkCircleRadius3, 2 * kkCircleRadius3, 2 * kkCircleRadius3);
        CGContextAddEllipseInRect(ctx, rect);
        CGContextMoveToPoint(ctx, kkOffsetX3, ((y_origin1) * kkStepY3)-23/8);
        
    }else  if([x1  isEqual: @"OFF"]){
        y_origin1 = 2;
        float x = kkOffsetX3;
        float y = ((y_origin1) * kkStepY3)-23/4;
        CGRect rect = CGRectMake(x - kkCircleRadius3, y - kkCircleRadius3, 2 * kkCircleRadius3, 2 * kkCircleRadius3);
        CGContextAddEllipseInRect(ctx, rect);
        CGContextMoveToPoint(ctx, kkOffsetX3, ((y_origin1) * kkStepY3)-23/4);
    }
    int data_size =(int) [newline count];
    for (int i = 0; i < data_size; i++)
    {
        int y_pos=0;
        NSArray *time = [newline[i][0] componentsSeparatedByString:@":"];
        NSString *second_part=@"";
        int float_part =(int) [time[1] integerValue];
        if (float_part>=0 && float_part<15) {
            second_part=@"00";
            multiplier1=0;
        }else if (float_part>=15 && float_part <30)    {
            second_part = @"15";
            multiplier1=1;
        }else if (float_part>=30 && float_part<45){
            second_part=@"30";
            multiplier1=2;
            
        }else if (float_part>=45){
            second_part=@"45";
            multiplier1=3;
            
            
        }
//        NSString *starttime = [NSString stringWithFormat:@"%@.%@",time[0],second_part];
        NSString *time_float=[NSString stringWithFormat:@"%@",time[0]];
        float x_one1 = [time_float floatValue];
        int y=0;
        NSString *x2 = newline[i][1];
        if([x2  isEqual: @"ON"]){
            y_pos =5;
            y = ((y_pos) * kkStepY3);
        }else  if([x2  isEqual: @"D"]){
            y_pos = 4;
            y = ((y_pos) * kkStepY3);
        }else  if([x2  isEqual: @"SB"]){
            y_pos = 3;
            y_offset = 23;
            y = ((y_pos) *kkStepY3)-23/8;
        }else  if([x2  isEqual: @"OFF"]){
            y_pos = 2;
            y = ((y_pos) * kkStepY3)-23/4;
        }
        float x = kkOffsetX3+(kkStepX3/4*(x_one1*4+multiplier1));
        
        CGContextAddLineToPoint(ctx, (x+kkCircleRadius3),y);
    }
    CGContextDrawPath(ctx, kCGPathStroke);
    CGContextSetFillColorWithColor(ctx, [[UIColor cyanColor] CGColor]);
    
    CGContextDrawPath(ctx, kCGPathFillStroke);

    UIGraphicsPushContext(ctx);
    CGContextStrokePath(ctx);
    
}

- (void)drawRect:(CGRect)rect {
    
    CGRect screenRect=[[UIScreen mainScreen] bounds];
    CGFloat screenWidth =screenRect.size.width;
    float kkOffsetX3=screenWidth*(31.25/414);
    float kkDefaultGraphWidth3=screenWidth-screenWidth*(58.5/414);
    current_rectfront4 = CGRectFromString(NSStringFromCGRect(rect));
    context_front4 = UIGraphicsGetCurrentContext();

    CGContextSetLineWidth(context_front4, 0.6);
    CGContextSetStrokeColorWithColor(context_front4, [[UIColor lightGrayColor] CGColor]);

    int howManyk=24;
    kkStepX3=kkDefaultGraphWidth3/howManyk;

    for (int i = 0; i <= howManyk; i++)
    {
        CGContextMoveToPoint(context_front4, kkOffsetX3 + i * kkStepX3, kkGraphTop3);
        CGContextAddLineToPoint(context_front4, kkOffsetX3 + i * kkStepX3, kkGraphBottom3);
    }
    int howManyHorizontalk = 4;
    for (int i = 0; i <howManyHorizontalk; i++)
    {
        CGContextMoveToPoint(context_front4, kkOffsetX3, kkGraphBottom3 - kkOffsetY3 - i * kkStepY3);

        CGContextAddLineToPoint(context_front4, kkDefaultGraphWidth3, kkGraphBottom3 - kkOffsetY3 - i * kkStepY3);
    }
    CGContextStrokePath(context_front4);
    CGContextSetLineDash(context_front4, 0, NULL, 0);
    
    
    UIImage *image = [UIImage imageNamed:@"graph_green.png"];
    CGRect imageRect = CGRectMake(0, 0, screenWidth,100);
    CGContextDrawImage(context_front4, imageRect, image.CGImage);

    
    [self drawLineGraphWithContext:context_front4];
    if (is_editing==1) {
        [self drawNewLine:context_front4];
        
        is_editing=0;
    }

}


@end
