//
//  TODocumentPickerViewController.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 8/26/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TODocumentPickerConstants.h"
#import "TODocumentPickerItem.h"
#import "TODocumentPickerConfiguration.h"

@interface TODocumentPickerViewController : UITableViewController

@property (nonatomic, strong,readonly, nonnull)  TODocumentPickerConfiguration *configuration; /* An object that holds the configuration for all controllers */

@property (nonatomic, strong, nullable)   id<TODocumentPickerViewControllerDataSource> dataSource;             /* Data source for file info. Retained by the document picker and shared among all children. */
@property (nonatomic, weak, nullable)     id<TODocumentPickerViewControllerDelegate>   documentPickerDelegate; /* Sends out delegate events to the assigned object */

@property (nonatomic,strong, readonly, nullable) NSString *filePath; /* The file path that this view controller corresponds to */
@property (nonatomic, strong, nullable)   NSArray  *items;    /* All of the items displayed by this view controller. (Setting this will trigger a UI refresh) */

@property (nonatomic,strong, readonly, nonnull)  TODocumentPickerViewController *rootViewController; /* In a navigation chain of picker controllers, the root controller at the front.  */
@property (nonatomic,strong, readonly, nonnull)  NSArray *viewControllers;         /* The chain of document picker view controllers in the navigation stack */

/* Create the base view controller with the initial starting file path, with a default configuration */
- (nullable instancetype)initWithFilePath:(nullable NSString *)filePath;

/* Create an instance with a custom created configuration object */
- (nullable instancetype)initWithConfiguration:(nullable TODocumentPickerConfiguration *)configuration filePath:(nullable NSString *)filePath;

/* Sets the items for the view controller in this chain, controlling that file path */
- (void)setItems:(nullable NSArray<TODocumentPickerItem *> *)items forFilePath:(nullable NSString *)filePath;

/* An initial file path must be specified, so use initWithFilePath: */
- (nonnull instancetype)init __attribute__((unavailable("Must use initWithFilePath: instead.")));
+ (nonnull instancetype)new __attribute__((unavailable("Must use initWithFilePath: instead.")));

@end

