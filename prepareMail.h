//
//  prepareMail.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/21/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "listFiles.h"


@interface prepareMail : UIViewController<MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *ccReceipient;
@property (weak, nonatomic) IBOutlet UITextField *subject;

- (IBAction)send:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *toReceipient;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@end
