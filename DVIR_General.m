//
//  DVIR_General.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/12/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "DVIR_General.h"
#import "LogSettings.h"
#import "DVIR_Main.h"

NSString *address_full2;

@interface DVIR_General (){
    
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    
}

@end
//UITextField *time;
@implementation DVIR_General
@synthesize general_Info;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *backgroundTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backgroundTap)];
    [self.view addGestureRecognizer:backgroundTapped];
    general_Info.delegate=self;
    general_Info.dataSource=self;
    location.delegate=self;
    odo.delegate=self;
    carrier.delegate=self;
   
    
    NSDate *now =[NSDate date];
    NSDateFormatter *newDate = [[NSDateFormatter alloc]init];
    [newDate setDateFormat:@"hh:mm a"];
    NSString *nowTime=[newDate stringFromDate:now];
    time=[[UITextField alloc]initWithFrame:CGRectMake(20, 10, 500, 30)];
    time.text=nowTime;
    time.delegate=self;
       startPicker=[[UIDatePicker alloc] init];
    //[startPicker datePickerMode:UIDatePickerModeTime];
    startPicker.datePickerMode =  UIDatePickerModeTime;
    startPicker.minuteInterval=15.0;
    //[event_picker addTarget:self action:@selector(ShowSelectedDate1) forControlEvents:UIControlEventValueChanged];
    time.inputView = startPicker;
    // _Enddate.inputView = datepicker;
    
    [startPicker addTarget:self action:@selector(ShowSelectedDate1) forControlEvents:UIControlEventValueChanged];
    // _start.inputView = startPickerpicker;
    // _Enddate.inputView = datepicker;
    
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
    [toolBar setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePressed1)];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space, doneBtn,nil]];
    [time setInputAccessoryView:toolBar];
    
    locationManager = [[CLLocationManager alloc] init];
    geocoder = [[CLGeocoder alloc] init];
    locationManager.delegate = self;
    [locationManager requestWhenInUseAuthorization];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    if (action==1) {
        if ([selectedDVIR count]!=0) {
            
            time.text=[selectedDVIR objectForKey:@"time"];
            location.text=[selectedDVIR objectForKey:@"location"];
            carrier.text=[selectedDVIR objectForKey:@"carrier"];
          //  BFLog(@"Odo:%@",[selectedDVIR objectForKey:@"odometer"]);
            odo.text=@"hi";
            
        }
    }
    // Do any additional setup after loading the view.
}
//minnu
// Called when the UIKeyboardDidShowNotification is sent.
// Called when the UIKeyboardWillHideNotification is sent
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    //BFLog(@"entered return key");
    return YES;
    
}
- (BOOL)allowsHeaderViewsToFloat{
    return YES ;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:self.general_Info];
    CGPoint contentOffset = self.general_Info.contentOffset;
    
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height)-100;
    
    //BFLog(@"contentOffset is: %@", NSStringFromCGPoint(contentOffset));
    
    [self.general_Info setContentOffset:contentOffset animated:YES];
    
    return YES;
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *newlocation = [locations lastObject];
    [geocoder reverseGeocodeLocation:newlocation completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error == nil&& [placemarks count] >0) {
            placemark = [placemarks lastObject];
            NSString *latitude, *longitude, *state, *country,*locality;
            latitude = [NSString stringWithFormat:@"%f",newlocation.coordinate.latitude];
            longitude = [NSString stringWithFormat:@"%f",newlocation.coordinate.longitude];
            state = placemark.administrativeArea;
            country = placemark.country;
            locality=placemark.locality;
           // BFLog(@"address1:%@,%@,%@",locality,state,country);
            address_full2=[NSString stringWithFormat:@"%@, %@",locality,state];
            if (action==1) {
                
                if ([selectedDVIR count]!=0) {
                    location.text=[selectedDVIR objectForKey:@"location"];
                }
                           }else{
                               
                               location.text=address_full2;

             
            }
            
                        //address1_full=@"";
        } else {
           // BFLog(@"%@", error.debugDescription);
        }
    }];
    // Turn off the location1 manager to save power.
    [manager stopUpdatingLocation];
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    BFLog(@"Cannot find the location.");
    if (action==1) {
        if ([selectedDVIR count]!=0) {
            
           // time.text=[selectedDVIR objectForKey:@"time"];
            location.text=[selectedDVIR objectForKey:@"location"];
           
            
        }
    }
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        UITableViewCell *cell = (UITableViewCell*)textField.superview.superview;
        NSIndexPath *indexPath = [self.general_Info indexPathForCell:cell];
        
        [self.general_Info scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    }
    
    return YES;
}

- (void)backgroundTap {
    [self.view endEditing:YES];
    //BFLog(@"Touched Inside");
}

//-(void)textFieldEndBeginEditing:(UITextField *)textField{
//}


- (void)donePressed1{
    
    
    [time resignFirstResponder];
    
    
    
}
- (void)ShowSelectedDate1
{
    
    NSDateFormatter *formatter1 =[[NSDateFormatter alloc]init];
    [formatter1 setDateFormat:@"hh:mm a"];
   time.text = [NSString stringWithFormat:@"%@",[formatter1 stringFromDate:startPicker.date]];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 4;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    int rows=1;
    if (section==3) {
        rows=2;
    }else{
        rows=1;
    }
    return rows;
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *title=@"";
    switch (section) {
        case 0:
            title=@"Time";
            break;
        case 1:
            title=@"Carrier";
            break;
        case 2:
            title=@"Location";
            break;
        case 3:
            title=@"Odometer";
            break;
            
        default:
            break;
    }
    return title;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier=@"cell";
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    //UITableViewCell *cell=[tableView de]
    cell.textLabel.textColor=[UIColor cyanColor];
    switch (indexPath.section) {
        case 0:
        {
            time.placeholder=@"Time";
            [cell.contentView addSubview:time];
            break;
        }
            
        case 1:
        {
           carrier=[[UITextField alloc]initWithFrame:CGRectMake(20, 10, 500, 30) ];
            carrier.placeholder=@"Carrier Name";
            carrier.text=cargoinfo;
            carrier.delegate=self;
            [cell.contentView addSubview:carrier];
            break;
        }
        case 2:
        {
            location = [[UITextField alloc]initWithFrame:CGRectMake(20, 10, 500, 30)];
            location.placeholder=@"Location";
            location.delegate=self;
           
            [cell.contentView addSubview:location];
            break;
            
        }
        case 3:
        {   if(indexPath.row==0){
            odo=[[UITextField alloc] initWithFrame:CGRectMake(20, 10, 500, 30)];
            odo.placeholder=@"Odometer";
            odo.delegate=self;
            odo.keyboardType = UIKeyboardTypeNumberPad;
           // odo.delegate=self;
            if (action==1) {
                if ([selectedDVIR count]!=0) {
                   
                   // time.text=[selectedDVIR objectForKey:@"time"];
                    //location.text=[selectedDVIR objectForKey:@"location"];
                    //carrier.text=[selectedDVIR objectForKey:@"carrier"];
                    //BFLog(@"Odo:%@",[selectedDVIR objectForKey:@"odometer"]);
                    odo.text=[selectedDVIR objectForKey:@"odometer"];
                    
                }
            }
            [cell.contentView addSubview:odo];
        }else if(indexPath.row==1){
            CGRect buttonRect = CGRectMake(tableView.frame.size.width/2-50, 10, 100, 30);
            UIButton *save = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            save.frame = buttonRect;
          
            [save setTitle:@"Done" forState:UIControlStateNormal];
            [save setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [save setBackgroundColor:[UIColor greenColor]];

            [save addTarget:self action:@selector(saveToDb) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:save];
            
        }
            
            break;
            
        }
        default:
            break;
    }
    return cell;

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
   // BFLog(@"time:%@",time.text);
    
    
}
-(void)saveToDb{
    //NSString *newTime=time.text;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Success!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    General_done=1;
    NSString *newLocation=location.text;
    timevalue=time.text;
    if ([newLocation isEqualToString:@""]) {
       locationValue=@"No Location";
    }
    else{
        locationValue=newLocation;
    }
    carrierValue=carrier.text;
    odovalue=odo.text;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    if ([time.text length]!=0) {
     //   BFLog(@"called from general");
        timevalue=time.text;

    }else{
        
    }
    
    if ([location.text length]!=0) {
      //  BFLog(@"called from general");
        locationValue=location.text;
        
    }else{
        
    }
}
-(void)saveEverything{
    
    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
