//
//  CaptureSignatureViewController.m
//  signature
//
//  Created by Vignesh on 2/10/16.
//  Copyright © 2016 vigneshuvi. All rights reserved.
//

#import "CaptureSignatureViewController.h"
#import "logScreenViewController.h"
#import "DVIR_Sign.h"
#import "Header.h"
#import "commonDataHandler.h"
#import "MBProgressHUD.h"
#import <QuartzCore/QuartzCore.h>
NSData *pngData;
NSData *syncResData;
NSMutableURLRequest *request;
UIView *transparentView_capture;
#define USER_SIGNATURE_PATH  @"user_signature_path"


@interface CaptureSignatureViewController ()
{
    BOOL isReAuth;
}
@end

@implementation CaptureSignatureViewController
@synthesize backToLogs;
@synthesize captureButton;
@synthesize signatureView,dbManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    sign_id=1;
    selected_sign=@"driver";
    dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
    self.signatureView.layer.borderWidth=2.0;
    self.signatureView.layer.borderColor=[[UIColor blackColor] CGColor];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)canBecomeFirstResponder {
    return YES;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self becomeFirstResponder];
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"sign"]!=nil){
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:USER_SIGNATURE_PATH];
    NSMutableArray *signPathArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    [self.signatureView setPathArray:signPathArray];
    [self.signatureView setNeedsDisplay];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [self resignFirstResponder];
    [super viewWillDisappear:animated];
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        // your code
    }
}


-(IBAction)captureSign:(id)sender {
//    if([self.signatureView signatureExists]){
    isReAuth = YES;
    UIAlertController * alertView=   [UIAlertController
                                  alertControllerWithTitle:@"Saving signature with name"
                                  message:@"Please enter your name"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [alertView addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Name";

    }];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes, please"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    //Handel your yes please button action here
                                    UITextField *textField = alertView.textFields[0];
                                    userName = textField.text;
                                    
                                    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                                    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
                                    signedDate  = [dateFormatter stringFromDate:[NSDate date]];
                                    if(userName != nil && ![userName isEqualToString:@""] && signedDate != nil  && ![signedDate isEqualToString:@""])
                                    {
                                        [alertView dismissViewControllerAnimated:YES completion:nil];
                                        [self.signatureView captureSignature];
                                        
                                        [self startSampleProcess:[NSString stringWithFormat:@"By: %@, %@",userName,signedDate]];
                                        [self.navigationController popToRootViewControllerAnimated:YES];
                                    }
                                   
                                }];
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"No, thanks"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   //Handel no, thanks button
                                   [alertView dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [alertView addAction:yesButton];
    [alertView addAction:noButton];
    [self presentViewController:alertView animated:YES completion:nil];
//    }
//    else{
//        dispatch_async(dispatch_get_main_queue(), ^{
//            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Signature"
//                                                             message:@"Please Enter Signature"
//                                                            delegate:nil
//                                                   cancelButtonTitle:@"OK"
//                                                   otherButtonTitles:nil, nil];
//            [alert1 show];
//        });
//    }

}
- (IBAction)backToLogs:(id)sender {
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"logsmenu"];
    [self presentViewController:vc animated:YES completion:nil];
}

-(void)startSampleProcess:(NSString*)text {
    UIImage *captureImage = [self.signatureView signatureImage:CGPointMake(self.signatureView.frame.origin.x+10 , self.signatureView.frame.size.height-25) text:text];
    NSData *imagedata = UIImagePNGRepresentation(captureImage);
    NSString *image_String = [imagedata base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
        NSUserDefaults *saving_image= [NSUserDefaults standardUserDefaults];
        [saving_image setValue:image_String forKey:@"Driversign"];
    CGSize destinationSize = CGSizeMake(200, 200);
    UIGraphicsBeginImageContext(destinationSize);
    [captureImage drawInRect:CGRectMake(0, 0, destinationSize.width, destinationSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
//     pngData=UIImageJPEGRepresentation(newImage,0.75);
    pngData=UIImagePNGRepresentation(newImage);
    [self createConnectionRequestToURL:@"anjana" withImage:captureImage];
    NSData *sign  = UIImagePNGRepresentation(captureImage);
   
   
    NSString *sign_image = [sign base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    NSUserDefaults *saveSign = [NSUserDefaults standardUserDefaults];
    [saveSign setValue:sign_image forKey:@"sign"];
    [self callSaveSignAPI];
    [self.delegate processCompleted:captureImage];
}

-(BOOL) setParams{
    //getting authCode
    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    NSString *authCode = @"";
    if([authData count]!=0){
        authCode = authData[0][1];
        
    }
    //---
     NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/saveSign.php?user=%@&authCode=%@",user,authCode];
    
    if(pngData != nil){
        
       
        
        request = [NSMutableURLRequest new];
        request.timeoutInterval = 20.0;
        [request setURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request setValue:@"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" forHTTPHeaderField:@"Accept"];
        [request setValue:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/536.26.14 (KHTML, like Gecko) Version/6.0.1 Safari/536.26.14" forHTTPHeaderField:@"User-Agent"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"uploadedfile\"; filename=\"%@.png\"\r\n", user] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:pngData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        [request setHTTPBody:body];
        [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
        
        return TRUE;
        
    }else{
        
        //BFLog(@"Failed");
        
        return FALSE;
    }
}

-(void)createConnectionRequestToURL:(NSString *)filename withImage:(UIImage *)image{
    
    if( [self setParams]){
        
        NSError *error = nil;
        NSURLResponse *responseStr = nil;
        syncResData = [NSURLConnection sendSynchronousRequest:request returningResponse:&responseStr error:&error];
       // NSString *returnString = [[NSString alloc] initWithData:syncResData encoding:NSUTF8StringEncoding];
        NSHTTPURLResponse *httpresponse = (NSHTTPURLResponse*)responseStr;
        
        if(error == nil){
            //BFLog(@"%@",returnString);
        }
        
       
        
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
    
   
    //BFLog(@"_responseData %@", [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding]);
    
   
    [UIApplication sharedApplication].networkActivityIndicatorVisible = FALSE;
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    
    //BFLog(@"didFailWithError %@", error);
    
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    // UITouch *touch = [touches anyObject];
    if([[self.view subviews] containsObject:transparentView_capture]){
        [transparentView_capture removeFromSuperview];
        
    }
}













    
   
- (IBAction)helpAlert:(id)sender {
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    transparentView_capture  = [[UIView alloc] initWithFrame:screenSize];
    transparentView_capture.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:0.6];
    UIView *view1=[backToLogs valueForKey:@"view"];
    UIView *view2 = [captureButton valueForKey:@"view"];
    
    
    
    
    UIImageView *arrow1=[[UIImageView alloc]initWithFrame:CGRectMake(view1.frame.origin.x+view1.frame.size.width, view1.frame.origin.y+view1.frame.size.height, 50, 50)];
    [arrow1 setImage:[UIImage imageNamed:@"icons/arrow2.png"]];
    UILabel *dashboard  = [[UILabel alloc] initWithFrame:CGRectMake(arrow1.frame.origin.x,arrow1.frame.origin.y+arrow1.frame.size.height, screenSize.size.width/2, 20)];
    dashboard.text=@"Back to Sign";
    dashboard.numberOfLines=2;
    dashboard.textColor=[UIColor whiteColor];
    dashboard.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    dashboard.minimumScaleFactor=10./dashboard.font.pointSize;
    dashboard.adjustsFontSizeToFitWidth=YES;
   
    UIImageView *arrow2=[[UIImageView alloc]initWithFrame:CGRectMake(screenSize.size.width-50, view2.frame.origin.y+view2.frame.size.height+20, 50, 50)];
    [arrow2 setImage:[UIImage imageNamed:@"icons/arrow7.png"]];
    
    UILabel *captureLabel = [[UILabel alloc] initWithFrame:CGRectMake(screenSize.origin.x+10, arrow2.frame.size.height+arrow2.frame.origin.y, screenSize.size.width-10, 20)];
    captureLabel.text=@"Click here to save drawn sign";
    captureLabel.numberOfLines=2;
    captureLabel.textColor=[UIColor whiteColor];
    captureLabel.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    captureLabel.minimumScaleFactor=10./captureLabel.font.pointSize;
    captureLabel.adjustsFontSizeToFitWidth=YES;
    
    UIImageView *arrow3=[[UIImageView alloc]initWithFrame:CGRectMake(signatureView.frame.size.width/2, signatureView.frame.origin.y-50, 50, 50)];
    [arrow3 setImage:[UIImage imageNamed:@"icons/arrow6.png"]];
    UILabel *signLabel = [[UILabel alloc] initWithFrame:CGRectMake(/*transparentView_capture.frame.origin.x+10*/250, arrow3.frame.origin.y-20, screenSize.size.width-10, 20)];
    signLabel.text=@"Draw your new sign here";
    signLabel.numberOfLines=2;
    signLabel.textColor=[UIColor whiteColor];
    signLabel.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    signLabel.minimumScaleFactor=10./signLabel.font.pointSize;
    signLabel.adjustsFontSizeToFitWidth=YES;
    
    UILabel *eraseLabel = [[UILabel alloc] initWithFrame:CGRectMake(signatureView.frame.origin.x, signatureView.frame.origin.y+signatureView.frame.size.height/2, signatureView.frame.size.width, 20)];
    eraseLabel.text=@"Long press here to clear the sign";
    eraseLabel.numberOfLines=2;
    eraseLabel.textColor=[UIColor whiteColor];
    eraseLabel.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    eraseLabel.minimumScaleFactor=10./eraseLabel.font.pointSize;
    eraseLabel.adjustsFontSizeToFitWidth=YES;

   
    [transparentView_capture addSubview: arrow1];
    [transparentView_capture addSubview:dashboard];
    [transparentView_capture addSubview:arrow2];
    [transparentView_capture addSubview:captureLabel];
    [transparentView_capture addSubview:arrow3];
    [transparentView_capture addSubview:signLabel];
 //   [transparentView_capture addSubview:eraseLabel];
    [self.view addSubview:transparentView_capture];
}
-(IBAction)clearSignAction:(id)sender{
    [self.signatureView erase];
}
#pragma mark    - <Save Sign>
-(void)callSaveSignAPI{
    
    NSMutableDictionary *requestDict = [[NSMutableDictionary alloc] init];
//    NSData * pngData=UIImageJPEGRepresentation([UIImage imageNamed:@"circle.png"],0.75);
    NSString *base64Img = [pngData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    NSString *strSign = [NSString stringWithFormat:@"data:img/png;base64,%@", base64Img];
    [requestDict setValue:OPERATION_SAVE_SIGN forKey:OPERATION];
    [requestDict setValue:[commonDataHandler sharedInstance].strUserName forKey:USER_NAME];
    [requestDict setValue:[Bugfender deviceIdentifier] forKey:DEVICE_ID];
    [requestDict setValue:[commonDataHandler sharedInstance].strAuthCode forKey:AUTH_CODE];
    [requestDict setValue:strSign forKey:SIGNATURE];
    [requestDict setValue:@"ios" forKey:@"useragent"];
    NSString *stringParams = [[commonDataHandler sharedInstance] constructInput:requestDict];
    __block NSString *responseString = @"";
    NSString *urlStringWithParam = [NSString stringWithFormat:@"payload=%@",stringParams];
    NSString *urlString = [NSString stringWithFormat:@"%@%@", WEB_ROOT, SAVE_SIGNATURE_URL];//need to get senemail php name
    NSString *enodedURl = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:enodedURl] ;
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    
    [request setHTTPBody:[urlStringWithParam dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue1 = [[NSOperationQueue alloc] init];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [NSURLConnection sendAsynchronousRequest:request queue:queue1 completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        responseString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
        if(![responseString isEqualToString:@""]){
            NSMutableArray *arrayResponse = [[NSMutableArray alloc] init];
            NSMutableArray *arrayMobileCheckIn   = [NSMutableArray new];
            [arrayMobileCheckIn removeAllObjects];
            [arrayResponse removeAllObjects];
            arrayResponse =[[commonDataHandler sharedInstance] parseResponse:responseString];
            
            if(arrayResponse!=nil && [arrayResponse count]!=0){
                
                for (NSMutableDictionary *objDict in arrayResponse) {
                    MobileCheckIn *objMobileCheckIn = [MobileCheckIn new];
                    [objMobileCheckIn getDataFromAPI:objDict];
                    [arrayMobileCheckIn addObject:objMobileCheckIn];
                }
                
                if(arrayMobileCheckIn.count > 0){
                    MobileCheckIn *objMobileCheckIn = [arrayMobileCheckIn firstObject];
                    if([objMobileCheckIn.result isEqualToString:@"failed"] && [objMobileCheckIn.operation isEqualToString:OPERATION_SAVE_SIGN]){
                        if([objMobileCheckIn.reason isEqualToString:AUTH_CODE_INVALID]){
                            //Re-auth
                            if(isReAuth){
                                isReAuth = NO;
//                                [[commonDataHandler sharedInstance] reAuthenticateUser:^(BOOL finished) {
//                                    [self callSaveSignAPI];
//                                }];
                            }
                        }else{
                            dispatch_async(dispatch_get_main_queue(), ^{
                                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Save Signature"
                                                                                 message:objMobileCheckIn.reason
                                                                                delegate:nil
                                                                       cancelButtonTitle:@"OK"
                                                                       otherButtonTitles:nil, nil];
                                [alert1 show];
                            });
                        }
                    }else{
                        //Call success
                        dispatch_async(dispatch_get_main_queue(), ^{
                            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Save Signature"
                                                                             message:objMobileCheckIn.result
                                                                            delegate:nil
                                                                   cancelButtonTitle:@"OK"
                                                                   otherButtonTitles:nil, nil];
                            [alert1 show];
                        });
                    }
                }
            }
        }
    }];
    
}

@end
