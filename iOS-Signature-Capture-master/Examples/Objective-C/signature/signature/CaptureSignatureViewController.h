//
//  CaptureSignatureViewController.h
//  signature
//
//  Created by Vignesh on 2/10/16.
//  Copyright © 2016 vigneshuvi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UviSignatureView.h"
#import "../../../../../MaTrackHOSV1/AppDelegate.h"
#import "DBManager1.h"





@interface CaptureSignatureViewController : UIViewController {
    // Delegate to respond back
    //id <CaptureSignatureViewDelegate> _delegate;
    NSString *userName, *signedDate;
    NSMutableData *_responseData;
    
}

@property (nonatomic,strong) id delegate;
-(void)startSampleProcess:(NSString*)text;
// Instance method
@property (nonatomic, strong) DBManager1 *dbManager;
@property (strong, nonatomic) IBOutlet UviSignatureView *signatureView;
- (IBAction)captureSign:(id)sender;
- (IBAction)backToLogs:(id)sender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *backToLogs;
- (IBAction)helpAlert:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *captureButton;
-(IBAction)clearSignAction:(id)sender;
@end
