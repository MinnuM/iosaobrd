//
//  logScreenViewController.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/26/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "logScreenViewController.h"
#import <BugfenderSDK/BugfenderSDK.h>
#import "KeychainItemWrapper.h"
#import "insertDutyCycle.h"
#import "MBProgressHUD.h"
#import "ELDInfoCell.h"
#import "menuScreenViewController.h"
#import "StatusViewController.h"
#import "NotificationHandler.h"
#import "Header.h"
#import "user2vehicle.h"
NSString *selected_previous;
NSUInteger selected_index;
NSDictionary *json_log;
NSString *today_log;
NSString *masterUser=@"";
NSString *user=@"";
NSString *current_status;
NSString *driver_names=@"";
NSString *carrier_name=@"";
NSString *main_office=@"";
NSString *selected_date=@"";
int no_of_days=0;
int section_index=0;
int row_index=0;
int rows_num=7;
NSDate * now;
int refresh_flag=0;
NSDictionary *json_loginscreen;
NSMutableArray *locations;
NSMutableArray *notesLog;
NSMutableArray *unapproved_dates;
NSMutableArray *dateslist;
NSMutableArray *dateList;
NSMutableArray *data_to_finaldb1;
NSMutableDictionary *data_dict1;
NSString *authCode;
UITableViewCell *cell;
UIView *transparentView ;
@interface logScreenViewController ()
{
    //driver_status driverStatus;
    NSString *strSelectedVehicle;
}
@end
#define TEST_USE_MG_DELEGATE 1
@implementation logScreenViewController
@synthesize scroller;
@synthesize addButton;
@synthesize dbManager;
//@synthesize dbManager1;
@synthesize menu;
@synthesize info,navBar,navBarItem;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:SELECTED_VEHICLE];
    if(savedValue.length > 0){
        strSelectedVehicle = savedValue;
        
    }
    else{
        strSelectedVehicle = @"No Vehicle";
    }
    if([strSelectedVehicle isEqualToString:@"No Vehicle"]){
    DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
        NSDictionary *token=@{@"columnName":@"userid",@"entry":[NSString stringWithFormat:@"%d",userId],};
        NSMutableDictionary *whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        user2vehicle *objUsrVeh=[user2vehicle new];
        objUsrVeh.userid=[NSString stringWithFormat:@"%d",userId];
        NSArray * selectVeh=[[NSArray alloc]initWithArray:[objDB selectFromTable:objUsrVeh whereToken:whereToken]];
        if([selectVeh count]!=0){
            strSelectedVehicle = selectVeh[0][2];
        }
    }
}
    
    //check for malfunction
//    BOOL malfunction = false;
//    if(malfunction){
//
//        navBarItem.title=@"Malfunction(P)";
//        navBar.barTintColor = [UIColor redColor];
//            }
//    dispatch_async(dispatch_get_main_queue(), ^{
//
//    UIAlertView *fleetSuggestionView = [[UIAlertView alloc] initWithTitle:@"Fleet Manager's Suggestions" message:@"The fleet manager has made the following changes \n\n\n \tDate\tStart \t End \t Event\n09/26/2017\t00:00\t03:00\tON" delegate:self cancelButtonTitle:@"Reject" otherButtonTitles:@"Accept", nil];
//   [fleetSuggestionView show];
//    });
    dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    authCode = @"";
    if([authData count]!=0){
        authCode = authData[0][1];
        
    }
    
    sign_id=0;
    form_id=0;
    DVIR_selected=0;
    json_loginscreen = [[NSDictionary alloc]init];
    json_log = [[NSDictionary alloc]init];
    dateslist=[[NSMutableArray alloc]init];
    locations=[[NSMutableArray alloc] init];
    notesLog=[[NSMutableArray alloc] init];
    data_to_finaldb1=[[NSMutableArray alloc]init];
    masterUser = [[NSUserDefaults standardUserDefaults] objectForKey:@"masteruser"];
    user = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
    
    now = [NSDate date];
    NSDateFormatter *outputFormatter_log = [[NSDateFormatter alloc] init];
    [outputFormatter_log setDateFormat:@"MM/dd/YYYY"];
    today_log = [outputFormatter_log stringFromDate:now];
    
    sign_id=0;
    form_id=0;
    
   
   /*raghee [self getTodaysData:today_log];*/
    rows_num=7;
    //minnu
    DBManager *objDB = [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    NSDictionary *token;
    ;
    NSMutableDictionary *whereToken;
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
        token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
        ;
    whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];

    //[objDB getLogEditData:whereToken];
    dateList =[NSMutableArray array];
    for(int i=0;i<15;i++){
        
       NSArray *dateTemp=[[NSArray alloc]initWithArray: [objDB getLast14Days:i]];
        token=@{@"userid":[NSString stringWithFormat:@"%d",userId],@"date":[NSString stringWithFormat:@"%@",dateTemp[0][0]]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        NSArray * approveArr=[[NSArray alloc] initWithArray:[objDB getApproveLog:whereToken]];
        NSString *approve=@"0";
        if([approveArr count]!=0){
            approve=approveArr[0][0];
        }
        NSArray* dataFromDB;
        if(i==0){
       dataFromDB=[[NSArray alloc]initWithArray:[objDB getLogEditData:whereToken]];
        }
        NSString *statusString=@"";
        if([dateTemp count]!=0){
            for(int j=0;j<[dataFromDB count];j++){
                if([dataFromDB[j][1] isEqualToString:@""])
                    statusString= [statusString stringByAppendingString:[NSString stringWithFormat:@"1"]];
                else
                    statusString= [statusString stringByAppendingString:[NSString stringWithFormat:@"%@",[[dataFromDB objectAtIndex:j] objectAtIndex:1]]];
            }
            //NSArray * currentStatus=[[NSArray alloc] initWithArray:[objDB getCurrentStatus:userId]];
            NSString* charStr=@"";
          //  if([currentStatus count]!=0)
          //      charStr=[@"" stringByPaddingToLength:96 withString: currentStatus[0][0] startingAtIndex:0];
           // else
                charStr=[@"" stringByPaddingToLength:96 withString:@"1" startingAtIndex:0];
            commonDataHandler *common=[commonDataHandler new];
            charStr=[common modifyData:charStr date:dateTemp[0][0]];
            if([dataFromDB count]==0)
                [dateList addObject:@{@"date":dateTemp[0][0],@"approved":approve,@"data":charStr}];
            else
            {
                if([statusString isEqualToString:@""] || statusString.length<96)
                    statusString=charStr;
                statusString=[common modifyData:statusString date:dateTemp[0][0]];
                [dateList addObject:@{@"date":dateTemp[0][0],@"approved":approve,@"data":statusString}];
            }
        }
    }
}
    if(!_isFromPush){
        NSString *temp=[[dateList[0] objectForKey:@"data"] stringByReplacingOccurrencesOfString:@"0" withString:@""];
        temp=[temp substringFromIndex:(int)(temp.length-1)];
        
        NSString *status=[self getStatusCode:temp];
        BFLog(@"current_status:%@",status);
        current_status=status;
    }else{
        if(_driverStatus == driver_status_onDuty || _driverStatus == driver_status_onDuty_immediate){
            if(self.isImmediate){
                self.isImmediate = NO;
                current_status = @"ON";
                
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    alert = [[UIAlertView alloc]initWithTitle:@"Status Change" message:@"Do you want to continue driving or change your duty status" delegate:self cancelButtonTitle:@"Continue Driving" otherButtonTitles:@"Change Status", nil];
                    alert.tag=1005;
                    alert.delegate=self;
                    [alert show];
                    [self performSelector:@selector(dismissAlertView) withObject:nil afterDelay:60.0];
                });
            }
        }else{
            if(_driverStatus == driver_status_offDuty)
               current_status = @"OFF";
           else if(_driverStatus == driver_status_none)
               current_status = @"NONE";
            else if(_driverStatus == driver_status_driving)
                current_status = @"D";
            else if (_driverStatus == driver_status_yard_move)
                current_status = @"YM";
            else if (_driverStatus == driver_status_personal_convey)
                current_status = @"PC";
                
        }
        //        NSLog(@"%@", current_status);
    }
    [dataDictArray removeAllObjects];
    graphDataOperations *obj=[graphDataOperations new];
    for(int j=0;j<[dateList count];j++){
        [dataarray removeAllObjects];
         NSMutableArray *dict=[obj generateGraphPoints:[[dateList objectAtIndex:j] objectForKey:@"data"]];
         for (int i=0; i<[dict count]; i++) {
             
//             NSLog(@"posx:%@,%d",[dict[0] objectForKey:@"posx"],i);
             float posx=[[dict[i] objectForKey:@"posx"] floatValue];
             float x1 = posx - 40.0;
             int x2 = x1/12.25;
             int x3 = x2/4.0;
             int x4 = x2%4;
             
             NSString *minutes;
             if (15*x4 <15) {
                 minutes=@"00";
             }
             else if(15*x4 >=15 && 15*x4 <30){
                 minutes=@"15";
             }else if(15*x4 >=30 && 15*x4 <45){
                 minutes=@"30";
             }else if(15*x4 >=45 && 15*x4 <60){
                 minutes=@"45";
             }
             NSString *posx_1 = [NSString stringWithFormat:@"%d:%@",x3,minutes];
             
             // float time = [posx_1 floatValue];
             int event_no =(int)[[dict[i] objectForKey:@"posy"] integerValue];
             NSString * event;
             if (event_no == 245) {
                 event=@"ON";
             }
             else if(event_no == 125){
                 event=@"SB";
                 
             }else if (event_no == 185){
                 event = @"D";
             }else if (event_no == 65){
                 event = @"OFF";
             }
             NSArray *final_data = [[NSArray alloc] initWithObjects:posx_1,event, nil];
             [dataarray addObject:final_data];
         }
        dataDictArray[j]=[dataarray mutableCopy];
         //dataarray=payload;
//         NSLog(@"payload:%@",dict);
         NSString *returnString = [obj generateCharString:dict];
//         NSLog(@"charStringFrompayload:%@\n",returnString);
//         NSLog(@"charStringFrompayload:%lu",(unsigned long)[returnString length]);
        
    }

        //minnu

    no_of_days =(int)[dateList count]/2;

    self.logtable.delegate=self;
    self.logtable.dataSource=self;
    
    
    
    //[self.view addSubview:addButton];
    self.scroller.delegate=self;
    [scroller setScrollEnabled:YES];
    [scroller setContentSize:(CGSizeMake(600, 1000))];
    CGPoint bottomOffset = CGPointMake(0,0);
    [scroller setContentOffset:bottomOffset animated:YES];
    //getting authCode
    dispatch_async(dispatch_get_main_queue(), ^{
        [_logtable reloadData];

    });

    
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DataUpdated" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUpdatedData:)
                                                 name:@"DataUpdated"
                                               object:nil];
//    [self loadFormData];
}
- (void)dismissAlertView {
    current_status = @"ON";
    [self saveData:@"ON"];
    dispatch_async(dispatch_get_main_queue(), ^{
        [_logtable reloadData];
        [alert dismissWithClickedButtonIndex:-1 animated:YES];
    });
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidAppear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)handleUpdatedData:(NSNotification *)notification {
    NSDictionary* userInfo = notification.userInfo;
    driver_status drvStat;
    int driverStat;
    driverStat = [[userInfo valueForKey:@"driverStatus"] intValue];
    if(driverStat == 1){
        drvStat = driver_status_driving;
        NotificationHandler *objHelper = [NotificationHandler new];
        [objHelper recievePushNotification:self withDriverStatus:drvStat];
    }
    else{
        if (driverStat == 2){
            //  current_status = @"ON";
            //  drvStat = driver_status_onDuty;
            dispatch_async(dispatch_get_main_queue(), ^{
                alert = [[UIAlertView alloc]initWithTitle:@"Status Change" message:@"Do you want to continue driving or change your duty status" delegate:self cancelButtonTitle:@"Continue Driving" otherButtonTitles:@"Change Status", nil];
                alert.tag=1005;
                alert.delegate=self;
                [alert show];
                [self performSelector:@selector(dismissAlertView) withObject:nil afterDelay:60.0];
            });
        }else{
            if (driverStat == 3)
                current_status = @"OFF";
            else if (driverStat == 4)
                current_status = @"NONE";
            else if (driverStat == 6)
                current_status = @"ON";
            
            [self saveData:current_status];
            
        }

        
        dispatch_async(dispatch_get_main_queue(), ^{
            [_logtable reloadData];
        });
    }
    
}
-(void)timedTask{
    
    // NSTimer *fiveSecTimer = [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(updatingData) userInfo:nil repeats:YES];
    
}
/*-(void)loadFormData{
    //getting authCode
    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    NSString *authCode = @"";
    if([authData count]!=0){
        authCode = authData[0][1];
        
    }
    //---
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/getEditFormData.php?authCode=%@",authCode];
    NSURL *url = [NSURL URLWithString:urlString];
    //NSData *data = [NSData dataWithContentsOfURL:url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSURLResponse *response;
    NSError *error;
    NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    
    
    if (httpResponse!=nil) {
        // [loader startAnimating];
        BFLog(@"Device connected to the internet");
        //NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:urlString] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            //  BFLog(@"Retrieved Data:%@,%@",data,response);
            if(data){
                NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                NSMutableArray *general = [json[0] objectForKey:@"generalDetails"];
                NSMutableArray *other = [json[0] objectForKey:@"otherDetails"];
                if([general count]!=0){
                    
                    NSString *query1 = [NSString stringWithFormat:@"select * from GeneralSettings"];
                    
                    // Get the results.
                    
                    NSArray *values = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query1]];
                    
                    if ([values count]!=0) {
                        
                        
                        
                        NSString *query_update = [NSString stringWithFormat:@"UPDATE GeneralSettings SET vehicles='%@',trailers='%@',distance='%@',shipping_docu='%@',driver='%@',driverId='%@'",general[0][@"vehicle"],general[0][@"trailer"],general[0][@"distance"],general[0][@"shipping"],[NSString stringWithFormat:@"%@ %@",general[0][@"firstName"],general[0][@"lastName"]],general[0][@"driverId"]];
                        
                        // Execute the query.
                        
                        //NSString *query_del = @"delete from TempTable";
                        [dbManager executeQuery:query_update];
                        
                        // If the query was successfully executed then pop the view controller.
                        if (dbManager.affectedRows != 0) {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Updated Successfully!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(220, 0, 30, 30)];
                            [img setImage:[UIImage imageNamed:@"icons/mark.png"]];
                            [alert addSubview:img];
                            //[alert show];
                            
                        }
                        else{
                            
                           // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ooops" message:@"Something went wrong!!Please try again later!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            //[alert show];
                            
                            //BFLog(@"Could not execute the query");
                        }}
                    
                    else{
                        
                        NSString *query_insert1 = [NSString stringWithFormat:@"insert into GeneralSettings values(null, '%@', '%@', '%@' ,'%@','%@','%@')",general[0][@"vehicle"],general[0][@"trailer"],general[0][@"distance"],general[0][@"shippingDoc"],[NSString stringWithFormat:@"%@ %@",general[0][@"firstName"],general[0][@"lastName"]],general[0][@"driverId"]];
                        
                        // Execute the query.
                        
                        //NSString *query_del = @"delete from TempTable";
                        [dbManager executeQuery:query_insert1];
                        
                        // If the query was successfully executed then pop the view controller.
                        if (dbManager.affectedRows != 0) {
                            BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Updated Successfully!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(220, 0, 30, 30)];
                            [img setImage:[UIImage imageNamed:@"icons/mark.png"]];
                            [alert addSubview:img];
                            // [alert show];
                            
                        }
                        else{
                            
                           // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ooops" message:@"Something went wrong!!Please try again later!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            //   [alert show];
                            // BFLog(@"Could not execute the query");
                        }
                        
                        
                        
                    }
                    
                    
                    
                    
                    
                }
                if([other count]!=0){
                    
                    
                    
                    NSString *query3 = [NSString stringWithFormat:@"select * from OtherSettings"];
                    
                    // Get the results.
                    
                    NSArray *values2 = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query3]];
                    
                    if ([values2 count]!=0) {
                        
                        
                        
                        NSString *query_update = [NSString stringWithFormat:@"UPDATE OtherSettings SET codriver='%@',origin='%@',destination='%@',notes='%@'",other[0][@"coDriver"],other[0][@"from"],other[0][@"to"],other[0][@"note"]];
                        
                        // Execute the query.
                        
                        //NSString *query_del = @"delete from TempTable";
                        [dbManager executeQuery:query_update];
                        
                        // If the query was successfully executed then pop the view controller.
                        if (dbManager.affectedRows != 0) {
                            BFLog(@"Table updated successfully. Affected rows = %d", dbManager.affectedRows);
                            //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Updated Successfully!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            // [alert show];
                            
                            
                        }
                        else{
                            BFLog(@"Could not execute the query");
                            //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ooops" message:@"Something went wrong.Please try again later!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            // [alert show];
                            
                        }}
                    
                    else{
                        
                        NSString *query_insert1 = [NSString stringWithFormat:@"insert into OtherSettings values(null, '%@', '%@', '%@','%@')",other[0][@"coDriver"],other[0][@"from"],other[0][@"to"],other[0][@"note"]];
                        
                        // Execute the query.
                        
                        //NSString *query_del = @"delete from TempTable";
                        [dbManager executeQuery:query_insert1];
                        
                        // If the query was successfully executed then pop the view controller.
                        if (dbManager.affectedRows != 0) {
                            BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
                           // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Updated Successfully!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            // [alert show];
                            
                            //c_flag=1;
                        }
                        else{
                            BFLog(@"Could not execute the query");
                           // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ooops" message:@"Something went wrong.Please try again later!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            //[alert show];
                            
                        }
                        
                        
                        
                    }
                    
                    
                    
                    
                    
                    
                    
                }
                
                
                
            }
        }];
        [dataTask resume];
        
        
    }
    
    
}

*/


-(NSString*)current_status{
//    NSLog(@"dataFirst:%@",dataDictArray[0]);
    NSString *sta;
    NSString *query_status = [NSString stringWithFormat:@"select last_status from TempTable where date='%@';",today_log];
    
    // Get the results.
    
    NSArray *todays_laststatus = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query_status]];
    // BFLog(@"todays last status:%@",todays_laststatus[0][0]);
    
    
    if([todays_laststatus count]==0){
        
        NSDate *today = [NSDate date];
        NSDate *previous = [today dateByAddingTimeInterval:-1*24*60*60];
        
        NSDateFormatter *outputFormatter11 = [[NSDateFormatter alloc] init];
        [outputFormatter11 setDateFormat:@"MM/dd/YYYY"];
        NSString *previousdate1 = [outputFormatter11 stringFromDate:previous];
        NSString *query_status = [NSString stringWithFormat:@"select flag,last_status from TempTable where date='%@';",previousdate1];
        
        // Get the results.
        
        NSArray *ystrdaays_laststatus = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query_status]];
        if ([ystrdaays_laststatus count]!=0) {
            
            
            if ([ystrdaays_laststatus[0][0] integerValue]==1) {
                sta=ystrdaays_laststatus[0][1];
            }else{
                sta=@"OFF";
            }
            BFLog(@"previous last status:%@",ystrdaays_laststatus);
            
        } else{
            sta=@"OFF";
            
        }
        
        
        
    }  else{
        
        sta=todays_laststatus[0][0];
    }
    
    
    return sta;
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    // if(scrollView.contentOffset.y==0){
    
    
    /*  UIImageView *image=NULL;
     NSIndexPath *indexpath1 = [NSIndexPath indexPathForRow:0 inSection:0];
     UITableViewCell *cell1=[_logtable cellForRowAtIndexPath:indexpath1];
     cell1.accessoryView=image;
     NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:1];
     UITableViewCell *cell=[_logtable cellForRowAtIndexPath:indexpath];
     cell.imageView.image=Nil;*/
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [_logtable reloadData];
        
    });
    
}


-(void)updatedatelist:(int)count{
    [dateslist removeAllObjects];
    [unapproved_dates removeAllObjects];
    
    
    // NSString *query_dates =[NSString stringWithFormat:@"select date,flag from TempTable where \"24hr_off\"='%d'  order by date desc limit '%d';",0,count];
    
    NSString *query_dates =[NSString stringWithFormat:@"select date,flag,\"24hr_off\" from TempTable where \"24hr_off\"='%d' and date!='%@' order by dateInSecs desc limit '%d';",0,@"(null)",count];
    
    // Get the results.
    
    NSArray *working_days = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query_dates]];
    BFLog(@"Result:%@",working_days);
    //minnu
    //  if(![working_days isKindOfClass:[NSNull class]])
    //   {
    
    for (int i=1; i<[working_days count]; i++) {
        if ([working_days[i][2] integerValue]==0) {
            [dateslist addObject:working_days[i][0]];
            
            
        }else{
            
            if ([working_days[i][1] integerValue]==1) {
                [dateslist addObject:working_days[i][0]];            }
        }
        //   }
    }
    
    
    NSString *query0 = [NSString stringWithFormat:@"select date,flag from TempTable order by dateInSecs desc limit '%d';",count];
    
    // Get the results.
    
    NSArray *previousdates = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    BFLog(@"Result:%@",previousdates);
    unapproved_dates=[[NSMutableArray alloc] init];
    NSString *msg=@"";
    //minnu
    //if(![working_days isKindOfClass:[NSNull class]])
    //  {
    
    for (int i=1; i<[working_days count]; i++) {
        BFLog(@"%@",previousdates[i][1]);
        if ([working_days[i][1] isEqual:@"0"]) {
            [unapproved_dates addObject:working_days[i][0]];
            NSString *sam = [NSString stringWithFormat:@"%C %@\n",(unichar)0x2022,previousdates[i][0]];
            msg = [NSString stringWithFormat:@"%@%@",msg,sam];
        }
        
    }
    //    }
    no_of_days=(int)[dateslist count];
    BFLog(@"Unapproved dates:%@",unapproved_dates);
    if([unapproved_dates containsObject:@"(null)"]){
        
        [unapproved_dates removeObject:@"(null)"];
        
    }
    // [self.logtable reloadSections:[[NSIndexSet alloc]initWithIndex:2] withRowAnimation:NO];
    // [_logtable reloadData];
    
}
-(void) didSelectMenuOptionAtIndex:(NSInteger)row
{
    BFLog(@"Floating action tapped index %tu",row);
    NSDate *minDate = [[NSDate date] dateByAddingTimeInterval:60*60*24*-90];
    picker = [[UIDatePicker alloc] init];
    [picker setMaximumDate:[NSDate date]];
    [picker setMinimumDate:minDate];
    [picker setDatePickerMode:UIDatePickerModeDate];
    alert = [[UIAlertView alloc] initWithTitle:@"Please select a date"
                                       message:@"  "
                                      delegate:self
                             cancelButtonTitle:@"Cancel"
                             otherButtonTitles:@"OK", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].inputView=picker;
    UIToolbar *toolBar2=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
    [toolBar2 setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn2=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePressed2)];
    UIBarButtonItem *spac2=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar2 setItems:[NSArray arrayWithObjects:spac2, doneBtn2,nil]];
    [[alert textFieldAtIndex:0]  setInputAccessoryView:toolBar2];
    [alert addSubview:picker];
    alert.tag=1;
    [alert show];
    
}
-(void)donePressed2{
    
    BFLog(@"%@",picker.date);
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/YYYY"];
    NSString *date_selected = [formatter stringFromDate:picker.date];
    [alert textFieldAtIndex:0].text=date_selected;
    [picker resignFirstResponder];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag==1){
        if (buttonIndex == 1) {
            NSString *name = [alertView textFieldAtIndex:0].text;
            BFLog(@"date:%@",name);
            if ([dateslist containsObject:name]) {
                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                 message:@"Date already exists!!!"
                                                                delegate:self
                                                       cancelButtonTitle:@"Cancel"
                                                       otherButtonTitles:@"OK", nil];
                
                [alert1 show];
                
            }else{
                [self setOffflag:name flag:0];}
            // Insert whatever needs to be done with "name"
        }
    }
    if(alertView.tag ==3 || alertView.tag ==2){
        
        if(buttonIndex==[alertView cancelButtonIndex]){
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"loginscreen"];
                [self presentViewController:vc animated:YES completion:nil];
            });
        }
        
        
    }
    
    if(alertView.tag == 1005){
        if(buttonIndex==1){
            
                UIStoryboard *storyBoard = self.storyboard;
                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"changedutystatus"];
                [self presentViewController:vc animated:YES completion:nil];
        }
    }
    
    
}
-(void) setOffflag:(NSString *)date flag:(int)offflag{
    NSString *query_check =[NSString stringWithFormat:@"select \"24hr_off\" from TempTable where date='%@';",date];
    
    // Get the results.
    
    NSArray *date_exist = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query_check]];
    BFLog(@"Result:%d",(int)[date_exist count]);
    
    if ([date_exist count]!=0) {
        
        
        NSString *query_update = [NSString stringWithFormat:@"UPDATE TempTable SET \"24hr_off\"='%d' where date='%@'",offflag,date];
        
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_update];
        
        // If the query was successfully executed then pop the view controller.
        //minnu
        [self getthedata:date];
        NSMutableDictionary *data_dict1=[[NSMutableDictionary alloc]init];
        NSMutableArray *outer_array = [[NSMutableArray alloc] init];
        NSMutableArray *location_array = [[NSMutableArray alloc] init];
        //NSMutableDictionary *data_dict = [[NSMutableDictionary alloc] init];
        
        for (int i=0; i<[data_to_finaldb1 count]; i++) {
            
            NSString *x12=data_to_finaldb1[i][0];
            NSArray *time = [x12 componentsSeparatedByString:@":"];
            int x4 =(int) [time[1] integerValue]/15;
            int x2 =(int) [time[0] integerValue]*4+x4;
            float x1 = 12.25*x2;
            float posx = x1+40.0;
            int posy;
            BFLog(@"posx:%.2f",posx);
            if ([data_to_finaldb1[i][1] isEqualToString:@"OFF"]) {
                posy = 65;
            }else if([data_to_finaldb1[i][1] isEqualToString:@"ON"]){
                posy = 245;
            }else if([data_to_finaldb1[i][1] isEqualToString:@"D"]){
                posy=185;
            }else {
                posy=125;
            }
            
            NSMutableDictionary *cont1 = [[NSMutableDictionary alloc] init];
            [cont1 setValue:@(i) forKey:@"id"];
            [cont1 setValue:@(posx) forKey:@"posx"];
            [cont1 setValue:@(posy) forKey:@"posy"];
            
            [outer_array addObject:cont1];
            
            
            
        }
        
        [data_dict1 setObject:outer_array forKey:@"payload"];
        [data_dict1 setObject:location_array forKey:@"location"];
        
        BFLog(@"dictionary:%@",data_dict1);
        //[data_dict1 setValue:data_to_finaldb1 forKey:@"payload"];
        // NSData *data = [NSJSONSerialization dataWithJSONObject:data_dict1 options:NSJSONWritingPrettyPrinted error:nil];
        //  NSString *jsonStr1 = [[NSString alloc] initWithData:data
        //encoding:NSUTF8StringEncoding];
        
        //minnu
        
        if (dbManager.affectedRows != 0) {
            BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
            rows_num=rows_num+1;
            [self updatedatelist:rows_num];
            UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                             message:@"Date added!!!"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
            
            [alert2 show];
            [self saveCurrent:data_dict1 date:date];
            dispatch_async(dispatch_get_main_queue(), ^{
                [_logtable reloadData];
                
            });
            
            
        }
        else{
            BFLog(@"Could not execute the query");
        }
    }
    
    else{
        
        NSMutableDictionary *main = [[NSMutableDictionary alloc] init];
        NSMutableArray *data1 = [[NSMutableArray alloc]init];
        NSMutableDictionary *data0 = [[NSMutableDictionary alloc] init];
        [data0 setValue:@"0" forKey:@"id"];
        [data0 setValue:@"40" forKey:@"posx"];
        [data0 setValue:@"65" forKey:@"posy"];
        NSMutableDictionary *data2 = [[NSMutableDictionary alloc] init];
        [data2 setValue:@"1" forKey:@"id"];
        [data2 setValue:@"1216" forKey:@"posx"];
        [data2 setValue:@"65" forKey:@"posy"];
        [data1 addObject:data0];
        [data1 addObject:data2];
        [main setValue:data1 forKey:@"payload"];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat=@"MM/dd/yyyy";
        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        NSDate *date1=[formatter dateFromString:date];
        NSTimeInterval interval=[date1 timeIntervalSince1970]*1000;
        BFLog(@"Timeinterval:%f",interval);
        BFLog(@"Main:%@",main);
        NSData *data = [NSJSONSerialization dataWithJSONObject:main options:NSJSONWritingPrettyPrinted error:nil];
        NSString *jsonStr1 = [[NSString alloc] initWithData:data
                                                   encoding:NSUTF8StringEncoding];
        NSString *query_insert1 = [NSString stringWithFormat:@"insert into TempTable values(null, '%@', '%@', 'OFF' ,'%d','%d','%f')", date,jsonStr1,0,0,interval];
        
        [dbManager executeQuery:query_insert1];
        
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
            BFLog(@"Query was executed successfully for empty data. Affected rows = %d", dbManager.affectedRows);
            rows_num=rows_num+1;
            [self updatedatelist:rows_num];
            UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                             message:@"Date added!!!"
                                                            delegate:self
                                                   cancelButtonTitle:@"Cancel"
                                                   otherButtonTitles:@"OK", nil];
            
            [alert2 show];
            [self saveCurrent:main date:date];
            dispatch_async(dispatch_get_main_queue(), ^{
                [_logtable reloadData];
                
            });
            
            // Pop the view controller.
            //[self.navigationController popViewControllerAnimated:YES];
        }
        else{
            BFLog(@"Could not execute the query");
        }
        
        
        
    }
    
}
/*-(void)decisiontoload{
 NSString *cycle_flag =[[NSUserDefaults standardUserDefaults] objectForKey:@"decision_to_db_load"];
 BFLog(@"cycle flag:%@",cycle_flag);
 NSUserDefaults *userdata = [NSUserDefaults standardUserDefaults];
 cycle_flag=@"0";
 if ([cycle_flag integerValue]==0) {
 NSString *query_del =[NSString stringWithFormat:@"delete from TempTable;"];
 [dbManager executeQuery:query_del];
 [self getData:today_log];
 
 [userdata setObject:@"1" forKey:@"decision_to_db_load"];
 
 }else{
 
 [self getTodaysData:today_log];
 [_logtable reloadData];
 }
 
 
 
 
 
 
 }
 -(void)loadtoDb{
 NSString *table = @"TempTable";
 NSString *tableexist = [dbManager getRecords:table];
 BFLog(@"Table exists or not:%@",tableexist);
 if ([tableexist isEqualToString:@"YES"]) {
 
 
 [self getData:today_log];
 
 }
 
 
 
 }*/

-(void)updatingData{
    dispatch_group_t group = dispatch_group_create();
    
    dispatch_group_async(group,dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        
        //getting authCode
        NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
        NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
        NSString *authCode = @"";
        if([authData count]!=0){
            authCode = authData[0][1];
            
        }
        //---------------------------
        NSString *urlString1 = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/getHosDataPerDate.php?masterUser=%@&user=%@&date=%@&authCode=%@",masterUser,user,today_log,authCode];
        NSURL *url = [NSURL URLWithString:urlString1];
        NSData *data = [NSData dataWithContentsOfURL:url];
        
        if (data) {
            NSURLSession *session1 = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask1 = [session1 dataTaskWithURL:[NSURL URLWithString:urlString1] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                //  BFLog(@"Retrieved Data:%@,%@",data,response);
                if(data){
                    
                    json_loginscreen= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    
                    BFLog(@"Data From File:%@",json_loginscreen);
                    if ([json_loginscreen count]==0) {
                        // NSMutableDictionary *data_empty = [[NSMutableDictionary alloc] init];
                        NSDate * now1 = [NSDate date];
                        NSDateFormatter *outputFormatter_log1 = [[NSDateFormatter alloc] init];
                        [outputFormatter_log1 setDateFormat:@"HH:mm"];
                        NSString *today_log1 = [outputFormatter_log1 stringFromDate:now1];
                        NSArray *sendpart = [today_log1 componentsSeparatedByString:@":"];
                        int secd=(int)[sendpart[1] integerValue];
                        NSString *secnd_part;
                        if (secd <15) {
                            secnd_part=@"00";
                        }
                        else if(secd >=15 && secd <30){
                            secnd_part=@"15";
                        }else if(secd >=30 && secd <45){
                            secnd_part=@"30";
                        }else if(secd >=45 && secd <60){
                            secnd_part=@"45";
                        }
                        today_log1 = [NSString stringWithFormat:@"%@:%@",sendpart[0],secnd_part];
                        NSArray *initial = @[@"00:00",current_status];
                        [dataarray addObject:initial];
                        NSArray *con1 = @[today_log1,current_status];
                        [dataarray addObject:con1];
                        [self cleansingData:dataarray date:today_log];
                        [self saveToDB:dataarray];
                        
                        BFLog(@"Final array for loading:%@",dataarray);
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [_logtable reloadData];
                            
                        });
                    }else{
                        
                        
                        NSMutableArray *dict = [json_loginscreen objectForKey:@"payload"];
                        locations = [json_loginscreen objectForKey:@"location"];
                        notesLog = [json_loginscreen objectForKey:@"notes"];
                        BFLog(@"Location array:%@",locations);
                        int json_size = (int)[dict count];
                        BFLog(@"inner json count:%d",json_size);
                        [dataarray removeAllObjects];
                        for (int i=0; i<json_size; i++) {
                            
                            BFLog(@"posx:%@,%d",[dict[0] objectForKey:@"posx"],i);
                            float posx=[[dict[i] objectForKey:@"posx"] floatValue];
                            float x1 = posx - 40.0;
                            int x2 = x1/12.25;
                            int x3 = x2/4.0;
                            int x4 = x2%4;
                            
                            NSString *minutes;
                            if (15*x4 <15) {
                                minutes=@"00";
                            }
                            else if(15*x4 >=15 && 15*x4 <30){
                                minutes=@"15";
                            }else if(15*x4 >=30 && 15*x4 <45){
                                minutes=@"30";
                            }else if(15*x4 >=45 && 15*x4 <60){
                                minutes=@"45";
                            }
                            NSString *posx_1 = [NSString stringWithFormat:@"%d:%@",x3,minutes];
                            
                            // float time = [posx_1 floatValue];
                            int event_no =(int)[[dict[i] objectForKey:@"posy"] integerValue];
                            NSString * event;
                            if (event_no == 245) {
                                event=@"ON";
                            }
                            else if(event_no == 125){
                                event=@"SB";
                                
                            }else if (event_no == 185){
                                event = @"D";
                            }else if (event_no == 65){
                                event = @"OFF";
                            }
                            NSArray *final_data = [[NSArray alloc] initWithObjects:posx_1,event, nil];
                            [dataarray addObject:final_data];
                            [self cleansingData:dataarray date:today_log];
                            [self saveToDB:dataarray];
                            
                            /* BFLog(@"Final array for loading:%@",dataarray);
                             UIAlertView *alertShow = [[UIAlertView alloc] initWithTitle:@"" message:@"Refreshing..." delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
                             [alertShow show];
                             [self performSelector:@selector(dismissAlert:) withObject:alertShow afterDelay:0.50f];*/
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [_logtable reloadData];
                                
                            });
                            
                            
                        }
                        
                    }
                }
                
                
            }];
            [dataTask1 resume];
        }
        
        
    });
    
    
}
-(void)dismissAlert:(UIAlertView *)alertView{
    
    [alertView dismissWithClickedButtonIndex:nil animated:YES];
    
    
}
-(void)callChanges:(NSString*)authCode{
//    dispatch_async(dispatch_get_main_queue(), ^{
//        //getting authCode
//        NSString *authCode1 = authCode;
//        if([authCode isEqualToString:@""]){
//            NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
//            NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
//
//            if([authData count]!=0){
//                authCode1 = authData[0][1];
//
//            }
//        }
//        //---------------------------
//        NSString *urlString1 = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/getHosChangedData.php?masterUser=%@&user=%@&authCode=%@",masterUser,user,authCode1];
//        NSURL *url = [NSURL URLWithString:urlString1];
//        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//        [request setHTTPMethod:@"HEAD"];
//        NSURLResponse *response;
//        NSError *error;
//        NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
//        if(httpResponse!=nil){
//            if ([httpResponse statusCode] ==200 ) {
//                NSURLSession *session1 = [NSURLSession sharedSession];
//                NSURLSessionDataTask *dataTask1 = [session1 dataTaskWithURL:[NSURL URLWithString:urlString1] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                    //  BFLog(@"Retrieved Data:%@,%@",data,response);
//                    if(data){
//                        json_loginscreen= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//
//                        BFLog(@"Data From File:%@",json_loginscreen);
//                        if ([json_loginscreen count]!=0 || json_loginscreen!=nil) {
//
//                            NSArray *keys = [json_loginscreen allKeys];
//                            BFLog(@"%@",keys);
//                            for (NSString *key in keys){
//
//                                NSString *value=[json_loginscreen objectForKey:key];
//                                NSArray *parts=[value componentsSeparatedByString:@"$"];
//                                //BFLog(@"data:%@",parts[2]);
//                                if ([parts[2]  isEqual: @"[]"] || [parts[2]  isEqual: @"null"]) {
//                                    // BFLog(@"Empty");
//
//                                }
//
//
//                                NSData *jdata = [parts[2] dataUsingEncoding:NSUTF8StringEncoding];
//                                NSDictionary *data = [NSJSONSerialization JSONObjectWithData:jdata options:0 error:nil];
//                                NSArray *payload = data[@"payload"];
//                                // BFLog(@"%lu",(unsigned long)[payload count]);
//                                //minnu
//
//
//
//
//                                if (![payload isEqual:[NSNull null]] && payload!=nil && [payload count]!=0) {
//                                    NSString *status = [self getLastStatus:parts[2]];
//                                    //BFLog(@"status:%@",status);
//
//
//                                    NSString *query_check =[NSString stringWithFormat:@"select * from TempTable where date='%@';",key];
//
//                                    // Get the results.
//
//                                    NSArray *date_exist = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query_check]];
//
//                                    if([date_exist count]>0){
//
//                                        NSString *query_update = [NSString stringWithFormat:@"UPDATE TempTable SET data='%@',last_status='%@',\"24hr_off\"='%d',flag='%ld' WHERE date='%@'",parts[2],status,0,(long)[parts[4] integerValue],key];
//
//                                        // Execute the query.
//
//                                        //NSString *query_del = @"delete from TempTable";
//                                        [dbManager executeQuery:query_update];
//
//                                        // If the query was successfully executed then pop the view controller.
//                                        if (dbManager.affectedRows != 0) {
//                                            //BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
//
//                                        }
//                                        else{
//                                            //BFLog(@"Could not execute the query");
//                                        }
//                                    }else{
//                                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//                                        formatter.dateFormat=@"MM/dd/yyyy";
//                                        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
//                                        NSDate *date1=[formatter dateFromString:key];
//                                        NSTimeInterval interval=[date1 timeIntervalSince1970]*1000;
//                                        NSString *query_insert1 = [NSString stringWithFormat:@"insert into TempTable values(null, '%@', '%@', 'OFF' ,'%@','%d','%f')", key,parts[2],parts[4],0,interval];
//
//                                        [dbManager executeQuery:query_insert1];
//                                        if (dbManager.affectedRows != 0) {
//                                            //BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
//
//                                        }
//                                        else{
//                                            //BFLog(@"Could not execute the query");
//                                        }
//
//                                    }
//
//
//
//                                }
//
//
//                            }
//                        }
//                    }
//
//                }];
//                [dataTask1 resume];
//
//            }else if([httpResponse statusCode]==403){
//                if(autoLoginCount<=1){
//                    insertDutyCycle *obj = [insertDutyCycle new];
//                    BOOL success =  [obj autoLogin];
//
//                    if(success){
//                       // [self callChanges:authCode];
//
//                    }else{
//                        KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
//                        [credentials resetKeychainItem];
//                        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
//                                                                         message:@"Please Login Again"
//                                                                        delegate:self
//                                                               cancelButtonTitle:@"OK"
//                                                               otherButtonTitles:nil, nil];
//                        alert1.tag = 3;
//                        alert1.delegate = self;
//
//                        //[alert1 show];
//
//                    }
//                }else{
//                    KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
//                    [credentials resetKeychainItem];
//                    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
//                                                                     message:@"Please Login Again"
//                                                                    delegate:self
//                                                           cancelButtonTitle:@"OK"
//                                                           otherButtonTitles:nil, nil];
//                    alert1.tag = 2;
//                    alert1.delegate = self;
//
//                    [alert1 show];
//
//
//
//                }
//
//            }else if([httpResponse statusCode]==503){
//                BFLog(@"Device not connected to the internet");
//                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Service Unavailable"
//                                                                 message:@"No connectivity with server at this time,it will sync automatically"
//                                                                delegate:self
//                                                       cancelButtonTitle:@"OK"
//                                                       otherButtonTitles:nil, nil];
//
//                [alert1 show];
//
//            }
//        }
//
//    });
//
}

//-(void)viewWillAppear:(BOOL)animated{
//
//
//    //rows_num=7;
//
//    //[self updatingData];
//    // [self performSelectorInBackground:@selector(updatingData) withObject:nil];
//    // [self getTodaysData:today_log];
//    /*if (refresh_flag==1) {
//     rows_num=14;
//     [_logtable reloadData];
//     [self viewDidLoad];
//
//
//     }*/
//    //
//    // [self viewWillAppear:YES];
//
//}
-(void)refreshContent{
    BFLog(@"calling...");
    
   // [self addData];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [_logtable reloadData];
        
    });
    
}

-(void)addData{
    NSDate * now1 = [NSDate date];
    NSDateFormatter *outputFormatter_log1 = [[NSDateFormatter alloc] init];
    [outputFormatter_log1 setDateFormat:@"HH:mm"];
    NSString *today_log1 = [outputFormatter_log1 stringFromDate:now1];
    NSArray *sendpart = [today_log1 componentsSeparatedByString:@":"];
    int secd=(int)[sendpart[1] integerValue];
    BFLog(@"minute:%d",secd);
    NSString *secnd_part;
    if (secd <15) {
        secnd_part=@"00";
    }
    else if(secd >=15 && secd <30){
        secnd_part=@"15";
    }else if(secd >=30 && secd <45){
        secnd_part=@"30";
    }else if(secd >=45 && secd <60){
        secnd_part=@"45";
    }
    if([current_status isEqualToString:@"NONE"]){
        NSString *today_log2 = [NSString stringWithFormat:@"%@:%@",sendpart[0],secnd_part];
        NSArray *con1 = @[today_log2,@"OFF"];
        [dataarray addObject:con1];
    }else if([current_status isEqualToString:@"PC"]){
        current_status = @"OFF";
        NSString *today_log2 = [NSString stringWithFormat:@"%@:%@",sendpart[0],secnd_part];
        NSArray *con1 = @[today_log2,@"OFF"];
        [dataarray addObject:con1];
    }else if([current_status isEqualToString:@"YM"]){
        current_status = @"ON";
        NSString *today_log2 = [NSString stringWithFormat:@"%@:%@",sendpart[0],secnd_part];
        NSArray *con1 = @[today_log2,@"ON"];
        [dataarray addObject:con1];
    }
    else{
        NSString *today_log2 = [NSString stringWithFormat:@"%@:%@",sendpart[0],secnd_part];
        NSArray *con1 = @[today_log2,current_status];
        [dataarray addObject:con1];
    }
    
}
-(void)getData:(NSString*)date {
    int countday = 14;
    [dataarray1 removeAllObjects];
    //getting authCode
    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    NSString *authCode = @"";
    if([authData count]!=0){
        authCode = authData[0][1];
        
    }
    //---------------------------
    NSString *urlString1 = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/getHosLast7DaysData.php?masterUser=%@&user=%@&date=%@&countDays=%d&authCode=%@",masterUser,user,date,countday,authCode];
    NSURL *url = [NSURL URLWithString:urlString1];
    NSData *data = [NSData dataWithContentsOfURL:url];
    if (data) {
        NSURLSession *session1 = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask1 = [session1 dataTaskWithURL:[NSURL URLWithString:urlString1] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            //  BFLog(@"Retrieved Data:%@,%@",data,response);
            
            
            json_log = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            if(json_log!=nil){
                //if (error == nil) {
                //NSString *query_del = @"delete from TempTable";
                //[dbManager executeQuery:query_del];
                BFLog(@"Data From File:%@",json_log);
                NSArray *keys = [json_log allKeys];
                for (NSString *key in keys){
                    
                    NSString *value=[json_log objectForKey:key];
                    BFLog(@"values in dict:key:%@,value:%@",key,value);
                    if ([value isEqualToString:@""]) {
                        if (![key isEqualToString:today_log]) {
                            
                            NSMutableDictionary *main = [[NSMutableDictionary alloc] init];
                            NSMutableArray *data1 = [[NSMutableArray alloc]init];
                            NSMutableDictionary *data0 = [[NSMutableDictionary alloc] init];
                            [data0 setValue:@"0" forKey:@"id"];
                            [data0 setValue:@"40" forKey:@"posx"];
                            [data0 setValue:@"65" forKey:@"posy"];
                            NSMutableDictionary *data2 = [[NSMutableDictionary alloc] init];
                            [data2 setValue:@"1" forKey:@"id"];
                            [data2 setValue:@"1216" forKey:@"posx"];
                            [data2 setValue:@"65" forKey:@"posy"];
                            [data1 addObject:data0];
                            [data1 addObject:data2];
                            [main setValue:data1 forKey:@"payload"];
                            
                            BFLog(@"Main:%@",main);
                            NSData *data = [NSJSONSerialization dataWithJSONObject:main options:NSJSONWritingPrettyPrinted error:nil];
                            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                            formatter.dateFormat=@"MM/dd/yyyy";
                            [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                            NSDate *date1=[formatter dateFromString:key];
                            NSTimeInterval interval=[date1 timeIntervalSince1970]*1000;
                            BFLog(@"Timeinterval:%f",interval);
                            
                            
                            NSString *jsonStr1 = [[NSString alloc] initWithData:data
                                                                       encoding:NSUTF8StringEncoding];
                            NSString *query_insert1 = [NSString stringWithFormat:@"insert into TempTable values(null, '%@', '%@', 'OFF' ,'%d','%d','%f')", key,jsonStr1,0,1,interval];
                            
                            [dbManager executeQuery:query_insert1];
                            
                            // If the query was successfully executed then pop the view controller.
                            if (dbManager.affectedRows != 0) {
                                BFLog(@"Query was executed successfully for empty data. Affected rows = %d", dbManager.affectedRows);
                                
                                // Pop the view controller.
                                //[self.navigationController popViewControllerAnimated:YES];
                            }
                            else{
                                BFLog(@"Could not execute the query");
                            }
                            
                        }
                    }
                    else{
                        
                        
                        
                        NSArray *parts=[value componentsSeparatedByString:@"$"];
                        BFLog(@"data:%@",parts[2]);
                        NSString *status = [self getLastStatus:parts[2]];
                        BFLog(@"status:%@",status);
                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                        formatter.dateFormat=@"MM/dd/yyyy";
                        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                        NSDate *date1=[formatter dateFromString:parts[0]];
                        NSTimeInterval interval=[date1 timeIntervalSince1970]*1000;
                        BFLog(@"Timeinterval:%f",interval);
                        
                        NSString *query_insert = [NSString stringWithFormat:@"insert into TempTable values(null, '%@', '%@', '%@' ,'%d','%d','%f')", parts[0],parts[2],status,0,0,interval];
                        
                        // Execute the query.
                        
                        //NSString *query_del = @"delete from TempTable";
                        [dbManager executeQuery:query_insert];
                        
                        // If the query was successfully executed then pop the view controller.
                        if (dbManager.affectedRows != 0) {
                            BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
                            //[self saveCurrent:data_dict1 date:date];
                            
                        }
                        else{
                            BFLog(@"Could not execute the query");
                        }
                        
                        
                    }
                    
                }
            }
            
        }];
        
        [dataTask1 resume];
        
    }else{
        
        
        UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"" message:@"Please check your internet connectivity" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert1 show];
        
    }
}


-(void)getTodaysData:(NSString *)date{
    [dataarray removeAllObjects];
    // date=@"09/09/2016";
    selected_date=date;
    
    NSString *getLast = [NSString stringWithFormat:@"select date from TempTable order by dateInSecs Desc"];
    NSArray *lastDate = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:getLast]];
    BFLog(@"Result:%@",lastDate);
    NSDate *today= [NSDate date];
    NSDate *previous = [today dateByAddingTimeInterval:-1*24*60*60];
    NSDateFormatter *outputFormatter1 = [[NSDateFormatter alloc] init];
    [outputFormatter1 setDateFormat:@"MM/dd/yyyy"];
    [outputFormatter1 setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSString *previousdate = [outputFormatter1 stringFromDate:previous];
    if ([lastDate count]!=0) {
        
        if(![previousdate isEqualToString:lastDate[0][0]]){
            NSDate *date1=previous;
            BFLog(@"previous date:%@",lastDate[0][0]);
            // NSString *x=[NSString stringWithFormat:@"%@ 00:00:00 +0000",lastDate[0][0]];
            NSDate *date2=[outputFormatter1 dateFromString:lastDate[0][0]];
            NSTimeInterval secondsBtwn = [date2 timeIntervalSinceDate:date1];
            int numofDays=secondsBtwn/86400;
            BFLog(@"Date difference:%d",numofDays);
            if(numofDays<0){
                
                numofDays=numofDays*-1;
            }
            // [self addMissingData:previousdate count:numofDays];
            
        }
    }
    
    NSString *query1 = [NSString stringWithFormat:@"select * from TempTable where date='%@'",date];
    
    // Get the results.
    if (self.arrPeopleInfo != nil) {
        self.arrPeopleInfo = nil;
    }
    self.arrPeopleInfo = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query1]];
    BFLog(@"Result2:%@",self.arrPeopleInfo);
    if ([_arrPeopleInfo count]!=0) {
        NSString *dbresult = self.arrPeopleInfo[[_arrPeopleInfo count]-1][2];
        
        
        
        BFLog(@"string:%@",dbresult);
        
        
        //BFLog(@"timezone:%lu",(unsigned long)[dbresult count]);
        NSData *data2= [dbresult dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
        BFLog(@"Dictionary of db data:%@",json_log1);
        if ([json_log1 count]==0) {
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@" " withString:@""];
            dbresult = [NSString stringWithFormat:@"[%@]",dbresult];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"\";" withString:@";"];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"=\"" withString:@"="];
            
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"location=(" withString:@"\"location\":["];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"payload=(" withString:@"\"payload\":["];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@");" withString:@"],"];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@";" withString:@"\","];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"=" withString:@":\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"id" withString:@"\"id\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"value" withString:@"\"value\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"posx" withString:@"\"posx\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"posy" withString:@"\"posy\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"timezone" withString:@"\"timezone\""];
            NSData *data2= [dbresult dataUsingEncoding:NSUTF8StringEncoding];
            json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
            BFLog(@"Dictionary of db data2:%@",json_log1);
            
        }
        
        
        //  [location_array removeAllObjects];
        //   BFLog(@"data portion:%@",[json_log1 allKeys]);
        // NSMutableArray *dict=[[NSMutableArray alloc] init];
        NSMutableArray *dict = [json_log1 objectForKey:@"payload"];
        locations = [json_log1 objectForKey:@"location"];
        notesLog=[json_log1 objectForKey:@"notes"];
        //BFLog(@"Location array:%@",location_array);
        int json_size = (int)[dict count];
        BFLog(@"inner json count:%d",json_size);
        [dataarray removeAllObjects];
        for (int i=0; i<json_size; i++) {
            
            BFLog(@"posx:%@,%d",[dict[0] objectForKey:@"posx"],i);
            float posx=[[dict[i] objectForKey:@"posx"] floatValue];
            float x1 = posx - 40.0;
            int x2 = x1/12.25;
            int x3 = x2/4.0;
            int x4 = x2%4;
            
            NSString *minutes;
            if (15*x4 <15) {
                minutes=@"00";
            }
            else if(15*x4 >=15 && 15*x4 <30){
                minutes=@"15";
            }else if(15*x4 >=30 && 15*x4 <45){
                minutes=@"30";
            }else if(15*x4 >=45 && 15*x4 <60){
                minutes=@"45";
            }
            NSString *posx_1 = [NSString stringWithFormat:@"%d:%@",x3,minutes];
            
            // float time = [posx_1 floatValue];
            int event_no =(int)[[dict[i] objectForKey:@"posy"] integerValue];
            NSString * event;
            if (event_no == 245) {
                event=@"ON";
            }
            else if(event_no == 125){
                event=@"SB";
                
            }else if (event_no == 185){
                event = @"D";
            }else if (event_no == 65){
                event = @"OFF";
            }
            NSArray *final_data = [[NSArray alloc] initWithObjects:posx_1,event, nil];
            [dataarray addObject:final_data];
            
            
        }
        NSDate * now11 = [NSDate date];
        NSDateFormatter *outputFormatter_log11 = [[NSDateFormatter alloc] init];
        [outputFormatter_log11 setDateFormat:@"HH:mm"];
        NSString *today_log11 = [outputFormatter_log11 stringFromDate:now11];
        NSArray *sendpart1= [today_log11 componentsSeparatedByString:@":"];
        int secd1=(int)[sendpart1[1] integerValue];
        NSString *secnd_part1;
        if (secd1 <15) {
            secnd_part1=@"00";
        }
        else if(secd1 >=15 && secd1 <30){
            secnd_part1=@"15";
        }else if(secd1 >=30 && secd1 <45){
            secnd_part1=@"30";
        }else if(secd1 >=45 && secd1 <60){
            secnd_part1=@"45";
        }
        today_log11 = [NSString stringWithFormat:@"%@:%@",sendpart1[0],secnd_part1];
        BFLog(@"%@time",today_log11);
        NSString *last_status =dataarray[[dataarray count]-1][1];
        NSString *second_last = dataarray[[dataarray count]-2][1];
        if ([last_status isEqualToString:second_last]) {
            [dataarray removeLastObject];
            NSArray *con11 = @[today_log11,self.arrPeopleInfo[0][3]];
            [dataarray addObject:con11];
            
        }
        else {
            NSArray *con11 = @[today_log11,self.arrPeopleInfo[0][3]];
            [dataarray addObject:con11];
            
        }
        [self cleansingData:dataarray date:date];
        [self saveToDB:dataarray];
        // [self saveCurrent:<#(NSMutableDictionary *)#> date:<#(NSString *)#>];
        
    }else{
        
        dispatch_queue_t queue = dispatch_queue_create("com.SievaNetworks.app.queue", DISPATCH_QUEUE_CONCURRENT);
        //getting authCode
        NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
        NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
        NSString *authCode = @"";
        if([authData count]!=0){
            authCode = authData[0][1];
            
        }
        //---------------------------
        NSString *urlString1 = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/getHosDataPerDate.php?masterUser=%@&user=%@&date=%@&authCode=%@",masterUser,user,today_log,authCode];
        NSURLSession *session1 = [NSURLSession sharedSession];
        NSURL *url = [NSURL URLWithString:urlString1];
        NSData *data = [NSData dataWithContentsOfURL:url];
        if (data) {
            
            dispatch_async(queue, ^{
                
                
                // [dataarray1 removeAllObjects];
                
                NSURLSessionDataTask *dataTask1 = [session1 dataTaskWithURL:[NSURL URLWithString:urlString1] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    //  BFLog(@"Retrieved Data:%@,%@",data,response);
                    
                    
                    json_loginscreen= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    
                }];
                [dataTask1 resume];
                sleep(2);
                
            });
            dispatch_barrier_sync(queue, ^{
                BFLog(@"Data From File:%@",json_loginscreen);
                if ([json_loginscreen count]==0) {
                    // NSMutableDictionary *data_empty = [[NSMutableDictionary alloc] init];
                    NSDate * now1 = [NSDate date];
                    NSDateFormatter *outputFormatter_log1 = [[NSDateFormatter alloc] init];
                    [outputFormatter_log1 setDateFormat:@"HH:mm"];
                    NSString *today_log1 = [outputFormatter_log1 stringFromDate:now1];
                    NSArray *sendpart = [today_log1 componentsSeparatedByString:@":"];
                    int secd=(int)[sendpart[1] integerValue];
                    NSString *secnd_part;
                    if (secd <15) {
                        secnd_part=@"00";
                    }
                    else if(secd >=15 && secd <30){
                        secnd_part=@"15";
                    }else if(secd >=30 && secd <45){
                        secnd_part=@"30";
                    }else if(secd >=45 && secd <60){
                        secnd_part=@"45";
                    }
                    today_log1 = [NSString stringWithFormat:@"%@:%@",sendpart[0],secnd_part];
                    if([current_status isEqualToString:@"NONE"]){
                        NSArray *initial = @[@"00:00",@"OFF"];
                        [dataarray addObject:initial];
                        NSArray *con1 = @[today_log1,@"OFF"];
                        [dataarray addObject:con1];
                    }else if([current_status isEqualToString:@"PC"]){
                        current_status = @"OFF";
                        NSString *today_log2 = [NSString stringWithFormat:@"%@:%@",sendpart[0],secnd_part];
                        NSArray *con1 = @[today_log2,@"OFF"];
                        [dataarray addObject:con1];
                    }else if([current_status isEqualToString:@"YM"]){
                        current_status = @"ON";
                        NSString *today_log2 = [NSString stringWithFormat:@"%@:%@",sendpart[0],secnd_part];
                        NSArray *con1 = @[today_log2,@"ON"];
                        [dataarray addObject:con1];
                    }else{
                        NSArray *initial = @[@"00:00",current_status];
                        [dataarray addObject:initial];
                        NSArray *con1 = @[today_log1,current_status];
                        [dataarray addObject:con1];
                    }

                    [self cleansingData:dataarray date:date];
                    [self saveToDB:dataarray];
                    
                    BFLog(@"Final array for loading:%@",dataarray);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_logtable reloadData];
                        
                    });
                }else{
                    
                    
                    NSMutableArray *dict = [json_loginscreen objectForKey:@"payload"];
                    locations = [json_loginscreen objectForKey:@"location"];
                    notesLog=[json_loginscreen objectForKey:@"location"];
                    BFLog(@"Location array:%@",locations);
                    int json_size = (int)[dict count];
                    BFLog(@"inner json count:%d",json_size);
                    [dataarray removeAllObjects];
                    for (int i=0; i<json_size; i++) {
                        
                        BFLog(@"posx:%@,%d",[dict[0] objectForKey:@"posx"],i);
                        float posx=[[dict[i] objectForKey:@"posx"] floatValue];
                        float x1 = posx - 40.0;
                        int x2 = x1/12.25;
                        int x3 = x2/4.0;
                        int x4 = x2%4;
                        
                        NSString *minutes;
                        if (15*x4 <15) {
                            minutes=@"00";
                        }
                        else if(15*x4 >=15 && 15*x4 <30){
                            minutes=@"15";
                        }else if(15*x4 >=30 && 15*x4 <45){
                            minutes=@"30";
                        }else if(15*x4 >=45 && 15*x4 <60){
                            minutes=@"45";
                        }
                        NSString *posx_1 = [NSString stringWithFormat:@"%d:%@",x3,minutes];
                        
                        // float time = [posx_1 floatValue];
                        int event_no =(int)[[dict[i] objectForKey:@"posy"] integerValue];
                        NSString * event;
                        if (event_no == 245) {
                            event=@"ON";
                        }
                        else if(event_no == 125){
                            event=@"SB";
                            
                        }else if (event_no == 185){
                            event = @"D";
                        }else if (event_no == 65){
                            event = @"OFF";
                        }
                        NSArray *final_data = [[NSArray alloc] initWithObjects:posx_1,event, nil];
                        [dataarray addObject:final_data];
                        [self cleansingData:dataarray date:date];
                        [self saveToDB:dataarray];
                        
                        BFLog(@"Final array for loading:%@",dataarray);
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [_logtable reloadData];
                            
                        });
                        
                        
                    }
                    
                }
                
            });
            
        }else{
            UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"" message:@"Please check your internet connectivity" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert1 show];
            
            
            
        }
        
        
        
        
        
    }
}

-(void)cleansingData:(NSMutableArray *)data date:(NSString *)date{
    
    NSDateFormatter *outputFormatter_tody = [[NSDateFormatter alloc] init];
    [outputFormatter_tody setDateFormat:@"MM/dd/YYYY"];
    NSString *newDateString2 = [outputFormatter_tody stringFromDate:now];
    if([date isEqualToString:newDateString2]){
        if([data count]%2!=0){
            
            NSMutableDictionary *last = data[[data count]-1];
            [data removeLastObject];
            [data removeLastObject];
            [data addObject:last];
            
            
            
            
            
        }
        
        
        
        
    }
    dataarray = data;
    
    
    
}

-(void)addMissingData:(NSString *)date count:(int)count{
    
    
    // [dataarray1 removeAllObjects];
    //getting authCode
    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    NSString *authCode = @"";
    if([authData count]!=0){
        authCode = authData[0][1];
        
    }
    //---------------------------
    NSString *urlString1 = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/getHosLast7DaysData.php?masterUser=%@&user=%@&date=%@&countDays=%d&authCode=%@",masterUser,user,date,count,authCode];
    NSURLSession *session1 = [NSURLSession sharedSession];
    NSURL *url = [NSURL URLWithString:urlString1];
    NSData *data = [NSData dataWithContentsOfURL:url];
    if (data) {
        
        NSURLSessionDataTask *dataTask1 = [session1 dataTaskWithURL:[NSURL URLWithString:urlString1] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            //  BFLog(@"Retrieved Data:%@,%@",data,response);
            
            NSDictionary *json_login = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            //if (error == nil) {
            //NSString *query_del = @"delete from TempTable";
            //[dbManager executeQuery:query_del];
            BFLog(@"Data From File:%@",json_login);
            NSArray *keys = [json_login allKeys];
            for (NSString *key in keys){
                //        __block k=0;
                NSString *value=[json_login objectForKey:key];
                BFLog(@"values in dict:key:%@,value:%@",key,value);
                if ([value isEqualToString:@""]) {
                    
                    
                    NSMutableDictionary *main = [[NSMutableDictionary alloc] init];
                    NSMutableArray *data1 = [[NSMutableArray alloc]init];
                    NSMutableDictionary *data0 = [[NSMutableDictionary alloc] init];
                    [data0 setValue:@"0" forKey:@"id"];
                    [data0 setValue:@"40" forKey:@"posx"];
                    [data0 setValue:@"65" forKey:@"posy"];
                    NSMutableDictionary *data2 = [[NSMutableDictionary alloc] init];
                    [data2 setValue:@"1" forKey:@"id"];
                    [data2 setValue:@"1216" forKey:@"posx"];
                    [data2 setValue:@"65" forKey:@"posy"];
                    [data1 addObject:data0];
                    [data1 addObject:data2];
                    [main setValue:data1 forKey:@"payload"];
                    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                    formatter.dateFormat=@"MM/dd/yyyy";
                    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                    NSDate *date1=[formatter dateFromString:key];
                    NSTimeInterval interval=[date1 timeIntervalSince1970]*1000;
                    BFLog(@"Main:%@",main);
                    NSData *data = [NSJSONSerialization dataWithJSONObject:main options:NSJSONWritingPrettyPrinted error:nil];
                    NSString *jsonStr1 = [[NSString alloc] initWithData:data
                                                               encoding:NSUTF8StringEncoding];
                    NSString *query_insert1 = [NSString stringWithFormat:@"insert into TempTable values(null, '%@', '%@', 'OFF' ,'%d','%d','%f')", key,jsonStr1,0,1,interval];
                    
                    [dbManager executeQuery:query_insert1];
                    
                    // If the query was successfully executed then pop the view controller.
                    if (dbManager.affectedRows != 0) {
                        BFLog(@"Query was executed successfully for empty data. Affected rows = %d", dbManager.affectedRows);
                        
                        // Pop the view controller.
                        //[self.navigationController popViewControllerAnimated:YES];
                    }
                    else{
                        BFLog(@"Could not execute the query");
                    }
                    
                    
                }
                else{
                    
                    NSArray *parts=[value componentsSeparatedByString:@"$"];
                    
                    NSData *jdata = [parts[2] dataUsingEncoding:NSUTF8StringEncoding];
                    NSDictionary *data = [NSJSONSerialization JSONObjectWithData:jdata options:0 error:nil];
                    NSArray *payload = data[@"payload"];
                    // BFLog(@"%lu",(unsigned long)[payload count]);
                    if (payload!=nil && data[@"payload"]!=NULL){
                        NSString *status = [self getLastStatus:parts[2]];
                        BFLog(@"status:%@",status);
                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                        formatter.dateFormat=@"MM/dd/yyyy";
                        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                        NSDate *date1=[formatter dateFromString:parts[0]];
                        NSTimeInterval interval=[date1 timeIntervalSince1970]*1000;
                        BFLog(@"Timeinterval:%f",interval);
                        
                        NSString *query_insert = [NSString stringWithFormat:@"insert into TempTable values(null, '%@', '%@', '%@' ,'%d','%d','%f')", parts[0],parts[2],status,0,0,interval];
                        
                        // Execute the query.
                        
                        //NSString *query_del = @"delete from TempTable";
                        [dbManager executeQuery:query_insert];
                        
                        // If the query was successfully executed then pop the view controller.
                        if (dbManager.affectedRows != 0) {
                            BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
                            
                        }
                        else{
                            BFLog(@"Could not execute the query");
                        }
                    }
                    else{
                        
                        NSMutableDictionary *main = [[NSMutableDictionary alloc] init];
                        NSMutableArray *data1 = [[NSMutableArray alloc]init];
                        NSMutableDictionary *data0 = [[NSMutableDictionary alloc] init];
                        [data0 setValue:@"0" forKey:@"id"];
                        [data0 setValue:@"40" forKey:@"posx"];
                        [data0 setValue:@"65" forKey:@"posy"];
                        NSMutableDictionary *data2 = [[NSMutableDictionary alloc] init];
                        [data2 setValue:@"1" forKey:@"id"];
                        [data2 setValue:@"1216" forKey:@"posx"];
                        [data2 setValue:@"65" forKey:@"posy"];
                        [data1 addObject:data0];
                        [data1 addObject:data2];
                        [main setValue:data1 forKey:@"payload"];
                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                        formatter.dateFormat=@"MM/dd/yyyy";
                        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                        NSDate *date1=[formatter dateFromString:key];
                        NSTimeInterval interval=[date1 timeIntervalSince1970]*1000;
                        BFLog(@"Timeinterval:%f",interval);
                        BFLog(@"Main:%@",main);
                        NSData *data = [NSJSONSerialization dataWithJSONObject:main options:NSJSONWritingPrettyPrinted error:nil];
                        NSString *jsonStr1 = [[NSString alloc] initWithData:data
                                                                   encoding:NSUTF8StringEncoding];
                        NSString *query_insert1 = [NSString stringWithFormat:@"insert into TempTable values(null, '%@', '%@', 'OFF' ,'%d','%d','%f')", key,jsonStr1,0,1,interval];
                        
                        [dbManager executeQuery:query_insert1];
                        
                        // If the query was successfully executed then pop the view controller.
                        if (dbManager.affectedRows != 0) {
                            BFLog(@"Query was executed successfully for empty data. Affected rows = %d", dbManager.affectedRows);
                            
                            // Pop the view controller.
                            //[self.navigationController popViewControllerAnimated:YES];
                        }
                        else{
                            BFLog(@"Could not execute the query");
                        }
                        
                        
                        
                        
                    }
                }
            }
            
            
        }];
        
        [dataTask1 resume];
        
    }else{
        
        UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"" message:@"Please check your internet connectivity" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert1 show];
        
        
    }
    
    
}
-(void)saveToDB:(NSMutableArray *)array{
    NSString *get_count = [NSString stringWithFormat:@"select Count(*) from TempTable"];
    
    // Get the results.
    
    NSArray *count = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:get_count]];
    //BFLog(@"count:%lu",[count count]);
    
    if ([count count]!=0 && [count[0][0] integerValue]>90) {
        NSString *get_details = [NSString stringWithFormat:@"select date from TempTable order by date limit 1"];
        
        // Get the results.
        
        NSArray *last_row = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:get_details]];
        BFLog(@"last row:%@",last_row);
        NSString *query_del =[NSString stringWithFormat:@"delete from TempTable where date='%@';",last_row[0][0]];
        [dbManager executeQuery:query_del];
        
    }
    
    
    
    int id1;
    if ([array count]!=0) {
        id1 =(int) [array count]-1;
    }else{
        
        id1=0;
    }
    
    // if ([location_array count]==0) {
    
    //  [location_array addObject:dict1];
    //}else{
    
    
    
    //}
    NSMutableArray *outer_array = [[NSMutableArray alloc] init];
    NSMutableDictionary *data_dict = [[NSMutableDictionary alloc] init];
    
    for (int i=0; i<[array count]; i++) {
        
        NSString *x12=array[i][0];
        NSArray *time = [x12 componentsSeparatedByString:@":"];
        int x4 =(int) [time[1] integerValue]/15;
        int x2 =(int) [time[0] integerValue]*4+x4;
        float x1 = 12.25*x2;
        float posx = x1+40.0;
        int posy;
        BFLog(@"posx:%.2f",posx);
        if ([array[i][1] isEqualToString:@"OFF"]) {
            posy = 65;
        }else if([array[i][1] isEqualToString:@"ON"]){
            posy = 245;
        }else if([array[i][1] isEqualToString:@"D"]){
            posy=185;
        }else{
            posy=125;
        }
        
        NSMutableDictionary *cont1 = [[NSMutableDictionary alloc] init];
        [cont1 setValue:@(i) forKey:@"id"];
        [cont1 setValue:@(posx) forKey:@"posx"];
        [cont1 setValue:@(posy) forKey:@"posy"];
        
        [outer_array addObject:cont1];
        
        
        
    }
    
    [data_dict setObject:outer_array forKey:@"payload"];
    if (locations!=nil) {
        [data_dict setObject:locations forKey:@"location"];
    }
    if (notesLog!=nil) {
        [data_dict setObject:notesLog forKey:@"notes"];
        
    }
    
    [self saveCurrent:data_dict date:selected_date];
    BFLog(@"dictionary:%@",data_dict);
    NSData *data = [NSJSONSerialization dataWithJSONObject:data_dict options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonStr = [[NSString alloc] initWithData:data
                                              encoding:NSUTF8StringEncoding];
    BFLog(@"JSON:%@",jsonStr);
    NSString *query1 = [NSString stringWithFormat:@"select * from TempTable where date='%@'",selected_date];
    
    // Get the results.
    
    NSArray *values = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query1]];
    
    if ([values count]!=0) {
        NSString *query_update;
        if([current_status isEqualToString:@"NONE"] || [current_status isEqualToString:@"PC"]){
            query_update = [NSString stringWithFormat:@"UPDATE TempTable SET data='%@',last_status='OFF' WHERE date='%@'",jsonStr,selected_date];
        }else if([current_status isEqualToString:@"YM"]){
             query_update = [NSString stringWithFormat:@"UPDATE TempTable SET data='%@',last_status='ON' WHERE date='%@'",jsonStr,selected_date];
        }
        else{
             query_update = [NSString stringWithFormat:@"UPDATE TempTable SET data='%@',last_status='%@' WHERE date='%@'",jsonStr,current_status,selected_date];
        }
        
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_update];
        
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
            BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
            
        }
        else{
            BFLog(@"Could not execute the query");
        }}
    
    else{
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat=@"MM/dd/yyyy";
        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        NSDate *date1=[formatter dateFromString:selected_date];
        NSTimeInterval interval=[date1 timeIntervalSince1970]*1000;
        BFLog(@"Timeinterval:%f",interval);
        
        NSString *query_insert1;
        if([current_status isEqualToString:@"NONE"] || [current_status isEqualToString:@"PC"]){
            query_insert1 = [NSString stringWithFormat:@"insert into TempTable values(null, '%@', '%@', 'OFF' ,'%d','%d','%f')", selected_date,jsonStr,0,0,interval];
        }else if([current_status isEqualToString:@"YM"]){
            query_insert1 = [NSString stringWithFormat:@"insert into TempTable values(null, '%@', '%@', 'ON' ,'%d','%d','%f')", selected_date,jsonStr,0,0,interval];
        }else{
            query_insert1 = [NSString stringWithFormat:@"insert into TempTable values(null, '%@', '%@', '%@' ,'%d','%d','%f')", selected_date,jsonStr,current_status,0,0,interval];
        }
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_insert1];
        
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
            BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
            
        }
        else{
            BFLog(@"Could not execute the query");
        }
        
        
        
    }
    
}








-(NSString *)getLastStatus:(NSString *)dataString{
    
   
    NSData *data2= [dataString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
    NSArray *dict = [json_log1 objectForKey:@"payload"];
    NSString * event1;
    if([dict count]!=0 && dict!=nil && dict!=NULL){
        int json_size1 = (int)[dict count];
        int event_no1 =(int)[[dict[json_size1-1] objectForKey:@"posy"] integerValue];
        
        
        if (event_no1 == 245) {
            event1=@"ON";
        }
        else if(event_no1 == 125){
            event1=@"SB";
            
        }else if (event_no1 == 185){
            event1 = @"D";
        }else if (event_no1 == 65){
            event1 = @"OFF";
        }
        
        BFLog(@"Last stored status:%@",event1);
    }else{
        
        event1 = @"OFF";
        
    }
    return event1;
    
}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger rows=1;
    if (section==0) {
        rows=1;
    }else if (section ==1){
        rows=1;
    }else if (section ==2){
        
        rows=no_of_days+1;
    }
    
    return rows;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if(section==0){
        
        return 0;
    }else{
    
    return 30;
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    header.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0];
    UILabel *headerTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width-10, 30) ];
    headerTitle.font = [UIFont fontWithName:@"Helvetica-bold" size:15.0];
    if(section == 0){
        headerTitle.text=@"Current Status";
        headerTitle.textColor = [UIColor blackColor];
    }else if(section ==1){
        if([[NSUserDefaults standardUserDefaults] objectForKey:@"enableDebug"]!=nil){
            if([[NSUserDefaults standardUserDefaults] boolForKey:@"enableDebug"]==YES){
                
                
                NSString *title = @"Debug is Enabled!";
                NSMutableAttributedString *attributedTitle = [[NSMutableAttributedString alloc] initWithString:title];
                NSRange range = {0,[title length]};
                UIColor *color = [UIColor redColor];
                UIFont *font = [UIFont fontWithName:@"Helvetica-bold" size:15.0];
                [attributedTitle addAttribute:NSForegroundColorAttributeName value:color range:range];
                [attributedTitle addAttribute:NSFontAttributeName value:font range:range];
                [headerTitle setAttributedText:attributedTitle];
                [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse animations:^{
                    headerTitle.alpha=1;
                } completion:nil];
                
            }else{
                
                
                headerTitle.text=@"Tap on the graph to update the past date & time";
                
            }
            
            
        }else{
            headerTitle.text=@"Tap on the graph to update the past date & time";
        }
    }else if (section ==2){
        
        headerTitle.text = [NSString stringWithFormat:@"Last %d Days",no_of_days];
        
        //title=@"Last 14 Days";
    }
    
    [header addSubview:headerTitle];
    
    return header;
    
}

/*-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
 
 NSString *title;
 if (section == 0) {
 title = @"Current Status";
 }else if(section ==1){
 if([[NSUserDefaults standardUserDefaults] objectForKey:@"enableDebug"]!=nil){
 if([[NSUserDefaults standardUserDefaults] boolForKey:@"enableDebug"]==YES){
 
 title = @"Debug is Enabled!";
 
 }else{
 
 
 title=@"Tap on the graph to update the past date & time";
 
 }
 
 
 }else{
 title=@"Tap on the graph to update the past date & time";
 }
 }else if (section ==2){
 
 title = [NSString stringWithFormat:@"Last %d Days",no_of_days];
 
 //title=@"Last 14 Days";
 }
 return title;
 }*/

-(void)clearcell:(UITableViewCell *)cell{
    
    for (UIView * view in[cell.contentView subviews]){
        [view removeFromSuperview];
    }
    cell.textLabel.text=nil;
    cell.accessoryView=nil;
    
    
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UIImageView *image1 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"warning.png"]];
       UIImageView *image3 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"approve.png"]];
    if(indexPath.section==0){
        
        ELDInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell1"];
        
        if ([current_status isEqual:@"D"]) {
            cell.status.text=@"Driving";
            cell.icon.image= [UIImage imageNamed:@"images/driving.png"];
        }
        if ([current_status isEqual:@"SB"]) {
            cell.status.text=@"Sleeping";
            cell.icon.image= [UIImage imageNamed:@"sleeping.png"];
        }
        if ([current_status isEqual:@"ON"]) {
            cell.status.text=@"On Duty";
             cell.icon.image= [UIImage imageNamed:@"icons/onduty.png"];
        }
        if ([current_status isEqual:@"OFF"]) {
            cell.status.text=@"Off Duty";
            cell.icon.image= [UIImage imageNamed:@"icons/offduty.png"];
        }
        if ([current_status isEqual:@"NONE"]) {
            cell.status.text=@"None";
            cell.icon.image= [UIImage imageNamed:@"icons/offduty.png"];
        }
        if ([current_status isEqual:@"PC"]) {
            cell.status.text=@"PC";
            cell.icon.image= [UIImage imageNamed:@"icons/offduty.png"];
        }
        if ([current_status isEqual:@"YM"]) {
            cell.status.text=@"YM";
            cell.icon.image= [UIImage imageNamed:@"icons/onduty.png"];
        }
        cell.currentVehicle.text = strSelectedVehicle;
        UITapGestureRecognizer *statusTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(chageStatus)];
        [cell.currentStatusView addGestureRecognizer:statusTap];
        UITapGestureRecognizer *vehicleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showVehicleList)];
        [cell.currentVehicleView addGestureRecognizer:vehicleTap];
        return cell;
        
        
    }else if(indexPath.section==1){
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (indexPath.row==0) {
            
          //  if(![current_status isEqual:@"NONE"]){
                CGRect someRect = CGRectMake(0, 100, 100, 50);
//                UITextField *text1 = [[UITextField alloc] initWithFrame:someRect];
//                NSLog(@"%f,%f",[[UIScreen mainScreen] bounds].size.width,tableView.frame.size.width);
                firstGraphView *graph = [[firstGraphView alloc]initWithFrame:CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width,150)];
                UILabel *off = [[UILabel alloc]initWithFrame:CGRectMake(550, 10, 40, 40)];
                off.text=@"sam";
                off.textColor=[UIColor yellowColor];
                // [graph ins   ertSubview:off aboveSubview:graph];
//                [cell.contentView addSubview:text1];
                [cell.contentView addSubview:graph];
           // }
        }
        return  cell;
        
        
        
        
    }else{

        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        
        if(no_of_days > 90){
            
            no_of_days = 90;
        }
        
        
        
        if (indexPath.row<no_of_days)
        {
            
            if ([dateList count]!=0) {
                
                
                NSArray *date_elements = [[[dateList objectAtIndex:(int)(indexPath.row+1)]objectForKey:@"date"] componentsSeparatedByString:@"/"];
                NSString *month=@"";
                switch ([date_elements[0] integerValue]) {
                    case 1:
                        month=@"Jan";
                        break;
                    case 2:
                        month=@"Feb";
                        break;
                    case 3:
                        month=@"Mar";
                        break;
                    case 4:
                        month=@"Apr";
                        break;
                    case 5:
                        month=@"May";
                        break;
                    case 6:
                        month=@"Jun";
                        break;
                    case 7:
                        month=@"Jul";
                        break;
                    case 8:
                        month=@"Aug";
                        break;
                    case 9:
                        month=@"Sep";
                        break;
                    case 10:
                        month=@"Oct";
                        break;
                    case 11:
                        month=@"Nov";
                        break;
                    case 12:
                        month=@"Dec";
                        break;
                        
                        
                        
                    default:
                        break;
                }//end of switch
                
                NSString *date_string=[NSString stringWithFormat:@"%@ %@ %@",date_elements[1],month,date_elements[2]];
                //cell.textLabel.text=dateslist[indexPath.row];
                cell.textLabel.text=date_string;
                
                // }
                if([dateList count]!=0){
                    if ([[dateList[indexPath.row+1] objectForKey:@"approved"] isEqualToString:@"1"])
                    {
                        //  BFLog(@"yes");
                        cell.accessoryView=image3;
                    }else{
                        
                        cell.accessoryView=image1;
                        
                    }
                }else{
                    cell.accessoryView=image1;
                    
                }
            }else{
                cell.textLabel.text=@"No data";
                
                
                
            }
            //  BFLog(@"previous:%@,%d",previousdate,day);
            //  _date.text=previousdate;
            
            cell.textLabel.textAlignment=NSTextAlignmentLeft;
            cell.imageView.image=[UIImage imageNamed:@"menuicon.jpg"];
            cell.textLabel.textColor=[UIColor purpleColor];
        }
        
        //}
        else if (rows_num!=14) {
            
            if (indexPath.row==no_of_days) {
                cell.textLabel.text=@"Load More.....";
                cell.accessoryView=NULL;
                
                cell.textLabel.textAlignment=NSTextAlignmentCenter;
                cell.imageView.image=[UIImage imageNamed:@"menuicon.jpg"];
                cell.textLabel.textColor=[UIColor blackColor];
            }
            
            
        }else if(rows_num>=14){
            if (indexPath.row==14) {
                BFLog(@"No more data");
                cell.textLabel.text=@"No more data to load";
                cell.textLabel.textAlignment=NSTextAlignmentCenter;
                cell.imageView.image=[UIImage imageNamed:@"menuicon.jpg"];
                cell.textLabel.textColor=[UIColor blackColor];
                
            }
        }
        
        
        

        
        return cell;
        
    }


}
/*
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UIImageView *image1 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"warning.png"]];
    UIImageView *image2 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"navigation.png"]];
    UIImageView *image3 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"approve.png"]];
    static NSString *cellIdentifier=@"cell";
   
    cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    [self clearcell:cell];
    //firstGraphView *graph = [[firstGraphView alloc]initWithFrame:CGRectMake(0, 0, 600, 150)];
    if(cell==nil){
        // cell = [tableView dequeueReusableCellWithIdentifier:nil forIndexPath:indexPath];
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }else{
        //  [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        
    }
    
    
    
    //UITableViewCell *cell = [[MGSwipeTableCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    
    // [cell.contentView clearsContextBeforeDrawing];
    cell.detailTextLabel.backgroundColor=[UIColor grayColor];
    if (indexPath.section ==0) {
        cell.textLabel.textAlignment=NSTextAlignmentLeft;
        // data=@"driving";
        if ([current_status isEqual:@"D"]) {
            cell.textLabel.text=@"Driving";
            cell.imageView.image= [UIImage imageNamed:@"images/driving.png"];
        }
        if ([current_status isEqual:@"SB"]) {
            cell.textLabel.text=@"Sleeping";
            cell.imageView.image= [UIImage imageNamed:@"sleeping.png"];
        }
        if ([current_status isEqual:@"ON"]) {
            cell.textLabel.text=@"On Duty";
            cell.imageView.image= [UIImage imageNamed:@"icons/onduty.png"];
        }
        if ([current_status isEqual:@"OFF"]) {
            cell.textLabel.text=@"Off Duty";
            cell.imageView.image= [UIImage imageNamed:@"icons/offduty.png"];
        }
        cell.accessoryView=image2;
    }
    if (indexPath.section ==1){
        if (indexPath.row==0) {
            
            
            CGRect someRect = CGRectMake(0, 100, 100, 50);
            UITextField *text1 = [[UITextField alloc] initWithFrame:someRect];
            NSLog(@"%f,%f",[[UIScreen mainScreen] bounds].size.width,tableView.frame.size.width);
            firstGraphView *graph = [[firstGraphView alloc]initWithFrame:CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width,150)];
            UILabel *off = [[UILabel alloc]initWithFrame:CGRectMake(550, 10, 40, 40)];
            off.text=@"sam";
            off.textColor=[UIColor yellowColor];
            // [graph insertSubview:off aboveSubview:graph];
            [cell.contentView addSubview:text1];
            [cell.contentView addSubview:graph];
            
        }
        
        
    }
    if (indexPath.section == 2) {
        
        
        if(no_of_days > 90){
            
            no_of_days = 90;
        }
        if (indexPath.row<no_of_days)
        {
            
            if ([dateslist count]!=0) {
                
                
                NSArray *date_elements = [dateslist[indexPath.row] componentsSeparatedByString:@"/"];
                NSString *month=@"";
                switch ([date_elements[0] integerValue]) {
                    case 1:
                        month=@"Jan";
                        break;
                    case 2:
                        month=@"Feb";
                        break;
                    case 3:
                        month=@"Mar";
                        break;
                    case 4:
                        month=@"Apr";
                        break;
                    case 5:
                        month=@"May";
                        break;
                    case 6:
                        month=@"Jun";
                        break;
                    case 7:
                        month=@"Jul";
                        break;
                    case 8:
                        month=@"Aug";
                        break;
                    case 9:
                        month=@"Sep";
                        break;
                    case 10:
                        month=@"Oct";
                        break;
                    case 11:
                        month=@"Nov";
                        break;
                    case 12:
                        month=@"Dec";
                        break;
                        
                        
                        
                    default:
                        break;
                }
                NSString *date_string=[NSString stringWithFormat:@"%@ %@ %@",date_elements[1],month,date_elements[2]];
                //cell.textLabel.text=dateslist[indexPath.row];
                cell.textLabel.text=date_string;
                
                // }
                if([unapproved_dates count]!=0){
                    if ([unapproved_dates containsObject:dateslist[indexPath.row]]) {
                        //  BFLog(@"yes");
                        cell.accessoryView=image1;
                    }else{
                        
                        cell.accessoryView=image3;
                        
                    }
                }else{
                    cell.accessoryView=image3;
                    
                }
            }else{
                cell.textLabel.text=@"No data";
                
                
                
            }
            //  BFLog(@"previous:%@,%d",previousdate,day);
            //  _date.text=previousdate;
            
            cell.textLabel.textAlignment=NSTextAlignmentLeft;
            cell.imageView.image=[UIImage imageNamed:@"menuicon.jpg"];
            cell.textLabel.textColor=[UIColor purpleColor];
        }
        
        //}
        else if (rows_num!=14) {
            
            if (indexPath.row==no_of_days) {
                cell.textLabel.text=@"Load More.....";
                cell.accessoryView=NULL;
                
                cell.textLabel.textAlignment=NSTextAlignmentCenter;
                cell.imageView.image=[UIImage imageNamed:@"menuicon.jpg"];
                cell.textLabel.textColor=[UIColor blackColor];
            }
            
            
        }else if(rows_num>=14){
            if (indexPath.row==14) {
                BFLog(@"No more data");
                cell.textLabel.text=@"No more data to load";
                cell.textLabel.textAlignment=NSTextAlignmentCenter;
                cell.imageView.image=[UIImage imageNamed:@"menuicon.jpg"];
                cell.textLabel.textColor=[UIColor blackColor];
                
            }
        }
        
        
    }
    
    return cell;
    
    
}


*/
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat height = 30;
    if (indexPath.section==1) {
        height = 100;
    }
    else if (indexPath.section ==0){
        
        height=80.0;
    }
    else{
        
        height=30;
    }
    return height;
}

-(void)prepareForReuse{
    
    [self prepareForReuse];
    cell.textLabel.text=nil;
    cell.imageView.image=nil;
    [cell.contentView removeFromSuperview];
    firstGraphView *graph = [[firstGraphView alloc]initWithFrame:CGRectMake(0, 0, 600, 150)];
    [graph removeFromSuperview];
 
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (indexPath.section==2) {
//        if (indexPath.row<no_of_days) {
//
//
//            if (editingStyle==UITableViewCellEditingStyleDelete) {
//
//                NSString *dateback = dateslist[indexPath.row];
//                [self setOffflagback:dateback flag:1];
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [_logtable reloadData];
//
//                });
//
//            }
//        }
//
//    }
    
}
-(void) setOffflagback:(NSString *)date flag:(int)offflag{
    
    NSString *getflag = [NSString stringWithFormat:@"select flag from TempTable where date='%@'",date];
    NSArray *values = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:getflag]];
    
    BFLog(@"flag value:%@",values[0][0]);
    //  if ([values[0][0] isEqualToString:@"0"]) {
    
    
    NSString *query_update = [NSString stringWithFormat:@"UPDATE TempTable SET \"24hr_off\"='%d' where date='%@'",offflag,date];
    
    // Execute the query.
    
    //NSString *query_del = @"delete from TempTable";
    [dbManager executeQuery:query_update];
    
    // If the query was successfully executed then pop the view controller.
    if (dbManager.affectedRows != 0) {
        BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
        [self updatedatelist:rows_num];
        UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                         message:@"Date Deleted!!!"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:@"OK", nil];
        
        [alert2 show];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [_logtable reloadData];
            
        });
    }
    else{
        BFLog(@"Could not execute the query");
    }
    /* }
     else{
     UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@"Sorry"
     message:@"U can't delete approved date!!!"
     delegate:self
     cancelButtonTitle:@"Cancel"
     otherButtonTitles:@"OK", nil];
     
     [alert2 show];
     
     }*/
    
}
-(void)getDVIRData:(NSString *)date{
    //getting authCode
    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    NSString *authCode = @"";
    if([authData count]!=0){
        authCode = authData[0][1];
        
    }
    //---
    NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/getDVIRData.php?masterUser=%@&user=%@&date=%@&authCode=%@",masterUser,user,date,authCode];
    //  NSString *urlString = [NSString stringWithFormat:@"http://23.239.28.100/mydealership/HOSIOSDotInspection/getDVIRData.php?masterUser=hosApp&user=Anjana&date=%@",date];
    NSURL *url = [NSURL URLWithString:urlString];
    NSData *data = [NSData dataWithContentsOfURL:url];
    
    if (data) {
        //dispatch_queue_t queue1 = dispatch_queue_create("com.SievaNetworks.app.queue", DISPATCH_QUEUE_CONCURRENT);
        
        
        // dispatch_sync(queue1, ^{
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:urlString] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            //  BFLog(@"Retrieved Data:%@,%@",data,response);
            BFLog(@"---------------------------------------");
            BFLog(@"error:%@",error);
            if(data){
                NSArray *dvirData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                // NSArray *dvirData = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                NSString *query_del = @"delete from DVIR_Report";
                [dbManager executeQuery:query_del];
                
                for (int i=0; i<[dvirData count]; i++) {
                    //NSMutableDictionary *DVIR=[[NSMutableDictionary alloc] init];
                    NSMutableDictionary *dict = dvirData[i];
                    NSMutableDictionary *vehicle=[[NSMutableDictionary alloc] init];
                    NSMutableDictionary *trailer=[[NSMutableDictionary alloc] init];
                    if ([dict objectForKey:@"vehicle"]!=nil && ![[dict objectForKey:@"vehicle"] isEqual:@"Array\n"]) {
                        vehicle=[dict objectForKey:@"vehicle"];
                        BFLog(@"Vehicles:%@",[dict objectForKey:@"vehicle"]);
                        
                    }
                    if ([dict objectForKey:@"trailer"]!=nil && ![[dict objectForKey:@"trailer"] isEqual:@"Array"]) {
                        
                        trailer=[dict objectForKey:@"trailer"];
                        BFLog(@"Trailer:%@",[dict objectForKey:@"trailer"]);
                        
                    }
                    
                    
                    
                    //NSData *data_vehicle = [NSJSONSerialization dataWithJSONObject:vehicle options:0 error:nil];
                    //NSString *jsonStr_vehicle = [[NSString alloc] initWithData:data_vehicle
                    //encoding:NSUTF8StringEncoding];
                    BFLog(@"JSON:%@",vehicle);
                    // NSData *data_trailer = [NSJSONSerialization dataWithJSONObject:trailer options:0 error:nil];
                    //  NSString *jsonStr_trailer = [[NSString alloc] initWithData:data_trailer
                    //  encoding:NSUTF8StringEncoding];
                    BFLog(@"JSON:%@",trailer);
                    
                    NSString *query_insert1 = [NSString stringWithFormat:@"insert into DVIR_Report values(null, '%@', '%@', '%@' ,'%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')", date,[dict objectForKey:@"time"],[dict objectForKey:@"location"],vehicle,trailer,[dict objectForKey:@"dvirId"],[dict objectForKey:@"driverSigned"],[dict objectForKey:@"mechanicSigned"],[dict objectForKey:@"defectsCorrected"],[dict objectForKey:@"defectsNotCorrected"],[dict objectForKey:@"odometer"],[dict objectForKey:@"carrier"],[dict objectForKey:@"mechNotes"]];
                    
                    
                    // Execute the query.
                    
                    [dbManager executeQuery:query_insert1];
                    
                    // If the query was successfully executed then pop the view controller.
                    if (dbManager.affectedRows != 0) {
                        BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
                        
                        
                    }
                    else{
                        BFLog(@"Could not execute the query");
                    }
                }
                
            }
            
            
        }];
        [dataTask resume];
        sleep(2);
    }

}
-(void)chageStatus{
    
//        UIStoryboard *storyBoard = self.storyboard;
//        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"drivingProgress"];
//        [self presentViewController:vc animated:YES completion:nil];
    
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"changedutystatus"];
    [self presentViewController:vc animated:YES completion:nil];
}
-(void)showVehicleList{
    
    UIStoryboard *storyBoard = self.storyboard;
    VehicleListViewController *vc =(VehicleListViewController *) [storyBoard instantiateViewControllerWithIdentifier:@"vehicleList"];
    vc.delegate = self;
    [self presentViewController:vc animated:YES completion:nil];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    section_index=(int)indexPath.section;
    row_index = (int)indexPath.row;
    
    if (indexPath.section==0) {
        }
    if (indexPath.section==1) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.label.text = @"Please wait a moment";
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_group_t group = dispatch_group_create();
        if (indexPath.row==0) {
            selected_date=[[dateList objectAtIndex:0] objectForKey:@"date"];
            selected_previous=@"";
            dispatch_group_async(group, queue, ^{
                //dateslist[indexPath.row];
          //      [self getDVIRData:today_log];
            });
        }else{
            NSDate *previous = [now dateByAddingTimeInterval:-(row_index+1)*24*60*60];
            NSDateFormatter *outputFormatter1 = [[NSDateFormatter alloc] init];
            [outputFormatter1 setDateFormat:@"MM/dd/YYYY"];
            NSString *previousdate = [outputFormatter1 stringFromDate:previous];
            BFLog(@"previous1:%@",previousdate);
            dispatch_group_async(group, queue, ^{
            //    [self getDVIRData:previousdate];
            });
        }
        dispatch_group_notify(group, queue, ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES];
                UIStoryboard *storyBoard = self.storyboard;
                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"logsmenu"];
                [self presentViewController:vc animated:YES completion:nil];
                
            });
        });
    }
    
    if (indexPath.section==2) {
        BFLog(@"row:%ld",(long)indexPath.row);
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.label.text = @"Please wait a moment";
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_group_t group = dispatch_group_create();
        
        
        if (indexPath.row==no_of_days) {
            
            rows_num=rows_num+7;
            if(rows_num>14){
                
                rows_num=14;
            }
            
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                    [self loadmore:rows_num];
                    
                    [_logtable reloadData];
                });
        }
        else{
            dispatch_group_async(group, queue, ^{
                selected_date=@"";
                selected_previous=[[dateList objectAtIndex:(int)(indexPath.row+1)] objectForKey:@"date"];//dateslist[indexPath.row];
//                NSLog(@"selected: %@",selected_previous);
                selected_index=[dateList indexOfObjectPassingTest:^BOOL(NSDictionary *dict, NSUInteger idx, BOOL *stop){
                    return [[dict objectForKey:@"date"] isEqualToString:selected_previous];
                    
                }];
                
               // [self getDVIRData:selected_previous];
            });
            dispatch_group_notify(group, queue, ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                    
                    UIStoryboard *storyBoard = self.storyboard;
                    UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"logsmenu"];
                    [self presentViewController:vc animated:YES completion:nil];
                });
            });
            
        }
        
        
    }
    
    
}


-(void)loadmore:(int)count{
    no_of_days=14;
    // int previous_count=no_of_days;
    /*[dateslist removeAllObjects];
    [unapproved_dates removeAllObjects];
    
    //NSString *query_dates =[NSString stringWithFormat:@"select date,flag from TempTable where \"24hr_off\"='%d'  order by date desc limit '%d';",0,count];
    
    NSString *query_dates =[NSString stringWithFormat:@"select date,flag,\"24hr_off\" from TempTable  order by dateInSecs desc limit '%d';",count+1];
    
    // Get the results.
    
    NSArray *working_days = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query_dates]];
    BFLog(@"Result:%@",working_days);
    for (int i=1; i<[working_days count]; i++) {
        //if ([working_days[i][2] integerValue]==0) {
        [dateslist addObject:working_days[i][0]];
        
        
        //  }else{
        
        //    if ([working_days[i][1] integerValue]==1) {
        // [dateslist addObject:working_days[i][0]];            //}
        //}
    }
    
    
    NSString *query0 = [NSString stringWithFormat:@"select date,flag from TempTable order by dateInSecs desc limit '%d';",count+1];
    
    // Get the results.
    
    NSArray *previousdates = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    BFLog(@"Result:%@",previousdates);
    unapproved_dates=[[NSMutableArray alloc] init];
    NSString *msg=@"";
    for (int i=1; i<[working_days count]; i++) {
        BFLog(@"%@",previousdates[i][1]);
        if ([working_days[i][1] isEqual:@"0"]) {
            [unapproved_dates addObject:working_days[i][0]];
            NSString *sam = [NSString stringWithFormat:@"%C %@\n",(unichar)0x2022,previousdates[i][0]];
            msg = [NSString stringWithFormat:@"%@%@",msg,sam];
        }
        
        
    }
    no_of_days=(int)[dateslist count];
    BFLog(@"Unapproved dates:%@",unapproved_dates);
    if([unapproved_dates containsObject:@"(null)"]){
        
        [unapproved_dates removeObject:@"(null)"];
        
    }
    /*if (previous_count==no_of_days) {
     rows_num=rows_num+14;
     if (rows_num<90) {
     [self loadmore:rows_num];
     }else{
     rows_num=90;
     }
     
     }else{
     [self loadmore:rows_num];
     }
     UIImageView *image=NULL;
     NSIndexPath *indexpath1 = [NSIndexPath indexPathForRow:0 inSection:0];
     UITableViewCell *cell1=[_logtable cellForRowAtIndexPath:indexpath1];
     cell1.accessoryView=image;
     firstGraphView *graph = [[firstGraphView alloc]initWithFrame:CGRectMake(0, 0, 600, 150)];
     NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:1];
     UITableViewCell *cell=[_logtable cellForRowAtIndexPath:indexpath];
     //cell.contentView=[UIView ];
     [graph removeFromSuperview];
     _logtable.delegate=self;
     //[cell.contentView addSubview:graph];*/
    dispatch_async(dispatch_get_main_queue(), ^{
        [_logtable reloadData];
        
    });
}

- (void)reloadSections:(NSInteger *)sections withRowAnimation:(UITableViewRowAnimation)animation{
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


-(void)saveToServer:(NSMutableDictionary *)driver_data date:(NSString *)date{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    //getting authCode
    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    NSString *authCode = @"";
    if([authData count]!=0){
        authCode = authData[0][1];
        
    }
    //---------------------------
    NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/saveHosGraphData.php?masterUser=%@&user=%@&date=%@&approve=0&authCode=%@",masterUser,user,date,authCode];
    // NSString *urlString = [NSString stringWithFormat:@"http://23.239.28.100/mydealership/saveHosGraphData_app.php?masterUser=%@&user=%@&date=%@&approve=0",master_menu,user_menu,current_date];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSURLResponse *response;
    NSError *error;
    NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    if(httpResponse!=nil){
        if ([httpResponse statusCode] ==200 ) {
            BFLog(@"Device connected to the internet");
            NSURL *url = [NSURL URLWithString:urlString];
            
            //NSError *error;
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:60.0];
            
            [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
            
            [request setHTTPMethod:@"POST"];
            /* NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
             @"IOS TYPE", @"typemap",
             nil];*/
            
            //NSMutableDictionary *currentStatus=[[NSMutableDictionary alloc]init];
            //[currentStatus setValue:status forKey:@"Status"];*/
            /*  NSMutableArray *inner = [[NSMutableArray alloc] initWithArray:@[@"00:00",@"OFF"]];
             NSMutableDictionary *data_dict = [[NSMutableDictionary alloc] init];
             [data_dict setObject:inner forKey:@"payload"];
             NSError *error;*/
            NSData *postData = [NSJSONSerialization dataWithJSONObject:driver_data options:0 error:&error];
            [request setHTTPBody:postData];
            
            NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                NSDictionary *responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
            }];
            
            [postDataTask resume];
        }else if([httpResponse statusCode]==403){
            if(autoLoginCount<=1){
                insertDutyCycle *obj = [insertDutyCycle new];
                BOOL success =  [obj autoLogin];
                
                if(success){
                    [self saveToServer:driver_data date:date];
                    
                }else{
                    KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
                    [credentials resetKeychainItem];
                    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
                                                                     message:@"Please Login Again"
                                                                    delegate:self
                                                           cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil, nil];
                    alert1.tag = 3;
                    alert1.delegate = self;
                    
                    [alert1 show];
                    menuScreenViewController *obj = [menuScreenViewController new];
                    [obj syncServerLater:driver_data date:date];
                }
            }else{
                KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
                [credentials resetKeychainItem];
                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
                                                                 message:@"Please Login Again"
                                                                delegate:self
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil, nil];
                alert1.tag = 2;
                alert1.delegate = self;
                
                [alert1 show];
                
                menuScreenViewController *obj = [menuScreenViewController new];
                [obj syncServerLater:driver_data date:date];
                
            }
            
        }else if([httpResponse statusCode]==503){
            BFLog(@"Device not connected to the internet");
            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Service Unavailable"
                                                             message:@"No connectivity with server at this time,it will sync automatically"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
            
            [alert1 show];
            menuScreenViewController *obj = [menuScreenViewController new];
            [obj syncServerLater:driver_data date:date];
            
        }
    }else{
        
        
        
        
    }
    
}


-(void)saveCurrent:(NSMutableDictionary *)data date:(NSString *)date{
    // dispatch_group_t group = dispatch_group_create();
    dispatch_queue_t queue = dispatch_queue_create("com.SievaNetworks.app.queue", DISPATCH_QUEUE_CONCURRENT);
    
    /*NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
     NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
     //getting authCode
     NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
     NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
     NSString *authCode = @"";
     if([authData count]!=0){
     authCode = authData[0][1];
     
     }
     //---------------------------
     NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp1/saveHosGraphData.php?masterUser=%@&user=%@&date=%@&approve=0&authCode=%@",masterUser,user,date,authCode];
     NSURL *url = [NSURL URLWithString:urlString];
     NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
     [request setHTTPMethod:@"HEAD"];
     NSURLResponse *response;
     NSError *error;
     NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
     NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
     if(httpResponse!=nil){
     if ([httpResponse statusCode] ==200 ) {
     [loader startAnimating];
     BFLog(@"Device connected to the internet");
     NSURL *url = [NSURL URLWithString:urlString];*/
    dispatch_async(queue, ^{
        [self saveToServer:data date:date];
        /*NSError *error;
         
         NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
         cachePolicy:NSURLRequestUseProtocolCachePolicy
         timeoutInterval:60.0];
         
         [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
         [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
         
         [request setHTTPMethod:@"POST"];
         /* NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
         @"IOS TYPE", @"typemap",
         nil];*/
        
        /*  NSMutableDictionary *currentStatus=[[NSMutableDictionary alloc]init];
         [currentStatus setValue:status forKey:@"Status"];
         
         NSData *postData = [NSJSONSerialization dataWithJSONObject:data options:0 error:&error];
         [request setHTTPBody:postData];
         
         
         NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
         
         }];
         
         [postDataTask resume];*/
        
    });
    dispatch_barrier_sync(queue, ^{
        UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Your Data is Saved" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [alert1 dismissViewControllerAnimated:YES completion:nil];
        }];
        
        
        [alert1 addAction:ok1];
        /// [self presentViewController:alert1 animated:YES completion:nil];
        
    });
    //[loader stopAnimating];
    //loader.hidesWhenStopped = YES;
    
    /*}
     /*else if([httpResponse statusCode]==503){
     BFLog(@"Device not connected to the internet");
     UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Service Unavailable"
     message:@"No connectivity with server at this time,it will sync automatically"
     delegate:self
     cancelButtonTitle:@"Cancel"
     otherButtonTitles:@"OK", nil];
     
     [alert1 show];
     
     NSString *query_update = [NSString stringWithFormat:@"UPDATE FinalTable SET server_sync='%d' WHERE date='%@'",0,date];
     
     // Execute the query.
     
     //NSString *query_del = @"delete from TempTable";
     [dbManager executeQuery:query_update];
     
     // If the query was successfully executed then pop the view controller.
     if (dbManager.affectedRows != 0) {
     BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
     
     }
     else{
     BFLog(@"Could not execute the query");
     }
     
     
     }*
     }*/
    
}
-(void)getthedata:(NSString *)date{
    
    NSString *query1 = [NSString stringWithFormat:@"select * from TempTable where date='%@'",date];
    
    // Get the results.
    NSArray *data = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query1]];
    BFLog(@"Result:%@",data);
    if ([data count]!=0) {
        NSString *dbresult = data[[data count]-1][2];
        
        
        
        BFLog(@"string:%@",dbresult);
        
        
        //BFLog(@"timezone:%lu",(unsigned long)[dbresult count]);
        NSData *data2= [dbresult dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
        BFLog(@"Dictionary of db data:%@",json_log1);
        if ([json_log1 count]==0) {
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@" " withString:@""];
            dbresult = [NSString stringWithFormat:@"[%@]",dbresult];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"\";" withString:@";"];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"=\"" withString:@"="];
            
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"location=(" withString:@"\"location\":["];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"payload=(" withString:@"\"payload\":["];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@");" withString:@"],"];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@";" withString:@"\","];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"=" withString:@":\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"id" withString:@"\"id\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"value" withString:@"\"value\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"posx" withString:@"\"posx\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"posy" withString:@"\"posy\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"timezone" withString:@"\"timezone\""];
            NSData *data2= [dbresult dataUsingEncoding:NSUTF8StringEncoding];
            json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
            BFLog(@"Dictionary of db data2:%@",json_log1);
            
        }
        
        
        //[location_array removeAllObjects];
        //   BFLog(@"data portion:%@",[json_log1 allKeys]);
        // NSMutableArray *dict=[[NSMutableArray alloc] init];
        NSMutableArray *dict = [json_log1 objectForKey:@"payload"];
        //location_array = [json_log1 objectForKey:@"location"];
        //BFLog(@"Location array:%@",location_array);
        int json_size = (int)[dict count];
        BFLog(@"inner json count:%d",json_size);
        [data_to_finaldb1 removeAllObjects];
        for (int i=0; i<json_size; i++) {
            
            BFLog(@"posx:%@,%d",[dict[0] objectForKey:@"posx"],i);
            float posx=[[dict[i] objectForKey:@"posx"] floatValue];
            float x1 = posx - 40.0;
            int x2 = x1/12.25;
            int x3 = x2/4.0;
            int x4 = x2%4;
            
            /* NSString *minutes;
             if (15*x4 <15) {
             minutes=@"00";
             }
             else if(15*x4 >=15 && 15*x4 <30){
             minutes=@"15";
             }else if(15*x4 >=30 && 15*x4 <45){
             minutes=@"30";
             }else if(15*x4 >=45 && 15*x4 <60){
             minutes=@"45";
             }*/
            NSString *posx_1 = [NSString stringWithFormat:@"%d:%d",x3,15*x4];
            
            // float time = [posx_1 floatValue];
            int event_no =(int)[[dict[i] objectForKey:@"posy"] integerValue];
            NSString * event;
            if (event_no == 245) {
                event=@"ON";
            }
            else if(event_no == 125){
                event=@"SB";
                
            }else if (event_no == 185){
                event = @"D";
            }else if (event_no == 65){
                event = @"OFF";
            }
            NSArray *final_data = [[NSArray alloc] initWithObjects:posx_1,event, nil];
            [data_to_finaldb1 addObject:final_data];
            
            
            
        }
        NSString *last_status =data_to_finaldb1[[data_to_finaldb1 count]-1][1];
        [data_to_finaldb1 removeLastObject];
        NSArray *last = [[NSArray alloc] initWithObjects:@"24:00",last_status, nil];
        [data_to_finaldb1 addObject:last];
        
    }
    BFLog(@"data array 4:%@",data_to_finaldb1);
    
    
    
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    if([[self.view subviews] containsObject:transparentView]){
        [transparentView removeFromSuperview];
        
    }
}
-(void)showHelp{
    
    sourceVC = [self valueForKey:@"storyboardIdentifier"];
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc1 = [storyBoard instantiateViewControllerWithIdentifier:@"helpScreen"];
    [self presentViewController:vc1 animated:YES completion:nil];
    
    
    
}


- (IBAction)infoBtn:(id)sender {
    
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    transparentView  = [[UIView alloc] initWithFrame:screenSize];
    transparentView.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:0.6];
    UIButton *tutorialLink = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    tutorialLink.frame = CGRectMake(screenSize.origin.x+(screenSize.size.width/3), screenSize.size.height-40, screenSize.size.width/3, 40);
    [tutorialLink addTarget:self action:@selector(showHelp) forControlEvents:UIControlEventTouchUpInside];
    tutorialLink.tintColor = [UIColor yellowColor];
    [tutorialLink setTitle:@"<<Know More>>" forState:UIControlStateNormal];
    
    [transparentView addSubview:tutorialLink];
    
    UIImageView *arrow1=[[UIImageView alloc]initWithFrame:CGRectMake(screenSize.size.width-90, screenSize.size.height/2, 50, 50)];
    [arrow1 setImage:[UIImage imageNamed:@"icons/arrow2.png"]];
    
    
    UIImageView *arrow=[[UIImageView alloc]initWithFrame:CGRectMake(addButton.frame.origin.x-50, addButton.frame.origin.y, 50, 50)];
    [arrow setImage:[UIImage imageNamed:@"icons/arrow1.png"]];
    UIImageView *arrow2=[[UIImageView alloc]initWithFrame:CGRectMake(screenSize.size.width/2+100, screenSize.size.height/4-60, 50, 50)];
    [arrow2 setImage:[UIImage imageNamed:@"icons/arrow4.png"]];
    UILabel *changeStatus  = [[UILabel alloc] initWithFrame:CGRectMake(screenSize.origin.x+(screenSize.size.width*20)/100, screenSize.size.height/4-(screenSize.size.width*10)/100, screenSize.size.width/2, 20)];
    changeStatus.text=@"Click here to change status";
    changeStatus.numberOfLines=2;
    changeStatus.textColor=[UIColor whiteColor];
    changeStatus.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    changeStatus.minimumScaleFactor=10./changeStatus.font.pointSize;
    changeStatus.adjustsFontSizeToFitWidth=YES;
    UIView *view1=[menu valueForKey:@"view"];
    UIImageView *arrow3=[[UIImageView alloc]initWithFrame:CGRectMake(view1.frame.origin.x+view1.frame.size.width, view1.frame.origin.y, 50, 50)];
    [arrow3 setImage:[UIImage imageNamed:@"icons/arrow2.png"]];
    UILabel *dashboard  = [[UILabel alloc] initWithFrame:CGRectMake(arrow3.frame.origin.x+arrow3.frame.size.width+20, arrow3.frame.origin.y+arrow3.frame.size.height/2, screenSize.size.width/2, 20)];
    dashboard.text=@"Dashboard";
    dashboard.numberOfLines=2;
    dashboard.textColor=[UIColor whiteColor];
    dashboard.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    dashboard.minimumScaleFactor=10./dashboard.font.pointSize;
    dashboard.adjustsFontSizeToFitWidth=YES;
    
    UIImageView *arrow4=[[UIImageView alloc]initWithFrame:CGRectMake(screenSize.origin.x+40, screenSize.size.height/2+(screenSize.size.height/4), 50, 50)];
    [arrow4 setImage:[UIImage imageNamed:@"icons/arrow2.png"]];
    UILabel *date  = [[UILabel alloc] initWithFrame:CGRectMake(arrow4.frame.origin.x+40,screenSize.size.height/2+(screenSize.size.height/4), screenSize.size.width/2+25, 20)];
    date.text=@"Click a date to see the detailed info";
    date.numberOfLines=2;
    date.textColor=[UIColor whiteColor];
    date.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    date.minimumScaleFactor=10./date.font.pointSize;
    date.adjustsFontSizeToFitWidth=YES;
    [transparentView addSubview:arrow4];
    [transparentView addSubview:date];
    [transparentView addSubview:dashboard];
    [transparentView addSubview:arrow3];
    [transparentView addSubview:arrow2];
    //[transparentView addSubview:arrow];
    //[transparentView addSubview:arrow1];
    // [transparentView addSubview:addBtn];
    // [transparentView addSubview:deleteBtn];
    [transparentView addSubview:changeStatus];
    [self.view addSubview:transparentView];
    
}

#pragma mark    - <Custom Delegates - VehicleListViewControllerDelegate>
- (void)onSelectVehicleName:(NSString *)strVehicleName{
    strSelectedVehicle = strVehicleName;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [_logtable reloadData];
        
    });
}

-(NSString *)getStatusCode:(NSString *)new_status{
    NSString *statusNew=@"";
    if([new_status isEqualToString:@"3"]||[new_status isEqualToString:@"7"]|| [new_status isEqualToString:@"9"])
        statusNew=@"D";
    else if([new_status isEqualToString:@"2"] || [new_status isEqualToString:@"6"])
        statusNew=@"SB";
    else if([new_status isEqualToString:@"1"]||  [new_status isEqualToString:@"5"])
        statusNew=@"OFF";
    else if([new_status isEqualToString:@"0"])
        statusNew=@"NONE";
    //else if([new_status isEqualToString:@"5"])
        //statusNew=@"OFF";
    else if([new_status isEqualToString:@"4"]|| [new_status isEqualToString:@"8"])
        statusNew=@"ON";
    return statusNew;
}


-(void)saveData: (NSString *)x1{
    DBManager *objDB = [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    NSDictionary *token;
    ;
    NSMutableDictionary *whereToken;
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
        token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        
        //[objDB getLogEditData:whereToken];
        commonDataHandler *common=[commonDataHandler new];
        NSString * date=[common getCurrentTimeInLocalTimeZone];
        token=@{@"userid":[NSString stringWithFormat:@"%d",userId],@"date":[NSString stringWithFormat:@"%@",date]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        NSArray* dataFromDB=[[NSArray alloc]initWithArray:[objDB getLogEditData:whereToken]];
        NSString *statusString=@"";
        for(int j=0;j<[dataFromDB count];j++){
            statusString= [statusString stringByAppendingString:[NSString stringWithFormat:@"%@",[[dataFromDB objectAtIndex:j] objectAtIndex:1]]];
        }
        
        NSString* charStr=[@"" stringByPaddingToLength:96 withString:@"1" startingAtIndex:0];
        if([dataFromDB count]==0)
            statusString=charStr;
        else
        {
            if([statusString isEqualToString:@""])
                statusString=charStr;
            
        }
        statusString=[common modifyData:statusString date:date];
        NSString *statusStringTemp=[statusString stringByReplacingOccurrencesOfString:@"0" withString:@""];
        int currentSlot=(int)statusStringTemp.length;
        statusString=[statusString stringByReplacingCharactersInRange:NSMakeRange((int)(currentSlot-1), 1) withString:[self getStatusBit:x1]];
        
        loggraph *objLog=[loggraph new];
        for(int i=0;i<statusString.length;i++){
            objLog.slot=[NSString stringWithFormat:@"%d",(int)(1+i)];
            objLog.driverStatus=[statusString substringWithRange:NSMakeRange(i, 1)];
            objLog.calculatedStatus=[statusString substringWithRange:NSMakeRange(i, 1)];
            objLog.date=date;
            objLog.userId=[NSString stringWithFormat:@"%d",userId];
            objLog.insert=[NSString stringWithFormat:@"%d",1];
            [objDB insertPastStatus:objLog];
        }
        if(objDB.affectedRows){
            current_status=x1;
            dispatch_async(dispatch_get_main_queue(), ^{
                [dataDictArray removeAllObjects];
                graphDataOperations *obj=[graphDataOperations new];
                    [dataarray removeAllObjects];
                    NSMutableArray *dict=[obj generateGraphPoints:statusString];
                    for (int i=0; i<[dict count]; i++) {
                        
                        //             NSLog(@"posx:%@,%d",[dict[0] objectForKey:@"posx"],i);
                        float posx=[[dict[i] objectForKey:@"posx"] floatValue];
                        float x1 = posx - 40.0;
                        int x2 = x1/12.25;
                        int x3 = x2/4.0;
                        int x4 = x2%4;
                        
                        NSString *minutes;
                        if (15*x4 <15) {
                            minutes=@"00";
                        }
                        else if(15*x4 >=15 && 15*x4 <30){
                            minutes=@"15";
                        }else if(15*x4 >=30 && 15*x4 <45){
                            minutes=@"30";
                        }else if(15*x4 >=45 && 15*x4 <60){
                            minutes=@"45";
                        }
                        NSString *posx_1 = [NSString stringWithFormat:@"%d:%@",x3,minutes];
                        
                        // float time = [posx_1 floatValue];
                        int event_no =(int)[[dict[i] objectForKey:@"posy"] integerValue];
                        NSString * event;
                        if (event_no == 245) {
                            event=@"ON";
                        }
                        else if(event_no == 125){
                            event=@"SB";
                            
                        }else if (event_no == 185){
                            event = @"D";
                        }else if (event_no == 65){
                            event = @"OFF";
                        }
                        NSArray *final_data = [[NSArray alloc] initWithObjects:posx_1,event, nil];
                        [dataarray addObject:final_data];
                    }
                    dataDictArray[0]=[dataarray mutableCopy];
                [_logtable reloadData];
                
            });
            
        }else{
          /*  [[[UIAlertView alloc] initWithTitle:@"Success" message:@"Modifications saved successfully"
                                       delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil, nil] show];*/
        }
        
    }
    
}
-(NSString *)getStatusBit:(NSString *)status_new{
    NSString *statusNew;
    if([status_new isEqualToString:@"D"])
        statusNew=@"7";
    else if([status_new isEqualToString:@"SB"])
        statusNew=@"6";
    else if([status_new isEqualToString:@"OFF"])
        statusNew=@"5";
    else if([status_new isEqualToString:@"ON"])
        statusNew=@"8";
    else if([status_new isEqualToString:@"NONE"])
        statusNew=@"1";
    return statusNew;
}

@end
