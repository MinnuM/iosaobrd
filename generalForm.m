//
//  generalForm.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/1/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "generalForm.h"
#import "accountSettings.h"
#import "SectionRows.h"
#import "FormCommonTableViewCell.h"

int count_edit1=0;
NSString *vehicleValue;
NSString *trailer;
NSString *distance_1;
NSString *shipping_docs;
NSString *driver_1;
CGRect textfieldFrame1;
UITextField *activeField;
@interface generalForm ()<FormCommonTableViewCellDelegate>
{
    NSMutableArray *arrayOfTableData;
    UITextField *txtFieldActive;
}
@end

@implementation generalForm
@synthesize dbManager,loader;
- (void)viewDidLoad {
    [super viewDidLoad];
    [_tblGeneral registerNib:[UINib nibWithNibName:@"FormCommonTableViewCell" bundle:nil] forCellReuseIdentifier:@"FormCommonTableViewCell"];
    [self  createTableData];
    [loader removeFromSuperview];
    dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];

//    UITapGestureRecognizer *backgroundTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backgroundTap)];
//    [self.view addGestureRecognizer:backgroundTapped];
    // Register notification when the keyboard will be show
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillShow:)
//                                                 name:UIKeyboardWillShowNotification
//                                               object:nil];
//
//    // Register notification when the keyboard will be hide
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillHide:)
//                                                 name:UIKeyboardWillHideNotification
//                                               object:nil];
//    //minnu

}
#pragma mark    - <UITableView Delegates & Datasources>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return arrayOfTableData.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    Section *objSec = [arrayOfTableData objectAtIndex:section];
    return objSec.arrayRows.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Section *objSec = [arrayOfTableData objectAtIndex:indexPath.section];
    SectionRows *objRow = [objSec.arrayRows objectAtIndex:indexPath.row];
    FormCommonTableViewCell *cell = (FormCommonTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"FormCommonTableViewCell" forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[FormCommonTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                              reuseIdentifier:@"FormCommonTableViewCell"];
    }
    [cell setDelegate:self];
    [cell setCellWithData:objRow];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 65;
}
//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return UITableViewAutomaticDimension;
//}
//minnu
#pragma mark    - <KeyBoard Notifications>
-(void)keyboardWillShow:(NSNotification *)note
{
    // Get the keyboard size
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue: &keyboardBounds];
    
    
    CGSize keyboardSize = [[[note userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    // Detect orientation
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGRect frame = _tblGeneral.frame;
    
    // Start animation
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3f];
    
    // Reduce size of the Table view
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
        frame.size.height -=keyboardSize.height;
    else
        frame.size.height -= keyboardSize.width;
    
    // Apply new size of table view
    _tblGeneral.frame = frame;
    
    // Scroll the table view to see the TextField just above the keyboard
    if (txtFieldActive)
    {
        CGRect textFieldRect = [txtFieldActive bounds];
         textFieldRect = [txtFieldActive convertRect:textFieldRect toView:_tblGeneral];//[_tblGeneral convertRect:txtFieldActive.bounds fromView:txtFieldActive];
        [_tblGeneral scrollRectToVisible:textFieldRect animated:NO];
    }
    
    [UIView commitAnimations];
}

-(void) keyboardWillHide:(NSNotification *)note
{
    // Get the keyboard size
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue: &keyboardBounds];
    
    // Detect orientation
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGRect frame = _tblGeneral.frame;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3f];
    
    // Increase size of the Table view
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
        frame.size.height += keyboardBounds.size.height;
    else
        frame.size.height += keyboardBounds.size.width;
    
    // Apply new size of table view
    _tblGeneral.frame = frame;
    
    [UIView commitAnimations];
}

#pragma mark    - <UITextField Delegates>
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
   // BFLog(@"entered return key");
    return YES;
    
}

//minnu
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    textfieldFrame1=textField.frame;
    activeField=textField;
    if (count_edit1==0) {
        count_edit1=1;
        gform_id=1;

    }
}

- (void)backgroundTap {
    [self.view endEditing:YES];
    //BFLog(@"Touched Inside");
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)doneedit:(id)sender {
//    [_tblGeneral beginUpdates];
    [txtFieldActive resignFirstResponder];
   
    [_tblGeneral reloadData];

    Section *objsec = [arrayOfTableData firstObject];
    int mandatory=1;
    DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];

    NSDictionary *token;
 //   NSMutableDictionary *whereToken;
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
        token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
        formGeneral *objGenForm=[formGeneral new];
        
        for (SectionRows *row in objsec.arrayRows) {
            if(row.rowType == row_type_form_general_driver){
                if([row.strRowContent containsString:@" "]){
                    NSArray *name=[row.strRowContent componentsSeparatedByString:@" "];
                    objGenForm.firstname=name[0];
                    objGenForm.lastname=name[1];
                }else{
                    objGenForm.firstname = row.strRowContent;
                }
                if([row.strRowContent isEqualToString:@""]){
                    mandatory=0;
                }
            }
            if(row.rowType == row_type_form_general_vehicle){
                objGenForm.vehicle = row.strRowContent;
            }
            if(row.rowType == row_type_form_general_trailer){
                objGenForm.trailer = row.strRowContent;
            }
            if(row.rowType == row_type_form_general_distance){
                objGenForm.distance = row.strRowContent;
            }
            if(row.rowType == row_type_form_general_driverId){
                objGenForm.driverid = row.strRowContent;
            }
            if(row.rowType == row_type_form_general_shippingDoc){
                objGenForm.shippingdoc = row.strRowContent;
            }
        }
    objGenForm.userid=[NSString stringWithFormat:@"%d",userId];
    objGenForm.date=dateValue;
        if(mandatory==0){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Required field is empty!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }else{
            [objDB saveGeneralForm:objGenForm];
            if(objDB.affectedRows!=0){
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Updated Successfully!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else{
                //BFLog(@"Could not execute the query");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ooops" message:@"Something went wrong!! Please try again later!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
}
    }}
#pragma mark    - <Create Table Data>
-(void)createTableData{
    arrayOfTableData = [NSMutableArray new];
    Section *objSec = [Section new];
    
    NSString *strVehicle;
    NSString *strTrailer;
    NSString *strMiles;
    NSString *strShipDocs;
    NSString *strDriverName;
    NSString *strDriverID;
        if ([general count]!=0) {
    
            if([general[0] isEqualToString:@"<null>"] ||[general[0] isEqualToString:@""]|| [general[0] isEqualToString:@"(null)"]){
    
                strVehicle = @"";
            }else{
                strVehicle=general[0];
            }
    
            if([general[1] isEqualToString:@"<null>"] ||[general[1] isEqualToString:@""]|| [general[1] isEqualToString:@"(null)"]){
    
                strTrailer = @"";
            }else{
                strTrailer=general[1];
            }
    
            if([general[3] isEqualToString:@"<null>"] ||[general[3] isEqualToString:@""]|| [general[3] isEqualToString:@"(null)"]){
    
                strShipDocs = @"";
                //shipping_doc.text = @"";
            }else{
                strShipDocs = general[3];
                //shipping_doc.text=general[2];
            }
            if([general[2] isEqualToString:@"<null>"] ||[general[2] isEqualToString:@""]|| [general[2] isEqualToString:@"(null)"]){
    
               // distance.text = @"";
                strMiles = @"";
            }else{
                //distance.text=general[3];
                strMiles = general[2];
            }
            if([general[4] isEqualToString:@"<null> <null>"] ||[general[4] isEqualToString:@""]|| [general[4] isEqualToString:@"(null) (null)"]){
    
               strDriverName = @"";
            }else{
                strDriverName=general[4];
            }
    
            if([general[5] isEqualToString:@"<null>"] ||[general[5] isEqualToString:@""]|| [general[5] isEqualToString:@"(null)"]){
    
                strDriverID = @"";
            }else{
                strDriverID = general[5];
            }
        }
    
    SectionRows *objRow;
    objRow = [SectionRows new];
    objRow.rowType = row_type_form_general_vehicle;
    objRow.isMandatory = NO;
    objRow.strPlaceHolder = @"Truck/Tractor Numbers";
    objRow.strRowTitle = @"Vehicles";
    objRow.strRowContent = strVehicle;
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_form_general_trailer;
    objRow.isMandatory = NO;
    objRow.strPlaceHolder = @"Trailer Numbers";
    objRow.strRowTitle = @"Trailer";
    objRow.strRowContent = strTrailer;
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_form_general_distance;
    objRow.isMandatory = NO;
    objRow.strPlaceHolder = @"Total Miles Driving";
    objRow.strRowTitle = @"Distance";
    objRow.strRowContent = strMiles;
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_form_general_shippingDoc;
    objRow.isMandatory = NO;
    objRow.strPlaceHolder = @"Shipping Document Number";
    objRow.strRowTitle = @"Shipping Documents";
    objRow.strRowContent = strShipDocs;
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_form_general_driver;
    objRow.isMandatory = YES;
    objRow.strPlaceHolder = @"Driver Name";
    objRow.strRowTitle = @"Driver*";
    if([strDriverName containsString:@"(null)"]){
        strDriverName=[strDriverName stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    }
    objRow.strRowContent = strDriverName;
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_form_general_driverId;
    objRow.isMandatory = NO;
    objRow.strPlaceHolder = @"Driver ID";
    objRow.strRowTitle = @"Driver ID";
    objRow.strRowContent = strDriverID;
    [objSec.arrayRows addObject:objRow];
    
    [arrayOfTableData addObject:objSec];
    
    [_tblGeneral reloadData];
}
#pragma mark    - <Custom Delefates>
- (void)sendChangedRowData:(SectionRows *)objRow fortheCell:(FormCommonTableViewCell *)objCell withTextField:(UITextField *)txtActiveField{
    Section *sec = [arrayOfTableData firstObject];
    NSIndexPath *index = [_tblGeneral indexPathForCell:objCell];
    [sec.arrayRows replaceObjectAtIndex:index.row withObject:objRow];
    [_tblGeneral beginUpdates];
    txtFieldActive = txtActiveField;
    [_tblGeneral reloadRowsAtIndexPaths:[NSArray arrayWithObjects:index, nil] withRowAnimation:UITableViewRowAnimationNone];
    [_tblGeneral endUpdates];
   
//    [self setContinueButtonEnabled:YES];

}
- (void)scrollTableViewForTextField:(UITextField *)txtActiveField withCell:(FormCommonTableViewCell *)objCell{
//    NSIndexPath *index = [_tblGeneral indexPathForCell:objCell];
    txtFieldActive = txtActiveField;
}
@end
