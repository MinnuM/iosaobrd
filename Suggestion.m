//
//  Suggestion.m
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/27/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "Suggestion.h"
#import "NSString+NullCheck.h"
#import "Header.h"
@implementation Suggestion
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initializeData];
    }
    return self;
}
-(void)initializeData{
    self.operation = @"";
    self.username = @"";
    self.deviceidentifier = @"";
    self.date = @"";
    self.slot = @"";
    self.repeat = @"";
    self.status = @"";
    self.suggestedby = @"";
    self.ack = @"";
    self.start = @"";
    self.end = @"";
}
- (void)getDataFromAPI:(NSMutableDictionary *)dict{
    self.operation = [[dict valueForKey:OPERATION] NullCheck];
    self.username = [[dict valueForKey:USER_NAME] NullCheck];
    self.deviceidentifier = [[dict valueForKey:DEVICE_ID] NullCheck];
    self.date = [[dict valueForKey:RESPONSE_DATE] NullCheck];
    self.slot = [[dict valueForKey:SLOT] NullCheck];
    self.repeat = [[dict valueForKey:SUGGESTION_REPEAT] NullCheck];
    self.status = [[dict valueForKey:SUGGESTION_STATUS] NullCheck];
    self.suggestedby = [[dict valueForKey:SUGGESTION_BY] NullCheck];
    self.ack = [[dict valueForKey:ACK] NullCheck];
    self.start = [self getTimeForSlot:[self.slot intValue]];
    self.end = [self getTimeForSlot:[self.repeat intValue]];
}
-(NSString *)getTimeForSlot:(int)slot{
    NSString * time=@"";
    int hour=(slot/4);
    NSString *minutes=@"0";
    if(slot%4==1){
        minutes=@"00";
    }
    else if(slot%4==2){
        minutes=@"15";
    }
    else if(slot%4==3){
        minutes=@"30";
    }
    else if(slot%4==0){
        minutes=@"45";
        hour=hour-1;
    }
    if(hour>9){
        time=[NSString stringWithFormat:@"%d:%@",hour,minutes];
    }
    else{
        time=[NSString stringWithFormat:@"0%d:%@",hour,minutes];
    }
    
    return time;
}
@end
