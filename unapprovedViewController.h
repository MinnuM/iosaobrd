//
//  unapprovedViewController.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/25/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
#import "KeychainItemWrapper.h"
#import "AppDelegate.h"
@interface unapprovedViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *unapprovedList;

@end
