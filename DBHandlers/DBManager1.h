//
//  DBManager1.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 8/8/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <sqlite3.h>

@interface DBManager1 : NSObject{
    NSString *databasePath;
}

-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename;


@property (nonatomic, strong) NSString *documentsDirectory;
@property (nonatomic, strong) NSString *databaseFilename;
@property (nonatomic, strong) NSMutableArray *arrResults;

-(NSArray *)loadDataFromDB:(NSString *)query;
-(NSString *)getRecords:(NSString*) tablename;
-(void)executeQuery:(NSString *)query;
@property (nonatomic, strong) NSMutableArray *arrColumnNames;

@property (nonatomic) int affectedRows;

@property (nonatomic) long long lastInsertedRowID;

-(void)runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable;
-(void)copyDatabaseIntoDocumentsDirectory;


@end
