//
//  Vehicle_Defects.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/12/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

extern int vehicleNum;
extern int vehicle1;
@interface Vehicle_Defects : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>{
    
    
    UITextField *notes;
}

- (IBAction)done:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *defects_list;
@property (weak, nonatomic) IBOutlet UIScrollView *scroller_defects;
@end
