//
//  pageContentViewController.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/14/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "pageContentViewController.h"

@interface pageContentViewController ()

@end

@implementation pageContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenImage.image = [UIImage imageNamed:self.imageFile];
    self.titleLabel.text = self.titleText;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)menuScreen:(id)sender {
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"menuscreen"];
    [self presentViewController:vc animated:YES completion:nil];

}
@end
