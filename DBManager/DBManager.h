//
//  DBManager1.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 8/8/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <sqlite3.h>
#import "Carrier.h"
#import "DVIR.h"
#import "DVIR2Defects.h"
#import "DVIRDefects.h"
#import "Log.h"
#import "Log2shippingdoc.h"
#import "Log2trailors.h"
#import "Log2vehicle.h"
#import "pendingUpdate.h"
#import "Shippingdoc.h"
#import "Trailer.h"
#import "User.h"
#import "user2carrier.h"
#import "Vehicle.h"
#import "DBManager.h"
#import "preferences.h"
#import "formGeneral.h"
#import "MobileCheckIn.h"
#import "Suggestion.h"
#import "loggraph.h"
#import "rules.h"
#import "commonDataHandler.h"
#import "user2vehicle.h"
#import "approve.h"
typedef enum{
    tableCarrier,
    tableDVIR,
}tableType;
@interface DBManager: NSObject{
    NSString *databasePathNew;    

}

-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename;


@property (nonatomic, strong) NSString *documentsDirectory;
@property (nonatomic, strong) NSString *databaseFilename;
@property (nonatomic, strong) NSMutableArray *arrResults;
//@property (nonatomic) tableType *tableName;
@property (nonatomic, strong) DBManager *dbManager;
-(void)loadDataFromDB:(NSString *)query;
-(NSString *)getRecords:(NSString*) tablename;
-(void)executeQuery:(NSString *)query;
@property (nonatomic, strong) NSMutableArray *arrColumnNames;
-(NSArray*)getUserId;
-(NSArray *)getCarrierId:(NSMutableDictionary*)whereToken;
-(int)getInsertId:(NSString*)table;
@property (nonatomic) int affectedRows;

@property (nonatomic) long long lastInsertedRowID;

-(void)runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable;
-(void)copyDatabaseIntoDocumentsDirectory;
//-(void)selectStatement:(NSObject *)table;
-(void)insertIntoTable:(id)table;
-(void)updateIntoTable:(id)table whereToken:(NSMutableDictionary*)whereToken;
-(NSArray *)selectFromTable:(id)table whereToken:(NSMutableDictionary*)whereToken;
-(void)deleteFromTable:(id)table whereToken:(NSMutableDictionary*)whereToken;
-(NSArray *)getUnapprovedList:(NSMutableDictionary*)whereToken;
-(NSArray *)getlogSettings:(NSMutableDictionary*)whereToken;
-(NSArray *)getLogData:(NSMutableDictionary*)whereToken;
-(void)saveOtherForm:(id)table;
-(void)saveGeneralForm:(id)table;
-(void)saveCarrierForm:(id)table;
-(NSArray *)getLogEditData:(NSMutableDictionary*)whereToken;
-(NSArray *)getLast14Days:(int)i;
-(void)updateCred:(id)table;
-(void)mobileCheckinUpdation:(id)table;
-(void)logout:(int)userId;
-(void)insertPastStatus:(id)table;
-(void)insertLogSettings:(id)table;
-(void)updateLogSettings:(id)table;
-(NSArray *)getPreviousStatus:(id)modal;
-(NSArray*)fetchSuggestionWithwhereToken:(NSMutableDictionary*)whereToken;
-(NSArray *)getCurrentStatus:(int)userid;
-(NSMutableDictionary *)getDOTInspectionData:(NSMutableDictionary*)whereToken;
-(NSString *)findRestart:whereToken;
-(NSArray *)getVehDataPerDay:(NSMutableDictionary *)whereToken;
-(void)updateDistance:(id)table dist:(int)dist;
-(void)userToVehicle:(id)table;
-(NSString *)returnRestart:(NSMutableDictionary*) whereToken;
-(NSString *)convertDate:(NSString *)date;
-(NSMutableArray *)getApproveLog:(NSMutableDictionary *)whereToken;
-(void)saveStatus:(id)table;
-(NSArray *)getLast14DaysFromDate:(int)i date:dateNew;
-(void)removeLoggedIn;
@end

