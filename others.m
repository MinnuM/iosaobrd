//
//  others.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/1/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "others.h"
int count_edit=0;
 NSString *codriver;
 NSString *origin_1;
 NSString *dest;
 NSString *notes_1;
UITextField *activeField1;
@interface others ()

@end

@implementation others
@synthesize donebtn;
@synthesize co_driver;
@synthesize origin;
@synthesize destination;
@synthesize notes;
@synthesize scroller03;
@synthesize dbManager;
- (void)viewDidLoad {
    [super viewDidLoad];
     dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
    //minnu
    [scroller03 setScrollEnabled:YES];
    [scroller03 setContentSize:(CGSizeMake(600, 1000))];
    UITapGestureRecognizer *backgroundTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backgroundTap)];
    [self.view addGestureRecognizer:backgroundTapped];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    //minnu
    
    //donebtn.hidden=YES;
    co_driver.delegate=self;
    origin.delegate=self;
    destination.delegate=self;
    notes.delegate=self;
    if ([other count]!=0) {
        if([other[0] isEqualToString:@"<null>"]){
            
            co_driver.text = @"";
        }else{
        co_driver.text=other[0];
        }
        
        if([other[1] isEqualToString:@"<null>"]){
            
            origin.text = @"";
        }else{
            origin.text=other[1];
        }
        
        if([other[2] isEqualToString:@"<null>"]){
            
            destination.text = @"";
        }else{
             destination.text=other[2];
        }
        if([other[3] isEqualToString:@"<null>"]){
            
            notes.text = @"";
        }else{
           notes.text=other[3];
        }
        
        
    }else{
        
        co_driver.text=@"";
        origin.text=@"";
        destination.text=@"";
        notes.text=@"";
        
        
    }
    // Do any additional setup after loading the view.
}

//minnu
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    //scroller03.contentInset = contentInsets;
    scroller03.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect orginFrame=[self.origin frame];
    CGRect destinationFrame=[self.destination frame];
    CGRect co_driverFrame=[self.co_driver frame];
    CGRect notesFrame=[self.notes frame];
    CGRect textfieldFrame;
    CGRect aRect = self.view.frame;
    //BFLog(@"MainScreen ---(%f,%f)",aRect.size.height,kbSize.height);
   aRect.size.height -= kbSize.height+30;
    if([self.origin isEditing])
    {
        textfieldFrame=orginFrame;
    }
    else if([self.destination isEditing])
    {
        textfieldFrame=destinationFrame;
    }
    else if([self.notes isEditing])
    {
        textfieldFrame=notesFrame;
    }
    else
    {
        textfieldFrame=co_driverFrame;
    }
   // BFLog(@"Carrier-scrollpoint--(%f,%f)",textfieldFrame.origin.x,textfieldFrame.origin.y);
    CGRect bgrndRect = [self view].frame;
    bgrndRect.size.height -=kbSize.height;
   
    if (bgrndRect.origin.y+bgrndRect.size.height<activeField1.frame.origin.y) {
        
        CGPoint scrollPoint = CGPointMake( 0.0,kbSize.height+activeField1.frame.size.height-30);
        
        [scroller03 setContentOffset:scrollPoint animated:YES];
    }


}


// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scroller03.contentInset = contentInsets;
    scroller03.scrollIndicatorInsets = contentInsets;
    [scroller03 setContentOffset:CGPointZero animated:YES];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
   // BFLog(@"entered return key");
    return YES;
    
}

- (void)backgroundTap {
    [self.view endEditing:YES];
  //  BFLog(@"Touched Inside");
}
//minnu

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    activeField1=textField;
    if (count_edit==0) {
       donebtn.hidden=NO;
        count_edit=1;
        others_id=1;
    }
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    activeField1=nil;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)doneEdit:(id)sender {
    //getting authCode
    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    NSString *authCode = @"";
    if([authData count]!=0){
        authCode = authData[0][1];
        
    }
    //---
    codriver=co_driver.text;
    origin_1=origin.text;
    dest=destination.text;
    notes_1=notes.text;
    DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    
    NSDictionary *token;
    ;
    NSMutableDictionary *whereToken;
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
        token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
        
    Log *objLog=[Log new];
    objLog.codriver=co_driver.text;
    objLog.destination=destination.text;
    objLog.orgin=origin.text;
    objLog.date=dateValue;
    objLog.notes=notes.text;
    objLog.userid=[NSString stringWithFormat:@"%d",userId];
    [objDB saveOtherForm:objLog];
    }
    if(objDB.affectedRows!=0){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Updated Successfully!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else{
        //BFLog(@"Could not execute the query");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ooops" message:@"Something went wrong!! Please try again later!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    /*NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/saveEditHosFormOther.php?authCode=%@",authCode];
    NSURL *url = [NSURL URLWithString:urlString];
    //NSData *data = [NSData dataWithContentsOfURL:url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSURLResponse *response;
    NSError *error;
    NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    if(httpResponse!=nil){
        if ([httpResponse statusCode] ==200 ) {
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:60.0];
            
            [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
            
            [request setHTTPMethod:@"POST"];
            
            NSMutableDictionary *general_data = [[NSMutableDictionary alloc] init];
            [general_data setValue:co_driver.text forKey:@"coDriver"];
            [general_data setValue:origin.text forKey:@"origin"];
            [general_data setValue:destination.text forKey:@"destination"];
            [general_data setValue:notes.text forKey:@"notes"];
            
            /* NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
             @"IOS TYPE", @"typemap",
             nil];*/
/*raghee            NSData *postData = [NSJSONSerialization dataWithJSONObject:general_data options:0 error:&error];
            [request setHTTPBody:postData];
            
            
            NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error1) {
                NSString *status = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                //  BFLog(@"error:%@,%@",error,response);
                if(error1==nil){
                    
                    UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                                     message:@"Data Saved Successfully!!!"
                                                                    delegate:self
                                                           cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil, nil];
                    
                    [alert2 show];
                    
                    
                    
                }
                
                
                
                
            }];
            ;
            [postDataTask resume];
        }else  if ([httpResponse statusCode] ==503 ){
            
            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Service Unavailable"
                                                             message:@"Please check your internet connectivity!!!"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
            
            [alert1 show];
            
            
        }else{
            
            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                             message:@"Internal Issue.Please Contact the Admin "
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
            
            [alert1 show];
            
            
        }
    }else{
        if(error.code==-1001){
            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Request Timed Out"]
                                                             message:@"Please check your internet connectivity!!!"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
            
            [alert1 show];
            
            
        }
    }
    


    
    
    NSString *query3 = [NSString stringWithFormat:@"select * from OtherSettings"];
    
    // Get the results.
    
    NSArray *values2 = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query3]];
    
    if ([values2 count]!=0) {
        
        
        
        NSString *query_update = [NSString stringWithFormat:@"UPDATE OtherSettings SET codriver='%@',origin='%@',destination='%@',notes='%@'",codriver,origin_1,dest,notes_1];
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_update];
        
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
            BFLog(@"Table updated successfully. Affected rows = %d", dbManager.affectedRows);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Updated Successfully!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
           // [alert show];

            
        }
        else{
            BFLog(@"Could not execute the query");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ooops" message:@"Something went wrong.Please try again later!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
           // [alert show];

        }}
    
    else{
        
        NSString *query_insert1 = [NSString stringWithFormat:@"insert into OtherSettings values(null, '%@', '%@', '%@','%@')",codriver,origin_1,dest,notes_1];
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_insert1];
        
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
            BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Updated Successfully!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
           // [alert show];

            //c_flag=1;
        }
        else{
            BFLog(@"Could not execute the query");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ooops" message:@"Something went wrong.Please try again later!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //[alert show];
            
        }
        
        
        
    }
*/
    
}
@end
