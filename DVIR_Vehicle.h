//
//  DVIR_Vehicle.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/12/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DVIRMenu.h"
#import "AppDelegate.h"



@interface DVIR_Vehicle : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>{
    
    UITextField *vehicle_num;
    UITextField *trailer_num;
    UITextField *vehicleMiles;
    UITextField *vehicleHours;
    
    
}
@property (weak, nonatomic) IBOutlet UIScrollView *scroller_vehicles;
@property (weak, nonatomic) IBOutlet UITableView *defects_tables;

@end
