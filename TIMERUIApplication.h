//
//  TIMERUIApplication.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 4/6/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kApplicationTimeoutInMinutes 5
#define kApplicationDidTimeoutNotification @"AppTimeOut"

@interface TIMERUIApplication : UIApplication
{
    
    NSTimer *myidleTimer;
    
}

-(void)resetIdleTimer;

@end
