//
//  menuScreenViewController.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/25/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "menuScreenViewController.h"
#import "LogSettings.h"
#import "loginViewController.h"
#import "logScreenViewController.h"
#import "KeychainItemWrapper.h"
#import "insertDutyCycle.h"
#import "MBProgressHUD.h"
#import "NotificationHandler.h"
#import "StatusViewController.h"
#import "commonDataHandler.h"
#import "Header.h"
@interface menuScreenViewController ()
{
    UIAlertView *alert;
}
@end

@implementation menuScreenViewController{

   // NSArray *menu;
    NSArray *menu_images;

}
NSTimer *fiveSecTimer;
NSDictionary *cargo1;
NSMutableArray *locationsMenu;
NSMutableArray *notesMenu;
NSMutableArray *dataMenu;
NSMutableArray *deviceData;
NSMutableArray *unapprovedDates;
NSDictionary *json_menuscreen;
NSMutableDictionary *carrier1;
NSDictionary *cycle1;
NSDictionary *restart1;
NSDictionary *breakdetails1;
NSDictionary *driver1;
NSDictionary *logs1;
NSArray *labels;
NSString *urlString;
NSString *cycleinfo=@"USA 60 hours/7 days";
NSString *cargoinfo=@"Property";
NSString *restartinfo=0;
NSString *master_menu;
UIAlertView *alertPop;
NSString *todaysDate;
NSString *user_menu;
NSString *sourceVC=@"";
dispatch_queue_t queue1;
dispatch_group_t group1;
MBProgressHUD *hud1;
@synthesize menu;
@synthesize dbManager;

-(void)viewWillAppear:(BOOL)animated{
    [self getDeviceData];
    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    NSString *authCode = @"";
    if([authData count]!=0){
        authCode = authData[0][1];
        
    }
    
    logScreenViewController *obj1 = [logScreenViewController new];
    [obj1 callChanges:authCode];
    if([commonDataHandler sharedInstance].strUserName.length <= 0 || [commonDataHandler sharedInstance].strAuthCode.length <= 0){
        DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
        NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
        NSArray *detailsArray = [userInfoArray firstObject];
        [commonDataHandler sharedInstance].strUserId = detailsArray[0];
        [commonDataHandler sharedInstance].strUserName = detailsArray[1];
        if([commonDataHandler sharedInstance].strAuthCode.length <= 0)
          [commonDataHandler sharedInstance].strAuthCode = detailsArray[[detailsArray count]-2];
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DataUpdated" object:nil];


    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUpdatedData:)
                                                 name:@"DataUpdated"
                                               object:nil];
    
}
-(void)dismissAlert:(UIAlertView *)alert{
    [alert dismissWithClickedButtonIndex:-1 animated:YES];
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    autoLoginCount=0;
//    if([commonDataHandler sharedInstance].strUserName.length <= 0 || [[commonDataHandler sharedInstance].strUserName isEqualToString:@"null"]){
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:SELECTED_VEHICLE];
//
//        NSUserDefaults *userdata = [NSUserDefaults standardUserDefaults];
//        NSString *previous_user = [userdata objectForKey:@"username"];
//        [userdata setValue:previous_user forKey:@"previousUser"];
//        [userdata removeObjectForKey:@"username"];
//        [userdata removeObjectForKey:@"password"];
//        [userdata removeObjectForKey:@"masteruser"];
//        [userdata removeObjectForKey:@"user"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        [credentials resetKeychainItem];
//
//        BFLog(@"username n password deleted");
//        DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
//        NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
//        int userId = [userInfoArray[0][0] intValue];;
//        [objDB logout:userId];
//         [self dismissViewControllerAnimated:YES completion:^{
//          }];
//        UIStoryboard *storyBoard = self.storyboard;
//        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"main1"];
//        [self presentViewController:vc animated:YES completion:nil];
//    }
    if (popup==0) {
        popup=1;
        DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
        NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
        NSArray *detailsArray = [userInfoArray firstObject];
        if(detailsArray.count){
            [commonDataHandler sharedInstance].strUserId = detailsArray[0];
           // if([commonDataHandler sharedInstance].strUserName.length <= 0)
                [commonDataHandler sharedInstance].strUserName = detailsArray[1];
            //if([commonDataHandler sharedInstance].strAuthCode.length <= 0)
                [commonDataHandler sharedInstance].strAuthCode = detailsArray[[detailsArray count]-2];
        }
        
        user_menu=[[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
        alertPop=[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"Hi %@",[commonDataHandler sharedInstance].strUserName] message:@"Welcome back." delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
        [alertPop show];
        [self performSelector:@selector(dismissAlert:) withObject:alertPop afterDelay:3.0f];
        
    }
    hud1 = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud1.label.text = @"Please wait a moment";
    queue1 = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    group1 = dispatch_group_create();
    
    LogSettings *obj = [LogSettings new];
    restartinfo=[NSString stringWithFormat:@"%f",[obj calculatePosx:@"34:00"]];
    [self getLast7Days];
    
    dataMenu=[[NSMutableArray alloc]init];
   // carrier1=[[NSMutableDictionary alloc]init];
    NSDate *now = [NSDate date];
    NSDateFormatter *outputFormatter_log = [[NSDateFormatter alloc] init];
    [outputFormatter_log setDateFormat:@"MM/dd/YYYY"];
    todaysDate = [outputFormatter_log stringFromDate:now];
    // [self timedTask];
    master_menu=[[NSUserDefaults standardUserDefaults] objectForKey:@"masteruser"];
    user_menu=[[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
    dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
   /* NSString *query_del3 =[NSString stringWithFormat:@"delete from OtherSettings;"];
    [dbManager executeQuery:query_del3];
    
    NSString *query_del4 =[NSString stringWithFormat:@"delete from GeneralSettings;"];
    [dbManager executeQuery:query_del4];*/
   
    NSString *query_dates =[NSString stringWithFormat:@"select * from FinalTable where server_sync='%d';",0];
    // Get the results.
    dispatch_group_async(group1, queue1, ^{

    NSArray *sync_data = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query_dates]];
    BFLog(@"Sync the data:%@",sync_data);
    if ([sync_data count]!=0) {
        for (int i=0; i<[sync_data count]; i++) {

            if([sync_data[i] count]==6){

            if([sync_data[i][5] integerValue]==1){
            NSData *data2= [sync_data[i][2] dataUsingEncoding:NSUTF8StringEncoding];
            NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
            
            [self saveCurrent:json_log1 date:sync_data[i][1]];
            }

        }
        }
    }
     });

            menu.delegate=self;
    menu.dataSource=self;
    dispatch_group_async(group1, queue1, ^{

         [self loadSettings];
     });
     dispatch_group_notify(group1, queue1, ^{
         dispatch_async(dispatch_get_main_queue(), ^{
             [hud1 hideAnimated:YES];
         });
     });
   // [self.menu setContentInset :UIEdgeInsetsMake(0,0,616,0)];
    
   // menu.layer.borderWidth=1.5;
   // menu.layer.borderColor = [[UIColor grayColor] CGColor];
    labels = @[@[@"Logs",@"DOT Inspection"],@[@"Dashboard",@"Documents"],@[@"Alerts",@"Settings"],@[@"Unapproved Dates",@"Help"]];
    refresh_flag=1;
    
   
    
    menu_images = @[@[@"icons/logs_3.png",@"icons/inspect.jpg"],@[@"icons/recap_2 2.png",@"icons/docs_3.png"],@[@"icons/alert1.png",@"icons/sett_3.jpg"],@[@"icons/unapproved.png",@"icons/help_3.png"]];
    
    KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
    NSString *username1 = [credentials objectForKey:(id)kSecAttrAccount];
    NSData *password1 = [credentials objectForKey:(id)kSecValueData];
    NSString *pass = [[NSString alloc] initWithData:password1 encoding:NSUTF8StringEncoding];
  // NSString *username1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    //NSString *password1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
   urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/EnableLogin_hos.php?username=%@&password=%@",username1,pass];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSData *data = [NSData dataWithContentsOfURL:url];
   /* if (data) {
        BFLog(@"Device connected to the internet");
        NSURLSession *session = [NSURLSession sharedSession];
               NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:urlString] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            //  BFLog(@"Retrieved Data:%@,%@",data,response);
            BFLog(@"---------------------------------------");
            BFLog(@"error:%@",error);
            if(data!=nil){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                [[NSUserDefaults standardUserDefaults] setObject:json[@"masterUser"] forKey:@"masteruser"];
                [[NSUserDefaults standardUserDefaults] setObject:json[@"user"] forKey:@"user"];
                [self loadSettings];
                BFLog(@"%@",json);
                BFLog(@"%@",json[@"url"]);
            }
            //if (error == nil) {
            //NSString *text = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
           
            //}
            
        }];
        [dataTask resume];
        

    }else{
        BFLog(@"Device not connected to the internet");
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                         message:@"Please check your internet connectivity!!!"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:@"OK", nil];
        
        [alert1 show];

        
    }

*/
   
}
-(void)dismissModalStack {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIViewController *vc = self.presentingViewController;
        while (vc.presentingViewController) {
            vc = vc.presentingViewController;
        }
        [vc dismissViewControllerAnimated:YES completion:^{
            
        }];
        
        [self dismissViewControllerAnimated:YES completion:^{
            UIStoryboard *storyBoard = self.storyboard;
            UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"main1"];
            [self presentViewController:vc animated:YES completion:nil];
        }];
    });
    
    
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidAppear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)handleUpdatedData:(NSNotification *)notification {
    NSDictionary* userInfo = notification.userInfo;
    driver_status drvStat;
    int driverStat;
    driverStat = [[userInfo valueForKey:@"driverStatus"] intValue];
    if(driverStat == 1)
        drvStat = driver_status_driving;
    else if (driverStat == 2)
        drvStat = driver_status_onDuty;
    else if (driverStat == 3)
        drvStat = driver_status_offDuty;
    else if (driverStat == 4)
        drvStat = driver_status_none;
    else if (driverStat == 6)
        drvStat = driver_status_onDuty_immediate;
    else
        drvStat = driver_status_connect_back;
//    if(driverStat == 2){
//        dispatch_async(dispatch_get_main_queue(), ^{
//            alert = [[UIAlertView alloc]initWithTitle:@"Status Change" message:@"Do you want to continue driving or change your duty status" delegate:self cancelButtonTitle:@"Continue Driving" otherButtonTitles:@"Change Status", nil];
//            alert.tag=1005;
//            alert.delegate=self;
//            [alert show];
//            [self performSelector:@selector(dismissAlertView) withObject:nil afterDelay:60.0];
//        });
//    }else{
        NotificationHandler *objHelper = [NotificationHandler new];
        [objHelper recievePushNotification:self withDriverStatus:drvStat];
  //  }
}
//- (void)dismissAlertView {
//    NotificationHandler *objHelper = [NotificationHandler new];
//    [objHelper recievePushNotification:self withDriverStatus:driver_status_onDuty_immediate];
//    [alert dismissWithClickedButtonIndex:-1 animated:YES];
//}
-(void)getLast7Days{
    NSDate *today = [NSDate date];
    for(int i=1;i<8;i++){
    
   
    
    NSDate *previous = [today dateByAddingTimeInterval:-i*24*60*60];
    NSDateFormatter *outputFormatter1 = [[NSDateFormatter alloc] init];
    [outputFormatter1 setDateFormat:@"MM/dd/yyyy"];
    NSString *previousdate = [outputFormatter1 stringFromDate:previous];
        
        NSString *query_check =[NSString stringWithFormat:@"select * from TempTable where date='%@';",previousdate];
        
        // Get the results.
        
        NSArray *date_exist = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query_check]];
        
        if([date_exist count]==0){
            NSMutableDictionary *main = [[NSMutableDictionary alloc] init];
            NSMutableArray *data1 = [[NSMutableArray alloc]init];
            NSMutableDictionary *data0 = [[NSMutableDictionary alloc] init];
            [data0 setValue:@"0" forKey:@"id"];
            [data0 setValue:@"40" forKey:@"posx"];
            [data0 setValue:@"65" forKey:@"posy"];
            NSMutableDictionary *data2 = [[NSMutableDictionary alloc] init];
            [data2 setValue:@"1" forKey:@"id"];
            [data2 setValue:@"1216" forKey:@"posx"];
            [data2 setValue:@"65" forKey:@"posy"];
            [data1 addObject:data0];
            [data1 addObject:data2];
            [main setValue:data1 forKey:@"payload"];
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            formatter.dateFormat=@"MM/dd/yyyy";
            [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
            NSDate *date1=[formatter dateFromString:previousdate];
            NSTimeInterval interval=[date1 timeIntervalSince1970]*1000;
            BFLog(@"Timeinterval:%f",interval);
            
            BFLog(@"Main:%@",main);
            NSData *data = [NSJSONSerialization dataWithJSONObject:main options:NSJSONWritingPrettyPrinted error:nil];
            NSString *jsonStr1 = [[NSString alloc] initWithData:data
                                                       encoding:NSUTF8StringEncoding];
            NSString *query_insert1 = [NSString stringWithFormat:@"insert into TempTable values(null, '%@', '%@', 'OFF' ,'%d','%d','%f')", previousdate,jsonStr1,0,1,interval];
            
            [dbManager executeQuery:query_insert1];
            
            // If the query was successfully executed then pop the view controller.
            if (dbManager.affectedRows != 0) {
                BFLog(@"Query was executed successfully for empty data. Affected rows = %d", dbManager.affectedRows);
                
                // Pop the view controller.
                //[self.navigationController popViewControllerAnimated:YES];
            }
            else{
                BFLog(@"Could not execute the query");
            }
            
        }
    
        
        
    }
    
}
-(void)getDeviceData{
    //getting authCode
    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    NSString *authCode = @"";
    if([authData count]!=0){
        authCode = authData[0][1];
        
    }
    //---
    NSString *path = [[NSUserDefaults standardUserDefaults] valueForKey:@"path"];
    NSString *urlString1 = [NSString stringWithFormat:@"%@/hosapp/getDeviceData_app.php?master=%@&user=%@&date=%@&authCode=%@",path,master_menu,user_menu,todaysDate,authCode];
    NSURL *url = [NSURL URLWithString:urlString1];
    NSData *data = [NSData dataWithContentsOfURL:url];
    
    if (data) {
        NSURLSession *session1 = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask1 = [session1 dataTaskWithURL:[NSURL URLWithString:urlString1] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if(data!=nil){
            deviceData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            BFLog(@"Data From File:%@",deviceData);
            }
            
            
        }];
        [dataTask1 resume];
    }
    
    
}

-(void)loadSettings{
    NSString *master = [[NSUserDefaults standardUserDefaults] objectForKey:@"masteruser"];
    NSString *USer = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
    //getting authCode
    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    NSString *authCode = @"";
    if([authData count]!=0){
        authCode = authData[0][1];
        
    }
    //---------------------------
    NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp1/getHosSettings.php?masterUser=%@&user=%@&authCode=%@",master,USer,authCode];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSURLResponse *response;
    NSError *error1;
    NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error1];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    if(httpResponse!=nil){
        if ([httpResponse statusCode] ==200 ) {
            
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:urlString] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        //  BFLog(@"Retrieved Data:%@,%@",data,response);
        BFLog(@"---------------------------------------");
        BFLog(@"error:%@",error);
       // if (error.code) {
            
        if(data){
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        if(json!=nil && [json count]!=0){
       
        cargo1=json[@"Cargo"];
        carrier1=[json[@"Carrier"] mutableCopy];
        cycle1=json[@"Cycle"];
        restart1=json[@"Restart"];
        breakdetails1=json[@"Break"];
        driver1=json[@"Driver"];
        logs1=json[@"Logs"];
        BFLog(@"cargo:%@",cargo1);
        BFLog(@"carrier1:%@",carrier1);
        
        BFLog(@"cycle1:%@",cycle1);
        BFLog(@"restart1:%@",restart1);
        BFLog(@"breakdetails1:%@",breakdetails1);
        BFLog(@"driver1:%@",driver1);
        BFLog(@"logs1:%@",logs1);
       
        if([cycle1[@"CycleType"] isEqualToString:@"USA70hours"]){
            
           
            cycleinfo=@"USA 70 hours/8 days";
            
        }else if([cycle1[@"CycleType"] isEqualToString:@"USA60hours"]){
           
            cycleinfo=@"USA 60 hours/7 days";
            
            
            
        }else if([cycle1[@"CycleType"] isEqualToString:@"Texas70hours"]){
           
            cycleinfo=@"Texas 70 hours/7 days";
            
            
            
        }else if([cycle1[@"CycleType"] isEqualToString:@"California80hours"]){
         
            cycleinfo=@"California 80 hours/8 days";
            
            
        }else if([cycle1[@"CycleType"] isEqualToString:@"Others"]){
          
            cycleinfo=@"Others";
            
            
        }
        
        if ([cargo1[@"CargoType"] isEqualToString:@"PropertyCarrying"]) {
           
            cargoinfo=@"Property";
            
        }else if ([cargo1[@"CargoType"] isEqualToString:@"PassengerCarrying"]){
         
            cargoinfo=@"Passenger";
            
        }else if ([cargo1[@"CargoType"] isEqualToString:@"OilandGas"]){
           
            cargoinfo=@"Oil and Gas";
        }
              LogSettings *obj = [LogSettings new];
            restartinfo = [NSString stringWithFormat:@"%f", [obj calculatePosx:[NSString stringWithFormat:@"%@:00",restart1[@"RestartType"]]]]; ;
                NSString *query_del5 =[NSString stringWithFormat:@"delete from Carrier_Sett;"];
                [dbManager executeQuery:query_del5];
                
                NSString *query_del7 =[NSString stringWithFormat:@"delete from AccountSettings;"];
                [dbManager executeQuery:query_del7];
                NSString *query_del8 =[NSString stringWithFormat:@"delete from LogSettings;"];
                [dbManager executeQuery:query_del8];       
        
        NSString *query_insert1 = [NSString stringWithFormat:@"insert into LogSettings values(null, '%@', '%@', '%@','%@','%@')",logs1[@"Timezone"],cycle1[@"CycleType"],cargo1[@"CargoType"],@"",restart1[@"RestartType"]];
            
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_insert1];
        
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
            BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
            //c_flag=1;
        }
        else{
            BFLog(@"Could not execute the query");
        }
        NSString *query_insert2 = [NSString stringWithFormat:@"insert into AccountSettings values(null, '%@','%@', '%@','%@','%@','%@')",driver1[@"FirstName" ],driver1[@"LastName"],driver1[@"LicenceNumber"],driver1[@"Phone"],driver1[@"Email"],driver1[@"dotNumber"]];
            
            //minnu insert User details
            DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
            User *objUser=[User new];
         //   KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
        /*    objUser.username=[credentials objectForKey:(id)kSecAttrAccount];
            NSLog(@"Credentials: %@",[credentials objectForKey:(id)kSecAttrAccount]);
            NSData *password1 = [credentials objectForKey:(id)kSecValueData];
            objUser.password=[[NSString alloc] initWithData:password1 encoding:NSUTF8StringEncoding];
            if([objUser.username isEqualToString:@""] )objUser.username=@"ragtesthos1";
            if([objUser.password isEqualToString:@""] )objUser.password=@"123456";
            //[credentials objectForKey:(id)kSecValueData];
            objUser.firstname=driver1[@"FirstName" ];
            objUser.lastname=driver1[@"LastName" ];
            objUser.licenseNo=driver1[@"LicenceNumber"];
            objUser.mobile=driver1[@"Phone"];
            objUser.email=driver1[@"Email"];
            objUser.homeTimezone=@"1";
            objUser.cycle=@"1";
            [objDB insertIntoTable:objUser];
            NSString *username = [credentials objectForKey:(id)kSecAttrAccount];
            if ([username isEqualToString:@""])username=@"ragtesthos1";
            NSDictionary *token=@{@"columnName":@"username",@"entry":username};
            ;
            NSMutableDictionary *whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
            NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId:whereToken]];
            if([userInfoArray count]!=0){
                int userId = [userInfoArray[0][0] intValue];
            token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
            ;
            whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
            user2carrier *objUser2carrier=[user2carrier new];
            NSArray *values = [[NSArray alloc] initWithArray:[objDB selectFromTable:objUser2carrier whereToken:whereToken]];
            if([values count]==0){
                Carrier *objCarrier=[Carrier new];
                objCarrier.name=carrier1[@"Carrier"];
                objCarrier.mainAddress=carrier1[@"offAddressline1"];
                objCarrier.mainZip=carrier1[@"offZip"];
                objCarrier.mainCity=carrier1[@"offAddressline3"];
                objCarrier.mainState=carrier1[@"offAddressline2"];
                objCarrier.homeZip=carrier1[@"homeZip"];
                objCarrier.homeCity=carrier1[@"homeAddressline3"];
                objCarrier.homeState=carrier1[@"homeAddressline2"];
                objCarrier.homeAddress=carrier1[@"homeAddressline1"];
                
                [objDB  insertIntoTable:objCarrier];
                NSInteger *carrierId= [objDB getInsertId:@"Carrier"];
                user2carrier *objUser2carrier=[user2carrier new];
                objUser2carrier.userid=[NSString stringWithFormat:@"%@",userId];
                objUser2carrier.carrierid=[NSString stringWithFormat:@"%@",carrierId];
                [objDB  insertIntoTable:objUser2carrier];
                
                
                
            }
            }*/
            //minnu
            
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_insert2];
        
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
            BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
            //c_flag=1;
        }
        else{
            BFLog(@"Could not execute the query");
        }
        
        NSString *query_insert3 = [NSString stringWithFormat:@"insert into Carrier_Sett values(null, '%@', '%@', '%@')", carrier1[@"Carrier"],[NSString stringWithFormat:@"%@,%@,%@,%@", carrier1[@"offAddressline1"],carrier1[@"offAddressline2"],carrier1[@"offAddressline3"],carrier1[@"offZip"]],[NSString stringWithFormat:@"%@,%@,%@,%@",carrier1[@"homeAddressline1"],carrier1[@"homeAddressline2"],carrier1[@"homeAddressline3"],carrier1[@"homeZip"]] ];
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_insert3];
        
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
            BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
            //c_flag=1;
        }
        else{
            BFLog(@"Could not execute the query");
        }
        }
        }else{
            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                             message:[NSString stringWithFormat:@"%ld",error.code]
                                                            delegate:self
                                                   cancelButtonTitle:@"Cancel"
                                                   otherButtonTitles:@"OK", nil];
    
            [alert1 show];
            
        }

    }];
    [dataTask resume];
    }else if([httpResponse statusCode]==503){
        BFLog(@"Device not connected to the internet");
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Service Unavailable"
                                                         message:@"Please check your internet connectivity!!!"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:@"OK", nil];
        
        [alert1 show];
        
        
    }
    }
    
    
    
    
}
-(void)timedTask{
    
    fiveSecTimer = [NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(updatingData) userInfo:nil repeats:YES];
    
}
-(void)updatingData{
    
    BFLog(@"Refreshing..............");
    //dispatch_group_t group = dispatch_group_create();
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
      ///  dispatch_async(dispatch_get_main_queue(), ^{
            
        
        
        //getting authCode
        NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
        NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
        NSString *authCode = @"";
        if([authData count]!=0){
            authCode = authData[0][1];
            
        }
        //---------------------------
        NSString *urlString1 = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/getHosDataPerDate.php?masterUser=%@&user=%@&date=%@&authCode=%@",masterUser,user,todaysDate,authCode];
        NSURL *url = [NSURL URLWithString:urlString1];
        NSData *data = [NSData dataWithContentsOfURL:url];
        
        if (data) {
            NSURLSession *session1 = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask1 = [session1 dataTaskWithURL:[NSURL URLWithString:urlString1] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                json_menuscreen= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                BFLog(@"Data From File:%@",json_menuscreen);
                if ([json_menuscreen count]==0) {
                    // NSMutableDictionary *data_empty = [[NSMutableDictionary alloc] init];
                    NSDate * now1 = [NSDate date];
                    NSDateFormatter *outputFormatter_log1 = [[NSDateFormatter alloc] init];
                    [outputFormatter_log1 setDateFormat:@"HH:mm"];
                    NSString *today_log1 = [outputFormatter_log1 stringFromDate:now1];
                    NSArray *sendpart = [today_log1 componentsSeparatedByString:@":"];
                    int secd=(int)[sendpart[1] integerValue];
                    NSString *secnd_part;
                    if (secd <15) {
                        secnd_part=@"00";
                    }
                    else if(secd >=15 && secd <30){
                        secnd_part=@"15";
                    }else if(secd >=30 && secd <45){
                        secnd_part=@"30";
                    }else if(secd >=45 && secd <60){
                        secnd_part=@"45";
                    }
                    today_log1 = [NSString stringWithFormat:@"%@:%@",sendpart[0],secnd_part];
                    NSArray *initial = @[@"00:00",current_status];
                    [dataMenu addObject:initial];
                    NSArray *con1 = @[today_log1,current_status];
                    [dataMenu addObject:con1];
                    [self saveToDB:dataMenu];
                    
                    BFLog(@"Final array for loading:%@",dataMenu);
                    
                   
                }else{
                    
                    
                    NSMutableArray *dict = [json_menuscreen objectForKey:@"payload"];
                    locationsMenu = [json_menuscreen objectForKey:@"location"];
                    notesMenu = [json_menuscreen objectForKey:@"notes"];
                    BFLog(@"Location array:%@",locationsMenu);
                    int json_size = (int)[dict count];
                    BFLog(@"inner json count:%d",json_size);
                    [dataMenu removeAllObjects];
                    for (int i=0; i<json_size; i++) {
                        
                        BFLog(@"posx:%@,%d",[dict[0] objectForKey:@"posx"],i);
                        float posx=[[dict[i] objectForKey:@"posx"] floatValue];
                        float x1 = posx - 40.0;
                        int x2 = x1/12.25;
                        int x3 = x2/4.0;
                        int x4 = x2%4;
                        
                        NSString *minutes;
                        if (15*x4 <15) {
                            minutes=@"00";
                        }
                        else if(15*x4 >=15 && 15*x4 <30){
                            minutes=@"15";
                        }else if(15*x4 >=30 && 15*x4 <45){
                            minutes=@"30";
                        }else if(15*x4 >=45 && 15*x4 <60){
                            minutes=@"45";
                        }
                        NSString *posx_1 = [NSString stringWithFormat:@"%d:%@",x3,minutes];
                        
                        // float time = [posx_1 floatValue];
                        int event_no =(int)[[dict[i] objectForKey:@"posy"] integerValue];
                        NSString * event;
                        if (event_no == 245) {
                            event=@"ON";
                        }
                        else if(event_no == 125){
                            event=@"SB";
                            
                        }else if (event_no == 185){
                            event = @"D";
                        }else if (event_no == 65){
                            event = @"OFF";
                        }
                        NSArray *final_data = [[NSArray alloc] initWithObjects:posx_1,event, nil];
                        [dataMenu addObject:final_data];
                        
                        [self saveToDB:dataMenu];
                        
                        
                        
                    }
                    
                }
                    
            }];
            [dataTask1 resume];
        }
       }); 
        
    //});

}

-(void)saveToDB:(NSMutableArray *)array{
       // if ([location_array count]==0) {
    
    //  [location_array addObject:dict1];
    //}else{
    
    
    //dataarray = [dataMenu mutableCopy];
    //}
    NSMutableArray *outer_array = [[NSMutableArray alloc] init];
    NSMutableDictionary *data_dict = [[NSMutableDictionary alloc] init];
    
    for (int i=0; i<[array count]; i++) {
        
        NSString *x12=array[i][0];
        NSArray *time = [x12 componentsSeparatedByString:@":"];
        int x4 =(int) [time[1] integerValue]/15;
        int x2 =(int) [time[0] integerValue]*4+x4;
        float x1 = 12.25*x2;
        float posx = x1+40.0;
        int posy = 65;
        BFLog(@"posx:%.2f",posx);
        
        if ([array[i][1] isEqualToString:@"OFF"]) {
            posy = 65;
        }else if([array[i][1] isEqualToString:@"ON"]){
            posy = 245;
        }else if([array[i][1] isEqualToString:@"D"]){
            posy=185;
        }else if ([array[i][1] isEqualToString:@"SB"]){
            posy=125;
        }
        
        NSMutableDictionary *cont1 = [[NSMutableDictionary alloc] init];
        [cont1 setValue:@(i) forKey:@"id"];
        [cont1 setValue:@(posx) forKey:@"posx"];
        [cont1 setValue:@(posy) forKey:@"posy"];
        
        [outer_array addObject:cont1];
        
        
        
    }
    
    [data_dict setObject:outer_array forKey:@"payload"];
    if (locationsMenu!=nil) {
        [data_dict setObject:locationsMenu forKey:@"location"];
    }
    if (notesMenu!=nil) {
        [data_dict setObject:notesMenu forKey:@"notes"];
        
    }
    
    
    BFLog(@"dictionary:%@",data_dict);
    NSData *data = [NSJSONSerialization dataWithJSONObject:data_dict options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonStr = [[NSString alloc] initWithData:data
                                              encoding:NSUTF8StringEncoding];
    BFLog(@"JSON:%@",jsonStr);
    NSString *query1 = [NSString stringWithFormat:@"select * from TempTable where date='%@'",selected_date];
    
    // Get the results.
    
    NSArray *values = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query1]];
    
    if ([values count]!=0) {
        
        
        
        NSString *query_update = [NSString stringWithFormat:@"UPDATE TempTable SET data='%@',last_status='%@' WHERE date='%@'",jsonStr,current_status,selected_date];
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_update];
        
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
            BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
            
        }
        else{
            BFLog(@"Could not execute the query");
        }}
    
    else{
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat=@"MM/dd/yyyy";
        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        NSDate *date1=[formatter dateFromString:selected_date];
        NSTimeInterval interval=[date1 timeIntervalSince1970]*1000;
        BFLog(@"Timeinterval:%f",interval);
        
        NSString *query_insert1 = [NSString stringWithFormat:@"insert into TempTable values(null, '%@', '%@', '%@' ,'%d','%d','%f')", selected_date,jsonStr,current_status,0,0,interval];
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_insert1];
        
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
            BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
            
        }
        else{
            BFLog(@"Could not execute the query");
        }
        
        
        
    }
    
}





-(void)saveCurrent:(NSMutableDictionary *)data date:(NSString *)date_graph{
    // dispatch_group_t group = dispatch_group_create();

    [self saveToServer:data date:date_graph];
   // dispatch_queue_t queue = dispatch_queue_create("com.SievaNetworks.app.queue", DISPATCH_QUEUE_CONCURRENT);
    
    /*NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    //getting authCode
    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    NSString *authCode = @"";
    if([authData count]!=0){
        authCode = authData[0][1];
        
    }
    //---------------------------
    NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp1/saveHosGraphData.php?masterUser=%@&user=%@&date=%@&approve=0&authCode=%@",master_menu,user_menu,date_graph,authCode];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSURLResponse *response;
    NSError *error;
    NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    if(httpResponse!=nil){
        if ([httpResponse statusCode] ==200 ) {
        
        BFLog(@"Device connected to the internet");
        NSURL *url = [NSURL URLWithString:urlString];
       // dispatch_async(queue, ^{
            NSError *error;
        
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:60.0];
            
            [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
            
            [request setHTTPMethod:@"POST"];
          NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
             @"IOS TYPE", @"typemap",
             nil];
            
<<<<<<< HEAD
            /*  NSMutableDictionary *currentStatus=[[NSMutableDictionary alloc]init];
=======
             NSMutableDictionary *currentStatus=[[NSMutableDictionary alloc]init];
>>>>>>> remotes/origin/MatrackHosV2
             [currentStatus setValue:status forKey:@"Status"];
            
            NSData *postData = [NSJSONSerialization dataWithJSONObject:data options:0 error:&error];
            [request setHTTPBody:postData];
            
            
            NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                               
            }];
            
            [postDataTask resume];*/
            
      //  });
        //dispatch_barrier_sync(queue, ^{
        
    
       // });
        
        
    //}
    //else if([httpResponse statusCode]==503){
            
            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Service Unavailable"
                                                             message:@"Please check your internet connectivity!!!"
                                                            delegate:self
                                                   cancelButtonTitle:@"Cancel"
                                                   otherButtonTitles:@"OK", nil];
            
           // [alert1 show];


}
-(void)saveToServer:(NSMutableDictionary *)driver_data date:(NSString *)date_graph{
    
//    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
//    //getting authCode
//    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
//    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
//    NSString *authCode = @"";
//    if([authData count]!=0){
//        authCode = authData[0][1];
//
//    }
//
//    NSString *query_dates1 =[NSString stringWithFormat:@"select flag from TempTable where date='%@';",date_graph];
//    NSArray *sync_data1 = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query_dates1]];
//
//
//    //---------------------------
//
//       NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/saveHosGraphData.php?masterUser=%@&user=%@&date=%@&approve=%ld&authCode=%@",master_menu,user_menu,date_graph,(long)[sync_data1[0][0] integerValue],authCode];
//
//    // NSString *urlString = [NSString stringWithFormat:@"http://23.239.28.100/mydealership/saveHosGraphData_app.php?masterUser=%@&user=%@&date=%@&approve=0",master_menu,user_menu,current_date];
//    NSURL *url = [NSURL URLWithString:urlString];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    [request setHTTPMethod:@"HEAD"];
//    NSURLResponse *response;
//    NSError *error;
//    NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
//    if(httpResponse!=nil){
//        if ([httpResponse statusCode] ==200 ) {
//            BFLog(@"Device connected to the internet");
//            NSURL *url = [NSURL URLWithString:urlString];
//
//            //NSError *error;
//
//            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
//                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                               timeoutInterval:60.0];
//
//            [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//            [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
//
//            [request setHTTPMethod:@"POST"];
//            /* NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
//             @"IOS TYPE", @"typemap",
//             nil];*/
//
//            //NSMutableDictionary *currentStatus=[[NSMutableDictionary alloc]init];
//            //[currentStatus setValue:status forKey:@"Status"];*/
//            /*  NSMutableArray *inner = [[NSMutableArray alloc] initWithArray:@[@"00:00",@"OFF"]];
//             NSMutableDictionary *data_dict = [[NSMutableDictionary alloc] init];
//             [data_dict setObject:inner forKey:@"payload"];
//             NSError *error;*/
//            NSData *postData = [NSJSONSerialization dataWithJSONObject:driver_data options:0 error:&error];
//            [request setHTTPBody:postData];
//
//            NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//
//                NSDictionary *responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//
//            }];
//
//            [postDataTask resume];
//        }else if([httpResponse statusCode]==403){
//            if(autoLoginCount<=1){
//                insertDutyCycle *obj = [insertDutyCycle new];
//                BOOL success =  [obj autoLogin];
//
//                if(success){
//                    [self saveToServer:driver_data date:date_graph];
//
//                }else{
//                    KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
//                    [credentials resetKeychainItem];
//                    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
//                                                                     message:@"Please Login Again"
//                                                                    delegate:self
//                                                           cancelButtonTitle:@"OK"
//                                                           otherButtonTitles:nil, nil];
//                    alert1.tag = 1;
//                    alert1.delegate = self;
//
//                    [alert1 show];
//                    [self syncServerLater:driver_data date:date_graph];
//
//                }
//            }else{
//                KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
//                [credentials resetKeychainItem];
//                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
//                                                                 message:@"Please Login Again"
//                                                                delegate:self
//                                                       cancelButtonTitle:@"OK"
//                                                       otherButtonTitles:nil, nil];
//                alert1.tag = 2;
//                alert1.delegate = self;
//
//                [alert1 show];
//                  [self syncServerLater:driver_data date:date_graph];
//
//
//            }
//
//        }else if([httpResponse statusCode]==503){
//            BFLog(@"Device not connected to the internet");
//            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Service Unavailable"
//                                                             message:@"No connectivity with server at this time,it will sync automatically"
//                                                            delegate:self
//                                                   cancelButtonTitle:@"OK"
//                                                   otherButtonTitles:nil, nil];
//
//            [alert1 show];
//            [self syncServerLater:driver_data date:date_graph];
//        }
//    }else{
//
//    }
//
}
-(void)syncServerLater:(NSMutableDictionary *)driver_data date:(NSString *)date_graph{
    
    NSArray *dataForDate = [driver_data objectForKey:@"payload"];
    NSInteger lastEventNumber = [dataForDate[[dataForDate count]-1][@"posy"] integerValue];
    NSString *lastEvent =@"";
    if (lastEventNumber == 245) {
        lastEvent=@"ON";
    }
    else if(lastEventNumber == 125){
        lastEvent=@"SB";
        
    }else if (lastEventNumber == 185){
        lastEvent = @"D";
    }else if (lastEventNumber == 65){
        lastEvent = @"OFF";
    }
    
    NSString *querySelect = [NSString stringWithFormat:@"SELECT * FROM FinalTable WHERE date='%@'",date_graph];
    NSArray *unsynchedData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:querySelect]];
    if([unsynchedData count]!=0){
        
        NSString *query_update1 = [NSString stringWithFormat:@"UPDATE FinalTable SET server_sync='%d' WHERE date='%@'",0,date_graph];
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_update1];
        
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
            BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
            
        }
        else{
            BFLog(@"Could not execute the query");
        }
    }else{
        NSData *data = [NSJSONSerialization dataWithJSONObject:driver_data options:0 error:nil];
        
        
        NSString *jsonStr = [[NSString alloc] initWithData:data
                                                  encoding:NSUTF8StringEncoding];
        NSString *query_insert1 = [NSString stringWithFormat:@"insert into FinalTable values(null, '%@', '%@', '%@' ,'%d','%d')", date_graph,jsonStr,lastEvent,0,0];
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_insert1];
        
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
            BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
            
        }
        else{
            // BFLog(@"Could not execute the query");
        }
        
        
        
    }

    
    
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag ==1 || alertView.tag ==2){
        
        if(buttonIndex==[alertView cancelButtonIndex]){
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"loginscreen"];
                [self presentViewController:vc animated:YES completion:nil];
            });
        }
        
        
    }
    if(alertView.tag == 1005){
        if(buttonIndex==1){
            
            UIStoryboard *storyBoard = self.storyboard;
            UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"changedutystatus"];
            [self presentViewController:vc animated:YES completion:nil];
        }
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    //#warning Incomplete implementation, return the number of sections
    return 4;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath{

    NSString *device = [UIDevice currentDevice].model;
    CGSize cellSize = CGSizeMake(100, 80);
    if([device isEqualToString:@"iPad"] || [device isEqualToString:@"iPad Simulator"]){
        
        cellSize = CGSizeMake(200, 180);
        
    }
    return cellSize;
    
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    
    NSString *device = [UIDevice currentDevice].model;
      if([device isEqualToString:@"iPad"] || [device isEqualToString:@"iPad Simulator"]){
          CGFloat totalCellWidth = 200*2;
          CGFloat totalSpacingWidth =20;
          CGFloat leftInset = (collectionView.frame.size.width-(totalCellWidth+totalSpacingWidth))/2;
          CGFloat rightInset = leftInset;
       return UIEdgeInsetsMake(50, leftInset, 0, rightInset);
        
    }
    return collectionView.contentInset;
    

    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    //#warning Incomplete implementation, return the number of items
    return 2;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cell";
    collectionView.backgroundColor=[UIColor whiteColor];
    //[collectionView registerClass:[CustomCell class] forCellWithReuseIdentifier:cellIdentifier];
    
    CustomCell  *cell = (CustomCell *) [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    //if (indexPath.item==0) {
    // [[cell textlabel]setText:[NSString stringWithFormat:@"cell:%ld,%ld",(long)indexPath.item,(long)indexPath.item]];
    //}*/
    //cell.icon.image=[UIImage imageNamed:@"Notepad_icon.svg"];
    NSString *device = [UIDevice currentDevice].model;
   // CGSize cellSize = CGSizeMake(100, 100);
    if([device isEqualToString:@"iPad"] || [device isEqualToString:@"iPad Simulator"]){
        
        CGRect newFrame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, 200, 200);
        [cell setFrame:newFrame];
        
    }
//    NSLog(@"%f",cell.label.frame.origin.y);
    //cell.layer.borderWidth =2.0;
    //cell.layer.borderColor = [[UIColor blackColor] CGColor];
        cell.icon.image=[UIImage imageNamed:[menu_images[indexPath.section]objectAtIndex:indexPath.row]];
    cell.label.text=[labels[indexPath.section]objectAtIndex:indexPath.row];
    //cell.textlabel.text=[cells[indexPath.section] objectAtIndex:indexPath.row];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        switch (indexPath.row) {
            case 0:
            {
               
                    dispatch_async(dispatch_get_main_queue(), ^{
                       // [hud1 hideAnimated:YES];
                UIStoryboard *storyBoard = self.storyboard;
                UIViewController *vc1 = [storyBoard instantiateViewControllerWithIdentifier:@"logscreen"];
                [self presentViewController:vc1 animated:YES completion:nil];
                    });
                
                break;
            }
            case 1:
            {
                UIStoryboard *storyBoard = self.storyboard;
                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"dotmenu"];
                [self presentViewController:vc animated:YES completion:nil];
                break;
                
            }
                
            default:
                break;
        }
    }
    else if (indexPath.section==1){
        
        switch (indexPath.row) {
            case 0:
            {
                UIStoryboard *storyBoard = self.storyboard;
                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"recap_screen"];
                [self presentViewController:vc animated:YES completion:nil];
  
                
            break;
            }
            case 1:
            {
                
                UIStoryboard *storyBoard = self.storyboard;
                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"docs"];
                [self presentViewController:vc animated:YES completion:nil];
                break;
            }
            default:
                break;
        }
        
        
        
        
    }else if (indexPath.section==2){
        
        switch (indexPath.row) {
            case 0:
            {   UIStoryboard *storyBoard = self.storyboard;
                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"message"];
                [self presentViewController:vc animated:YES completion:nil];

                break;
            }
            case 1:
            {
                UIStoryboard *storyBoard = self.storyboard;
                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"settingsMenu"];
                [self presentViewController:vc animated:YES completion:nil];
                break;
            }
            default:
                break;
        }
        
    }else if (indexPath.section==3){
        switch (indexPath.row) {
            case 0:
            {
                [self getUnApprovedDates];
                UIStoryboard *storyBoard = self.storyboard;
                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"unapproved"];
                [self presentViewController:vc animated:YES completion:nil];
                break;
            }
            case 1:{
                UIStoryboard *storyBoard = self.storyboard;
                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"helpMenu"];
                [self presentViewController:vc animated:YES completion:nil];
                break;
               /* [fiveSecTimer invalidate];
                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@""
                                                                 message:@"Do You want to exit!!!"
                                                                delegate:self
                                                       cancelButtonTitle:@"YES"
                                                       otherButtonTitles:@"NO", nil];
                
                [alert1 show];*/

                
                break;
            }
            default:
                break;
        }
        
        
        
        
    }
    
    
    
}

-(void)getUnApprovedDates{
    
    
    NSString *query0 = [NSString stringWithFormat:@"select date from TempTable where flag='%d' order by dateInSecs desc;",0];
    
    // Get the results.
    
    unapprovedDates = [[NSMutableArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    
    
    
    
    
    
}
/*-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    
    if (buttonIndex==0) {
        
        NSUserDefaults *userdata = [NSUserDefaults standardUserDefaults];
        // [userdata setObject:[user text] forKey:@"username"];
        //[userdata setObject:[pass text] forKey:@"password"];
        NSString *previous_user = [userdata objectForKey:@"username"];
        //NSString *previous_pass = [userdata objectForKey:@"password"];
        [userdata setValue:previous_user forKey:@"previousUser"];
        //[userdata setValue:previous_pass forKey:@"previousPass"];
        [userdata removeObjectForKey:@"username"];
        [userdata removeObjectForKey:@"password"];
        [userdata removeObjectForKey:@"masteruser"];
        [userdata removeObjectForKey:@"user"];
      ///
        //[[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"firstLaunch%@",user_menu]];
        BFLog(@"username n password deleted");
        UIStoryboard *storyBoard = self.storyboard;
        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"main1"];
        [self presentViewController:vc animated:YES completion:nil];
        
    }
    
    
    
}*/
#pragma mark - Table view data source
/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [menu count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.textLabel.font=[UIFont boldSystemFontOfSize:20.0];
    // Configure the cell...
    
    
    for(int i=0; i<[menu count];i++){
        
        if (indexPath.row == i) {
            cell.textLabel.text = menu[i];
            UIImage *icons = [UIImage imageNamed:[menu_images objectAtIndex:i]];
           cell.imageView.image = icons;
            //UIImageView  *img = [[UIImageView alloc] initWithImage:[menu_images objectAtIndex:i]];
            //cell.accessoryView = img;
                    }
        
    }
    
    return cell;
}
 

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        UIStoryboard *storyBoard = self.storyboard;
        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"logscreen"];
        [self presentViewController:vc animated:YES completion:nil];
    }
    if (indexPath.row==1) {
        UIStoryboard *storyBoard = self.storyboard;
        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"recap_screen"];
        [self presentViewController:vc animated:YES completion:nil];
    }
    if (indexPath.row==2) {
        UIStoryboard *storyBoard = self.storyboard;
        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"dotmenu"];
        [self presentViewController:vc animated:YES completion:nil];
    }
    
    if (indexPath.row==3) {
        UIStoryboard *storyBoard = self.storyboard;
        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"docs"];
        [self presentViewController:vc animated:YES completion:nil];
    }
    if (indexPath.row==4) {
        UIStoryboard *storyBoard = self.storyboard;
        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"message"];
        [self presentViewController:vc animated:YES completion:nil];
    }
    if (indexPath.row==5) {
        UIStoryboard *storyBoard = self.storyboard;
        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"settingsMenu"];
        [self presentViewController:vc animated:YES completion:nil];
    }    if (indexPath.row==7) {
        NSUserDefaults *userdata = [NSUserDefaults standardUserDefaults];
        // [userdata setObject:[user text] forKey:@"username"];
        //[userdata setObject:[pass text] forKey:@"password"];
        [userdata removeObjectForKey:@"username"];
        [userdata removeObjectForKey:@"password"];
        BFLog(@"username n password deleted");
        UIStoryboard *storyBoard = self.storyboard;
        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"loginscreen"];
        [self presentViewController:vc animated:YES completion:nil];
    }
}
*/
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
