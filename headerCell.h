//
//  headerCell.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/19/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface headerCell : UITableViewCell
@property (nonatomic,weak)IBOutlet UILabel *status;
//@property (nonatomic,weak)IBOutlet UILabel *start;
@property (nonatomic,weak)IBOutlet UILabel *duration;
@property (nonatomic,weak)IBOutlet UILabel *location;
@property (nonatomic,weak)IBOutlet UILabel *notes;


@end
