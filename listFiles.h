//
//  listFiles.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 8/30/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
extern NSArray *filePathsArray;
extern int selected_fileIndex;
@interface listFiles : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *infoMenu;
- (IBAction)helpAlert:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menu_listdot;

@property (weak, nonatomic) IBOutlet UITableView *filelist;
@end
