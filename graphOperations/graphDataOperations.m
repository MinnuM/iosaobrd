//
//  graphDataOperations.m
//  graph
//
//  Created by Raghee Chandran M on 9/23/17.
//  Copyright © 2017 Sieva Networks. All rights reserved.
//

#import "graphDataOperations.h"

@implementation graphDataOperations


-(NSMutableArray *)generateGraphPoints:(NSString *)charString{
    NSMutableArray *dataArray = [[NSMutableArray alloc]init];
    for (int i=0; i<96; i++) {
       
//        NSLog(@"length:%lu",(unsigned long)charString.length); //NSInteger posy = [[charString substringWithRange:NSMakeRange(i,1)] integerValue];
        NSString* posyTemp = [charString substringWithRange:NSMakeRange(i,1)];
        if([posyTemp isEqualToString:@"A"])
            posyTemp=@"10";
        else
            if([posyTemp isEqualToString:@"B"])
                posyTemp=@"11";
        else if([posyTemp isEqualToString:@"C"])
            posyTemp=@"12";
        NSInteger posy=[posyTemp intValue];
        NSMutableDictionary *objects =[[NSMutableDictionary alloc]init];
        
        switch (posy) {
            case 5:
            case 11:
            case 1:
                [objects setValue:[NSNumber numberWithInt:65] forKey:@"posy"];
                break;
            case 6:
            case 2:
                [objects setValue:[NSNumber numberWithInt:125] forKey:@"posy"];
                break;
            case 9:
            case 7:
            case 3:
                [objects setValue:[NSNumber numberWithInt:185] forKey:@"posy"];
                break;
            case 10:
            case 12:
            case 8:
            case 4:
                [objects setValue:[NSNumber numberWithInt:245] forKey:@"posy"];
                break;
            default:
                break;
        }
        float posx = (float)((i)*12.25)+40;
       // NSLog(@"%d. %f",i,posx);
        [objects setValue:[NSNumber numberWithFloat:posx] forKey:@"posx"];
        if(i==0){
            
            [objects setObject:[NSNumber numberWithInt:i] forKey:@"id"];
            [dataArray addObject:objects];
            NSMutableDictionary *objects1 = [[NSMutableDictionary alloc]init];
            [objects1 setObject:[NSNumber numberWithInt:i+1] forKey:@"id"];
            [objects1 setValue:[NSNumber numberWithFloat:posx] forKey:@"posx"];
            [objects1 setValue:[objects valueForKey:@"posy"] forKey:@"posy"];
            [dataArray addObject:objects1];
            
        }else if(dataArray[[dataArray count]-1][@"posy"]!=[objects valueForKey:@"posy"]){
            
            dataArray[[dataArray count]-1][@"posx"]=[objects valueForKey:@"posx"];
            int idValue = [dataArray[[dataArray count]-1][@"id"] intValue];
            [objects setValue:[NSNumber numberWithInt:idValue+1] forKey:@"id"];
            [dataArray addObject:objects];
            NSMutableDictionary *objects1 = [[NSMutableDictionary alloc]init];
            int idValue2 =[[objects valueForKey:@"id"] intValue];
            [objects1 setObject:[NSNumber numberWithInt:idValue2+1] forKey:@"id"];
            [objects1 setValue:[NSNumber numberWithFloat:posx] forKey:@"posx"];
            [objects1 setValue:[objects valueForKey:@"posy"] forKey:@"posy"];
            [dataArray addObject:objects1];
            
        }
        
        if(i==95){
            
            dataArray[[dataArray count]-1][@"posx"]=[NSNumber numberWithFloat:1216];
            
        }
        
            
    }
   for(int i=0;i<[dataArray count];){
        if(![dataArray[i] objectForKey:@"posy"]){
            [dataArray removeObjectAtIndex:i];
        }
        else i++;
    }
    return  dataArray;
}


-(NSString *)generateCharString:(NSMutableArray *)graphPayload{
    
    NSString *str =@"";
    
    for (int i=0; i<[graphPayload count]-1; i=i+2) {
        
        int d = ([graphPayload[i+1][@"posx"] integerValue]-[graphPayload[i][@"posx"] integerValue])/12.25;
        
        if(d==0){
            
            if([graphPayload[i][@"posy"] integerValue]==125.0){
                
                str = [str stringByAppendingString:@"2"];
                
                
            }else if([graphPayload[i][@"posy"] integerValue]==185.0){
                
                str = [str stringByAppendingString:@"3"];
                
                
            }else if([graphPayload[i][@"posy"] integerValue]==245.0){
                
                str = [str stringByAppendingString:@"4"];
                
            }else if([graphPayload[i][@"posy"] integerValue]==65.0){
                
                str = [str stringByAppendingString:@"1"];
                
                
            }

            
        }else{
        for (int j=0; j<d; j++) {
            
            if([graphPayload[i][@"posy"] integerValue]==125.0){
                
                str = [str stringByAppendingString:@"2"];
                
                
            }else if([graphPayload[i][@"posy"] integerValue]==185.0){
                
                str = [str stringByAppendingString:@"3"];
                
                
            }else if([graphPayload[i][@"posy"] integerValue]==245.0){
                
                str = [str stringByAppendingString:@"4"];
                
            }else if([graphPayload[i][@"posy"] integerValue]==65.0){
                
                str = [str stringByAppendingString:@"1"];
                
                
            }
            
            
            
        }//end of j for loop
        }
        
        if(graphPayload[i+1][@"posx"]==graphPayload[[graphPayload count]-1][@"posx"]){
            
            
            NSInteger d = (1216-[graphPayload[i+1][@"posx"] integerValue])/12.25;
            if(d==0){
                
                if([graphPayload[i][@"posy"] integerValue]==125.0){
                    
                    str = [str stringByAppendingString:@"2"];
                    
                    
                }else if([graphPayload[i][@"posy"] integerValue]==185.0){
                    
                    str = [str stringByAppendingString:@"3"];
                    
                    
                }else if([graphPayload[i][@"posy"] integerValue]==245.0){
                    
                    str = [str stringByAppendingString:@"4"];
                    
                    
                }else if([graphPayload[i][@"posy"] integerValue]==65.0){
                    
                    str = [str stringByAppendingString:@"1"];
                    
                    
                }

                
            }else{
            
            for (int j=0; j<d; j++) {
                
                if([graphPayload[i][@"posy"] integerValue]==125.0){
                    
                    str = [str stringByAppendingString:@"2"];
                    
                    
                }else if([graphPayload[i][@"posy"] integerValue]==185.0){
                    
                    str = [str stringByAppendingString:@"3"];
                    
                    
                }else if([graphPayload[i][@"posy"] integerValue]==245.0){
                    
                    str = [str stringByAppendingString:@"4"];
                    
                    
                }else if([graphPayload[i][@"posy"] integerValue]==65.0){
                    
                    str = [str stringByAppendingString:@"1"];
                    
                    
                }
                
                
                
            }//end of j for loop
            
            }
            
        }
        
        
        
        
        
        
    }
    
    
    
    return str;
}


@end
