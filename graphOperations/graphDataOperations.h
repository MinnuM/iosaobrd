//
//  graphDataOperations.h
//  graph
//
//  Created by Raghee Chandran M on 9/23/17.
//  Copyright © 2017 Sieva Networks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface graphDataOperations : NSObject
-(NSMutableArray *)generateGraphPoints:(NSString *)charString;
-(NSString *)generateCharString:(NSMutableArray *)graphPayload;

@end
