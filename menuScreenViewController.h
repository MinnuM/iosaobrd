//
//  menuScreenViewController.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/25/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "logScreenViewController.h"
#import "CustomCell.h"
#import "Reachability.h"
#import "DBManager1.h"
#import "DBManager.h"
#import "User.h"
#import "Carrier.h"
#import "user2carrier.h"
#import "AppDelegate.h"
extern NSMutableArray *unapprovedDates;
extern NSMutableDictionary *carrier1;
extern NSString *master_menu;
extern NSString *user_menu;
extern NSString *cycleinfo;
extern NSString *cargoinfo;
extern NSString *restartinfo;
extern NSString *sourceVC;
extern NSMutableArray *deviceData;
@interface menuScreenViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UIAlertViewDelegate,UICollectionViewDelegateFlowLayout>{
    Reachability *internetReachableFoo;
}


@property (weak, nonatomic) IBOutlet UICollectionView *menu;
@property (nonatomic, strong) DBManager1 *dbManager;
-(void)syncServerLater:(NSMutableDictionary *)driver_data date:(NSString *)date_graph;
@end
