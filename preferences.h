//
//  preferences.h
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 19/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface preferences : NSObject
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *userid;
@property (nonatomic, strong) NSString *preference;
@property (nonatomic, strong) NSString *val;

@end
