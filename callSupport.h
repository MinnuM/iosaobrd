//
//  callSupport.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/22/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface callSupport : UIViewController<UIWebViewDelegate>
- (IBAction)callSupport:(id)sender;

- (IBAction)helpPage:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menu_support;
@property (weak, nonatomic) IBOutlet UIButton *emailsupport;

@property (weak, nonatomic) IBOutlet UIScrollView *scroller;

@property (weak, nonatomic) IBOutlet UIWebView *supportView;
@property (weak, nonatomic) IBOutlet UISwitch *debugSwitch;
- (IBAction)changeDebug:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *deviceId;

@property (weak, nonatomic) IBOutlet UIButton *call_support;
@end
