//
//  forgotPassword.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 3/16/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "forgotPassword.h"

@interface forgotPassword ()

@end

@implementation forgotPassword
@synthesize email;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    //test msg
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)submit:(id)sender {
    
    if (email.text.length==0 || email.text==nil || [email.text isEqualToString:@""]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please fill provide the email id" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }else{
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
        NSString *urlString =[NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/resetPassword_app.php"];
        NSURL *url = [NSURL URLWithString:urlString];
        NSData *data1 = [NSData dataWithContentsOfURL:url];
        if (data1) {
            
            //BFLog(@"Device connected to the internet");
            NSURL *url = [NSURL URLWithString:urlString];
            NSError *error;
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:60.0];
            
            [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
            
            [request setHTTPMethod:@"POST"];
            /* NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
             @"IOS TYPE", @"typemap",
             nil];*/
            
            NSMutableDictionary *currentStatus=[[NSMutableDictionary alloc]init];
            [currentStatus setValue:email.text forKey:@"email"];
            //  [currentStatus setValue:user forKey:@"usr"];
            // NSData *data = [NSJSONSerialization dataWithJSONObject:currentStatus options:0 error:nil];
            NSData *postData = [NSJSONSerialization dataWithJSONObject:currentStatus options:0 error:&error];
            [request setHTTPBody:postData];
            
            
            NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                NSString *status = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:status delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                });

                
                
                
                
                            }];
            
            [postDataTask resume];
            
            
        }else{
            
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Ooops" message:@"Something went wrong!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        }
    }

}
@end
