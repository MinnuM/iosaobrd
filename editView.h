//
//  editView.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/9/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

extern CGRect current_rectfront4;
extern int flag_in;
extern int is_editing;
#define kkGraphHeight3 75

#define kkGraphBottom3 0
#define kkGraphTop3 117
#define kkStepY3 15
#define kkOffsetY3 0
#define kkCircleRadius3 1

@interface editView : UIView

@end
