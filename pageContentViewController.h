//
//  pageContentViewController.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/14/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface pageContentViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *screenImage;
- (IBAction)menuScreen:(id)sender;
@property NSUInteger pageIndex;
@property NSString *titleText;
@property NSString *imageFile;
@property (weak, nonatomic) IBOutlet UIButton *moveToMenu;
@end
