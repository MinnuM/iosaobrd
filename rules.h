//
//  rules.h
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 25/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface rules : NSObject
@property (nonatomic, strong) NSString *cycle;
@property (nonatomic, strong) NSString *timezone;
@property (nonatomic, strong) NSString *restart;
@property (nonatomic, strong) NSString *breakhour;
@property (nonatomic, strong) NSString *cargotype;
@property (nonatomic, strong) NSString *userid;
@property (nonatomic, strong) NSString *odounit;
@end
