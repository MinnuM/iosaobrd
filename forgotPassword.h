//
//  forgotPassword.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 3/16/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface forgotPassword : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *email;

- (IBAction)submit:(id)sender;
@end
