//
//  editStatus.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/9/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "editStatus.h"
#import "KeychainItemWrapper.h"
#import "insertDutyCycle.h"


@interface editStatus ()

@end
int slot;
UIButton *button,*button1,*button2,*button3;
int off_status,on_status,d_status,sb_status=0;
NSString *new_status=@"";
NSMutableArray *locationList_edit;
NSMutableArray *notesList_edit;
NSMutableArray *statusArray;

@implementation editStatus
@synthesize dbManager;
@synthesize edit_Table;

- (void)viewDidLoad {
    [super viewDidLoad];
    locationList_edit=[[NSMutableArray alloc] init];
    notesList_edit=[[NSMutableArray alloc] init];
    dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
    edit_Table.delegate=self;
    edit_Table.dataSource=self;
    if(self.objEditRow){
        _lblDate.text = dateValue;
        _lblStartEndTime.text = [NSString stringWithFormat:@"Start Time :%@         End Time: %@", _objEditRow.strRowTitle, _objEditRow.strPlaceHolder];
    }
    if([selected_previous isEqualToString:@""])
        [self getthedata:selected_date];
    else
        [self getthedata:selected_previous];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)getthedata:(NSString *)date{
    [dataarray5 removeAllObjects];
   /* NSString *query1 = [NSString stringWithFormat:@"select * from TempTable where date='%@'",date];
    
    // Get the results.
    NSArray *data = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query1]];
  //  BFLog(@"Result:%@",data);
    if ([data count]!=0) {
        NSString *dbresult = data[[data count]-1][2];
        
        
        
       // BFLog(@"string:%@",dbresult);
        
        
        //BFLog(@"timezone:%lu",(unsigned long)[dbresult count]);
        NSData *data2= [dbresult dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
        //BFLog(@"Dictionary of db data:%@",json_log1);
        if ([json_log1 count]==0) {
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@" " withString:@""];
            dbresult = [NSString stringWithFormat:@"[%@]",dbresult];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"\";" withString:@";"];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"=\"" withString:@"="];
            
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"location=(" withString:@"\"location\":["];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"payload=(" withString:@"\"payload\":["];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@");" withString:@"],"];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@";" withString:@"\","];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"=" withString:@":\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"id" withString:@"\"id\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"value" withString:@"\"value\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"posx" withString:@"\"posx\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"posy" withString:@"\"posy\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"timezone" withString:@"\"timezone\""];
            NSData *data2= [dbresult dataUsingEncoding:NSUTF8StringEncoding];
            json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
           // BFLog(@"Dictionary of db data2:%@",json_log1);
            
        }
        
        
        //[location_array removeAllObjects];
        //   BFLog(@"data portion:%@",[json_log1 allKeys]);
        // NSMutableArray *dict=[[NSMutableArray alloc] init];
        NSMutableArray *dict = [json_log1 objectForKey:@"payload"];
        if ([json_log1 objectForKey:@"location"]!=[NSNull null] && [[json_log1 objectForKey:@"location"] count]!=0) {
             locationList_edit = [[json_log1 objectForKey:@"location"] mutableCopy];
        }
      if ([json_log1 objectForKey:@"notes"]!=[NSNull null] && [[json_log1 objectForKey:@"notes"] count]!=0) {
        notesList_edit=[[json_log1 objectForKey:@"notes"] mutableCopy];
      }
        //BFLog(@"Location array:%@",locationList_edit);
        int json_size = (int)[dict count];
        //BFLog(@"inner json count:%d",json_size);
        [dataarray5 removeAllObjects];
        for (int i=0; i<json_size; i++) {
            
            //BFLog(@"posx:%@,%d",[dict[0] objectForKey:@"posx"],i);
            float posx=[[dict[i] objectForKey:@"posx"] floatValue];
            float x1 = posx - 40.0;
            int x2 = x1/12.25;
            int x3 = x2/4.0;
            int x4 = x2%4;
            
            /* NSString *minutes;
             if (15*x4 <15) {
             minutes=@"00";
             }
             else if(15*x4 >=15 && 15*x4 <30){
             minutes=@"15";
             }else if(15*x4 >=30 && 15*x4 <45){
             minutes=@"30";
             }else if(15*x4 >=45 && 15*x4 <60){
             minutes=@"45";
             }*/
    DBManager *objDB = [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    NSDictionary *token;
    ;
    NSMutableDictionary *whereToken;
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
        token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        
        //[objDB getLogEditData:whereToken];
        token=@{@"userid":[NSString stringWithFormat:@"%d",userId],@"date":[NSString stringWithFormat:@"%@",date]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        NSArray* dataFromDB=[[NSArray alloc]initWithArray:[objDB getLogEditData:whereToken]];
        statusString=@"";
        for(int j=0;j<[dataFromDB count];j++){
            if([dataFromDB[j][1] isEqualToString:@""])
                statusString= [statusString stringByAppendingString:[NSString stringWithFormat:@"1"]];
            else
                statusString= [statusString stringByAppendingString:[NSString stringWithFormat:@"%@",[[dataFromDB objectAtIndex:j] objectAtIndex:1]]];
        }
        
        NSString* charStr=[@"" stringByPaddingToLength:96 withString:@"1" startingAtIndex:0];
        if([dataFromDB count]==0)
            statusString=charStr;
        else
        {
            if([statusString isEqualToString:@""])
                statusString=charStr;
            
        }
        commonDataHandler *common=[commonDataHandler new];
        statusString=[common modifyData:statusString date:date];
        [dataarray5 removeAllObjects];
        graphDataOperations *obj=[graphDataOperations new];
        NSMutableArray *dict=[obj generateGraphPoints:statusString];
        for (int i=0; i<[dict count]; i++) {
            
//            NSLog(@"posx:%@,%d",[dict[0] objectForKey:@"posx"],i);
            float posx=[[dict[i] objectForKey:@"posx"] floatValue];
            float x1 = posx - 40.0;
            int x2 = x1/12.25;
            int x3 = x2/4.0;
            int x4 = x2%4;
            
            NSString *minutes;
            if (15*x4 <15) {
                minutes=@"00";
            }
            else if(15*x4 >=15 && 15*x4 <30){
                minutes=@"15";
            }else if(15*x4 >=30 && 15*x4 <45){
                minutes=@"30";
            }else if(15*x4 >=45 && 15*x4 <60){
                minutes=@"45";
            }
            NSString *posx_1 = [NSString stringWithFormat:@"%d:%@",x3,minutes];
            
            // float time = [posx_1 floatValue];
            int event_no =(int)[[dict[i] objectForKey:@"posy"] integerValue];
            NSString * event;
            if (event_no == 245) {
                event=@"ON";
            }
            else if(event_no == 125){
                event=@"SB";
                
            }else if (event_no == 185){
                event = @"D";
            }else if (event_no == 65){
                event = @"OFF";
            }
            NSArray *final_data = [[NSArray alloc] initWithObjects:posx_1,event, nil];
            [dataarray5 addObject:final_data];
        }
        /*
            NSString *posx_1 = [NSString stringWithFormat:@"%d:%d",x3,15*x4];
        
            // float time = [posx_1 floatValue];
            int event_no =(int)[[dict[i] objectForKey:@"posy"] integerValue];
            NSString * event;
            if (event_no == 245) {
                event=@"ON";
            }
            else if(event_no == 125){
                event=@"SB";
                
            }else if (event_no == 185){
                event = @"D";
            }else if (event_no == 65){
                event = @"OFF";
            }
            NSArray *final_data = [[NSArray alloc] initWithObjects:posx_1,event, nil];
            [dataarray5 addObject:final_data];
            
            
            
        }*/
        if (section_index==2) {
            
            
            NSString *last_status =dataarray5[[dataarray5 count]-1][1];
            [dataarray5 removeLastObject];
            
            NSArray *last = [[NSArray alloc] initWithObjects:@"24:00",last_status, nil];
            [dataarray5 addObject:last];
        }
        
    }
    //BFLog(@"data array 4:%@",dataarray5);
    
   // NSArray *subViews = [self.view subviews];
  /*  for(UIView *view in subViews){
        if ([view isKindOfClass:[insertView class]]) {
            [view removeFromSuperview];
            flag_in = flag_in +1;
            
            insertView *i = [[insertView alloc] initWithFrame:(CGRectMake(0, 100, 600, 100))];
            y_origin1=0;
            [i setOpaque:NO];
            
            [self.view addSubview:i];
            [i setNeedsDisplay];
        }
    }
    */
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger rows = 1;
    if (section==0) {
        rows=1;
    }else if (section ==1){
        rows=1;
    }
    return rows;
    
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *title;
    if (section==0){
        title=@"";
    }else{
        title=@"Duty Status";
    }
    return title;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    float height;
    if (indexPath.section==0) {
        height=100;
    }else{
        height=50;
    }
    return height;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   static NSString *cellIdentifier = @"cell";
    // NSString *cellIdentifier2 =@"more cell";
    // NSString *data=@"";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if (indexPath.section==0) {
        editView *graph = [[editView alloc]initWithFrame:CGRectMake(0, 0, cell.contentView.frame.size.width, 150)];
        UILabel *off = [[UILabel alloc]initWithFrame:CGRectMake(550, 10, 40, 40)];
        off.text=@"sam";
        off.textColor=[UIColor yellowColor];
       // [graph insertSubview:off aboveSubview:graph];
        [cell.contentView addSubview:graph];

    }else if(indexPath.section==1){
        CGRect buttonRect = CGRectMake(50, 10, 30, 30);
        button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        button.frame = buttonRect;
        // set the button title here if it will always be the same
        [button setTitle:@"OFF" forState:UIControlStateNormal];
        button.tag = 1;
        [button addTarget:self action:@selector(offDuty) forControlEvents:UIControlEventTouchUpInside];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        button.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
        //[button addTarget:self action:@selector(myAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:button];
        
        CGRect buttonRect1 = CGRectMake(110, 10, 30, 30);
       button1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        button1.frame = buttonRect1;
        button1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
        [button1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button1 addTarget:self action:@selector(sleeper) forControlEvents:UIControlEventTouchUpInside];
        // set the button title here if it will always be the same
        [button1 setTitle:@"SB" forState:UIControlStateNormal];
        
        //[button addTarget:self action:@selector(myAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:button1];
        
        CGRect buttonRect2 = CGRectMake(170, 10, 30, 30);
       button2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        button2.frame = buttonRect2;
        [button2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button2 addTarget:self action:@selector(driving) forControlEvents:UIControlEventTouchUpInside];
        // set the button title here if it will always be the same
        [button2 setTitle:@"D" forState:UIControlStateNormal];
        button2.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
        //[button addTarget:self action:@selector(myAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:button2];
        CGRect buttonRect3 = CGRectMake(230, 10, 30, 30);
        button3 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        button3.frame = buttonRect3;
        [button3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button3 addTarget:self action:@selector(onDuty) forControlEvents:UIControlEventTouchUpInside];
        // set the button title here if it will always be the same
        [button3 setTitle:@"ON" forState:UIControlStateNormal];
       button3.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
        //[button addTarget:self action:@selector(myAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:button3];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}
-(void)offDuty{
     [self getthedata:current_date];
     new_status=@"OFF";
     is_editing=1;
    button.backgroundColor=[UIColor cyanColor];
    
    button1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
    
    
    button2.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
    
    
    button3.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
    [self showNewLine];

    
}
-(void)onDuty{
      [self getthedata:current_date];
    new_status=@"ON";
     is_editing=1;
    button3.backgroundColor=[UIColor cyanColor];
    button.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
    
    button1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
    
    
    button2.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
    [self showNewLine];

    
}
-(void)sleeper{
      [self getthedata:current_date];
    new_status=@"SB";
     is_editing=1;
    button1.backgroundColor=[UIColor cyanColor];
    button3.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
    button.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
    
    
    
    button2.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];

    [self showNewLine];
    
}
-(void)driving{
      [self getthedata:current_date];
    new_status=@"D";
    is_editing=1;
    

    button2.backgroundColor=[UIColor cyanColor];
    button1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
    button3.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
    button.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
    [self showNewLine];
    
    

}
-(void)showNewLine{
    NSMutableArray *dummy_array=[[NSMutableArray alloc]init];
   // [newline removeAllObjects];
    NSInteger index_pos;
    NSString *current_status=@"";
    NSString *previous_status=@"";
    NSString *next_status=@"";
//    selected_row=selected_row-1;
    newline=[[NSMutableArray alloc]initWithArray:dataarray5];
    //minnu
    if(selected_row!=0)
        index_pos=selected_row*2;
    else
        index_pos=selected_row;
    NSString *start=newline[index_pos][0];
    NSString *end=newline[index_pos+1][0];
    NSString *init=@"0:0";
    NSArray *initTime=[init componentsSeparatedByString:@":"];
    NSArray *startTime=[start componentsSeparatedByString:@":"];
    NSArray *endTime=[end componentsSeparatedByString:@":"];
    int duration=([endTime[0]intValue]*60+[endTime[1] intValue])-([startTime[0]intValue]*60+[startTime[1] intValue]);
    int repeat=(int)duration/15;
    repeat=repeat;
    duration=([startTime[0]intValue]*60+[startTime[1] intValue])-([initTime[0]intValue]*60+[initTime[1] intValue]);
    slot=(int)duration/15;
    slot=slot+1;
    NSString *statusNew=[self getStatusCode:new_status slot:slot];
    annotation *obj=[annotation new];
    NSString* charStr=statusString;
    NSMutableString *charStrWithSlash=[NSMutableString stringWithFormat:@""];
    for(int i=0;i<charStr.length;i++){
        NSUInteger len=1;
       [charStrWithSlash appendFormat:@"%@|",[charStr substringWithRange:NSMakeRange(i, len)]];
    }
    NSString *tempStr;
    if([statusNew isEqualToString:@"9"]){
        tempStr=[obj updateStr:charStrWithSlash index1:slot index2:repeat notes:@"7"];
        NSRange range=[tempStr rangeOfString:@"7"];
        if(NSNotFound !=range.location){
            tempStr=[tempStr stringByReplacingCharactersInRange:range withString:@"9"];
        }
    }
    else
    tempStr=[obj updateStr:charStrWithSlash index1:slot index2:repeat notes:statusNew];
    

     NSString* newCharStr=[tempStr stringByReplacingOccurrencesOfString:@"|" withString:@""];
    statusArray=[tempStr componentsSeparatedByString:@"|"];
    [newline removeAllObjects];
    //minnu
    
    /* raghee
     if (selected_row!=0) {
        index_pos=selected_row*2;
        previous_status = newline[index_pos-1][1];
        current_status = newline[index_pos][1];
        if (index_pos+2<[newline count]) {
           
        next_status = newline[index_pos+2][1];
        }
        
    }else{
        index_pos=selected_row;
        current_status = newline[index_pos][1];
        if((index_pos+2)<[newline count]){
        next_status = newline[index_pos+2][1];
        }

    }
    NSInteger index_pos2=index_pos+1;
    
    if ([current_status isEqualToString:new_status]) {
        
        //BFLog(@"No Change");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No change for the graph.." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }else if ([previous_status isEqualToString:new_status]) {
        
        if (index_pos2!=[newline count]-1) {
            
        
       
        if([next_status isEqualToString:new_status]){
          //  NSMutableArray *dummyLine = [newline mutableCopy];
            NSMutableArray *sam1 = [NSMutableArray arrayWithArray:newline[index_pos-1] ];
            [sam1 removeObjectAtIndex:0];
            [sam1 insertObject:newline[index_pos2+2][0] atIndex:0];
            if([newline[index_pos2+2][0]  isEqual: @"24:00"]){
                for(int i=0;i<index_pos;i++){
                    
                    [dummy_array addObject:newline[i]];
                    
                }
                [newline removeAllObjects];
                newline=dummy_array;
                [newline removeObjectAtIndex:index_pos-1];
                [newline insertObject:sam1 atIndex:index_pos-1];
                
            }else{
                NSMutableIndexSet *indexes = [[NSMutableIndexSet alloc]init];
                [indexes addIndex:index_pos-1];
                [indexes addIndex:index_pos];
                [indexes addIndex:index_pos2];
                [indexes addIndex:index_pos2+1];
                [newline removeObjectsAtIndexes:indexes];
               /* [newline removeObjectAtIndex:index_pos-1];
                [newline removeObjectAtIndex:index_pos];
                [newline removeObjectAtIndex:index_pos2];
                [newline removeObjectAtIndex:index_pos2+1];
                */
   /*raghee         }
            
            
        }else{
        NSMutableArray *sam1 = [NSMutableArray arrayWithArray:newline[index_pos2] ];
        [sam1 removeObjectAtIndex:1];
        [sam1 insertObject:new_status atIndex:1];
        [newline removeObjectAtIndex:index_pos2];
        [newline insertObject:sam1 atIndex:index_pos2];
        [newline removeObjectAtIndex:index_pos];
        [newline removeObjectAtIndex:index_pos-1];
        
        }}
        
        else{
            
            [newline removeLastObject];

            [newline removeLastObject];
            NSMutableArray *sam1 = [NSMutableArray arrayWithArray:newline[index_pos-1] ];
                       [sam1 removeObjectAtIndex:0];
            [sam1 insertObject:@"24:00" atIndex:0];
            [newline removeLastObject];
            [newline addObject:sam1];

            
        }

   
    }else if([next_status isEqualToString:new_status]){
        
        NSMutableArray *sam = [NSMutableArray arrayWithArray:newline[index_pos]];
       // NSMutableArray *sam1 = [NSMutableArray arrayWithArray:newline[index_pos2] ];
        [sam removeObjectAtIndex:1];
        [sam insertObject:new_status atIndex:1];
        [newline removeObjectAtIndex:index_pos];
        [newline insertObject:sam atIndex:index_pos];
        NSMutableIndexSet *indexes = [[NSMutableIndexSet alloc]init];
        [indexes addIndex:index_pos2];
       // [indexes addIndex:index_pos];
        //[indexes addIndex:index_pos2];
        [indexes addIndex:index_pos2+1];
        [newline removeObjectsAtIndexes:indexes];
        
        
        
        
        }else{
        
        NSMutableArray *sam = [NSMutableArray arrayWithArray:newline[index_pos]];
        NSMutableArray *sam1 = [NSMutableArray arrayWithArray:newline[index_pos2] ];
        [sam removeObjectAtIndex:1];
        [sam insertObject:new_status atIndex:1];
        [sam1 removeObjectAtIndex:1];
        [sam1 insertObject:new_status atIndex:1];
        [newline removeObjectAtIndex:index_pos];
        [newline insertObject:sam atIndex:index_pos];
        [newline removeObjectAtIndex:index_pos2];
        [newline insertObject:sam1 atIndex:index_pos2];
        
        
    }
    NSLog(@"Repeatslot:%@",newline);
    */
   // commonDataHandler *common=[commonDataHandler new];
   // newCharStr=[common modifyData:newCharStr date:date];
    graphDataOperations *objGraph=[graphDataOperations new];
    NSMutableArray *dict=[objGraph generateGraphPoints:newCharStr];
    for (int i=0; i<[dict count]; i++) {
        
//        NSLog(@"posx:%@,%d",[dict[0] objectForKey:@"posx"],i);
        float posx=[[dict[i] objectForKey:@"posx"] floatValue];
        float x1 = posx - 40.0;
        int x2 = x1/12.25;
        int x3 = x2/4.0;
        int x4 = x2%4;
        
        NSString *minutes;
        if (15*x4 <15) {
            minutes=@"00";
        }
        else if(15*x4 >=15 && 15*x4 <30){
            minutes=@"15";
        }else if(15*x4 >=30 && 15*x4 <45){
            minutes=@"30";
        }else if(15*x4 >=45 && 15*x4 <60){
            minutes=@"45";
        }
        NSString *posx_1 = [NSString stringWithFormat:@"%d:%@",x3,minutes];
        
        // float time = [posx_1 floatValue];
        int event_no =(int)[[dict[i] objectForKey:@"posy"] integerValue];
        NSString * event;
        if (event_no == 245) {
            event=@"ON";
        }
        else if(event_no == 125){
            event=@"SB";
            
        }else if (event_no == 185){
            event = @"D";
        }else if (event_no == 65){
            event = @"OFF";
        }
        NSArray *final_data = [[NSArray alloc] initWithObjects:posx_1,event, nil];
        [newline addObject:final_data];
    }
    
    // BFLog(@"new Array:%@",newline);
    //dataarray5=newline;
   // [edit_Table reloadData];
//    NSLog(@"Repeatslot:%@",newline);
    
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
    [edit_Table beginUpdates];
    [edit_Table reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
    [edit_Table endUpdates];


    
    
}
- (void)savetoDb:(NSMutableArray*)data{
    
        NSMutableArray *outer_array = [[NSMutableArray alloc] init];
    NSMutableDictionary *data_dict = [[NSMutableDictionary alloc] init];
    
    for (int i=0; i<[data count]; i++) {
        
        NSString *x12=data[i][0];
        NSArray *time = [x12 componentsSeparatedByString:@":"];
        int x4 =(int) [time[1] integerValue]/15;
        int x2 =(int) [time[0] integerValue]*4+x4;
        float x1 = 12.25*x2;
        float posx = x1+40.0;
        int posy = 65;
      //  BFLog(@"posx:%.2f",posx);
        if ([data[i][1] isEqualToString:@"OFF"]) {
            posy = 65;
        }else if([data[i][1] isEqualToString:@"ON"]){
            posy = 245;
        }else if([data[i][1] isEqualToString:@"D"]){
            posy=185;
        }else if ([data[i][1] isEqualToString:@"SB"]){
            posy=125;
        }
        
        NSMutableDictionary *cont1 = [[NSMutableDictionary alloc] init];
        [cont1 setValue:@(i) forKey:@"id"];
        [cont1 setValue:@(posx) forKey:@"posx"];
        [cont1 setValue:@(posy) forKey:@"posy"];
        
        [outer_array addObject:cont1];
        
        
        
    }
    
    [data_dict setObject:outer_array forKey:@"payload"];
    if (locationList_edit!=NULL && locationList_edit!=(id)[NSNull null]  && locationList_edit!=nil && [locationList_edit count]!=0) {
        
        [data_dict setObject:locationList_edit forKey:@"location"];
        
        
    }
    if (notesList_edit!=NULL && notesList_edit!=(id)[NSNull null]  && notesList_edit!=nil && [notesList_edit count]!=0) {
        [data_dict setObject:notesList_edit forKey:@"notes"];
    }
    
   // BFLog(@"dictionary:%@",data_dict);
    NSData *data_object = [NSJSONSerialization dataWithJSONObject:data_dict options:NSJSONWritingPrettyPrinted error:nil];
    [self saveToServer:data_dict];

    NSString *jsonStr = [[NSString alloc] initWithData:data_object
                                              encoding:NSUTF8StringEncoding];
   // BFLog(@"JSON:%@",jsonStr);
    //  NSString *query1 = [NSString stringWithFormat:@"select * from TempTable where date='%@'",selected_date];
    
    // Get the results.
    NSString *status = data[[data count]-1][1];
    // NSArray *values = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query1]];
    NSString *query1 = [NSString stringWithFormat:@"select * from TempTable where date='%@'",current_date];
    
    // Get the results.
    
    NSArray *values = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query1]];
    
    if ([values count]!=0) {
        
        
        
        NSString *query_update = [NSString stringWithFormat:@"UPDATE TempTable SET data='%@',last_status='%@',flag='%d' WHERE date='%@'",jsonStr,status,0,current_date];
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
       // [objDB executeQuery:query_update];
        
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
           // BFLog(@"Table updated successfully. Affected rows = %d", dbManager.affectedRows);
            UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Modification Saved" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                [alert1 dismissViewControllerAnimated:YES completion:nil];
            }];
            
            
            [alert1 addAction:ok1];
            [self presentViewController:alert1 animated:YES completion:nil];
            
            
        }
        else{
          //  BFLog(@"Could not execute the query");
        }}
    
    else{
        
        NSString *query_insert1 = [NSString stringWithFormat:@"insert into TempTable values(null, '%@', '%@', '%@' ,'%d')", current_date,jsonStr,status,0];
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_insert1];
        
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
           // BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
            UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Modification Saved" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                [alert1 dismissViewControllerAnimated:YES completion:nil];
            }];
            
            
            [alert1 addAction:ok1];
            [self presentViewController:alert1 animated:YES completion:nil];
            
            
        }
        else{
            //BFLog(@"Could not execute the query");
        }
        
        
        
    }
    
    
    
    
}

- (IBAction)saveBtn:(id)sender {
    if([newline count]!=0){
   /*    int index_pos=selected_row*2;
        int index_pos2=index_pos+1;
        NSMutableArray *sam = [NSMutableArray arrayWithArray:dataarray5[index_pos]];
        NSMutableArray *sam1 = [NSMutableArray arrayWithArray:dataarray5[index_pos2] ];
        [sam removeObjectAtIndex:1];
        [sam insertObject:new_status atIndex:1];
        [sam1 removeObjectAtIndex:1];
        [sam1 insertObject:new_status atIndex:1];
        [dataarray5 removeObjectAtIndex:index_pos];
        [dataarray5 insertObject:sam atIndex:index_pos];
        [dataarray5 removeObjectAtIndex:index_pos2];
        [dataarray5 insertObject:sam1 atIndex:index_pos2];
    
    if (index_pos!=[dataarray5 count]-2) {
        
    if (selected_row==0) {
        if ([dataarray5[index_pos][1] isEqualToString:dataarray5[index_pos+2][1]]) {
            
            [dataarray5 removeObjectAtIndex:index_pos2];
            [dataarray5 removeObjectAtIndex:index_pos2+1];
            
        }
    }else{
        if (index_pos!=[dataarray5 count]-2) {
            
       
            if ([dataarray5[index_pos][1] isEqualToString:dataarray5[index_pos+2][1]]) {
            
                [dataarray5 removeObjectAtIndex:index_pos2];
                [dataarray5 removeObjectAtIndex:index_pos2-1];
            }
        
        
        }else{
            if ([dataarray5[index_pos-2][1] isEqualToString:dataarray5[index_pos][1]]) {
                [dataarray5 removeObjectAtIndex:index_pos];
                [dataarray5 removeObjectAtIndex:index_pos-1];
            }
            
        }
        
    }
    }else{
        if (index_pos!=0) {
            
            if ([dataarray5[index_pos-2][1] isEqualToString:dataarray5[index_pos][1]]) {
                [dataarray5 removeObjectAtIndex:index_pos];
                [dataarray5 removeObjectAtIndex:index_pos-1];
            }
        }
        
        
        
    }*/
    dataarray5=newline;

    // BFLog(@"new Array:%@",dataarray5);
  [edit_Table reloadData];
  //  [self savetoDb:dataarray5];
   //minnu
    DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    int userId = 0;
    if([userInfoArray count]!=0)
        userId = [userInfoArray[0][0] intValue];
    loggraph *objLog=[loggraph new];
    for(int i=0;i<[statusArray count];i++){
        objLog.slot=[NSString stringWithFormat:@"%d",(int)(1+i)];
        objLog.driverStatus=[statusArray objectAtIndex:i];
        objLog.calculatedStatus=[statusArray objectAtIndex:i];
        objLog.date=dateValue;
        objLog.userId=[NSString stringWithFormat:@"%d",userId];
        objLog.insert=[NSString stringWithFormat:@"%d",0];
        if(i==0)
            objLog.approve=[NSString stringWithFormat:@"%d",0];
        [objDB insertPastStatus:objLog];
    }
    
   //minnu
    
    if (objDB.affectedRows != 0) {
        // BFLog(@"Table updated successfully. Affected rows = %d", dbManager.affectedRows);
        UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Modification Saved" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [alert1 dismissViewControllerAnimated:YES completion:nil];
        }];
        
        
        [alert1 addAction:ok1];
        [self presentViewController:alert1 animated:YES completion:nil];
        
        
    }
    else{
        //  BFLog(@"Could not execute the query");
    }
    
    
  //  BFLog(@"selected index:%d",selected_row);
    
    }
    else{
        UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please select the event before save." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [alert1 dismissViewControllerAnimated:YES completion:nil];
            button.backgroundColor = [UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
            /*NSIndexPath *index  = [NSIndexPath indexPathForRow:4 inSection:0];
             [contentTable beginUpdates];
             [contentTable reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
             [contentTable endUpdates];*/
        }];
        
        
        [alert1 addAction:ok1];
        [self presentViewController:alert1 animated:YES completion:nil];
        
    }
}

-(NSString *)getStatusCode:(NSString *)status_new slot:(int)slot{
    NSString *statusNew;
    if([status_new isEqualToString:@"D"])
        statusNew=@"7";
    else if([status_new isEqualToString:@"SB"])
        statusNew=@"6";
    else if([status_new isEqualToString:@"OFF"])
        statusNew=@"5";
    else if([status_new isEqualToString:@"ON"])
        statusNew=@"8";
    DBManager *objDB = [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    NSDictionary *token;
    ;
    NSMutableDictionary *whereToken;
    NSArray *userInfo=[[NSArray alloc]initWithArray:[objDB getUserId]];
    NSString * userId=@"";
    if([userInfo count]!=0)
      userId  =userInfo[0][0];
    token=@{@"date":dateValue,@"slot":[NSString stringWithFormat:@"%d",slot],@"userid":userId};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
    if([status_new isEqualToString:@"D"])
    statusNew=[objDB findRestart:whereToken];
    
    return statusNew;
}


-(void)saveToServer:(NSMutableDictionary *)driver_data{
    
//    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
//    //getting authCode
//    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
//    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
//    NSString *authCode = @"";
//    if([authData count]!=0){
//        authCode = authData[0][1];
//        
//    }
//    //---------------------------
//    NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/saveHosGraphData.php?masterUser=%@&user=%@&date=%@&approve=0&authCode=%@",master_menu,user_menu,current_date,authCode];    // NSString *urlString = [NSString stringWithFormat:@"http://23.239.28.100/mydealership/saveHosGraphData_app.php?masterUser=%@&user=%@&date=%@&approve=0",master_menu,user_menu,current_date];
//    NSURL *url = [NSURL URLWithString:urlString];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    [request setHTTPMethod:@"HEAD"];
//    NSURLResponse *response;
//    NSError *error;
//    NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
//    if(httpResponse!=nil){
//        if ([httpResponse statusCode] ==200 ) {
//            BFLog(@"Device connected to the internet");
//            NSURL *url = [NSURL URLWithString:urlString];
//            
//            //NSError *error;
//            
//            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
//                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                               timeoutInterval:60.0];
//            
//            [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//            [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
//            
//            [request setHTTPMethod:@"POST"];
//            /* NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
//             @"IOS TYPE", @"typemap",
//             nil];*/
//            
//            //NSMutableDictionary *currentStatus=[[NSMutableDictionary alloc]init];
//            //[currentStatus setValue:status forKey:@"Status"];*/
//            /*  NSMutableArray *inner = [[NSMutableArray alloc] initWithArray:@[@"00:00",@"OFF"]];
//             NSMutableDictionary *data_dict = [[NSMutableDictionary alloc] init];
//             [data_dict setObject:inner forKey:@"payload"];
//             NSError *error;*/
//            NSData *postData = [NSJSONSerialization dataWithJSONObject:driver_data options:0 error:&error];
//            [request setHTTPBody:postData];
//            
//            NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                
//                NSDictionary *responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//                if(error==nil){
//                    
//                    UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Modification Saved" preferredStyle:UIAlertControllerStyleAlert];
//                    UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
//                        [alert1 dismissViewControllerAnimated:YES completion:nil];
//                    }];
//                    
//                    
//                    [alert1 addAction:ok1];
//                     [self presentViewController:alert1 animated:YES completion:nil];
//                }
//
//                
//            }];
//            
//            [postDataTask resume];
//        }else if([httpResponse statusCode]==403){
//            if(autoLoginCount<=1){
//                insertDutyCycle *obj = [insertDutyCycle new];
//                BOOL success =  [obj autoLogin];
//                
//                if(success){
//                    [self saveToServer:driver_data];
//                    
//                }else{
//                    KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
//                    [credentials resetKeychainItem];
//                    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
//                                                                     message:@"Please Login Again"
//                                                                    delegate:self
//                                                           cancelButtonTitle:@"OK"
//                                                           otherButtonTitles:nil, nil];
//                    alert1.tag = 1;
//                    alert1.delegate = self;
//                    
//                    [alert1 show];
//                    menuScreenViewController *obj = [menuScreenViewController new];
//                    [obj syncServerLater:driver_data date:current_date];
//                    
//                }
//            }else{
//                KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
//                [credentials resetKeychainItem];
//                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
//                                                                 message:@"Please Login Again"
//                                                                delegate:self
//                                                       cancelButtonTitle:@"OK"
//                                                       otherButtonTitles:nil, nil];
//                alert1.tag = 2;
//                alert1.delegate = self;
//                
//                [alert1 show];
//                
//                
//                
//            }
//            
//        }else if([httpResponse statusCode]==503){
//            BFLog(@"Device not connected to the internet");
//            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Service Unavailable"
//                                                             message:@"No connectivity with server at this time,it will sync automatically"
//                                                            delegate:self
//                                                   cancelButtonTitle:@"OK"
//                                                   otherButtonTitles:nil, nil];
//            
//            [alert1 show];
//            menuScreenViewController *obj = [menuScreenViewController new];
//            [obj syncServerLater:driver_data date:current_date];
//            
//        }
//    }
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag ==1 || alertView.tag ==2){
        
        if(buttonIndex==[alertView cancelButtonIndex]){
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"loginscreen"];
                [self presentViewController:vc animated:YES completion:nil];
            });
            
        }
        
        
    }
    
    
    
}

/*-(void)saveToServer:(NSData *)data{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    //getting authCode
    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    NSString *authCode = @"";
    if([authData count]!=0){
        authCode = authData[0][1];
        
    }
    //---------------------------
    NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp1/saveHosGraphData.php?masterUser=%@&user=%@&date=%@&approve=0&authCode=%@",master_menu,user_menu,current_date,authCode];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSURLResponse *response;
    NSError *error;
    NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    if(httpResponse!=nil){
        if ([httpResponse statusCode] ==200 ) {
        
       // BFLog(@"Device connected to the internet");
        NSURL *url = [NSURL URLWithString:urlString];
        
      //  NSError *error;
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setHTTPMethod:@"POST"];
        /* NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
         @"IOS TYPE", @"typemap",
         nil];*/
        
        /*  NSMutableDictionary *currentStatus=[[NSMutableDictionary alloc]init];
         [currentStatus setValue:status forKey:@"Status"];
        
        //NSData *postData = [NSJSONSerialization dataWithJSONObject:data options:0 error:&error];
        [request setHTTPBody:data];
        
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
          //  BFLog(@"error:%@,%@",error,response);
            
        }];
        
        [postDataTask resume];
    }
    else if([httpResponse statusCode]==503){
       // BFLog(@"Device not connected to the internet");
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Service Unavailable"
                                                         message:@"No connectivity with server at this time,it will sync automatically"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:@"OK", nil];
        
        [alert1 show];
        
    }
    }
    
}*/


- (IBAction)backBtn:(id)sender {
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"logsmenu"];
    [self presentViewController:vc animated:YES completion:nil];


}

@end
