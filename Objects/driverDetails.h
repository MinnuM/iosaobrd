//
//  driverDetails.h
//  hos
//
//  Created by Minnu Mohandas on 25/09/17.
//  Copyright © 2017 Minnu Mohandas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBManager1.h"
@interface driverDetails : NSObject
@property (nonatomic, strong) NSString *fname;
@property (nonatomic, strong) NSString *lname;
@property (nonatomic, strong) NSString *license;
@property (nonatomic, strong) NSString *dotNum;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) DBManager1 *dbManager;

-(NSString*)getFirstName;
-(NSString*)getLastName;
-(NSString*)getLicense;
-(NSString*)getDotNum;
-(NSString*)getEmail;
-(NSString*)getPhone;
-(void)setFirstName :(NSString*)firstName;
-(void)setLastName :(NSString*)lastName;
-(void)setLicense :(NSString*)licenseId;
-(void)setDotNum :(NSString*)dotNumber;
-(void)setEmail :(NSString*)emailId;
-(void)setPhone :(NSString*)phoneNum;
-(void)insertIntoTable:(NSMutableArray*)objectArray;
-(void)deleteFromTable:(NSString*)license;
@end
