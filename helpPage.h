//
//  helpPage.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/19/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
@interface helpPage : UIViewController<MFMailComposeViewControllerDelegate,UITextFieldDelegate,UITextViewDelegate>
- (IBAction)sendMail:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *phNumber;

@property (weak, nonatomic) IBOutlet UITextView *messageBody;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *name;
@end
