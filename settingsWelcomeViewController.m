//
//  settingsWelcomeViewController.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/17/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "settingsWelcomeViewController.h"

@interface settingsWelcomeViewController ()

@end

@implementation settingsWelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.imageScreen.image = [UIImage imageNamed:self.imageFile];
    self.titleLabel.text = self.titleText;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)skipToMenu:(id)sender {
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"menuscreen"];
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)okToSettings:(id)sender {
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"settingsMenu"];
    [self presentViewController:vc animated:YES completion:nil];
}
@end
