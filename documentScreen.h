//
//  documentScreen.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 8/26/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager1.h"




@interface documentScreen : UIViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>{
    
    NSMutableData *_responseData1;

}
- (IBAction)buttonTapping:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addDoc;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *info;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menu_doc;

@property (weak, nonatomic) IBOutlet UIScrollView *scroller_doc;
@property (weak, nonatomic) IBOutlet UITableView *doc_list;
@property (nonatomic, strong) DBManager1 *dbManager;
- (IBAction)helpAlert:(id)sender;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;

@end
